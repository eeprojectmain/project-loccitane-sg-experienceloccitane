<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$promotionClass = new promotion();

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $_SESSION['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

if ($_POST) {
    $postfield = $_POST;
    unset($postfield['submit_form']);

    foreach ($postfield as $k => $v) {
        $_SESSION['member'][$k] = $v;
    }

    $queryUpdate = get_query_update($table['order'],$_SESSION['member']['order_id'], $postfield);
    $databaseClass->query($queryUpdate);

    header("Location: checkout-summary");
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">COURIER DELIVERY</h4>
            </div>
        </div>
        <div class="col-12 mt-4">
            <p class="w-100 text-center">Please fill in your address.</p>
            <form action="checkout-address" method="post" class="w-80 mx-auto">
                <div class="form-group">
                        <textarea name="address" class="form-control" required placeholder="ADDRESS"
                                  rows="3"></textarea>
                </div>
                <div class="form-group">
                    <input type="text" name="unit_no" class="form-control" required placeholder="UNIT NO.">
                </div>
                <div class="form-group">
                    <input type="tel" name="postcode" class="form-control" required placeholder="POSTAL CODE" maxlength="10">
                </div>
                <div class="form-group mt-3 pb-5 text-center">
                    <button type="submit" name="submit_form" value="online" class="btn btn-darkblue w-80">CONTINUE
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    fbq('track', 'InitiateCheckout');

    <?php
    foreach ($_SESSION['member'] as $k => $v) {
    if (preg_match("/_status/", $k)) {
    ?>
    $("input[name='<?=$k?>'][value='<?=$v?>']")
        .attr('checked', 'checked');
    <?php
    }
    } ?>
</script>
</body>

</html>
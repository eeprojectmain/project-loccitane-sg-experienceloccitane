<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$nav_step = '2';

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $_SESSION['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

if ($_POST) {
    $postfield = $_POST;
    unset($postfield['submit_form']);

    foreach ($postfield as $k => $v) {
        $_SESSION['member'][$k] = $v;
    }

    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], $postfield);
    $resultInsert = $databaseClass->query($queryUpdate);

    header("Location: gift-wrap");
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">RECIPIENT DETAILS</h4>
                <p>Please fill in recipient (your friend) details.</p>
            </div>
        </div>
        <div class="col-12 mt-4">
            <form action="checkout-friend" method="post" class="mx-auto formValidation">
                <div class="form-group">
                    <input type="text" name="receive_name" class="form-control" maxlength="100" placeholder="NAME"
                           required
                           value="<?= $_SESSION['member']['receive_name'] ?>"/>
                </div>
                <div class="form-group">
                    <input class="form-control" name="receive_mobile" placeholder="MOBILE NUMBER"
                           type="tel" required maxlength="11"
                           value="<?= $_SESSION['member']['receive_mobile'] ?>"/>
                </div>
                <div class="form-group">
                        <textarea class="form-control" rows="3" name="receive_message"
                                  placeholder="MESSAGE FOR YOUR FRIEND"><?= $_SESSION['member']['receive_message'] ?></textarea>
                </div>
                <div class="form-group mt-3 pb-5 text-center">
                    <button type="submit" name="submit_form" value="online" class="btn btn-blue w-80">CONTINUE
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    fbq('track', 'InitiateCheckout');

    <?php
    foreach ($_SESSION['member'] as $k => $v) {
    if (preg_match("/_status/", $k)) {
    ?>
    $("input[name='<?=$k?>'][value='<?=$v?>']")
        .attr('checked', 'checked');
    <?php
    }
    } ?>
</script>
</body>

</html>
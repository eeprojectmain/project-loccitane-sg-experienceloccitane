<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_date = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['date']);
$order_date_start = explode(" - ", $order_date)[0];
$order_date_end = explode(" - ", $order_date)[1];

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
    ->setSize(12);

// Set document properties
$objPHPExcel->getProperties()->setCreator("WOWSOME")
    ->setLastModifiedBy("WOWSOME");

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'External Tracking ID')
    ->setCellValue('B1', 'Billing UID')
    ->setCellValue('C1', 'Sender Company')
    ->setCellValue('D1', 'Sender Name')
    ->setCellValue('E1', 'Sender Address')
    ->setCellValue('F1', 'Sender Unit')
    ->setCellValue('G1', 'Sender Postal Code')
    ->setCellValue('H1', 'Sender Contact')
    ->setCellValue('I1', 'Sender Email')
    ->setCellValue('J1', 'Recipient Company')
    ->setCellValue('K1', 'Recipient Name')
    ->setCellValue('L1', 'Recipient Contact')
    ->setCellValue('M1', 'Recipient Address')
    ->setCellValue('N1', 'Recipient Unit')
    ->setCellValue('O1', 'Recipient Postal Code')
    ->setCellValue('P1', 'Recipient Email')
    ->setCellValue('Q1', 'Content Quantity')
    ->setCellValue('R1', 'Content Type')
    ->setCellValue('S1', 'Content Description')
    ->setCellValue('T1', 'Declared value')
    ->setCellValue('U1', 'Cash Collection')
    ->setCellValue('V1', 'Delivery Type')
    ->setCellValue('W1', 'Pickup Date')
    ->setCellValue('X1', 'Pickup Time')
    ->setCellValue('Y1', 'Delivery Date')
    ->setCellValue('Z1', 'Delivery Time')
    ->setCellValue('AA1', 'Weight')
    ->setCellValue('AB1', 'Length')
    ->setCellValue('AC1', 'Width')
    ->setCellValue('AD1', 'Height')
    ->setCellValue('AE1', 'Instruction')
    ->setCellValue('AF1', 'Service Type');

$key = 2;
$alpha = 'A';

$resultOrder = get_query_data($table['order'], "status>=1 and status!=6 and shipping_method='courier' and payment_status=1 and date(created_date) between date('$order_date_start') and date('$order_date_end') order by pkid asc");
while ($rs_order = $resultOrder->fetchRow()) {
    $pickup_date = date('dmY', strtotime("today +1 day"));
    $delivery_date = date('dmY', strtotime("today +2 day"));

    $objPHPExcel->getActiveSheet()->setCellValue("A" . $key, "LDSG20".$rs_order['pkid']);
    $objPHPExcel->getActiveSheet()->setCellValue("B" . $key, "17233");
    $objPHPExcel->getActiveSheet()->setCellValue("C" . $key, "Bollore");
    $objPHPExcel->getActiveSheet()->setCellValue("D" . $key, "Bollore");
    $objPHPExcel->getActiveSheet()->setCellValue("E" . $key, "47 Jalan Buroh");
    $objPHPExcel->getActiveSheet()->setCellValue("F" . $key, "#05-04");
    $objPHPExcel->getActiveSheet()->setCellValue("G" . $key, "619491");
    $objPHPExcel->getActiveSheet()->setCellValue("H" . $key, "NA");
    $objPHPExcel->getActiveSheet()->setCellValue("I" . $key, "NA");
    $objPHPExcel->getActiveSheet()->setCellValue("J" . $key, "NA");
    $objPHPExcel->getActiveSheet()->setCellValue("K" . $key, $rs_order['name']);
    $objPHPExcel->getActiveSheet()->setCellValue("L" . $key, str_replace("+65", "", $rs_order['mobile']));
    $objPHPExcel->getActiveSheet()->setCellValue("M" . $key, $rs_order['address']);
    $objPHPExcel->getActiveSheet()->setCellValue("N" . $key, '="'.$rs_order['unit_no'].'"');
    $objPHPExcel->getActiveSheet()->setCellValue("O" . $key, $rs_order['postcode']);
    $objPHPExcel->getActiveSheet()->setCellValue("P" . $key, $rs_order['email']);
    $objPHPExcel->getActiveSheet()->setCellValue("Q" . $key, "1");
    $objPHPExcel->getActiveSheet()->setCellValue("R" . $key, "Normal Delivery");
    $objPHPExcel->getActiveSheet()->setCellValue("S" . $key, "1 Box");
    $objPHPExcel->getActiveSheet()->setCellValue("T" . $key, $rs_order['total_amount']-$rs_order['discount_amount']);
    $objPHPExcel->getActiveSheet()->setCellValue("U" . $key, "0");
    $objPHPExcel->getActiveSheet()->setCellValue("V" . $key, "wh_delivery");
    $objPHPExcel->getActiveSheet()->setCellValue("W" . $key, $pickup_date);
    $objPHPExcel->getActiveSheet()->setCellValue("X" . $key, "1801");
    $objPHPExcel->getActiveSheet()->setCellValue("Y" . $key, $delivery_date);
    $objPHPExcel->getActiveSheet()->setCellValue("Z" . $key, "2201");
    $objPHPExcel->getActiveSheet()->setCellValue("AA" . $key, "5");
    $objPHPExcel->getActiveSheet()->setCellValue("AB" . $key, "31");
    $objPHPExcel->getActiveSheet()->setCellValue("AC" . $key, "5");
    $objPHPExcel->getActiveSheet()->setCellValue("AD" . $key, "20");
    $objPHPExcel->getActiveSheet()->setCellValue("AE" . $key, "");
    $objPHPExcel->getActiveSheet()->setCellValue("AF" . $key, "LITE");

    $ws = $objPHPExcel->getActiveSheet();
    $ws->getCell("N" . $key)->setValueExplicit($rs_order['unit_no'], PHPExcel_Cell_DataType::TYPE_STRING);
//    $ws->getCell("N" . $key)->setValueExplicit($rs_order['postcode'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//    $ws->getCell("Q" . $key)->setValueExplicit($rs_order['total_amount']-$rs_order['discount_amount'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//    $ws->getCell("T" . $key)->setValueExplicit($pickup_date, PHPExcel_Cell_DataType::TYPE_NUMERIC);
//    $ws->getCell("U" . $key)->setValueExplicit("1801", PHPExcel_Cell_DataType::TYPE_NUMERIC);
//    $ws->getCell("V" . $key)->setValueExplicit($delivery_date, PHPExcel_Cell_DataType::TYPE_NUMERIC);
//    $ws->getCell("W" . $key)->setValueExplicit("2201", PHPExcel_Cell_DataType::TYPE_NUMERIC);

    $key++;
}

$objPHPExcel->getActiveSheet()->getStyle('N2:N'.$key)->setQuotePrefix(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

header('Cache-Control: max-age=1');

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$filename = "URBANFOX-" . date("Y-m-d") . ".csv";

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
$objWriter->save('php://output');
exit;

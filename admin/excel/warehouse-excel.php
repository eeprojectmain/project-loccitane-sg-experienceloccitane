<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_date = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['date']);
$order_date_start = explode(" - ", $order_date)[0];
$order_date_end = explode(" - ", $order_date)[1];

$campaign_id=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['campaign_id']);

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
    ->setSize(12);

// Set document properties
$objPHPExcel->getProperties()->setCreator("WOWSOME")
    ->setLastModifiedBy("WOWSOME");

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Order#')
    ->setCellValue('B1', 'Customer#')
    ->setCellValue('C1', 'Email')
    ->setCellValue('D1', 'FirstName')
    ->setCellValue('E1', 'LastName')
    ->setCellValue('F1', 'NbLine')
    ->setCellValue('G1', 'Total')
    ->setCellValue('H1', 'ShippingFee')
    ->setCellValue('I1', 'Tax')
    ->setCellValue('J1', 'Status')
    ->setCellValue('K1', 'Comment')
    ->setCellValue('L1', 'Date created')
    ->setCellValue('M1', 'Info')
    ->setCellValue('N1', 'Tracking')
    ->setCellValue('O1', 'PaymentMethod')
    ->setCellValue('P1', 'Auth Number')
    ->setCellValue('Q1', 'Transaction ID')
    ->setCellValue('R1', 'Card Type')
    ->setCellValue('S1', 'Ship Address')
    ->setCellValue('T1', 'Ship Address2')
    ->setCellValue('U1', 'Ship Address3')
    ->setCellValue('V1', 'Ship Phone')
    ->setCellValue('W1', 'CustomerOptional1')
    ->setCellValue('X1', 'CustomerOptional2')
    ->setCellValue('Y1', 'CustomerOptional3')
    ->setCellValue('Z1', 'CustomerOptional4')
    ->setCellValue('AA1', 'CustomerOptional5')
    ->setCellValue('AB1', 'CustomerBirthDate')
    ->setCellValue('AC1', 'CustomerPhone')
    ->setCellValue('AD1', 'SKU')
    ->setCellValue('AE1', 'BinLocation')
    ->setCellValue('AF1', 'ShortTitlel1')
    ->setCellValue('AG1', 'OrderLineQuantity')
    ->setCellValue('AH1', 'OrderLinePrice')
    ->setCellValue('AI1', 'OrderLinePriceTotal')
    ->setCellValue('AJ1', 'Date Modified')
    ->setCellValue('AK1', 'ShippingFirstName')
    ->setCellValue('AL1', 'ShippingLastName')
    ->setCellValue('AM1', 'ShippingPhone')
    ->setCellValue('AN1', 'ShippingAddress')
    ->setCellValue('AO1', 'ShippingAddress2')
    ->setCellValue('AP1', 'ShippingAddress3')
    ->setCellValue('AQ1', 'ShippingZip')
    ->setCellValue('AR1', 'ShippingCity')
    ->setCellValue('AS1', 'ShippingState')
    ->setCellValue('AT1', 'GiftMessage')
    ->setCellValue('AU1', 'GiftInstructions')
    ->setCellValue('AV1', 'CreatedBy')
    ->setCellValue('AW1', 'ReleaseDate')
    ->setCellValue('AX1', 'StoreRetailId')
    ->setCellValue('AY1', 'Paper Gift Bag');

$key = 2;
$alpha = 'A';

if($campaign_id!=""){
    $where.="and campaign_id=$campaign_id";
}

$resultOrder = get_query_data($table['order'], "status>=1 and status!=6 and shipping_method='courier' and payment_status=1 and date(created_date) between date('$order_date_start') and date('$order_date_end') $where order by pkid asc");
while ($rs_order = $resultOrder->fetchRow()) {
    if (strtotime($rs_order['created_date']) < strtotime('2020-07-23')) {
        $table['product']="dc_product2";
    } else {
        $table['product']="dc_product";
    }

    $product_id = explode(",", $rs_order['product_id']);
    $quantity = explode(",", $rs_order['quantity']);

    $total_quantity=count($product_id);
    $count=0;

    foreach ($product_id as $k => $v) {
        $resultProduct=get_query_data($table['product'], "pkid=$v");
        $rs_product=$resultProduct->fetchRow();

        $count++;

        $objPHPExcel->getActiveSheet()->setCellValue("A" . $key, "LDSG20".$rs_order['pkid']);
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $key, $rs_order['email']);
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $key, $rs_order['name']);
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $key, $count);
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $key, $rs_order['total_amount']-$rs_order['discount_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $key, $rs_order['shipping_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $key, "NEW");
        $objPHPExcel->getActiveSheet()->setCellValue("K" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("L" . $key, str_replace(" ", "T", $rs_order['created_date']).".000Z");
        $objPHPExcel->getActiveSheet()->setCellValue("M" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("N" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("O" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("P" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("Q" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("R" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("S" . $key,  $rs_order['unit_no'] . " , " . $rs_order['address']);
        $objPHPExcel->getActiveSheet()->setCellValue("T" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("U" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("V" . $key, str_replace("+65", "", $rs_order['mobile']));
        $objPHPExcel->getActiveSheet()->setCellValue("W" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("X" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("Y" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("Z" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AA" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AB" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AC" . $key, str_replace("+65", "", $rs_order['mobile']));
        $objPHPExcel->getActiveSheet()->setCellValue("AD" . $key, $rs_product['item_code']);
        $objPHPExcel->getActiveSheet()->setCellValue("AE" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AF" . $key, $rs_product['title']);
        $objPHPExcel->getActiveSheet()->setCellValue("AG" . $key, $quantity[$k]);
        $objPHPExcel->getActiveSheet()->setCellValue("AH" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AI" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AJ" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AK" . $key, $rs_order['name']);
        $objPHPExcel->getActiveSheet()->setCellValue("AL" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AM" . $key, str_replace("+65", "", $rs_order['mobile']));
        $objPHPExcel->getActiveSheet()->setCellValue("AN" . $key, $rs_order['unit_no'] . " , " . $rs_order['address']);
        $objPHPExcel->getActiveSheet()->setCellValue("AO" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AP" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AQ" . $key, $rs_order['postcode']);
        $objPHPExcel->getActiveSheet()->setCellValue("AR" . $key, "SG");
        $objPHPExcel->getActiveSheet()->setCellValue("AS" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AT" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AU" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AV" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AW" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AX" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AY" . $key, $rs_order['free_giftbox']);

        $ws = $objPHPExcel->getActiveSheet();
        $ws->getCell("V" . $key)->setValueExplicit(str_replace("+65", "", $rs_order['mobile']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $ws->getCell("AC" . $key)->setValueExplicit(str_replace("+65", "", $rs_order['mobile']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $ws->getCell("AM" . $key)->setValueExplicit(str_replace("+65", "", $rs_order['mobile']), PHPExcel_Cell_DataType::TYPE_NUMERIC);

        $key++;
    }
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

header('Cache-Control: max-age=1');

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$filename = "BOLSG-" . date("Y-m-d") . ".xlsx";

// Redirect output to a client’s web browser (Excel2007)
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

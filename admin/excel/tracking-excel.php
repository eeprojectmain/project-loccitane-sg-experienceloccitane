<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Upload Tracking</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Upload Tracking</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <?
                                if ($_FILES['excel']['tmp_name']) {
                                    move_uploaded_file($_FILES['excel']['tmp_name'], '../files/tracking/' . $_FILES['excel']['name']);

                                    $inputFileName = '../files/tracking/' . $_FILES['excel']['name'];
                                    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                                    $sheetData = $objPHPExcel->getActiveSheet()->toArray();

                                    foreach ($sheetData as $k => $v) {
                                        if ($k == 0 || $v[4]=="") {
                                            continue;
                                        }

                                        $tracking_code = $v[0];
                                        $order_id = str_replace("LDSG20","",$v[4]);
                                        $signature_url = $v[15];

                                        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                                        $row_order = $resultOrder->fetchRow();

                                        if ($row_order > 0) {
                                            $queryUpdate = get_query_update($table['order'], $order_id, array('status' => '4', 'tracking_code' => $tracking_code, 'signature_url' => $signature_url, 'updated_date' => $time_config['now'], 'updated_by' => $_SESSION['admin']['username']));
                                            $database->query($queryUpdate);
                                            echo "<p>#$order_id - Updated</p>";
                                        } else {
                                            echo "<p>#$order_id - Invalid Order ID</p>";
                                        }
                                    }
                                }
                                ?>
                                <a href="<?=$this_folder?>/tracking" class="btn btn-success">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_date = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['date']);
$order_date_start = explode(" - ", $order_date)[0];
$order_date_end = explode(" - ", $order_date)[1];

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
    ->setSize(12);

// Set document properties
$objPHPExcel->getProperties()->setCreator("WOWSOME")
    ->setLastModifiedBy("WOWSOME");

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Order Type')
    ->setCellValue('B1', 'Date created')
    ->setCellValue('C1', 'Order#')
    ->setCellValue('D1', 'Email')
    ->setCellValue('E1', 'FirstName')
    ->setCellValue('F1', 'LastName')
    ->setCellValue('G1', 'CustomerPhone')
    ->setCellValue('H1', 'NbLine')
    ->setCellValue('I1', 'Product Description')
    ->setCellValue('J1', 'SKU')
    ->setCellValue('K1', 'Order Quantity')
    ->setCellValue('L1', 'SKU price x Quantity')
    ->setCellValue('M1', 'Gross sales (total before shipping and discounts)')
    ->setCellValue('N1', 'ShippingFee')
    ->setCellValue('O1', 'Voucher Value Redeemed ')
    ->setCellValue('P1', 'Type of Voucher')
    ->setCellValue('Q1', 'Total Paid ')
    ->setCellValue('R1', 'PaymentMethod')
    ->setCellValue('S1', 'ReceiptNumber')
    ->setCellValue('T1', 'Status')
    ->setCellValue('U1', 'ReleaseDate')
    ->setCellValue('V1', 'BACode')
    ->setCellValue('W1', 'RetailStoreCode')
    ->setCellValue('X1', 'Tracking ID')
    ->setCellValue('Y1', 'ShippingFirstName')
    ->setCellValue('Z1', 'ShippingLastName')
    ->setCellValue('AA1', 'Ship Address')
    ->setCellValue('AB1', 'Ship Phone')
    ->setCellValue('AC1', 'ShippingZip')
    ->setCellValue('AD1', 'ShippingCity')
    ->setCellValue('AE1', 'GiftMessage')
    ->setCellValue('AF1', 'GiftInstructions');

$key = 2;
$alpha = 'A';

if ($_SESSION['admin']['role'] == "1") {
    $resultOrder = get_query_data($table['order'], "payment_status=1 and date(created_date) between date('$order_date_start') and date('$order_date_end') order by pkid asc");
} else {
    $resultOrder = get_query_data($table['order'], "outlet_id='" . $_SESSION['admin']['outlet_id'] . "' and payment_status=1 and date(created_date) between date('$order_date_start') and date('$order_date_end') order by pkid asc");
}
while ($rs_order = $resultOrder->fetchRow()) {
    if (strtotime($rs_order['created_date']) < strtotime('2020-07-23')) {
        $table['product'] = "dc_product2";
    } else {
        $table['product'] = "dc_product";
    }

    $product_id = explode(",", $rs_order['product_id']);
    $quantity = explode(",", $rs_order['quantity']);

    $total_quantity = count($product_id);
    $count = 0;

    $resultPayment = get_query_data($table['payment'], "order_id=" . $rs_order['pkid']);
    $rs_payment = $resultPayment->fetchRow();

    if ($rs_order['voucher_discount_amount'] != "") {
        $voucher_name = $cprv_voucher_array[$rs_order['cprv_id']];
    }

    foreach ($product_id as $k => $v) {
        $resultProduct = get_query_data($table['product'], "pkid=$v");
        $rs_product = $resultProduct->fetchRow();

        $count++;

        $objPHPExcel->getActiveSheet()->setCellValue("A" . $key, strtoupper($rs_order['method'] == "pickup" ? "Collect" : "Delivery"));
        $objPHPExcel->getActiveSheet()->setCellValue("B" . $key, str_replace(" ", "T", $rs_order['created_date']) . ".000Z");
        $objPHPExcel->getActiveSheet()->setCellValue("C" . $key, "LGSG20" . $rs_order['pkid']);
        $objPHPExcel->getActiveSheet()->setCellValue("D" . $key, $rs_order['email']);
        $objPHPExcel->getActiveSheet()->setCellValue("E" . $key, $rs_order['name']);
        $objPHPExcel->getActiveSheet()->setCellValue("F" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("G" . $key, str_replace("+65", "", $rs_order['mobile']));
        $objPHPExcel->getActiveSheet()->setCellValue("H" . $key, $count);
        $objPHPExcel->getActiveSheet()->setCellValue("I" . $key, $rs_product['title'] . " " . $rs_product['size']);
        $objPHPExcel->getActiveSheet()->setCellValue("J" . $key, $rs_product['item_code']);
        $objPHPExcel->getActiveSheet()->setCellValue("K" . $key, $quantity[$k]);
        $objPHPExcel->getActiveSheet()->setCellValue("L" . $key, $rs_product['price']);
        $objPHPExcel->getActiveSheet()->setCellValue("M" . $key, $rs_order['total_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue("N" . $key, $rs_order['shipping_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue("O" . $key, $rs_order['voucher_discount_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue("P" . $key, $rs_order['voucher_discount_amount'] == "" ? "" : $voucher_name);
        $objPHPExcel->getActiveSheet()->setCellValue("Q" . $key, $rs_order['total_amount'] - $rs_order['voucher_discount_amount'] - $rs_order['promotion_discount_amount'] + $rs_order['shipping_amount']);
        $objPHPExcel->getActiveSheet()->setCellValue("R" . $key, $rs_payment['method']);
        $objPHPExcel->getActiveSheet()->setCellValue("S" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("T" . $key, $order_status_array[$rs_order['status']]);
        $objPHPExcel->getActiveSheet()->setCellValue("U" . $key, $rs_order['updated_date']);
        $objPHPExcel->getActiveSheet()->setCellValue("V" . $key, $rs_order['ba_code']);
        $objPHPExcel->getActiveSheet()->setCellValue("W" . $key, $outlet_code_array[$rs_order['outlet_id']]);
        $objPHPExcel->getActiveSheet()->setCellValue("X" . $key, $rs_order['tracking_code']);
        $objPHPExcel->getActiveSheet()->setCellValue("Y" . $key, $rs_order['name']);
        $objPHPExcel->getActiveSheet()->setCellValue("Z" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AA" . $key, $rs_order['address']);
        $objPHPExcel->getActiveSheet()->setCellValue("AB" . $key, str_replace("+65", "", $rs_order['mobile']));
        $objPHPExcel->getActiveSheet()->setCellValue("AC" . $key, $rs_order['postcode']);
        $objPHPExcel->getActiveSheet()->setCellValue("AD" . $key, "SG");
        $objPHPExcel->getActiveSheet()->setCellValue("AE" . $key, "");
        $objPHPExcel->getActiveSheet()->setCellValue("AF" . $key, "");

        $key++;
    }
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$filename = "POS-" . date("Y-m-d") . ".xlsx";

// Redirect output to a client’s web browser (Excel2007)
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

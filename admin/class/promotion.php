<?php

class promotion
{
    public function discount()
    {
        $databaseClass = new database();
        global $table, $time_config;

        $total_cart_amount = 0;
        $tier = "";
        $free_shipping = false;

        //set no discount
        $no_discount_array = array(468, 469, 470);
        //,764,765,766,770, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 771, 772, 773, 774, 775, 776, 777

        //set eco-refill promo 2022-08-26 till 2022-08-28
        //set eco-refill promo 2022-11-12 till 2022-11-14
        $ecorefill_array = array(4, 9, 29, 35, 41, 44, 52, 63, 66, 85, 154, 248, 253, 256, 258, 280, 283, 451, 452, 453, 568, 631);

        // for promo pkid=89 and 130
        $holidaySetArray = [517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,554, 764,765,766,770, 718, 719, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 771, 772, 773, 774, 775, 776, 777];

        // for promo pkid=90 and 131
        $setArray = [554,552,551,550,549,548,547,546,545,544,543,542,541,540,539,538,537,536,535,534,533,532,531,530,529,528,527,526,525,524,523,522,521,520,519,518,517,106,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,675,676,677, 764,765,766,770, 718, 719, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 771, 772, 773, 774, 775, 776, 777];

        foreach ($_SESSION['cart'] as $k => $v) {
            if (!in_array($v['product_id'], $no_discount_array))
                $product_count += $v['quantity'];
        }

        //get pwp product
        $pwp_promo_array = $this->pwp();
        if ($pwp_promo_array != false) {
            foreach ($pwp_promo_array as $k => $v) {
                if($v['promo_id']=='56'){
                    continue;
                }
                $min_spend = $v['min_spend'];
                foreach ($v['product_id'] as $k2 => $v2) {
                    $product_pwp_id_array[] = $v2;
                }
                foreach ($v['product_quantity'] as $k3 => $v3) {
                    $product_pwp_qty_array[] = $v3;
                }
            }
        }

        //get total cart amount
        foreach ($_SESSION['cart'] as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
            $rs_product = $resultProduct->fetchRow();

            if ($_SESSION['member']['custom_order'] == "1" && $v['price'] != "") {
                $rs_product['price'] = $v['price'] / $v['quantity'];
            }

            if ($v['free'] == "true") {
                $rs_product['price'] = "0";
            }
            
            // added by cc to implement no_discount_array
            // add this condition in if pwp item not entitled for discount: !in_array($v['product_id'], $product_pwp_id_array) &&  
            if (!in_array($v['product_id'], $no_discount_array)) {
                $total_cart_amount += $v['quantity'] * $rs_product['price'];
            }
        }

        if ($_SESSION['order']['id'] != "") {
            $resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['order']['id']);
            $rs_order = $resultOrder->fetchRow();

            $total_cart_amount = $total_cart_amount - $rs_order['voucher_discount_amount'];
        }

        $pair_array = array();

        $resultPromo = get_query_data($table['promotion'], "status=1 and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)");
        while ($rs_promo = $resultPromo->fetchRow()) {
            $total_discount = 0;

            if ($rs_promo['campaign_id'] != '0') {
                if ($_SESSION['campaign_id'] != $rs_promo['campaign_id']) {
                    continue;
                }
            }

            if ($_SESSION['member']['mobile'] == "" && $rs_promo['member_only'] != "0") {
                continue;
            }

            if ($total_cart_amount < $rs_promo['min_spend']) {
                continue;
            }

            //check member tier
            $cprv_mobile = $_SESSION['member']['mobile'];
            include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";
            if (preg_match("/gold/", strtolower($cprv_array['tier']))) {
                $tier = "gold";
            } elseif (preg_match("/club/", strtolower($cprv_array['tier']))) {
                $tier = "club";
            }

            if ($rs_promo['game_user_only'] == "1") {
                $game_mobile = $_SESSION['member']['mobile'];
                $game_mobile = ltrim($game_mobile, '+65');

                $row_check = get_query_data_row("tth_user", "phone='$game_mobile'");

                if ($row_check == 0) {
                    continue;
                }
            }

            //check member tier
            if ($rs_promo['member_only'] == "1" && $tier != $rs_promo['member_tier']) {
                if ($rs_promo['member_tier'] == "gold" && $tier != "gold") {
                    continue;
                } elseif ($rs_promo['member_tier'] == "club" && $tier != "club") {
                    continue;
                }
            } elseif ($rs_promo['member_only'] == "2" && $tier != "") {
                continue;
            }

            if ($rs_promo['type'] == "1") {
                $apply = false;

                //cater product discount by item, limited to 1 time discount per item
                if(in_array($rs_promo['pkid'], ["83","87","126"]))
                {
                    $customHandle = true;
                    $appliedPromoProductId = [];
                    $appliedPromos = get_query_data($table['order'], "payment_status=1 and mobile='" . $_SESSION['member']['mobile'] . "' and promotion_id=" .$rs_promo['pkid']);

                    while ($appliedPromo = $appliedPromos->fetchRow()) {
                        $appliedPromoProductId = array_merge($appliedPromoProductId, explode(",", $appliedPromo['product_id']));
                    }
                    
                }
                else
                {
                    $customHandle = false;
                }

                //product discount
                $product_id_array = explode(",", $rs_promo['product_id']);
                $product_qty_array = explode(",", $rs_promo['product_quantity']);
                $product_discount_array = explode(",", $rs_promo['product_discount']);

                foreach ($_SESSION['cart'] as $k => $v) {
                    if($customHandle && in_array($v['product_id'], $appliedPromoProductId))
                    {
                        continue;
                    }
                    if (in_array($v['product_id'], $product_id_array)) {
                        $apply = true;
                        $key = array_search($v['product_id'], $product_id_array);

                        for ($i = 1; $i <= $v['quantity']; $i++) {
                            $total_discount += $product_discount_array[$key];

                            if ($i == $product_qty_array[$key]) {
                                break;
                            }
                        }
                    }
                }

                if ($apply === true) {
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $total_discount);
                }
            } elseif ($rs_promo['type'] == "2") {
                //cart discount
                if ($rs_promo['discount_type'] == "p") {
                    $total_discount = ($total_cart_amount / 100) * $rs_promo['discount_value'];
                } elseif ($rs_promo['discount_type'] == "f") {
                    $total_discount += $rs_promo['discount_value'];
                }


                // Eco-refill product only: 20% off for 2nd Eco-Refill product (2022-8-26 till 2022-08-28)
                // Eco-refill product only: 20% off for 2nd Eco-Refill product (2023-04-20 till 2023-04-23)

                if ($rs_promo['pkid'] == '75' || $rs_promo['pkid'] == '100' ||  $rs_promo['pkid'] == '130') {
                    if ($product_count < 2) {
                        continue;
                    }
					
					$highest_specific_price = 0;
					$highest_specific_id = "";
                    $pair = 0;
                    $pair = floor($product_count / 2);
                    $lowest_price = 9999;

                    $lowest_price_1 = 9999;
                    $lowest_price_2 = 9999;

                    $highestQuantity = 0;
                    $lowestQuantity = 9999;

                    if($product_count >= 4 && count($_SESSION['cart']) != 4 )
                    {
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $ecorefill_array)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                                
                                if ($rs_product['price'] > $highest_specific_price) 
                                {   
                                    $highest_specific_price = $rs_product['price'];
                                    $highest_specific_id = $v['product_id'];
                                }
                            }
                        }
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }
                            
                            if (in_array($v['product_id'], $ecorefill_array)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                        
                                if ($rs_product['price'] < $lowest_price_1) 
                                {
                                    $lowest_price_1 = $rs_product['price'];
                                }
                            }
                        }


                        foreach($_SESSION['cart'] as $k => $v)
                        {
                            $quantity = $v['quantity'];
                            if ($quantity > $highestQuantity) {
                                $highestQuantity = $quantity;
                                $productIdWithHighestQuantity = $v['product_id'];
                            }
                            if ($quantity < $lowestQuantity) {
                                $lowestQuantity = $quantity;
                                $productIdWithLowestQuantity = $v['product_id'];

                            }
                        }
                        
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }

                            if (in_array($v['product_id'], $ecorefill_array)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();

                                if($productIdWithHighestQuantity == $rs_product['pkid']  )
                                {
                                    if ($rs_product['price'] >= $lowest_price_1) 
                                    {
                                        $lowest_price_2 = $rs_product['price'];
                                    }
                                }
                                if($productIdWithLowestQuantity == $rs_product['pkid'] && $rs_product['price'] >= $highest_specific_price )
                                {
                                    if ($rs_product['price'] > $lowest_price_1) 
                                    {
                                        $lowest_price_2 = $lowest_price_1;
                                    }
                                }
                            }
                        }

                        if ($lowest_price_1 <= $highest_specific_price && $lowest_price_2 <= $highest_specific_price)
                        {
                            $lowest_price = $lowest_price_1 + $lowest_price_2;
                            $total_discount = ($lowest_price / 100) * $rs_promo['discount_value'];
                        }
                        else 
                        {
                            $total_discount = 0;
                        }
                    }

                    else if(count($_SESSION['cart']) >= 4)
                    {
                        foreach($_SESSION['cart'] as $k => $v)
                        {
                            $quantity = $v['quantity'];
                            if ($quantity > $highestQuantity) {
                                $highestQuantity = $quantity;
                                $productIdWithHighestQuantity = $v['product_id'];
                            }
                            if ($quantity < $lowestQuantity) {
                                $lowestQuantity = $quantity;
                                $productIdWithLowestQuantity = $v['product_id'];

                            }
                        }
                    
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $ecorefill_array)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                                
                                // get highest specific item price
                                if ($rs_product['price'] > $highest_specific_price) 
                                {   
                                    $highest_specific_price = $rs_product['price'];
                                    $highest_specific_id = $v['product_id'];
                                }
                            }
                        }

                        
                        foreach ($_SESSION['cart'] as $k => $v) {
                            // if ($v['product_id'] == $highest_specific_id && $v['quantity'] < 2) {
                            //     continue;
                            // }
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }
                            
                            if (in_array($v['product_id'], $ecorefill_array)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();

                                if ($rs_product['price'] < $lowest_price_1) 
                                {
                                    $lowest_price_1 = $rs_product['price'];
                                }
                            }
                        }

                        foreach ($_SESSION['cart'] as $k => $v) {
                            // if ($v['product_id'] == $highest_specific_id && $v['quantity'] < 2) {
                            //     continue;
                            // }
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }
                            
                            if (in_array($v['product_id'], $ecorefill_array)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                            
                                    if($productIdWithHighestQuantity == $rs_product['pkid']  )
                                    {
                                        // echo $rs_product['price'];
                                        if ($rs_product['price'] >= $lowest_price_1) 
                                        {
                                            $lowest_price_2 = $rs_product['price'];
                                        }
                                    }
                                    if($productIdWithLowestQuantity == $rs_product['pkid'] && $rs_product['price'] >= $highest_specific_price )
                                    {
                                        // echo $rs_product['price'];
                                        if ($rs_product['price'] > $lowest_price_1) 
                                        {
                                            $lowest_price_2 = $rs_product['price'];
                                        }
                                    }
                                    if($productIdWithLowestQuantity == $rs_product['pkid'] && $rs_product['price'] >= $lowest_price_1 )
                                    {
                                        if ($rs_product['price'] > $lowest_price_1) 
                                        {
                                            $lowest_price_2 = $lowest_price_1;
                                        }
                                    }

                            }
                        }
                        
                        if($lowest_price_1 <= $highest_specific_price && $lowest_price_2 <= $highest_specific_price)
                        {
                            
                            $lowest_price = $lowest_price_1 += $lowest_price_2;
                            $total_discount = ($lowest_price / 100) * $rs_promo['discount_value'];
                        }
                        else 
                        {
                            $total_discount = 0;
                        }
                        
                    }
                    else 
                    {
    
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $ecorefill_array)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                                
                                // get highest specific item price
                                if ($rs_product['price'] > $highest_specific_price) 
                                {   
                                    $highest_specific_price = $rs_product['price'];
                                    $highest_specific_id = $v['product_id'];
                                }
                            }
                        }
    
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if ($v['product_id'] == $highest_specific_id && $v['quantity'] < 2) {
                                continue;
                            }
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }
                            
                            if (in_array($v['product_id'], $ecorefill_array)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
    
                            
                                    if ($rs_product['price'] < $lowest_price) 
                                    {
                                        $lowest_price = $rs_product['price'];
                                    }
    
                            }
                        }
                        if($lowest_price <= $highest_specific_price)
                        {
                            $total_discount = ($lowest_price / 100) * $rs_promo['discount_value'];
                        }
                        else
                        {
                            $total_discount = 0;
                        }
                    }

					// echo '<pre>';print_r("high:". $highest_specific_id . $highest_specific_price . ", " . $lowest_price);

                }
                // Eco-refill product only: 20% off for 2nd Eco-refill product (2022-8-26 till 2022-08-28)

                // 10% off 2nd Holiday Set (2022-12-21 till 2022-12-23)
                if ($rs_promo['pkid'] == '89') {
                    if ($product_count < 2) {
                        continue;
                    }
                    
                    $highest_specific_price = 0;
                    $highest_specific_id = "";
                    $lowest_price = 9999;
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $holidaySetArray)) {
                            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();
                            
                            // get highest specific item price
                            if ($rs_product['price'] > $highest_specific_price) {
                                $highest_specific_price = $rs_product['price'];
                                $highest_specific_id = $v['product_id'];
                            }
                        }
                    }

                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == $highest_specific_id && $v['quantity'] < 2) {
                            continue;
                        }
                        if (in_array($v['product_id'], $no_discount_array)) {
                            continue;
                        }
                        
                        if (in_array($v['product_id'], $holidaySetArray)) {
                            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();

                            if ($rs_product['price'] < $lowest_price) {
                                $lowest_price = $rs_product['price'];
                            }
                        }
                    }
                    //print_r("high:". $highest_specific_id . $highest_specific_price . ", " . $lowest_price);

                    if($lowest_price <= $highest_specific_price){
                        $total_discount = ($lowest_price / 100) * $rs_promo['discount_value'];
                    }else{
                        $total_discount = 0;
                    }
                }
                // 10% off 2nd Holiday Set (2022-12-21 till 2022-12-23)

                // 10% off all sets & limited edition
                if ($rs_promo['pkid'] == '90') {

                    $total_discount = 0;

                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $setArray)) {
                            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();

                            $price =  $rs_product['price'] * $v['quantity'];
                            $total_discount += ($price / 100) * $rs_promo['discount_value'];
                        }
                    }
                }
                // 10% off all sets & limited edition

                // 10% off 2nd Holiday Set (2023-12-21 till 2023-12-25)
                /*if ($rs_promo['pkid'] == '130') {
                    if ($product_count < 2) {
                        continue;
                    }
                    
                    $highest_specific_price = 0;
                    $highest_specific_id = "";
                    $lowest_price = 9999;
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $holidaySetArray)) {
                            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();
                            
                            // get highest specific item price
                            if ($rs_product['price'] > $highest_specific_price) {
                                $highest_specific_price = $rs_product['price'];
                                $highest_specific_id = $v['product_id'];
                            }
                        }
                    }

                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == $highest_specific_id && $v['quantity'] < 2) {
                            continue;
                        }
                        if (in_array($v['product_id'], $no_discount_array)) {
                            continue;
                        }
                        
                        if (in_array($v['product_id'], $holidaySetArray)) {
                            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();

                            if ($rs_product['price'] < $lowest_price) {
                                $lowest_price = $rs_product['price'];
                            }
                        }
                    }
                    //print_r("high:". $highest_specific_id . $highest_specific_price . ", " . $lowest_price);

                    if($lowest_price <= $highest_specific_price){
                        $total_discount = ($lowest_price / 100) * $rs_promo['discount_value'];
                    }else{
                        $total_discount = 0;
                    }
                }*/

                /*if ($rs_promo['pkid'] == '130') {
                    if ($product_count < 2) {
                        continue;
                    }
					
					$highest_specific_price = 0;
					$highest_specific_id = "";
                    $pair = 0;
                    $pair = floor($product_count / 2);
                    $lowest_price = 9999;

                    $lowest_price_1 = 9999;
                    $lowest_price_2 = 9999;

                    $highestQuantity = 0;
                    $lowestQuantity = 9999;

                    if($product_count >= 4 && count($_SESSION['cart']) != 4 )
                    {
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $holidaySetArray)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                                
                                if ($rs_product['price'] > $highest_specific_price) 
                                {   
                                    $highest_specific_price = $rs_product['price'];
                                    $highest_specific_id = $v['product_id'];
                                }
                            }
                        }
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }
                            
                            if (in_array($v['product_id'], $holidaySetArray)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                        
                                if ($rs_product['price'] < $lowest_price_1) 
                                {
                                    $lowest_price_1 = $rs_product['price'];
                                }
                            }
                        }


                        foreach($_SESSION['cart'] as $k => $v)
                        {
                            $quantity = $v['quantity'];
                            if ($quantity > $highestQuantity) {
                                $highestQuantity = $quantity;
                                $productIdWithHighestQuantity = $v['product_id'];
                            }
                            if ($quantity < $lowestQuantity) {
                                $lowestQuantity = $quantity;
                                $productIdWithLowestQuantity = $v['product_id'];

                            }
                        }
                        
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }

                            if (in_array($v['product_id'], $holidaySetArray)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();

                                if($productIdWithHighestQuantity == $rs_product['pkid']  )
                                {
                                    if ($rs_product['price'] >= $lowest_price_1) 
                                    {
                                        $lowest_price_2 = $rs_product['price'];
                                    }
                                }
                                if($productIdWithLowestQuantity == $rs_product['pkid'] && $rs_product['price'] >= $highest_specific_price )
                                {
                                    if ($rs_product['price'] > $lowest_price_1) 
                                    {
                                        $lowest_price_2 = $lowest_price_1;
                                    }
                                }
                            }
                        }

                        if ($lowest_price_1 <= $highest_specific_price && $lowest_price_2 <= $highest_specific_price)
                        {
                            $lowest_price = $lowest_price_1 + $lowest_price_2;
                            $total_discount = ($lowest_price / 100) * $rs_promo['discount_value'];
                        }
                        else 
                        {
                            $total_discount = 0;
                        }
                    }

                    else if(count($_SESSION['cart']) >= 4)
                    {
                        foreach($_SESSION['cart'] as $k => $v)
                        {
                            $quantity = $v['quantity'];
                            if ($quantity > $highestQuantity) {
                                $highestQuantity = $quantity;
                                $productIdWithHighestQuantity = $v['product_id'];
                            }
                            if ($quantity < $lowestQuantity) {
                                $lowestQuantity = $quantity;
                                $productIdWithLowestQuantity = $v['product_id'];

                            }
                        }
                    
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $holidaySetArray)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                                
                                // get highest specific item price
                                if ($rs_product['price'] > $highest_specific_price) 
                                {   
                                    $highest_specific_price = $rs_product['price'];
                                    $highest_specific_id = $v['product_id'];
                                }
                            }
                        }

                        
                        foreach ($_SESSION['cart'] as $k => $v) {
                            // if ($v['product_id'] == $highest_specific_id && $v['quantity'] < 2) {
                            //     continue;
                            // }
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }
                            
                            if (in_array($v['product_id'], $holidaySetArray)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();

                                if ($rs_product['price'] < $lowest_price_1) 
                                {
                                    $lowest_price_1 = $rs_product['price'];
                                }
                            }
                        }

                        foreach ($_SESSION['cart'] as $k => $v) {
                            // if ($v['product_id'] == $highest_specific_id && $v['quantity'] < 2) {
                            //     continue;
                            // }
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }
                            
                            if (in_array($v['product_id'], $holidaySetArray)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                            
                                    if($productIdWithHighestQuantity == $rs_product['pkid']  )
                                    {
                                        // echo $rs_product['price'];
                                        if ($rs_product['price'] >= $lowest_price_1) 
                                        {
                                            $lowest_price_2 = $rs_product['price'];
                                        }
                                    }
                                    if($productIdWithLowestQuantity == $rs_product['pkid'] && $rs_product['price'] >= $highest_specific_price )
                                    {
                                        // echo $rs_product['price'];
                                        if ($rs_product['price'] > $lowest_price_1) 
                                        {
                                            $lowest_price_2 = $rs_product['price'];
                                        }
                                    }
                                    if($productIdWithLowestQuantity == $rs_product['pkid'] && $rs_product['price'] >= $lowest_price_1 )
                                    {
                                        if ($rs_product['price'] > $lowest_price_1) 
                                        {
                                            $lowest_price_2 = $lowest_price_1;
                                        }
                                    }

                            }
                        }
                        
                        if($lowest_price_1 <= $highest_specific_price && $lowest_price_2 <= $highest_specific_price)
                        {
                            
                            $lowest_price = $lowest_price_1 += $lowest_price_2;
                            $total_discount = ($lowest_price / 100) * $rs_promo['discount_value'];
                        }
                        else 
                        {
                            $total_discount = 0;
                        }
                        
                    }
                    else 
                    {
    
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if (in_array($v['product_id'], $holidaySetArray)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
                                
                                // get highest specific item price
                                if ($rs_product['price'] > $highest_specific_price) 
                                {   
                                    $highest_specific_price = $rs_product['price'];
                                    $highest_specific_id = $v['product_id'];
                                }
                            }
                        }
    
                        foreach ($_SESSION['cart'] as $k => $v) {
                            if ($v['product_id'] == $highest_specific_id && $v['quantity'] < 2) {
                                continue;
                            }
                            if (in_array($v['product_id'], $no_discount_array)) {
                                continue;
                            }
                            
                            if (in_array($v['product_id'], $holidaySetArray)) {
                                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                                $rs_product = $resultProduct->fetchRow();
    
                            
                                    if ($rs_product['price'] < $lowest_price) 
                                    {
                                        $lowest_price = $rs_product['price'];
                                    }
    
                            }
                        }
                        if($lowest_price <= $highest_specific_price)
                        {
                            $total_discount = ($lowest_price / 100) * $rs_promo['discount_value'];
                        }
                        else
                        {
                            $total_discount = 0;
                        }
                    }

					// echo '<pre>';print_r("high:". $highest_specific_id . $highest_specific_price . ", " . $lowest_price);

                }*/

                if ($rs_promo['pkid'] == '130') {
                    // Arrays to store relevant information
                    $prices = [];
                    $quantities = [];

                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $holidaySetArray) && !in_array($v['product_id'], $no_discount_array)) {
                            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();

                            // Store prices and quantities in arrays
                            $prices[$v['product_id']] = $rs_product['price'];
                            $quantities[$v['product_id']] = $v['quantity'];
                        }
                    }

                    // Sort prices in ascending order
                    asort($prices);

                    // Pick the two lowest prices and their corresponding quantities
                    $lowest_prices = array_slice($prices, 0, 2);
                    $lowest_quantities = array_intersect_key($quantities, $lowest_prices);

                    // Calculate total discount based on the two lowest prices
                    $total_discount = array_sum($lowest_prices) / 100 * $rs_promo['discount_value'];
                }
                // 10% off 2nd Holiday Set (2023-12-21 till 2023-12-25)

                // 10% off all sets & limited edition 2023
                if ($rs_promo['pkid'] == '131') {

                    $total_discount = 0;

                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $setArray)) {
                            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();

                            $price =  $rs_product['price'] * $v['quantity'];
                            $total_discount += ($price / 100) * $rs_promo['discount_value'];
                        }
                    }
                }
                // 10% off all sets & limited edition 2023

                if ($total_discount > 0) {
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $total_discount);
                }
            } elseif ($rs_promo['type'] == "3") {
                $apply = false;
                $pwp_buy = 0;
                $pwp_get = false;
                $total_discount = 0;

                //product before qualify
                $product_id_array = explode(",", $rs_promo['product_id']);
                $product_qty_array = explode(",", $rs_promo['product_quantity']);

                $pwp_product_id_array = explode(",", $rs_promo['pwp_product_id']);
                $pwp_product_qty_array = explode(",", $rs_promo['pwp_product_quantity']);
                $pwp_product_discount_array = explode(",", $rs_promo['pwp_product_discount']);

                //must buy product first
                if ($rs_promo['product_id'] != "") {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        $remaining_in_array = false;
                        $remaining_qty = -1;

                        if (in_array($v['product_id'], $product_id_array)) {
                            $key = array_search($v['product_id'], $product_id_array);
                            if ($v['quantity'] >= $product_qty_array[$key]) {

                                foreach ($pair_array as $k2 => $v2) {
                                    if ($v2['product_id'] == $v['product_id']) {
                                        $remaining_in_array = true;
                                        $remaining_qty_from_array = $v2['remain_qty'];
                                        $key = $k2;
                                        break;
                                    }
                                }

                                if ($remaining_in_array == true) {
                                    if ($remaining_qty_from_array > 0) {
                                        $remaining_qty = $v['quantity'] - $remaining_qty_from_array;
                                    } else {
                                        $remaining_qty = -1;
                                    }

                                    $pair_array[$key]['remain_qty'] = $remaining_qty;
                                } else {
                                    $key = array_search($v['product_id'], $pwp_product_id_array);
                                    $remaining_qty = $v['quantity'] - $product_qty_array[$key];
                                    $pair_array[] = array('product_id' => $v['product_id'], 'remain_qty' => $remaining_qty);
                                }

                                if ($remaining_qty >= 0) {
                                    $pwp_buy++;
                                }

                            }
                        }

                        $remaining_in_array = false;
                        $remaining_qty = -1;

                        if (in_array($v['product_id'], $pwp_product_id_array)) {

                            foreach ($pair_array as $k2 => $v2) {
                                if ($v2['product_id'] == $v['product_id']) {
                                    $remaining_in_array = true;
                                    $remaining_qty_from_array = $v2['remain_qty'];
                                    $key = $k2;
                                    break;
                                }
                            }

                            if ($remaining_in_array == true) {
                                if ($remaining_qty_from_array > 0) {
                                    $remaining_qty = $v['quantity'] - $remaining_qty_from_array;
                                } else {
                                    $remaining_qty = -1;
                                }

                                $pair_array[$key]['remain_qty'] = $remaining_qty;
                            } else {
                                $key = array_search($v['product_id'], $pwp_product_id_array);
                                $remaining_qty = $v['quantity'] - $pwp_product_qty_array[$key];
                                $pair_array[] = array('product_id' => $v['product_id'], 'remain_qty' => $remaining_qty);
                            }

                            if ($remaining_qty >= 0) {
                                $pwp_get = true;
                            }

                        }
                    }

                    //pwp type (OR)
                    if ($rs_promo['pwp_type'] == "1") {
                        if ($pwp_buy > 0 && $pwp_get === true) {
                            foreach ($_SESSION['cart'] as $k => $v) {
                                if (in_array($v['product_id'], $pwp_product_id_array)) {
                                    $apply = true;
                                    for ($i = 1; $i <= $v['quantity']; $i++) {
                                        $key = array_search($v['product_id'], $pwp_product_id_array);
                                        $total_discount += $pwp_product_discount_array[$key];
                                        if ($i == $pwp_product_qty_array[$key]) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        //pwp type (AND)
                        if ($pwp_buy == count($product_id_array)) {
                            foreach ($_SESSION['cart'] as $k => $v) {
                                if (in_array($v['product_id'], $pwp_product_id_array)) {
                                    $apply = true;
                                    for ($i = 1; $i <= $v['quantity']; $i++) {
                                        $key = array_search($v['product_id'], $pwp_product_id_array);
                                        $total_discount += $pwp_product_discount_array[$key];
                                        if ($i == $pwp_product_qty_array[$key]) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    //no need buy product
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $pwp_product_id_array)) {
                            $apply = true;
                            for ($i = 1; $i <= $v['quantity']; $i++) {
                                $key = array_search($v['product_id'], $pwp_product_id_array);
                                $total_discount += $pwp_product_discount_array[$key];
                                if ($i == $pwp_product_qty_array[$key]) {
                                    break;
                                }
                            }
                        }
                    }
                }

                if ($apply === true) {
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $total_discount);
                }
            } elseif ($rs_promo['type'] == '5') {
                $apply = false;

                //product discount
                $product_id_array = explode(",", $rs_promo['product_id']);
                $min_product_qty_array = explode(",", $rs_promo['min_product_quantity']);
                $max_product_qty_array = explode(",", $rs_promo['max_product_quantity']);
                $product_discount_array = explode(",", $rs_promo['product_discount']);

                foreach ($_SESSION['cart'] as $k => $v) {
                    if (in_array($v['product_id'], $product_id_array)) {
                        $key = array_search($v['product_id'], $product_id_array);

                        if ($v['quantity'] >= $min_product_qty_array[$key]) {
                            $apply = true;

                            for ($i = 1; $i <= $v['quantity']; $i++) {
                                $total_discount += $product_discount_array[$key];

                                if ($i == $max_product_qty_array[$key]) {
                                    break;
                                }
                            }
                        }
                    }
                }

                if ($apply === true) {
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $total_discount);
                }
            }

            if ($_SESSION['member']['order_id'] != '' && $free_shipping == false) {
                if ($rs_promo['apply_time'] != "0") {
                    $row_apply = get_query_data_row($table['order'], "payment_status=1 and mobile='+65" . $_SESSION['member']['mobile'] . "' and date(created_date) between date('" . $rs_promo['start_date'] . "') and date('" . $rs_promo['end_date'] . "')");
                    if ($row_apply >= $rs_promo['apply_time']) {
                        continue;
                    }
                }

                if ($rs_promo['apply_once_a_day'] != '0') {
                    $row_apply = get_query_data_row($table['order'], "payment_status=1 and mobile='+65" . $_SESSION['member']['mobile'] . "' and date(created_date)=CURDATE() and date(created_date) between date('" . $rs_promo['start_date'] . "') and date('" . $rs_promo['end_date'] . "')");
                    if ($row_apply >= $rs_promo['apply_once_a_day']) {
                        continue;
                    }
                }

                $resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['member']['order_id']);
                $rs_order = $resultOrder->fetchRow();

                if ($rs_order['shipping_method'] == "delivery" && $rs_promo['free_lalamove'] == "1") {
                    $free_shipping = true;
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $rs_order['shipping_amount'], 'label' => 'Free Shipping', 'type' => 'free_shipping', 'min_spend' => $rs_promo['min_spend']);
                } else if ($rs_order['shipping_method'] == "courier" && $rs_promo['free_courier'] == "1") {
                    $free_shipping = true;
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $rs_order['shipping_amount'], 'label' => 'Free Shipping', 'type' => 'free_shipping', 'min_spend' => $rs_promo['min_spend']);
                }
            }
        }

        return $result;
    }

    public function freegift($promotion_total_amount=0)
    {
        $databaseClass = new database();
        global $table, $time_config;
		$orderClass = new order();

        //set no discount
        $no_discount_array = array(468, 469, 470, 560, 561, 562,764,765,766,770);
        $setProductArr = [554,552,551,550,549,548,547,546,545,544,543,542,541,540,539,538,537,536,535,534,533,532,531,530,529,528,527,526,525,524,523,522,521,520,519,518,517,106,764,765,766,770, 718, 719, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 771, 772, 773, 774, 775, 776, 777];

        //,743,744,745,746,747,752,754,755,756,757,759,760,761,762,763,771,772,773,774,775,776,777

        //,764,765,766,770

        $no_discount_array = array_merge($no_discount_array, $setProductArr);

        //exclude pwp product for total amount
        $pwp_promo_array = $this->pwp();
        if ($pwp_promo_array != false) {
            foreach ($pwp_promo_array as $k => $v) {
                foreach ($v['product_id'] as $k2 => $v2) {
                    $product_pwp_id_array[] = $v2;
                }
            }
        }

        //get total cart amount
        foreach ($_SESSION['cart'] as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
            $rs_product = $resultProduct->fetchRow();

            if ($v['free'] == "true") {
                $rs_product['price'] = "0";
            } else {
                $cart_paid_product_array[$k] = $v['product_id'];
            }

            $cart_product_array[$k] = $v['product_id'];

            // added by cc to implement no_discount_array
            if (!in_array($v['product_id'], $product_pwp_id_array) && !in_array($v['product_id'], $no_discount_array)) {
                $total_cart_amount += $v['quantity'] * $rs_product['price'];
            }
        }
		
		if ($_SESSION['member']['order_id'] != "") {
            $resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['member']['order_id']);
            $rs_order = $resultOrder->fetchRow();

            $total_cart_amount = $total_cart_amount - $rs_order['voucher_discount_amount'];
        }

        // mainly cater check promo min spend after deduct promo discount
        if($promotion_total_amount > 0)
        {
            $total_cart_amount = $total_cart_amount - $promotion_total_amount;
        }

        //check member tier
        $cprv_mobile = $_SESSION['member']['mobile'];
        include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";
        if (preg_match("/gold/", strtolower($cprv_array['tier']))) {
            $tier = "gold";
        } elseif (preg_match("/club/", strtolower($cprv_array['tier']))) {
            $tier = "club";
        }

        //vk testing
        if ($_SESSION['member']['mobile'] == "+65182268875" || $_SESSION['member']['mobile'] == "+65163562938" || $_SESSION['member']['mobile'] == "+65123123123") {
            $tier = "gold";
        }

        $pair_array = array();

        $resultPromo = get_query_data($table['promotion'], "status=1 and free_gift=1 and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)");
        while ($rs_promo = $resultPromo->fetchRow()) {
            $freegift_array = explode(",", $rs_promo['gift_product_id']);

            if ($rs_promo['apply_time'] != "0") {
                $row_apply = get_query_data_row($table['order'], "payment_status=1 and mobile='" . $_SESSION['member']['mobile'] . "' and date(created_date) between date('" . $rs_promo['start_date'] . "') and date('" . $rs_promo['end_date'] . "')");
                if ($row_apply >= $rs_promo['apply_time']) {
                    foreach ($freegift_array as $k => $v) {
                        $key = array_search($v, $cart_product_array);
                        if ($key) {
                            unset($_SESSION['cart'][$key]);
                        }
                    }
                    continue;
                }
            }

            if ($rs_promo['apply_once_a_day'] != "0") {
                $row_apply = get_query_data_row($table['order'], "payment_status=1 and mobile='" . $_SESSION['member']['mobile'] . "' and date(created_date)=CURDATE() and date(created_date) between date('" . $rs_promo['start_date'] . "') and date('" . $rs_promo['end_date'] . "')");
                if ($row_apply >= $rs_promo['apply_once_a_day']) {
                    foreach ($freegift_array as $k => $v) {
                        $key = array_search($v, $cart_product_array);
                        if ($key) {
                            unset($_SESSION['cart'][$key]);
                        }
                    }
                    continue;
                }
            }

            if ($rs_promo['campaign_id'] != '0') {
                if ($_SESSION['campaign_id'] != $rs_promo['campaign_id']) {
                    continue;
                }
            }

            if ($_SESSION['member']['mobile'] == "" && $rs_promo['member_only'] != "0") {
                continue;
            }

            //if free gift item already in cart, remove them from cart session
            foreach ($cart_product_array as $k => $v) {
                if (in_array($v, $freegift_array)) {
                    $key = array_search($v, $cart_product_array);
                    if ($key) {
                        unset($_SESSION['cart'][$key]);
                    }
                    unset($cart_product_array[$k]);
                }
            }

            //if free gift item should not be there, remove them from cart
            foreach ($cart_product_array as $k => $v) {
                if (!in_array($v, $freegift_array)) {
                    $key = array_search($v, $cart_product_array);
                    if ($key) {
                        if($_SESSION['cart'][$key]['free']){
                            if($_SESSION['cart'][$key]['free'] == true){
                                unset($_SESSION['cart'][$key]);
                                unset($cart_product_array[$k]);
                            }
                        }
                    }
                }
            }

            //if cart amount less than min spend, remove them from cart session
            if ($total_cart_amount < $rs_promo['min_spend']) {
                foreach ($freegift_array as $k => $v) {
                    $key = array_search($v, $cart_product_array);
                    if ($key) {
                        unset($_SESSION['cart'][$key]);
                    }
                }
                continue;
            }

            if ($rs_promo['member_only'] == "1") {
                if ($rs_promo['member_tier'] == "both" && $tier == "") {
                    continue;
                }

                if ($rs_promo['member_tier'] != "both" && $rs_promo['member_tier'] != $tier) {
                    continue;
                }
            } elseif ($rs_promo['member_only'] == "2" && $tier != "") {
                continue;
            }

            //custom hook
            if ($rs_promo['pkid'] == '29') {
                $hit_array = array(318, 319, 320, 149, 128, 129);

                $skip = true;

                if (
                    in_array('318', $cart_product_array) ||
                    in_array('319', $cart_product_array) ||
                    in_array('320', $cart_product_array) ||
                    in_array('149', $cart_product_array) ||
                    in_array('128', $cart_product_array) ||
                    in_array('129', $cart_product_array)
                ) {
                    $skip = false;
                }

                if ($skip == true) {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '332') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                    continue;
                }
            }

            if ($rs_promo['pkid'] == '30') {
                $skip = true;

                if (
                    in_array('319', $cart_paid_product_array) ||
                    in_array('320', $cart_paid_product_array)
                ) {
                    $skip = false;
                }

                /*if ($_SESSION['campaign_id'] != "") {
                    if (date("Y-m-d") == "2021-09-16" || date("Y-m-d") == "2021-09-17") {
                        $skip = true;
                    }
                }*/

                if ($skip == true) {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '333') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                    continue;
                } else {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '334') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                }
            }

            if ($rs_promo['pkid'] == '31') {
                $skip = true;

                if (count($cart_paid_product_array) > 1) {
                    if (
                        in_array('319', $cart_paid_product_array) ||
                        in_array('320', $cart_paid_product_array)
                    ) {
                        if (in_array('149', $cart_paid_product_array) ||
                            in_array('128', $cart_paid_product_array) ||
                            in_array('129', $cart_paid_product_array)) {
                            $skip = false;
                        }
                    }
                }

                /*if ($_SESSION['campaign_id'] != "") {
                    if (date("Y-m-d") == "2021-09-16" || date("Y-m-d") == "2021-09-17") {
                        $skip = true;
                    }
                }*/

                if ($skip == true) {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '334') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                    continue;
                } else {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '333') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                }
            }
            //end custom hook

            if ($rs_promo['type'] == "3") {
                $apply = false;
                $pwp_buy = 0;
                $pwp_get = 0;
                $remaining_in_array = false;
                $key = '';

                //product before qualify
                $product_id_array = explode(",", $rs_promo['product_id']);
                $product_qty_array = explode(",", $rs_promo['product_quantity']);

                $product_pwp_id_array = explode(",", $rs_promo['pwp_product_id']);
                $product_pwp_qty_array = explode(",", $rs_promo['pwp_product_quantity']);

                //must buy product first
                if ($rs_promo['product_id'] != "") {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $product_id_array)) {

                            $key = array_search($v['product_id'], $product_id_array);
                            if ($v['quantity'] >= $product_qty_array[$key]) {

                                foreach ($pair_array as $k2 => $v2) {
                                    if ($v2['product_id'] == $v['product_id']) {
                                        $remaining_in_array = true;
                                        $remaining_qty_from_array = $v2['remain_qty'];
                                        $key = $k2;
                                        break;
                                    }
                                }

                                if ($remaining_in_array == true) {
                                    $remaining_qty = $remaining_qty_from_array - $v['quantity'];
                                    $pair_array[$key]['remain_qty'] = $remaining_qty;
                                } else {
                                    $key = array_search($v['product_id'], $product_pwp_id_array);
                                    $remaining_qty = $v['quantity'] - $product_qty_array[$key];
                                    $pair_array[] = array('product_id' => $v['product_id'], 'remain_qty' => $remaining_qty);
                                }

                                if ($remaining_qty >= 0) {
                                    $pwp_buy++;
                                }
                            }
                        }
                    }

                    if ($rs_promo['pwp_pair'] == '1') {
                        foreach ($_SESSION['cart'] as $k => $v) {
                            $remaining_in_array = false;
                            $remaining_qty = -1;

                            if (in_array($v['product_id'], $product_pwp_id_array)) {

                                $key = array_search($v['product_id'], $product_pwp_id_array);
                                if ($v['quantity'] >= $product_pwp_qty_array[$key]) {

                                    foreach ($pair_array as $k2 => $v2) {
                                        if ($v2['product_id'] == $v['product_id']) {
                                            $remaining_in_array = true;
                                            $remaining_qty_from_array = $v2['remain_qty'];
                                            $key = $k2;
                                            break;
                                        }
                                    }

                                    if ($remaining_in_array == true) {
                                        if ($remaining_qty_from_array > 0) {
                                            $remaining_qty = $v['quantity'] - $remaining_qty_from_array;
                                        } else {
                                            $remaining_qty = -1;
                                        }

                                        $pair_array[$key]['remain_qty'] = $remaining_qty;
                                    } else {
                                        $key = array_search($v['product_id'], $product_pwp_id_array);
                                        $remaining_qty = $v['quantity'] - $product_pwp_qty_array[$key];
                                        $pair_array[] = array('product_id' => $v['product_id'], 'remain_qty' => $remaining_qty);
                                    }

                                    if ($remaining_qty >= 0) {
                                        $pwp_get++;
                                    }

                                }
                            }
                        }
                    }

                    /*echo 'B' . $pwp_buy . '-' . $rs_promo['pkid'];
                    echo '<br />';
                    echo 'G' . $pwp_get;
                    echo '<br />';*/

                    //pwp type (OR)
                    if ($rs_promo['pwp_type'] == "1") {
                        if ($rs_promo['pwp_pair'] == '1') {
                            if ($pwp_buy > 0 && $pwp_get > 0) {
                                $apply = true;
                            }
                        } else {
                            if ($pwp_buy > 0) {
                                $apply = true;
                            }
                        }
                    } else {
                        //pwp type (AND)
                        if ($pwp_buy == count($product_id_array)) {
                            foreach ($_SESSION['cart'] as $k => $v) {
                                if (in_array($v['product_id'], $product_id_array)) {
                                    $total_pwp += $v['quantity'];

                                    $key = array_search($v['product_id'], $product_id_array);
                                    if ($v['quantity'] < $product_qty_array[$key]) {
                                        $apply = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    //no need buy product
                    if ($pwp_get > 0) {
                        $apply = true;
                    }
                }
            }

            if ($apply != true && $rs_promo['type'] == '3') {
                continue;
            }

            foreach ($freegift_array as $k => $v) {
                if (!in_array($v, $cart_product_array)) {
                    $_SESSION['cart'][] = array('product_id' => $v, 'quantity' => '1', 'free' => 'true', 'min_spend' => $rs_promo['min_spend']);
                }
            }

            if ($_SESSION['member']['order_id'] != '') {
                foreach ($_SESSION['cart'] as $k => $v) {
                    if ($v['free'] == 'true') {
                        $freegift_product_id_array[] = $v['product_id'];
                    }
                }

                $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('freegift_product_id' => implode(",", $freegift_product_id_array)));
                $databaseClass->query($queryUpdate);
            }
        }
		
		// added by cc
		$orderClass->update();
    }

    public function pwp()
    {
        $databaseClass = new database();
        global $table, $time_config;

        $total_cart_amount = 0;
        $tier = '';
        $free_shipping = false;
        $pwp_get = false;
        $pwp_buy = 0;

        $campaign_id = $_SESSION['campaign_id'];

        if ($campaign_id == "") {
            $resultPromo = get_query_data($table['promotion'], "status=1 and type='3' and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)");
        } else {
            $resultPromo = get_query_data($table['promotion'], "status=1 and type='3' and date('" . $time_config['today'] . "') between date(start_date) and date(end_date) and campaign_id=$campaign_id");
        }
        $row_promo = $resultPromo->numRows();

        //check member tier
        $cprv_mobile = $_SESSION['member']['mobile'];
        include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";
        if (preg_match("/gold/", strtolower($cprv_array['tier']))) {
            $tier = "gold";
        } elseif (preg_match("/club/", strtolower($cprv_array['tier']))) {
            $tier = "club";
        }

        if ($_SESSION['member']['mobile'] == '+65182268875' || $_SESSION['member']['mobile'] == "+65163562938") {
            $tier = 'gold';
        }

        if ($row_promo > 0) {
            //get total cart amount
            foreach ($_SESSION['cart'] as $k => $v) {
                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                $rs_product = $resultProduct->fetchRow();

                if ($v['free'] == "true") {
                    $rs_product['price'] = "0";
                }

                $total_cart_amount += $v['quantity'] * $rs_product['price'];
            }

            while ($rs_promo = $resultPromo->fetchRow()) {
                if ($rs_promo['campaign_id'] != '0') {
                    if ($_SESSION['campaign_id'] != $rs_promo['campaign_id']) {
                        continue;
                    }
                }

                if ($_SESSION['member']['mobile'] == "" && $rs_promo['member_only'] != "0") {
                    continue;
                }

                if ($total_cart_amount < $rs_promo['min_spend']) {
                    continue;
                }

                if ($rs_promo['member_only'] == "1" && $tier != $rs_promo['member_tier']) {
                    continue;
                } elseif ($rs_promo['member_only'] == "2" && $tier != "") {
                    continue;
                }

                $product_id_array = explode(",", $rs_promo['product_id']);
                $product_qty_array = explode(",", $rs_promo['product_quantity']);

                $pwp_product_id_array = explode(",", $rs_promo['pwp_product_id']);
                $pwp_product_qty_array = explode(",", $rs_promo['pwp_product_quantity']);
                $pwp_product_discount_array = explode(",", $rs_promo['pwp_product_discount']);

                //must buy product first
                if ($rs_promo['product_id'] != "") {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $product_id_array)) {
                            $key = array_search($v['product_id'], $product_id_array);
                            if ($v['quantity'] >= $product_qty_array[$key]) {
                                $pwp_buy++;
                            }
                        }
                    }

                    //pwp type (OR)
                    if ($rs_promo['pwp_type'] == "1") {
                        if ($pwp_buy > 0) {
                            $result[] = array(
                                'promo_id' => $rs_promo['pkid'],
                                'min_spend' => $rs_promo['min_spend'],
                                'product_id' => explode(",", $rs_promo['pwp_product_id']),
                                'product_quantity' => explode(",", $rs_promo['pwp_product_quantity']),
                                'product_discount' => explode(",", $rs_promo['pwp_product_discount'])
                            );
                        }
                    } else {
                        //pwp type (AND)
                        if ($pwp_buy == count($product_id_array)) {
                            foreach ($_SESSION['cart'] as $k => $v) {
                                if (in_array($v['product_id'], $pwp_product_id_array)) {
                                    $result[] = array(
                                        'promo_id' => $rs_promo['pkid'],
                                        'min_spend' => $rs_promo['min_spend'],
                                        'product_id' => explode(",", $rs_promo['pwp_product_id']),
                                        'product_quantity' => explode(",", $rs_promo['pwp_product_quantity']),
                                        'product_discount' => explode(",", $rs_promo['pwp_product_discount'])
                                    );
                                }
                            }
                        }
                    }
                } else {
                    //no need buy product
                    $result[] = array(
                        'promo_id' => $rs_promo['pkid'],
                        'min_spend' => $rs_promo['min_spend'],
                        'product_id' => explode(",", $rs_promo['pwp_product_id']),
                        'product_quantity' => explode(",", $rs_promo['pwp_product_quantity']),
                        'product_discount' => explode(",", $rs_promo['pwp_product_discount'])
                    );
                }

                if ($_SESSION['member']['order_id'] != '' && $free_shipping == false) {
                    $resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['member']['order_id']);
                    $rs_order = $resultOrder->fetchRow();

                    if ($rs_order['shipping_method'] == "delivery" && $rs_promo['free_lalamove'] == "1") {
                        $free_shipping = true;
                        $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $rs_order['shipping_amount'], 'label' => 'Free Shipping');
                    } else if ($rs_order['shipping_method'] == "courier" && $rs_promo['free_courier'] == "1") {
                        $free_shipping = true;
                        $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $rs_order['shipping_amount'], 'label' => 'Free Shipping');
                    }
                }
            }

            return $result;
        } else {
            return false;
        }
    }

    public function gold_member()
    {
        $databaseClass = new database();
        global $table, $time_config, $product_holiday_category, $product_holiday_gold_discount_array;

        $cprv_mobile = $_SESSION['member']['mobile'];
        include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";

        if ($_SESSION['member']['mobile'] == "0182268875" || $_SESSION['member']['mobile'] == "65163562938") {
            $cprv_array['tier'] = "gold";
            $cprv_array['points'] = '100';
            $cprv_array['vouchers'][] = array('id' => '483', 'url' => "#");
            $cprv_array['vouchers'][] = array('id' => '482', 'url' => "#");
            $cprv_array['vouchers'][] = array('id' => '761', 'url' => "#");
            $_SESSION['member']['otp'] = "123";
            $_SESSION['member']['otp_request'] = "123";
        }

        if (preg_match("/gold/", strtolower($cprv_array['tier']))) {
            $tier = "gold";
        } elseif (preg_match("/club/", strtolower($cprv_array['tier']))) {
            $tier = "club";
        }

        $member_discount_amount = 0;

        if ($tier == 'gold') {
            foreach ($_SESSION['cart'] as $k => $v) {
                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                $rs_product = $resultProduct->fetchRow();

                if ($_SESSION['member']['custom_order'] == "1" && $v['price'] != "") {
                    $rs_product['price'] = $v['price'] / $v['quantity'];
                }

                if ($v['free'] == "true") {
                    $rs_product['price'] = "0";
                }

                //holiday set no discount
                if ($rs_product['price'] > 0) {
                    if ($rs_product['gold_discount'] == "1") {
                        $member_discount_amount += ($rs_product['price'] * $v['quantity']) * 0.05;
                    }
                }
            }
        }

        return $member_discount_amount;
    }

    public function special_label()
    {
        $databaseClass = new database();
        global $table, $time_config;

        $resultPromo = get_query_data($table['promotion'], "status=1 and special_label!='' and (product_id!='' or pwp_product_id!='') and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)");
        while ($rs_promo = $resultPromo->fetchRow()) {
            if ($rs_promo['type'] == "3") {
                $product_id_array = explode(",", $rs_promo['pwp_product_id']);
                $product_discount_array = explode(",", $rs_promo['pwp_product_discount']);

                foreach (explode(",", $rs_promo['pwp_product_id']) as $k => $v) {
                    $key = array_search($v, $product_id_array);

                    $promo_array[] = array('product_id' => $v, 'special_label' => $rs_promo['special_label'], 'discount' => $product_discount_array[$key]);
                }
            }

            $product_id_array = explode(",", $rs_promo['product_id']);
            $product_discount_array = explode(",", $rs_promo['product_discount']);

            foreach (explode(",", $rs_promo['product_id']) as $k => $v) {
                $key = array_search($v, $product_id_array);

                $promo_array[] = array('product_id' => $v, 'special_label' => $rs_promo['special_label'], 'discount' => $product_discount_array[$key]);
            }
        }

        return $promo_array;
    }
}

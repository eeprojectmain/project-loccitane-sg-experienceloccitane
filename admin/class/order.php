<?php

class order
{
    public function update()
    {
        $databaseClass = new database();
        global $table, $time_config;

        $order_id = $_SESSION['member']['order_id'];

        if ($_SESSION['member']['custom_order'] == "1") {
            return false;
        }

        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $rs_order = $resultOrder->fetchRow();

        foreach ($_SESSION['cart'] as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
            $rs_product = $resultProduct->fetchRow();

            if ($v['free'] == "true") {
                $rs_product['price'] = "0";
            }

            $total += $rs_product['price'] * $v['quantity'];
            $product_id[] = $v['product_id'];
            $product_qty[] = $v['quantity'];
        }

        $queryUpdate = get_query_update($table['order'], $order_id, array('total_amount' => $total, 'product_id' => implode(",", $product_id), 'quantity' => implode(",", $product_qty)));
        $databaseClass->query($queryUpdate);
    }

    public function update_final()
    {
        $databaseClass = new database();
        $promotionClass = new promotion();
        global $table, $time_config;

        $need_update = false;

        $order_id = $_SESSION['member']['order_id'];

        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $rs_order = $resultOrder->fetchRow();

        $total_nett = $rs_order['total_amount'] - $rs_order['member_discount_amount'] - $rs_order['voucher_discount_amount'] - $rs_order['point_discount_amount'] - $rs_order['promotion_discount_amount'];

        $promotion_array = $promotionClass->discount();
        foreach ($promotion_array as $k => $v) {
            $promotion_total_amount += $v['discount'];
            $promotion_discount_array[] = $v['discount'];
            $promotion_id_array[] = $v['promo_id'];

            if ($v['type'] == "free_shipping") {
                $total_nett += $v['discount'];

                if ($total_nett < $v['min_spend']) {
                    $need_update = true;
                    array_pop($promotion_discount_array);
                    array_pop($promotion_id_array);
                    $promotion_total_amount = $promotion_total_amount - $v['discount'];
                }
            }
        }

        if ($need_update == true) {
            $queryUpdate = get_query_update($table['order'], $order_id, array('promotion_id' => implode(",", $promotion_id_array), 'promotion_value' => implode(",", $promotion_discount_array), 'promotion_discount_amount' => $promotion_total_amount));
            $databaseClass->query($queryUpdate);
        }

        return true;
    }

    public function check()
    {
        $databaseClass = new database();
        $apiClass = new api();
        global $table, $time_config;

        $order_id = $_SESSION['member']['order_id'];

        if ($order_id == "") {
            header("Location: index");
            exit();
        }

        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $rs_order = $resultOrder->fetchRow();

        if ($rs_order['payment_status'] == "1") {
            header("Location: index");
            exit();
        } elseif ($rs_order['status'] == "8") {
//            header("Location: index");
//            exit();
        } elseif ($rs_order['status'] == "6") {
            header("Location: index");
            exit();
        } elseif ($rs_order['status'] == "7") {
            header("Location: index");
            exit();
        }
    }

    public function check_final()
    {
        $databaseClass = new database();
        $apiClass = new api();
        global $table, $time_config;

        $order_id = $_SESSION['member']['order_id'];

        if ($order_id == "") {
            header("Location: index");
            exit();
        }

        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $rs_order = $resultOrder->fetchRow();

        if ($rs_order['shipping_method'] == "delivery") {
            if ($rs_order['pickup_date'] == "" || $rs_order['pickup_time'] == "" || $rs_order['lat'] == "" || $rs_order['lng'] == "") {
                header("Location: checkout-lalamove");
                exit();
            }
        } elseif ($rs_order['shipping_method'] == "courier") {
            if ($rs_order['address'] == "") {
                header("Location: checkout-address");
                exit();
            }
        }

        if ($rs_order['member_voucher_id'] != '0') {
            $resultVoucher = get_query_data($table['member_voucher'], "pkid=" . $rs_order['member_voucher_id']);
            $rs_voucher = $resultVoucher->fetchRow();

            if ($rs_voucher['status'] == '0') {
                header("Location: checkout-summary");
                exit();
            }
        } elseif ($rs_order['game_voucher_id'] != '0') {
            $resultVoucher = get_query_data($table['member_voucher'], "pkid=" . $rs_order['game_voucher_id']);
            $rs_voucher = $resultVoucher->fetchRow();

            if ($rs_voucher['status'] == '0') {
                header("Location: checkout-summary");
                exit();
            }
        } if ($rs_order['cprv_id'] != '0') {
            $cprv_array = $apiClass->get_customer($rs_order['mobile']);

            foreach ($cprv_array['vouchers'] as $k => $v) {
                $voucher_array[] = $v['id'];
            }

            /*if (!in_array($rs_order['cprv_id'], $voucher_array)) {
                header("Location: checkout-summary?a=swal&title=Opps...&m=Invalid voucher, please select NONE to continue&i=error");
                exit();
            }*/
        }
    }
}

<?php

class promotion
{
    public function discount()
    {
        $databaseClass = new database();
        global $table, $time_config;

        $total_cart_amount = 0;
        $tier = "";
        $free_shipping = false;

        //get pwp product
        $pwp_promo_array = $this->pwp();
        if ($pwp_promo_array != false) {
            foreach ($pwp_promo_array as $k => $v) {
                if($v['promo_id']=='56'){
                    continue;
                }
                $min_spend = $v['min_spend'];
                foreach ($v['product_id'] as $k2 => $v2) {
                    $product_pwp_id_array[] = $v2;
                }
                foreach ($v['product_quantity'] as $k3 => $v3) {
                    $product_pwp_qty_array[] = $v3;
                }
            }
        }

        //get total cart amount
        foreach ($_SESSION['cart'] as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
            $rs_product = $resultProduct->fetchRow();

            if ($_SESSION['member']['custom_order'] == "1" && $v['price'] != "") {
                $rs_product['price'] = $v['price'] / $v['quantity'];
            }

            if ($v['free'] == "true") {
                $rs_product['price'] = "0";
            }

            if (!in_array($v['product_id'], $product_pwp_id_array)) {
                $total_cart_amount += $v['quantity'] * $rs_product['price'];
            }
        }

        if ($_SESSION['order']['id'] != "") {
            $resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['order']['id']);
            $rs_order = $resultOrder->fetchRow();

            $total_cart_amount = $total_cart_amount - $rs_order['voucher_discount_amount'];
        }

        $pair_array = array();

        $resultPromo = get_query_data($table['promotion'], "status=1 and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)");
        while ($rs_promo = $resultPromo->fetchRow()) {
            $total_discount = 0;

            if ($rs_promo['campaign_id'] != '0') {
                if ($_SESSION['campaign_id'] != $rs_promo['campaign_id']) {
                    continue;
                }
            }

            if ($_SESSION['member']['mobile'] == "" && $rs_promo['member_only'] != "0") {
                continue;
            }

            if ($total_cart_amount < $rs_promo['min_spend']) {
                continue;
            }

            //check member tier
            $cprv_mobile = $_SESSION['member']['mobile'];
            include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";
            if (preg_match("/gold/", strtolower($cprv_array['tier']))) {
                $tier = "gold";
            } elseif (preg_match("/club/", strtolower($cprv_array['tier']))) {
                $tier = "club";
            }

            if ($rs_promo['game_user_only'] == "1") {
                $game_mobile = $_SESSION['member']['mobile'];
                $game_mobile = ltrim($game_mobile, '+65');

                $row_check = get_query_data_row("tth_user", "phone='$game_mobile'");

                if ($row_check == 0) {
                    continue;
                }
            }

            //check member tier
            if ($rs_promo['member_only'] == "1" && $tier != $rs_promo['member_tier']) {
                if ($rs_promo['member_tier'] == "gold" && $tier != "gold") {
                    continue;
                } elseif ($rs_promo['member_tier'] == "club" && $tier != "club") {
                    continue;
                }
            } elseif ($rs_promo['member_only'] == "2" && $tier != "") {
                continue;
            }

            if ($rs_promo['type'] == "1") {
                $apply = false;

                //product discount
                $product_id_array = explode(",", $rs_promo['product_id']);
                $product_qty_array = explode(",", $rs_promo['product_quantity']);
                $product_discount_array = explode(",", $rs_promo['product_discount']);

                foreach ($_SESSION['cart'] as $k => $v) {
                    if (in_array($v['product_id'], $product_id_array)) {
                        $apply = true;
                        $key = array_search($v['product_id'], $product_id_array);

                        for ($i = 1; $i <= $v['quantity']; $i++) {
                            $total_discount += $product_discount_array[$key];

                            if ($i == $product_qty_array[$key]) {
                                break;
                            }
                        }
                    }
                }

                if ($apply === true) {
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $total_discount);
                }
            } elseif ($rs_promo['type'] == "2") {
                //cart discount
                if ($rs_promo['discount_type'] == "p") {
                    $total_discount = ($total_cart_amount / 100) * $rs_promo['discount_value'];
                } elseif ($rs_promo['discount_type'] == "f") {
                    $total_discount += $rs_promo['discount_value'];
                }

                $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $total_discount);
            } elseif ($rs_promo['type'] == "3") {
                $apply = false;
                $pwp_buy = 0;
                $pwp_get = false;
                $total_discount = 0;

                //product before qualify
                $product_id_array = explode(",", $rs_promo['product_id']);
                $product_qty_array = explode(",", $rs_promo['product_quantity']);

                $pwp_product_id_array = explode(",", $rs_promo['pwp_product_id']);
                $pwp_product_qty_array = explode(",", $rs_promo['pwp_product_quantity']);
                $pwp_product_discount_array = explode(",", $rs_promo['pwp_product_discount']);

                //must buy product first
                if ($rs_promo['product_id'] != "") {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        $remaining_in_array = false;
                        $remaining_qty = -1;

                        if (in_array($v['product_id'], $product_id_array)) {
                            $key = array_search($v['product_id'], $product_id_array);
                            if ($v['quantity'] >= $product_qty_array[$key]) {

                                foreach ($pair_array as $k2 => $v2) {
                                    if ($v2['product_id'] == $v['product_id']) {
                                        $remaining_in_array = true;
                                        $remaining_qty_from_array = $v2['remain_qty'];
                                        $key = $k2;
                                        break;
                                    }
                                }

                                if ($remaining_in_array == true) {
                                    if ($remaining_qty_from_array > 0) {
                                        $remaining_qty = $v['quantity'] - $remaining_qty_from_array;
                                    } else {
                                        $remaining_qty = -1;
                                    }

                                    $pair_array[$key]['remain_qty'] = $remaining_qty;
                                } else {
                                    $key = array_search($v['product_id'], $pwp_product_id_array);
                                    $remaining_qty = $v['quantity'] - $product_qty_array[$key];
                                    $pair_array[] = array('product_id' => $v['product_id'], 'remain_qty' => $remaining_qty);
                                }

                                if ($remaining_qty >= 0) {
                                    $pwp_buy++;
                                }

                            }
                        }

                        $remaining_in_array = false;
                        $remaining_qty = -1;

                        if (in_array($v['product_id'], $pwp_product_id_array)) {

                            foreach ($pair_array as $k2 => $v2) {
                                if ($v2['product_id'] == $v['product_id']) {
                                    $remaining_in_array = true;
                                    $remaining_qty_from_array = $v2['remain_qty'];
                                    $key = $k2;
                                    break;
                                }
                            }

                            if ($remaining_in_array == true) {
                                if ($remaining_qty_from_array > 0) {
                                    $remaining_qty = $v['quantity'] - $remaining_qty_from_array;
                                } else {
                                    $remaining_qty = -1;
                                }

                                $pair_array[$key]['remain_qty'] = $remaining_qty;
                            } else {
                                $key = array_search($v['product_id'], $pwp_product_id_array);
                                $remaining_qty = $v['quantity'] - $pwp_product_qty_array[$key];
                                $pair_array[] = array('product_id' => $v['product_id'], 'remain_qty' => $remaining_qty);
                            }

                            if ($remaining_qty >= 0) {
                                $pwp_get = true;
                            }

                        }
                    }

                    //pwp type (OR)
                    if ($rs_promo['pwp_type'] == "1") {
                        if ($pwp_buy > 0 && $pwp_get === true) {
                            foreach ($_SESSION['cart'] as $k => $v) {
                                if (in_array($v['product_id'], $pwp_product_id_array)) {
                                    $apply = true;
                                    for ($i = 1; $i <= $v['quantity']; $i++) {
                                        $key = array_search($v['product_id'], $pwp_product_id_array);
                                        $total_discount += $pwp_product_discount_array[$key];
                                        if ($i == $pwp_product_qty_array[$key]) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        //pwp type (AND)
                        if ($pwp_buy == count($product_id_array)) {
                            foreach ($_SESSION['cart'] as $k => $v) {
                                if (in_array($v['product_id'], $pwp_product_id_array)) {
                                    $apply = true;
                                    for ($i = 1; $i <= $v['quantity']; $i++) {
                                        $key = array_search($v['product_id'], $pwp_product_id_array);
                                        $total_discount += $pwp_product_discount_array[$key];
                                        if ($i == $pwp_product_qty_array[$key]) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    //no need buy product
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $pwp_product_id_array)) {
                            $apply = true;
                            for ($i = 1; $i <= $v['quantity']; $i++) {
                                $key = array_search($v['product_id'], $pwp_product_id_array);
                                $total_discount += $pwp_product_discount_array[$key];
                                if ($i == $pwp_product_qty_array[$key]) {
                                    break;
                                }
                            }
                        }
                    }
                }

                if ($apply === true) {
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $total_discount);
                }
            } elseif ($rs_promo['type'] == '5') {
                $apply = false;

                //product discount
                $product_id_array = explode(",", $rs_promo['product_id']);
                $min_product_qty_array = explode(",", $rs_promo['min_product_quantity']);
                $max_product_qty_array = explode(",", $rs_promo['max_product_quantity']);
                $product_discount_array = explode(",", $rs_promo['product_discount']);

                foreach ($_SESSION['cart'] as $k => $v) {
                    if (in_array($v['product_id'], $product_id_array)) {
                        $key = array_search($v['product_id'], $product_id_array);

                        if ($v['quantity'] >= $min_product_qty_array[$key]) {
                            $apply = true;

                            for ($i = 1; $i <= $v['quantity']; $i++) {
                                $total_discount += $product_discount_array[$key];

                                if ($i == $max_product_qty_array[$key]) {
                                    break;
                                }
                            }
                        }
                    }
                }

                if ($apply === true) {
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $total_discount);
                }
            }

            if ($_SESSION['member']['order_id'] != '' && $free_shipping == false) {
                if ($rs_promo['apply_time'] != "0") {
                    $row_apply = get_query_data_row($table['order'], "payment_status=1 and mobile='+65" . $_SESSION['member']['mobile'] . "' and date(created_date) between date('" . $rs_promo['start_date'] . "') and date('" . $rs_promo['end_date'] . "')");
                    if ($row_apply >= $rs_promo['apply_time']) {
                        continue;
                    }
                }
                $resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['member']['order_id']);
                $rs_order = $resultOrder->fetchRow();

                if ($rs_order['shipping_method'] == "delivery" && $rs_promo['free_lalamove'] == "1") {
                    $free_shipping = true;
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $rs_order['shipping_amount'], 'label' => 'Free Shipping', 'type' => 'free_shipping', 'min_spend' => $rs_promo['min_spend']);
                } else if ($rs_order['shipping_method'] == "courier" && $rs_promo['free_courier'] == "1") {
                    $free_shipping = true;
                    $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $rs_order['shipping_amount'], 'label' => 'Free Shipping', 'type' => 'free_shipping', 'min_spend' => $rs_promo['min_spend']);
                }
            }
        }

        return $result;
    }

    public function freegift()
    {
        $databaseClass = new database();
        global $table, $time_config;

        //exclude pwp product for total amount
        $pwp_promo_array = $this->pwp();
        if ($pwp_promo_array != false) {
            foreach ($pwp_promo_array as $k => $v) {
                foreach ($v['product_id'] as $k2 => $v2) {
                    $product_pwp_id_array[] = $v2;
                }
            }
        }

        //get total cart amount
        foreach ($_SESSION['cart'] as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
            $rs_product = $resultProduct->fetchRow();

            if ($v['free'] == "true") {
                $rs_product['price'] = "0";
            } else {
                $cart_paid_product_array[$k] = $v['product_id'];
            }

            $cart_product_array[$k] = $v['product_id'];

            if (!in_array($v['product_id'], $product_pwp_id_array)) {
                $total_cart_amount += $v['quantity'] * $rs_product['price'];
            }
        }

        //check member tier
        $cprv_mobile = $_SESSION['member']['mobile'];
        include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";
        if (preg_match("/gold/", strtolower($cprv_array['tier']))) {
            $tier = "gold";
        } elseif (preg_match("/club/", strtolower($cprv_array['tier']))) {
            $tier = "club";
        }

        //vk testing
        if ($_SESSION['member']['mobile'] == "+65182268875") {
            $tier = "gold";
        }

        $pair_array = array();

        $resultPromo = get_query_data($table['promotion'], "status=1 and free_gift=1 and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)");
        while ($rs_promo = $resultPromo->fetchRow()) {
            $freegift_array = explode(",", $rs_promo['gift_product_id']);

            if ($rs_promo['apply_time'] != "0") {
                $row_apply = get_query_data_row($table['order'], "payment_status=1 and mobile='" . $_SESSION['member']['mobile'] . "' and date(created_date) between date('" . $rs_promo['start_date'] . "') and date('" . $rs_promo['end_date'] . "')");
                if ($row_apply >= $rs_promo['apply_time']) {
                    foreach ($freegift_array as $k => $v) {
                        $key = array_search($v, $cart_product_array);
                        if ($key) {
                            unset($_SESSION['cart'][$key]);
                        }
                    }
                    continue;
                }
            }

            if ($rs_promo['campaign_id'] != '0') {
                if ($_SESSION['campaign_id'] != $rs_promo['campaign_id']) {
                    continue;
                }
            }

            if ($_SESSION['member']['mobile'] == "" && $rs_promo['member_only'] != "0") {
                continue;
            }

            //if already in cart
            foreach ($cart_product_array as $k => $v) {
                if (in_array($v, $freegift_array)) {
                    $key = array_search($v, $cart_product_array);
                    if ($key) {
                        unset($_SESSION['cart'][$key]);
                    }
                    unset($cart_product_array[$k]);
                }
            }

            //if cart amount less than min spend
            if ($total_cart_amount < $rs_promo['min_spend']) {
                foreach ($freegift_array as $k => $v) {
                    $key = array_search($v, $cart_product_array);
                    if ($key) {
                        unset($_SESSION['cart'][$key]);
                    }
                }
                continue;
            }

            if ($rs_promo['member_only'] == "1") {
                if ($rs_promo['member_tier'] == "both" && $tier == "") {
                    continue;
                }

                if ($rs_promo['member_tier'] != "both" && $rs_promo['member_tier'] != $tier) {
                    continue;
                }
            } elseif ($rs_promo['member_only'] == "2" && $tier != "") {
                continue;
            }

            //custom hook
            if ($rs_promo['pkid'] == '29') {
                $hit_array = array(318, 319, 320, 149, 128, 129);

                $skip = true;

                if (
                    in_array('318', $cart_product_array) ||
                    in_array('319', $cart_product_array) ||
                    in_array('320', $cart_product_array) ||
                    in_array('149', $cart_product_array) ||
                    in_array('128', $cart_product_array) ||
                    in_array('129', $cart_product_array)
                ) {
                    $skip = false;
                }

                if ($skip == true) {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '332') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                    continue;
                }
            }

            if ($rs_promo['pkid'] == '30') {
                $skip = true;

                if (
                    in_array('319', $cart_paid_product_array) ||
                    in_array('320', $cart_paid_product_array)
                ) {
                    $skip = false;
                }

                /*if ($_SESSION['campaign_id'] != "") {
                    if (date("Y-m-d") == "2021-09-16" || date("Y-m-d") == "2021-09-17") {
                        $skip = true;
                    }
                }*/

                if ($skip == true) {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '333') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                    continue;
                } else {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '334') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                }
            }

            if ($rs_promo['pkid'] == '31') {
                $skip = true;

                if (count($cart_paid_product_array) > 1) {
                    if (
                        in_array('319', $cart_paid_product_array) ||
                        in_array('320', $cart_paid_product_array)
                    ) {
                        if (in_array('149', $cart_paid_product_array) ||
                            in_array('128', $cart_paid_product_array) ||
                            in_array('129', $cart_paid_product_array)) {
                            $skip = false;
                        }
                    }
                }

                /*if ($_SESSION['campaign_id'] != "") {
                    if (date("Y-m-d") == "2021-09-16" || date("Y-m-d") == "2021-09-17") {
                        $skip = true;
                    }
                }*/

                if ($skip == true) {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '334') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                    continue;
                } else {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if ($v['product_id'] == '333') {
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                }
            }
            //end custom hook

            if ($rs_promo['type'] == "3") {
                $apply = false;
                $pwp_buy = 0;
                $pwp_get = 0;
                $remaining_in_array = false;
                $key = '';

                //product before qualify
                $product_id_array = explode(",", $rs_promo['product_id']);
                $product_qty_array = explode(",", $rs_promo['product_quantity']);

                $product_pwp_id_array = explode(",", $rs_promo['pwp_product_id']);
                $product_pwp_qty_array = explode(",", $rs_promo['pwp_product_quantity']);

                //must buy product first
                if ($rs_promo['product_id'] != "") {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $product_id_array)) {

                            $key = array_search($v['product_id'], $product_id_array);
                            if ($v['quantity'] >= $product_qty_array[$key]) {

                                foreach ($pair_array as $k2 => $v2) {
                                    if ($v2['product_id'] == $v['product_id']) {
                                        $remaining_in_array = true;
                                        $remaining_qty_from_array = $v2['remain_qty'];
                                        $key = $k2;
                                        break;
                                    }
                                }

                                if ($remaining_in_array == true) {
                                    $remaining_qty = $remaining_qty_from_array - $v['quantity'];
                                    $pair_array[$key]['remain_qty'] = $remaining_qty;
                                } else {
                                    $key = array_search($v['product_id'], $product_pwp_id_array);
                                    $remaining_qty = $v['quantity'] - $product_qty_array[$key];
                                    $pair_array[] = array('product_id' => $v['product_id'], 'remain_qty' => $remaining_qty);
                                }

                                if ($remaining_qty >= 0) {
                                    $pwp_buy++;
                                }
                            }
                        }
                    }

                    if ($rs_promo['pwp_pair'] == '1') {
                        foreach ($_SESSION['cart'] as $k => $v) {
                            $remaining_in_array = false;
                            $remaining_qty = -1;

                            if (in_array($v['product_id'], $product_pwp_id_array)) {

                                $key = array_search($v['product_id'], $product_pwp_id_array);
                                if ($v['quantity'] >= $product_pwp_qty_array[$key]) {

                                    foreach ($pair_array as $k2 => $v2) {
                                        if ($v2['product_id'] == $v['product_id']) {
                                            $remaining_in_array = true;
                                            $remaining_qty_from_array = $v2['remain_qty'];
                                            $key = $k2;
                                            break;
                                        }
                                    }

                                    if ($remaining_in_array == true) {
                                        if ($remaining_qty_from_array > 0) {
                                            $remaining_qty = $v['quantity'] - $remaining_qty_from_array;
                                        } else {
                                            $remaining_qty = -1;
                                        }

                                        $pair_array[$key]['remain_qty'] = $remaining_qty;
                                    } else {
                                        $key = array_search($v['product_id'], $product_pwp_id_array);
                                        $remaining_qty = $v['quantity'] - $product_pwp_qty_array[$key];
                                        $pair_array[] = array('product_id' => $v['product_id'], 'remain_qty' => $remaining_qty);
                                    }

                                    if ($remaining_qty >= 0) {
                                        $pwp_get++;
                                    }

                                }
                            }
                        }
                    }

                    /*echo 'B' . $pwp_buy . '-' . $rs_promo['pkid'];
                    echo '<br />';
                    echo 'G' . $pwp_get;
                    echo '<br />';*/

                    //pwp type (OR)
                    if ($rs_promo['pwp_type'] == "1") {
                        if ($rs_promo['pwp_pair'] == '1') {
                            if ($pwp_buy > 0 && $pwp_get > 0) {
                                $apply = true;
                            }
                        } else {
                            if ($pwp_buy > 0) {
                                $apply = true;
                            }
                        }
                    } else {
                        //pwp type (AND)
                        if ($pwp_buy == count($product_id_array)) {
                            foreach ($_SESSION['cart'] as $k => $v) {
                                if (in_array($v['product_id'], $product_id_array)) {
                                    $total_pwp += $v['quantity'];

                                    $key = array_search($v['product_id'], $product_id_array);
                                    if ($v['quantity'] < $product_qty_array[$key]) {
                                        $apply = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    //no need buy product
                    if ($pwp_get > 0) {
                        $apply = true;
                    }
                }
            }

            if ($apply != true && $rs_promo['type'] == '3') {
                continue;
            }

            foreach ($freegift_array as $k => $v) {
                if (!in_array($v, $cart_product_array)) {
                    $_SESSION['cart'][] = array('product_id' => $v, 'quantity' => '1', 'free' => 'true');
                }
            }

            if ($_SESSION['member']['order_id'] != '') {
                foreach ($_SESSION['cart'] as $k => $v) {
                    if ($v['free'] == 'true') {
                        $freegift_product_id_array[] = $v['product_id'];
                    }
                }

                $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('freegift_product_id' => implode(",", $freegift_product_id_array)));
                $databaseClass->query($queryUpdate);
            }
        }
    }

    public function pwp()
    {
        $databaseClass = new database();
        global $table, $time_config;

        $total_cart_amount = 0;
        $tier = '';
        $free_shipping = false;
        $pwp_get = false;
        $pwp_buy = 0;

        $campaign_id = $_SESSION['campaign_id'];

        if ($campaign_id == "") {
            $resultPromo = get_query_data($table['promotion'], "status=1 and type='3' and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)");
        } else {
            $resultPromo = get_query_data($table['promotion'], "status=1 and type='3' and date('" . $time_config['today'] . "') between date(start_date) and date(end_date) and campaign_id=$campaign_id");
        }
        $row_promo = $resultPromo->numRows();

        //check member tier
        $cprv_mobile = $_SESSION['member']['mobile'];
        include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";
        if (preg_match("/gold/", strtolower($cprv_array['tier']))) {
            $tier = "gold";
        } elseif (preg_match("/club/", strtolower($cprv_array['tier']))) {
            $tier = "club";
        }

        if ($_SESSION['member']['mobile'] == '+65182268875') {
            $tier = 'gold';
        }

        if ($row_promo > 0) {
            //get total cart amount
            foreach ($_SESSION['cart'] as $k => $v) {
                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                $rs_product = $resultProduct->fetchRow();

                if ($v['free'] == "true") {
                    $rs_product['price'] = "0";
                }

                $total_cart_amount += $v['quantity'] * $rs_product['price'];
            }

            while ($rs_promo = $resultPromo->fetchRow()) {
                if ($rs_promo['campaign_id'] != '0') {
                    if ($_SESSION['campaign_id'] != $rs_promo['campaign_id']) {
                        continue;
                    }
                }

                if ($_SESSION['member']['mobile'] == "" && $rs_promo['member_only'] != "0") {
                    continue;
                }

                if ($total_cart_amount < $rs_promo['min_spend']) {
                    continue;
                }

                if ($rs_promo['member_only'] == "1" && $tier != $rs_promo['member_tier']) {
                    continue;
                } elseif ($rs_promo['member_only'] == "2" && $tier != "") {
                    continue;
                }

                $product_id_array = explode(",", $rs_promo['product_id']);
                $product_qty_array = explode(",", $rs_promo['product_quantity']);

                $pwp_product_id_array = explode(",", $rs_promo['pwp_product_id']);
                $pwp_product_qty_array = explode(",", $rs_promo['pwp_product_quantity']);
                $pwp_product_discount_array = explode(",", $rs_promo['pwp_product_discount']);

                //must buy product first
                if ($rs_promo['product_id'] != "") {
                    foreach ($_SESSION['cart'] as $k => $v) {
                        if (in_array($v['product_id'], $product_id_array)) {
                            $key = array_search($v['product_id'], $product_id_array);
                            if ($v['quantity'] >= $product_qty_array[$key]) {
                                $pwp_buy++;
                            }
                        }
                    }

                    //pwp type (OR)
                    if ($rs_promo['pwp_type'] == "1") {
                        if ($pwp_buy > 0) {
                            $result[] = array(
                                'promo_id' => $rs_promo['pkid'],
                                'min_spend' => $rs_promo['min_spend'],
                                'product_id' => explode(",", $rs_promo['pwp_product_id']),
                                'product_quantity' => explode(",", $rs_promo['pwp_product_quantity']),
                                'product_discount' => explode(",", $rs_promo['pwp_product_discount'])
                            );
                        }
                    } else {
                        //pwp type (AND)
                        if ($pwp_buy == count($product_id_array)) {
                            foreach ($_SESSION['cart'] as $k => $v) {
                                if (in_array($v['product_id'], $pwp_product_id_array)) {
                                    $result[] = array(
                                        'promo_id' => $rs_promo['pkid'],
                                        'min_spend' => $rs_promo['min_spend'],
                                        'product_id' => explode(",", $rs_promo['pwp_product_id']),
                                        'product_quantity' => explode(",", $rs_promo['pwp_product_quantity']),
                                        'product_discount' => explode(",", $rs_promo['pwp_product_discount'])
                                    );
                                }
                            }
                        }
                    }
                } else {
                    //no need buy product
                    $result[] = array(
                        'promo_id' => $rs_promo['pkid'],
                        'min_spend' => $rs_promo['min_spend'],
                        'product_id' => explode(",", $rs_promo['pwp_product_id']),
                        'product_quantity' => explode(",", $rs_promo['pwp_product_quantity']),
                        'product_discount' => explode(",", $rs_promo['pwp_product_discount'])
                    );
                }

                if ($_SESSION['member']['order_id'] != '' && $free_shipping == false) {
                    $resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['member']['order_id']);
                    $rs_order = $resultOrder->fetchRow();

                    if ($rs_order['shipping_method'] == "delivery" && $rs_promo['free_lalamove'] == "1") {
                        $free_shipping = true;
                        $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $rs_order['shipping_amount'], 'label' => 'Free Shipping');
                    } else if ($rs_order['shipping_method'] == "courier" && $rs_promo['free_courier'] == "1") {
                        $free_shipping = true;
                        $result[] = array('promo_id' => $rs_promo['pkid'], 'discount' => $rs_order['shipping_amount'], 'label' => 'Free Shipping');
                    }
                }
            }

            return $result;
        } else {
            return false;
        }
    }

    public
    function gold_member()
    {
        $databaseClass = new database();
        global $table, $time_config, $product_holiday_category, $product_holiday_gold_discount_array;

        $cprv_mobile = $_SESSION['member']['mobile'];
        include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";

        if ($_SESSION['member']['mobile'] == "0182268875") {
            $cprv_array['tier'] = "gold";
            $cprv_array['points'] = '100';
            $cprv_array['vouchers'][] = array('id' => '483', 'url' => "#");
            $cprv_array['vouchers'][] = array('id' => '482', 'url' => "#");
            $cprv_array['vouchers'][] = array('id' => '761', 'url' => "#");
            $_SESSION['member']['otp'] = "123";
            $_SESSION['member']['otp_request'] = "123";
        }

        if (preg_match("/gold/", strtolower($cprv_array['tier']))) {
            $tier = "gold";
        } elseif (preg_match("/club/", strtolower($cprv_array['tier']))) {
            $tier = "club";
        }

        $member_discount_amount = 0;

        if ($tier == 'gold') {
            foreach ($_SESSION['cart'] as $k => $v) {
                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                $rs_product = $resultProduct->fetchRow();

                if ($_SESSION['member']['custom_order'] == "1" && $v['price'] != "") {
                    $rs_product['price'] = $v['price'] / $v['quantity'];
                }

                if ($v['free'] == "true") {
                    $rs_product['price'] = "0";
                }

                //holiday set no discount
                if ($rs_product['price'] > 0) {
                    if ($rs_product['gold_discount'] == "1") {
                        $member_discount_amount += ($rs_product['price'] * $v['quantity']) * 0.05;
                    }
                }
            }
        }

        return $member_discount_amount;
    }

    public
    function special_label()
    {
        $databaseClass = new database();
        global $table, $time_config;

        $resultPromo = get_query_data($table['promotion'], "status=1 and special_label!='' and (product_id!='' or pwp_product_id!='') and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)");
        while ($rs_promo = $resultPromo->fetchRow()) {
            if ($rs_promo['type'] == "3") {
                $product_id_array = explode(",", $rs_promo['pwp_product_id']);
                $product_discount_array = explode(",", $rs_promo['pwp_product_discount']);

                foreach (explode(",", $rs_promo['pwp_product_id']) as $k => $v) {
                    $key = array_search($v, $product_id_array);

                    $promo_array[] = array('product_id' => $v, 'special_label' => $rs_promo['special_label'], 'discount' => $product_discount_array[$key]);
                }
            }

            $product_id_array = explode(",", $rs_promo['product_id']);
            $product_discount_array = explode(",", $rs_promo['product_discount']);

            foreach (explode(",", $rs_promo['product_id']) as $k => $v) {
                $key = array_search($v, $product_id_array);

                $promo_array[] = array('product_id' => $v, 'special_label' => $rs_promo['special_label'], 'discount' => $product_discount_array[$key]);
            }
        }

        return $promo_array;
    }
}

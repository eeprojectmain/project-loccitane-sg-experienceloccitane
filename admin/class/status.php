<?

class status
{
    function getStatusLabel($status)
    {
        $green = "style=\"color:#5cb85c\"";
        $red = "style=\"color:#c9302c\"";
        $blue = "style=\"color:#428bca\"";
        $yellow = "style=\"color:#f0ad4e\"";

        $active = "<label class='label label-success'><b>Active</b></label>";
        $inactive = "<label class='label label-danger'><b>Inactive</b></label>";

        if ($status == "0") {
            return $inactive;
        } elseif ($status == "1") {
            return $active;
        }
    }

    function getPaymentStatusLabel($status, $type)
    {
        $green = "style=\"color:#5cb85c\"";
        $red = "style=\"color:#c9302c\"";
        $blue = "style=\"color:#428bca\"";
        $yellow = "style=\"color:#f0ad4e\"";

        $paid = "<label class='label label-success'><b>Paid</b></label>";
        $pending = "<label class='label label-danger'><b>Pending</b></label>";
        $instore = "<label class='label label-warning'><b>Refer to store</b></label>";

        if ($type == "1") {
            if ($status == "0") {
                return $pending;
            } elseif ($status == '1') {
                return $paid;
            }
        } elseif ($type == '2') {
            return $instore;
        }
    }

}

?>
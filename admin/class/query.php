<?

class query
{
    public function product($product_id)
    {
        global $table;
        $databaseClass = new database();

        $type = explode("-", $product_id)[0];
        $id = explode("-", $product_id)[1];

        if ($type == "C") {
            $result = get_query_data($table['custom_result'], "pkid=$id");
            $rs_array = $result->fetchRow();

            $resultBuilder = get_query_data($table['custom_builder'], "pkid=" . $rs_array['builder_id']);
            $rs_builder = $resultBuilder->fetchRow();

            $rs_array['type'] = "C";
            $rs_array['custom_builder'] = $rs_builder;

            return $rs_array;
        } elseif ($type == "D") {
            $result = get_query_data($table['design'], "pkid=$id");
            $rs_array = $result->fetchRow();

            $resultSeller = get_query_data($table['seller'], "pkid=" . $rs_array['seller_id']);
            $rs_seller = $resultSeller->fetchRow();

            $rs_array['seller'] = $rs_seller;

            return $rs_array;
        }
    }

    public function order($order_id)
    {
        global $table;
        $databaseClass = new database();

        $result = get_query_data($table['order'], "pkid=$order_id");
        $rs_array = $result->fetchRow();

        return $rs_array;
    }
}

?>
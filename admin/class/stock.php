<?php

class stock
{
    public function check($product_id)
    {
        global $table;
        $databaseClass = new database();

        $outlet_id = $_SESSION['outlet_id'];

        if ($outlet_id == "999") {
            return true;
        }

        if ($outlet_id != 1 && $outlet_id != 2) {
            if ($product_id == '440' || $product_id == '441' || $product_id == '442' || $product_id == '443') {
                return false;
            }
        }

        $resultProduct = get_query_data($table['product'], "pkid=$product_id");
        $rs_product = $resultProduct->fetchRow();

        if ($rs_product['status'] == '0' && $product_id != '560' && $product_id != '561' && $product_id != '562') { // for holiday 2022
            return false;
        }

        $resultOutlet = get_query_data($table['outlet'], "pkid=$outlet_id");
        $rs_outlet = $resultOutlet->fetchRow();

        $item_code_array = explode(",", $rs_product['item_code']);
        $item_code_array = array_filter($item_code_array);

        if (count($item_code_array) > 1) {
            if ($rs_product['group_sku'] == "1") {
                $sum_stock = 0;
                foreach ($item_code_array as $k => $v) {
                    $v = strtolower($v);
                    $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='$v'");
                    $rs_stock = $resultStock->fetchRow();

                    $sum_stock += $rs_stock['quantity'];
                }

                if ($sum_stock > $rs_outlet['inventory_limit']) {
                    return true;
                } else {
                    return false;
                }
            } else {
                foreach ($item_code_array as $k => $v) {
                    $v = strtolower($v);
                    $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='$v'");
                    $rs_stock = $resultStock->fetchRow();

                    if (intval($rs_stock['quantity']) <= intval($rs_outlet['inventory_limit'])) {
                        return false;
                    }
                }
                return true;
            }
        } else {
            $rs_product['item_code'] = strtolower($rs_product['item_code']);
            $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='" . $rs_product['item_code'] . "'");
            $rs_stock = $resultStock->fetchRow();

            if (intval($rs_stock['quantity']) > intval($rs_outlet['inventory_limit'])) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function check_cart($product_id)
    {
        global $table;
        $databaseClass = new database();

        $outlet_id = $_SESSION['outlet_id'];

        if ($outlet_id == "999") {
            return true;
        }

        foreach ($_SESSION['cart'] as $k => $v) {
            if ($v['product_id'] == $product_id) {
                $cart_qty = $v['quantity'];
            }
        }

        if ($cart_qty == '') {
            $cart_qty = '0';
        }

        $resultProduct = get_query_data($table['product'], "pkid=" . $product_id);
        $rs_product = $resultProduct->fetchRow();

        if ($rs_product['status'] == '0') {
            return true;
        }

        $resultOutlet = get_query_data($table['outlet'], "pkid=$outlet_id");
        $rs_outlet = $resultOutlet->fetchRow();

        $item_code_array = explode(",", $rs_product['item_code']);
        $item_code_array = array_filter($item_code_array);

        if (count($item_code_array) > 1) {
            if ($rs_product['group_sku'] == "1") {
                $sum_stock = 0;
                foreach ($item_code_array as $k2 => $v2) {
                    $v2 = strtolower($v2);
                    $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='$v2'");
                    $rs_stock = $resultStock->fetchRow();

                    $sum_stock += $rs_stock['quantity'];
                }
                if (($sum_stock - intval($rs_outlet['inventory_limit'])) >= $cart_qty) {
                    return true;
                } else {
                    return false;
                }
            } else {
                foreach ($item_code_array as $k2 => $v2) {
                    $v2 = strtolower($v2);
                    $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='$v2'");
                    $rs_stock = $resultStock->fetchRow();

                    if ((intval($rs_stock['quantity']) - intval($rs_outlet['inventory_limit'])) < $cart_qty) {
                        return false;
                    }
                }
                return true;
            }
        } else {
            $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='" . strtolower($rs_product['item_code']) . "'");
            $rs_stock = $resultStock->fetchRow();

            if ((intval($rs_stock['quantity']) - intval($rs_outlet['inventory_limit'])) > $cart_qty) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function check_summary()
    {
        global $table;
        $databaseClass = new database();

        $outlet_id = $_SESSION['outlet_id'];

        if ($outlet_id == "999") {
            return true;
        }

        $resultOutlet = get_query_data($table['outlet'], "pkid=$outlet_id");
        $rs_outlet = $resultOutlet->fetchRow();

        foreach ($_SESSION['cart'] as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
            $rs_product = $resultProduct->fetchRow();

            if ($rs_product['status'] == '0') {
                continue;
            }

            $item_code_array = explode(",", $rs_product['item_code']);
            $item_code_array = array_filter($item_code_array);

            if (count($item_code_array) > 1) {
                if ($rs_product['group_sku'] == "1") {
                    $sum_stock = 0;
                    foreach ($item_code_array as $k2 => $v2) {
                        $v2 = strtolower($v2);
                        $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='$v2'");
                        $rs_stock = $resultStock->fetchRow();

                        $sum_stock += $rs_stock['quantity'];
                    }

                    if (($sum_stock - intval($rs_outlet['inventory_limit'])) < intval($v['quantity'])) {
                        $ofs_product[] = $v2;
                        unset($_SESSION['cart'][$k]);
                    }
                } else {
                    foreach ($item_code_array as $k2 => $v2) {
                        $v2 = strtolower($v2);
                        $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='$v2'");
                        $rs_stock = $resultStock->fetchRow();

                        if ((intval($rs_stock['quantity']) - intval($rs_outlet['inventory_limit'])) < intval($v['quantity'])) {
                            $ofs_product[] = $v2;
                            unset($_SESSION['cart'][$k]);
                        }
                    }
                }
            } else {
                $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='" . strtolower($rs_product['item_code']) . "'");
                $rs_stock = $resultStock->fetchRow();

                if ((intval($rs_stock['quantity']) - intval($rs_outlet['inventory_limit'])) < intval($v['quantity'])) {
                    $ofs_product[] = $v['product_id'];
                    unset($_SESSION['cart'][$k]);
                }
            }
        }

        return $ofs_product;
    }

    public function minus($order_id)
    {
        global $table;
        $databaseClass = new database();

        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $rs_order = $resultOrder->fetchRow();

        $outlet_id = $rs_order['outlet_id'];
        $product_array = explode(",", $rs_order['product_id']);
        $quantity_array = explode(",", $rs_order['quantity']);

        foreach ($product_array as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
            $rs_product = $resultProduct->fetchRow();

            $item_code_array = explode(",", $rs_product['item_code']);
            $item_code_array = array_filter($item_code_array);

            if (count($item_code_array) > 1) {
                foreach ($item_code_array as $k2 => $v2) {
                    $v2 = strtolower($v2);
                    $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='$v2'");
                    $rs_stock = $resultStock->fetchRow();

                    $queryUpdate = get_query_update($table['inventory'], $rs_stock['pkid'], array('quantity' => ($rs_stock['quantity'] - $quantity_array[$k])));
                    $databaseClass->query($queryUpdate);
                }
            } else {
                $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='" . strtolower($rs_product['item_code']) . "'");
                $rs_stock = $resultStock->fetchRow();

                $queryUpdate = get_query_update($table['inventory'], $rs_stock['pkid'], array('quantity' => ($rs_stock['quantity'] - $quantity_array[$k])));
                $databaseClass->query($queryUpdate);
            }
        }
    }

    public function add($order_id)
    {
        global $table;
        $databaseClass = new database();

        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $rs_order = $resultOrder->fetchRow();

        $outlet_id = $rs_order['outlet_id'];
        $product_array = explode(",", $rs_order['product_id']);
        $quantity_array = explode(",", $rs_order['quantity']);

        foreach ($product_array as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
            $rs_product = $resultProduct->fetchRow();

            $item_code_array = explode(",", $rs_product['item_code']);
            $item_code_array = array_filter($item_code_array);

            if (count($item_code_array) > 1) {
                foreach ($item_code_array as $k2 => $v2) {
                    $v2 = strtolower($v2);
                    $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='$v2'");
                    $rs_stock = $resultStock->fetchRow();

                    $queryUpdate = get_query_update($table['inventory'], $rs_stock['pkid'], array('quantity' => ($rs_stock['quantity'] + $quantity_array[$k])));
                    $databaseClass->query($queryUpdate);
                }
            } else {
                $resultStock = get_query_data($table['inventory'], "outlet_id=$outlet_id and lower(product_code)='" . strtolower($rs_product['item_code']) . "'");
                $rs_stock = $resultStock->fetchRow();

                $queryUpdate = get_query_update($table['inventory'], $rs_stock['pkid'], array('quantity' => ($rs_stock['quantity'] + $quantity_array[$k])));
                $databaseClass->query($queryUpdate);
            }
        }
    }
}
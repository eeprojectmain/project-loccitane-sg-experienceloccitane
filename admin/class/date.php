<?

class date
{
    function getWeekStartAndEndDate($week, $year)
    {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('Y-m-d');
        return $ret;
    }

    function getThisMonthStartAndEnd()
    {
        $ret['start'] = date('Y-m-01 00:00:00', strtotime('this month'));
        $ret['end'] = date('Y-m-t', strtotime('this month'));

        return $ret;
    }
}

?>
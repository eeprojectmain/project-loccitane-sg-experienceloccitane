<?

class button
{
    function get_new_button($folder)
    {
        return '<a href="' . $folder . '/new"><button type="button" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus fa-fw"></i> <span>Add New</span></button></a>';
    }

    function get_edit_button($folder, $pkid)
    {
        return '<a href="' . $folder . '/edit?id=' . $pkid . '"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-pencil fa-fw"></i> <span>Edit</span></button></a>';
    }

    function get_view_button($folder, $pkid, $extraCss = null)
    {
        return '<a href="' . $folder . '/view?id=' . $pkid . '"><button type="button" class="btn btn-success '.$extraCss.' waves-effect waves-light"><i class="fa fa-eye fa-fw"></i> <span>View</span></button></a>';
    }

    function get_delete_button($folder, $pkid)
    {
        return '<button type="button" class="btn btn-danger waves-effect waves-light" onclick="ajax_delete_listing(\'' . $folder . '\',' . $pkid . ')"><i class="fa fa-trash fa-fw"></i> <span>Delete</span></button>';
    }

    function get_cancel_button($folder)
    {
        return '<a href="' . $folder . '"><button type="button" class="btn btn-danger waves-effect waves-light"><i class="fa fa-times fa-fw"></i> <span>Cancel</span></button></a>';
    }

    function get_back_button($folder)
    {
        return '<a href="' . $folder . '"><button type="button" class="btn btn-default waves-effect waves-light"><i class="fa fa-angle-double-left fa-fw"></i> <span>Back</span></button></a>';
    }

    function get_save_button()
    {
        return '<button type="submit" class="btn btn-success waves-effect waves-light" name="submit_save" value="true"><i class="fa fa-floppy-o fa-fw"></i> <span>Save</span></button></a>';
    }

    function get_submit_button()
    {
        return '<button type="submit" class="btn btn-success waves-effect waves-light" name="submit_filter" value="true"><i class="fa fa-check fa-fw"></i> <span>Submit</span></button></a>';
    }

    function get_clear_button()
    {
        return '<button type="submit" class="btn btn-danger waves-effect waves-light" name="submit_clear" value="true"><i class="fa fa-times fa-fw"></i> <span>Clear</span></button></a>';
    }
}

?>
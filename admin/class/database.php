<?php

/*

Database class

*/

class database {

	// Set DB_URL
	function database($url = '') {
		$this->db_url = $url;
		// Connect to database
		$this->connect();
		// Check for database connection error
		if($this->is_error()) {
			die($this->get_error());
		}
	}

	// Connect to the database
	function connect() {
		$status = $this->db_handle = ($GLOBALS["mysqli_conn"] = mysqli_connect(DB_HOST,  DB_USERNAME,  DB_PASSWORD));
		mysqli_set_charset($GLOBALS["mysqli_conn"],"utf8");
		if(mysqli_error($GLOBALS["mysqli_conn"])) {
			$this->connected = false;
			$this->error = mysqli_error($GLOBALS["mysqli_conn"]);
		} else {
			if(!mysqli_select_db($GLOBALS["mysqli_conn"], constant('DB_NAME'))) {
				$this->connected = false;
				$this->error = mysqli_error($GLOBALS["mysqli_conn"]);
			} else {
				$this->connected = true;
			}
		}
		return $this->connected;
	}

	// Disconnect from the database
	function disconnect() {
		if(isset($this->Database)) {
			((is_null($___mysqli_res = mysqli_close($GLOBALS["mysqli_conn"]))) ? false : $___mysqli_res);
			return true;
		} else {
			return false;
		}
	}

	// Run a query
	function query($statement) {
		$mysql = new mysql();
		$mysql->query($statement);
		if($mysql->error()) {
			$this->set_error($mysql->error());
			return null;
		} else {
			return $mysql;
		}
	}

	// Gets the first column of the first row
	function get_one($statement) {
		$fetch_row = mysqli_fetch_row(mysqli_query($GLOBALS["mysqli_conn"], $statement));
		$result = $fetch_row[0];
		if(mysqli_error($GLOBALS["mysqli_conn"])) {
			$this->set_error(mysqli_error($GLOBALS["mysqli_conn"]));
			return null;
		} else {
			return $result;
		}
	}

	// Set the DB error
	function set_error($message = null) {
		global $TABLE_DOES_NOT_EXIST, $TABLE_UNKNOWN;
		$this->error = $message;
		if(strpos($message, 'no such table')) {
			$this->error_type = $TABLE_DOES_NOT_EXIST;
		} else {
			$this->error_type = $TABLE_UNKNOWN;
		}
	}

	// Return true if there was an error
	function is_error() {
		return (!empty($this->error)) ? true : false;
	}

	// Return the error
	function get_error() {
		return $this->error;
	}

}

class mysql {

	// Run a query
	function query($statement) {
	    mysqli_set_charset($GLOBALS["mysqli_conn"],"utf8");
		return $this->result = mysqli_query($GLOBALS["mysqli_conn"], $statement);
		$this->error = mysqli_error($GLOBALS["mysqli_conn"]);
	}

	// Get the ID generated from the previous INSERT operation
	function insertID() {
		return ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["mysqli_conn"]))) ? false : $___mysqli_res);
	}

	// Fetch num rows
	function numRows() {
		return mysqli_num_rows($this->result);
	}

	// Fetch row
	function fetchRow() {
		return mysqli_fetch_assoc($this->result);
	}

	// Get error
	function error() {
		if(isset($this->error)) {
			return $this->error;
		} else {
			return null;
		}
	}

}

?>
<?php

class api
{
    public function __construct($mode = "")
    {
        if ($mode == "") {
            $this->mode = 'production';
        } else {
            $this->mode = $mode;
        }

        if ($this->mode == 'test') {
            $this->api_url = 'https://loccitanesguat.crmxs.com/?xs_app=';
            $this->api_client_id = '5vus5fnhdeeghff5de8c2nrq46fhy8nh';
            $this->api_client_secret = 'sum388my7amm8jp7k3ru5pyb4hp8g87g';
        } else {
            $this->api_url = 'https://loccitanesgwebservice.crmxs.com/?xs_app=';
            $this->api_client_id = '5vus5fnhdeeghff5de8c2nrq46fhy8nh';
            $this->api_client_secret = 'sum388my7amm8jp7k3ru5pyb4hp8g87g';
        }

        //if cprv side data got country code, change to 1
        $this->country_code = 1;
    }

    public function create_profile($postfield)
    {
        global $table, $time_config;
        $databaseClass = new database();

        $resultOrder = get_query_data($table['order'], "pkid=" . $postfield['order_id']);
        $rs_order = $resultOrder->fetchRow();

        $resultStore = get_query_data($table['outlet'], "pkid=" . $rs_order['outlet_id']);
        $rs_store = $resultStore->fetchRow();

        if ($postfield['sms_status'] == "1") {
            $subscription[] = 'sms';
        }
        if ($postfield['call_status'] == "1") {
            $subscription[] = 'call';
        }
        if ($postfield['whatsapp_status'] == "1") {
            $subscription[] = 'whatsapp';
        }
        if ($postfield['email_status'] == "1") {
            $subscription[] = 'edm';
        }

        $postfield_api = array(
            'hpno' => $postfield['mobile'],
            'firstname' => $postfield['first_name'],
            'lastname' => $postfield['last_name'],
            'email' => $postfield['email'],
            'subscription' => $subscription,
            'storecode' => $rs_store['code']
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api_url . "endemande.createCustomer",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($postfield_api),
            CURLOPT_HTTPHEADER => array(
                "client_id: " . $this->api_client_id,
                "client_secret: " . $this->api_client_secret,
                "Content-Type: text/plain",
            ),
        ));

        $server_response = curl_exec($curl);
        curl_close($curl);
        $cprv_array = json_decode($server_response, true);

        return $cprv_array;

        $header_array = array('client_id: ' . $this->api_client_id, 'client_secret: ' . $this->api_client_secret, 'Content-Type: application/json');
        $options = array(
            'http' => array(
                'header' => $header_array,
                'method' => 'POST',
                'content' => json_encode($postfield_api)
            )
        );
        $context = stream_context_create($options);
        $server_response = file_get_contents($this->api_url . 'endemande.createCustomer', false, $context);

        return json_decode($server_response, true);
    }

    public function create_sample_profile($postfield)
    {
        global $table, $time_config;
        $databaseClass = new database();

        if ($postfield['sms_status'] == "1") {
            $subscription[] = 'sms';
        }
        if ($postfield['call_status'] == "1") {
            $subscription[] = 'call';
        }
        if ($postfield['whatsapp_status'] == "1") {
            $subscription[] = 'whatsapp';
        }
        if ($postfield['email_status'] == "1") {
            $subscription[] = 'edm';
        }

        $mobile = $postfield['mobile'];
        if(!$this->country_code)
        {
            $mobile = str_replace("+65", "", $postfield['mobile']);
        }

        $postfield_api = array(
            'hpno' => $mobile,
            'firstname' => $postfield['first_name'],
            'lastname' => $postfield['last_name'],
            'email' => $postfield['email'],
            'subscription' => $subscription,
            'source_id' => $postfield['source_id'],
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api_url . 'endemande.createCustomerByEvent',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($postfield_api),
            CURLOPT_HTTPHEADER => array(
                "client_id: " . $this->api_client_id,
                "client_secret: " . $this->api_client_secret,
                "Content-Type: text/plain",
            ),
        ));

        $server_response = curl_exec($curl);
        curl_close($curl);
        $cprv_array = json_decode($server_response, true);

        return $cprv_array;

        /*$header_array = array('client_id: ' . $this->api_client_id, 'client_secret: ' . $this->api_client_secret, 'Content-Type: application/json');
        $options = array(
            'http' => array(
                'header' => $header_array,
                'method' => 'POST',
                'content' => json_encode($postfield_api)
            )
        );
        $context = stream_context_create($options);
        $server_response = file_get_contents($this->api_url . 'endemande.createCustomerByEvent', false, $context);
        */
        return json_decode($server_response, true);
    }

    public function redeem_point($order_id, $flag)
    {
        global $table, $time_config;
        $databaseClass = new database();

        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $rs_order = $resultOrder->fetchRow();

        $resultOutlet = get_query_data($table['outlet'], "pkid=" . $rs_order['outlet_id']);
        $rs_outlet = $resultOutlet->fetchRow();

        if ($rs_order['point'] == '0' || $rs_order['point_quantity'] == '') {
            return false;
        }

        $customer_data = $this->get_customer($rs_order['mobile']);

        $postfield = array(
            'order_id' => $order_id,
            'cid' => (int)$customer_data['cid'],
            'points' => array(json_decode($rs_order['point_quantity'], true)),
            'flag' => $flag,
            'storecode' => $rs_outlet['code']
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api_url . "endemande.redeemPoints",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($postfield),
            CURLOPT_HTTPHEADER => array(
                "client_id: " . $this->api_client_id,
                "client_secret: " . $this->api_client_secret,
                "Content-Type: text/plain",
            ),
        ));

        $server_response = curl_exec($curl);
        curl_close($curl);
        $cprv_array = json_decode($server_response, true);

        return $cprv_array;

        $header_array = array('client_id: ' . $this->api_client_id, 'client_secret: ' . $this->api_client_secret, 'Content-Type: application/json');
        $options = array(
            'http' => array(
                'header' => $header_array,
                'method' => 'POST',
                'content' => json_encode($postfield)
            )
        );
        $context = stream_context_create($options);
        $server_response = file_get_contents($this->api_url . 'endemande.redeemPoints', false, $context);

        return json_decode($server_response, true);
    }

    public function get_customer($mobile)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api_url . "endemande.getCustomerSummary",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"hpno\":\"" . $mobile . "\"}",
            CURLOPT_HTTPHEADER => array(
                "client_id: " . $this->api_client_id,
                "client_secret: " . $this->api_client_secret,
                "Content-Type: text/plain",
            ),
        ));

        $server_response = curl_exec($curl);
        curl_close($curl);
        $cprv_array = json_decode($server_response, true);

        return $cprv_array;

        $postfield = array('hpno' => $mobile);

        $header_array = array('client_id: ' . $this->api_client_id, 'client_secret: ' . $this->api_client_secret, 'Content-Type: application/json');
        $options = array(
            'http' => array(
                'header' => $header_array,
                'method' => 'POST',
                'content' => json_encode($postfield)
            )
        );
        $context = stream_context_create($options);
        $server_response = file_get_contents($this->api_url . 'endemande.getCustomerSummary', false, $context);


        return json_decode($server_response, true);
    }
}
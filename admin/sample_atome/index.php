<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$dateClass = new date();

$month_start = date("Y-m-d", strtotime('-29 day'));
$month_end = date("Y-m-d");

$this_folder = basename(__DIR__);

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['date']) {
        $date_range = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $postfield['date']);

        $month_start = date("Y-m-d", strtotime(explode(" - ", $date_range)[0]));
        $month_end = date("Y-m-d", strtotime(explode(" - ", $date_range)[1]));

        $where_array[] = "date(created_date) between date('$month_start') and date('$month_end')";
    }
    if ($postfield['status']) {
        $where_array[] = "status=" . $postfield['status'];
    }
    if ($postfield['shipping_method']) {
        $where_array[] = "shipping_method='" . $postfield['shipping_method'] . "'";
    }
    if ($postfield['type']) {
        $where_array[] = "type='" . $postfield['type'] . "'";
    }

    $where_text = implode(" and ", $where_array);
} else {
    $where_array[] = "date(created_date) between date('$month_start') and date('$month_end')";
    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);

    if (!empty($where)) {
        $where_query .= " and " . $where;
    }

    $resultOrder = get_query_data($table['sample_atome'], "1 " . $where_query);
    while ($rs_order = $resultOrder->fetchRow()) {
        $dataTable['data'][] = array(
            $rs_order['pkid'],
            $rs_order['first_name'],
            $rs_order['last_name'],
            $rs_order['mobile'],
            $rs_order['email'],
            $rs_order['how_to_other'] != "" ? $rs_order['how_to'] . " - " . $rs_order['how_to_other'] : $rs_order['how_to'],
            $rs_order['sms_status'] == "1" ? "Yes" : "No",
            $rs_order['email_status'] == "1" ? "Yes" : "No",
            $rs_order['call_status'] == "1" ? "Yes" : "No",
            $rs_order['whatsapp_status'] == "1" ? "Yes" : "No",
            $rs_order['created_date'],
            $buttonClass->get_delete_button($this_folder,$rs_order['pkid'])
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $resultDelete = get_query_data($table['order'], "pkid=$pkid");
    $rs_delete = $resultDelete->fetchRow();

    $query = get_query_delete($table['order'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">

<audio controls id="notification" style="width: 1px;height: 1px">
    <source src="/admin/noti.mp3" type="audio/mpeg">
</audio>
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Sample - Atome</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Sample - Atome</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form action="<?= $this_folder ?>/index" method="post" class="form-vertical">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Date Range</label>
                                    <input type="text" name="date" class="form-control daterange"
                                           value="<?= $month_start ?> - <?= $month_end ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="margin-left:1em;margin-top:1em;">
                                    <div class="pull-left">
                                        <?= $buttonClass->get_submit_button() ?>
                                        <? if ($postfield['submit_filter']) {
                                            echo $buttonClass->get_clear_button();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Source</th>
                                        <th>SMS</th>
                                        <th>Email</th>
                                        <th>Call</th>
                                        <th>Whatsapp</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    var autoprint;

    var table = $('#datatable').DataTable({
        "ajax": {
            "url": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        },
        "order": [
            [0, "desc"]
        ],
        responsive: true,
        dom: "<'row'<'col-sm-12'B>><'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function () {
        new Switchery($(this)[0], $(this).data());
        $(this)[0].onchange = function () {
            autoprint = $(this)[0].checked;
            $.ajax({
                method: "POST",
                url: "ajax/save-auto-print",
                data: {
                    status: autoprint
                }
            })
                .done(function (data) {
                    console.log(data);
                });
        };
    });

    setInterval(function (e) {
        table.ajax.reload(null, false);
        $.ajax({
            method: "POST",
            url: "ajax/check-new-order",
            dataType: "json"
        })
            .done(function (data) {
                console.log(data);
                if (data.print == 'true') {
                    $("#notification")[0].play();
                }
            });
    }, 300000);

    setInterval(function (e) {
        window.location.reload();
    }, 900000);

    function markComplete(id) {
        swal({
            title: "Are you sure?",
            text: "Once marked as complete, you will not be able to unmark it.",
            icon: "info",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: "ajax/order-complete",
                    type: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == "success") {
                            table.ajax.reload(null, false);
                            swal("Bravo!", "Successfully updated", {
                                icon: "success",
                                timer: 1000,
                            });
                        } else {
                            swal("Opps...", "Something went wrong, please try again.", {
                                icon: "error",
                            });
                        }
                    }
                });
            }
        })
    }
</script>
</body>

</html>
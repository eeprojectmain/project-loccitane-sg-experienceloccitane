<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['cat_id']) {
        $where_array[] = " find_in_set(".$postfield['cat_id'].",cat_id)";
    }

    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);

    if (!empty($where)) {
        $where_query .= " and " . $where;
    }

    $resultArray = get_query_data($table['product'], "1". $where_query);
    while ($rs_array = $resultArray->fetchRow()) {
        $category_text=array();
        foreach(explode(",",$rs_array['cat_id']) as $k=>$v) {
            $resultCategory = get_query_data($table['product_category'], "pkid=" . $v);
            $rs_category = $resultCategory->fetchRow();
            $category_text[]=$rs_category['title'];
        }

        if($rs_array['img_url']!=""){
            $img_url="<img src='../assets/product/".$rs_array['img_url']."' width='100px' />";
        }else{
            $img_url="<img src='https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=".$rs_array['item_code']."&v=2' width='100px' />";
        }

        $dataTable['data'][] = array(
            $rs_array['pkid'],
            $statusClass->getStatusLabel($rs_array['status']),
            $rs_array['title']." | ".$rs_array['size'],
            implode(", ",$category_text),
            $rs_array['item_code'],
            $rs_array['start_date']." - ".$rs_array['end_date'],
            $rs_array['price'],
            $img_url,
            $buttonClass->get_edit_button($this_folder, $rs_array['pkid']) . " " . $buttonClass->get_delete_button($this_folder, $rs_array['pkid'])
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $query = get_query_delete($table['product'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Product</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Product</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
            <div class="col-md-12">
                    <div class="white-box">
                        <form action="<?= $this_folder ?>/index" method="post" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Category</label>
                                    <select class="form-control" name='cat_id'>
                                    <option value=''>---Please Select---</option>
                                    <?
                                    $resultCategory=get_query_data($table['product_category'], "1 order by pkid asc");
                                    while($rs_category=$resultCategory->fetchRow()){
                                        ?>
                                        <option value="<?=$rs_category['pkid']?>" <?=$rs_category['pkid']==$postfield['cat_id']?"selected":""?>><?=$rs_category['title']?></option>
                                        <?
                                    }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left">
                                        <?= $buttonClass->get_submit_button() ?>
                                        <? if ($postfield['submit_filter']) {
                                            echo $buttonClass->get_clear_button();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $buttonClass->get_new_button($this_folder) ?>
                            </div>
                        </div>
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Status</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Code</th>
                                        <th>Avaliable Date</th>
                                        <th>Price (S$)</th>
                                        <th>Img</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    var table = $('#datatable').DataTable({
        "ajax": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        "order": [[0, "desc"]],
        responsive: true,
    });
</script>
</body>

</html>
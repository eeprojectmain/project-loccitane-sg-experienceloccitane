<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['product'], "pkid=$pkid");
$rs_array = $result->fetchRow();

if ($rs_array['img_url']!="") {
    $img_url="<img src='../assets/product/".$rs_array['img_url']."' width='100px' />";
} else {
    $img_url="<img src='https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=".$rs_array['item_code']."&v=2' width='100px' />";
}

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);
    unset($postfield['ava_date']);

    $postfield['cat_id']=implode(",",$_POST['cat_id']);
    $postfield['updated_date'] = $time_config['now'];
    $postfield['updated_by'] = $_SESSION['admin']['username'];

        if ($_FILES['img_url']['tmp_name']!="") {
        $path = $_FILES['img_url']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $filename=uniqid().".".$ext;

        $postfield['img_url']=$filename;

        move_uploaded_file($_FILES['img_url']['tmp_name'], "../../assets/product/$filename");
    }

    $postfield['start_date']=date("Y-m-d", strtotime(explode(" - ", $_POST['ava_date'])[0]));
    $postfield['end_date']=date("Y-m-d", strtotime(explode(" - ", $_POST['ava_date'])[1]));

    $queryUpdate = get_query_update($table['product'], $pkid, $postfield);
    $database->query($queryUpdate);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Product <i class="fa fa-angle-right"></i> Edit</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a href="<?= $this_folder ?>">Product</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post" action="<?= $this_folder ?>/edit?<?=http_build_query($_GET)?>"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <?
                                    if($_SESSION['admin']['role']=="1"){
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Status</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="status" id="status_1"
                                                       value="1" <?= $rs_array['status'] == "1" ? "checked" : "" ?>>
                                                <label for="status_1"> Active </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="status" id="status_0"
                                                       value="0" <?= $rs_array['status'] == "0" ? "checked" : "" ?>>
                                                <label for="status_0"> Inactive </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?}?>
                                    <div class="form-group">
                                            <label class="control-label col-sm-2">Category</label>
                                            <div class="col-sm-5">
                                                <select class="form-control selectpicker" name="cat_id[]" multiple required>
                                                    <option value="">---Please Select---</option>
                                                    <?php
                                                    $resultCat=get_query_data($table['product_category'], "1");
                                                    while ($rs_cat=$resultCat->fetchRow()) {
                                                        ?>
                                                    <option value="<?=$rs_cat['pkid']?>" <?=in_array($rs_cat['pkid'],explode(",",$rs_array['cat_id']))?"selected":""?>><?=$rs_cat['title']?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Name</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="title" maxlength="255" value="<?=$rs_array['title']?>"
                                                    required>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Size</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="size" maxlength="255" value="<?=$rs_array['size']?>"
                                                   required>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Price</label>
                                            <div class="col-sm-5">
                                                <input type="number" class="form-control" name="price" maxlength="255" value="<?=$rs_array['price']?>"
                                                    required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Long Code</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="item_code" value="<?=$rs_array['item_code']?>"
                                                    maxlength="255">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Details</label>
                                            <div class="col-sm-5">
                                                <textarea name="details" class="form-control" rows="5"><?=$rs_array['details']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Available Date</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control daterange" name="ava_date" value="<?=$rs_array['start_date']." - ".$rs_array['end_date']?>"
                                                    maxlength="255" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Display Order</label>
                                            <div class="col-sm-5">
                                                <input type="number" class="form-control" name="sort_order" value="<?=$rs_array['sort_order']?>"
                                                    maxlength="255" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Product Image</label>
                                            <div class="col-sm-5">
                                                <input type="file" name="img_url" id="fileinput">
                                                <br>
                                                <?=$img_url?><br>
                                                <small>If you leave blank, system will get the image from loccitane
                                                    image library by using long code.</small>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    $("#fileinput").fileinput({
        dropZoneEnabled:true,
        maxFileCount: 1,
        maxFileSize: 100000,
        showCaption: false,
        showRemove: false,
        showUpload: false,
        allowedFileExtensions: ['png','jpg','jpeg','gif','webp'],
    });
</script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);
    unset($postfield['new_password']);

    $postfield['password']=protect('encrypt',$_POST['password']);
    $postfield['created_date'] = $time_config['now'];
    $postfield['created_by'] = $_SESSION['user']['username'];

    $queryInsert = get_query_insert($table['user'], $postfield);
    $database->query($queryInsert);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Users <i class="fa fa-angle-right"></i> New</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a href="<?= $this_folder ?>">Users</a></li>
                        <li class="active">New</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post" action="<?= $this_folder ?>/new"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button($this_folder) . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Status</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="status" id="status_1" value="1" checked>
                                                <label for="status_1"> Active </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="status" id="status_0" value="0">
                                                <label for="status_0"> Inactive </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Name</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="name" maxlength="255"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Email</label>
                                        <div class="col-sm-5">
                                            <input type="email" class="form-control" name="username" maxlength="255"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Password</label>
                                        <div class="col-sm-5">
                                            <input type="password" class="form-control" name="password" maxlength="255"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Confirm Password</label>
                                        <div class="col-sm-5">
                                            <input type="password" class="form-control" name="new_password" maxlength="255"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Role</label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="role" required>
                                                <option value="">---Please Select---</option>
                                                <?
                                                foreach($user_role_array as $k=>$v){
                                                    echo '<option value="'.$k.'">'.$v.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button($this_folder) . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions:['png','jpg','jpeg','gif'],
    });
</script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['outlet'], "pkid=$pkid");
$rs_array = $result->fetchRow();

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);

    $queryUpdate = get_query_update($table['outlet'], $pkid, $postfield);
    $database->query($queryUpdate);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Stores <i class="fa fa-angle-right"></i> Edit</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a href="<?= $this_folder ?>">Stores</a>
                        </li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post"
                              action="<?= $this_folder ?>/edit?<?= http_build_query($_GET) ?>"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="title" required
                                                   value="<?= $rs_array['title'] ?>"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Enable Lalamove</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="lalamove_status" id="lalamove_status1" value="1" <?=$rs_array['lalamove_status']=="1"?"checked":""?>>
                                                <label for="lalamove_status1"> Yes </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="lalamove_status" id="lalamove_status0" value="0" <?=$rs_array['lalamove_status']=="0"?"checked":""?>>
                                                <label for="lalamove_status0"> No </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Enable Collect at Store</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="selfcollect_status" id="selfcollect_status1" value="1" <?=$rs_array['selfcollect_status']=="1"?"checked":""?>>
                                                <label for="selfcollect_status1"> Yes </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="selfcollect_status" id="selfcollect_status0" value="0" <?=$rs_array['selfcollect_status']=="0"?"checked":""?>>
                                                <label for="selfcollect_status0"> No </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group hidden">
                                        <label class="control-label col-sm-2">Enable Pay at Store</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="payinstore_status" id="payinstore_status1" value="1" <?=$rs_array['payinstore_status']=="1"?"checked":""?>>
                                                <label for="payinstore_status1"> Yes </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="payinstore_status" id="payinstore_status0" value="0" <?=$rs_array['payinstore_status']=="0"?"checked":""?>>
                                                <label for="payinstore_status0"> No </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">POS Code</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="code" class="form-control" required
                                                   value='<?= $rs_array['code'] ?>'>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">WhatsApp</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="whatsapp" class="form-control"
                                                   value='<?= $rs_array['whatsapp_no'] ?>'>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Map Lat</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="lat" class="form-control"
                                                   value='<?= $rs_array['lat'] ?>'>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Map Lng</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="lng" class="form-control"
                                                   value='<?= $rs_array['lng'] ?>'>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Inventory Threshold</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="inventory_limit" class="form-control" required
                                                   value='<?= $rs_array['inventory_limit'] ?>'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif', 'mp4'],
        initialPreview: []
    });

    $("#fileinput2").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif', 'mp4'],
    });
</script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['outlet_id']) {
        $where_array[] = "outlet_id=" . $postfield['outlet_id'];
    }

    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);

    if (!empty($where)) {
        $where_query .= " and " . $where;
    }

    if ($_SESSION['admin']['role'] != "1") {
        $where_query .= " and pkid=" . $_SESSION['admin']['outlet_id'];
    }

    $resultArray = get_query_data($table['outlet'], "1" . $where_query);
    while ($rs_array = $resultArray->fetchRow()) {
        $dataTable['data'][] = array(
            $rs_array['pkid'],
            $rs_array['title'],
            $rs_array['code'],
            $buttonClass->get_edit_button($this_folder, $rs_array['pkid'])
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $query = get_query_delete($table['outlet'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Stores</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Stores</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>POS Code</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    var table = $('#datatable').DataTable({
        "ajax": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        "order": [[0, "desc"]],
        responsive: true,
    });
</script>
</body>

</html>
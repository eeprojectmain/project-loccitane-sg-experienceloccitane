<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <div class="top-left-part">
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i
                                class="ti-close ti-menu"></i></a></li>
            </ul>
            <a class="logo hidden-xs" href="index.php"> <img src="plugins/images/loccitane-logo.png" alt="home"
                                                             class="light-logo"/> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown"><a class="dropdown-toggle profile-pic" data-toggle="dropdown"
                                    href="javascript:void(0)"> <span
                            class="chat-img bg-danger"><b><?= substr(strtoupper($_SESSION['admin']['username']), 0, 1) ?></b></span><b
                            class="hidden-xs"><?= $_SESSION['admin']['name'] ?></b><span class="caret"></span> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-text">
                                <h4><?= $_SESSION['admin']['name'] ?></h4>
                                <p class="text-muted"><?= $_SESSION['admin']['username'] ?></p>
                            </div>
                        </div>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a href="user/edit?id=<?= $_SESSION['admin']['id'] ?>"><i class="ti-settings"></i> Account
                            Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i
                            class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3>
        </div>

        <ul class="nav" id="side-menu">
            <li class="user-pro"><a href="javascript:void(0)" class="waves-effect"><span
                            class="chat-img bg-danger"><b><?= substr(strtoupper($_SESSION['admin']['username']), 0, 1) ?></b></span>
                    <span class="hide-menu"> <?= $_SESSION['admin']['name'] ?><span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                    <li><a href="javascript:void(0)"><i class="ti-settings"></i> <span class="hide-menu">Account
                                Setting</span></a></li>
                    <li><a href="logout"><i class="fa fa-power-off"></i> <span class="hide-menu">Logout</span></a></li>
                </ul>
            </li>

            <?php if ($_SESSION['admin']['role'] == '1' || $_SESSION['admin']['role'] == '2') { ?>

            <li><a href="dashboard" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span
                            class="hide-menu"> Dashboard <span class="fa arrow"></span></span></a></li>
            <li><a href="order" class="waves-effect "><i class="mdi mdi-view-list fa-fw"></i> <span
                            class="hide-menu">Order<span class="fa arrow"></span></span></a></li>
            <li><a href="member" class="waves-effect "><i class="mdi mdi-view-list fa-fw"></i> <span
                            class="hide-menu">Member<span class="fa arrow"></span></span></a></li>
            <li><a href="inventory" class="waves-effect "><i class="mdi mdi-dropbox fa-fw"></i> <span
                            class="hide-menu">Inventory<span class="fa arrow"></span></span></a></li>
            <?php if ($_SESSION['admin']['role'] == '1') { ?>
                <li><a href="banner" class="waves-effect "><i class="mdi mdi-image-area fa-fw"></i> <span
                                class="hide-menu">Banner<span class="fa arrow"></span></span></a></li>
                <li><a href="product" class="waves-effect "><i class="mdi mdi-package fa-fw"></i> <span
                                class="hide-menu">Product<span class="fa arrow"></span></span></a></li>
                <li><a href="promotion" class="waves-effect "><i class="mdi mdi-percent fa-fw"></i> <span
                                class="hide-menu">Promotion<span class="fa arrow"></span></span></a></li>
                <li><a href="outlet" class="waves-effect "><i class="mdi mdi-store fa-fw"></i> <span
                                class="hide-menu">Stores<span class="fa arrow"></span></span></a></li>
                <li class="hidden"><a href="javascript:void(0)" class="waves-effect"><i
                                class="mdi mdi-google-controller fa-fw"></i> <span
                                class="hide-menu">Game<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="game_voucher/"><i class="fa-fw">G</i><span
                                        class="hide-menu">Game Voucher</span></a>
                        </li>
                        <li><a href="member_voucher/"><i class="fa-fw">U</i><span
                                        class="hide-menu">User Voucher</span></a>
                        </li>
                    </ul>
                </li>

                <li><a href="javascript:void(0)" class="waves-effect"><i
                                class="mdi mdi-view-list fa-fw"></i> <span
                                class="hide-menu">Sampling<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="sample_loccitanepullman_2024"><i class="fa-fw">P</i><span
                                        class="hide-menu">Sample - Pullman</span></a>
                        </li>
                        <li><a href="sample_immortelle_skincare_sample_2024"><i class="fa-fw">IS</i><span
                                        class="hide-menu">Sample - Immortelle Skincare 2024</span></a>
                        </li>
                        <li><a href="sample_haircarediagnosis_2023"><i class="fa-fw">HD</i><span
                                        class="hide-menu">Sample - Haircare Diagnosis 2023</span></a>
                        </li>
                        <li><a href="immortelle_routine_sample"><i class="fa-fw">IR</i><span
                                        class="hide-menu">Sample - Immortelle Routine</span></a>
                        </li>
                        <li><a href="sample_dyo"><i class="fa-fw">D</i><span
                                        class="hide-menu">Sample - DYO</span></a>
                        </li>
                        <li><a href="sample_reset"><i class="fa-fw">R</i><span
                                        class="hide-menu">Sample - Reset</span></a>
                        </li>
						<li><a href="sample_shea"><i class="fa-fw">S</i><span
                                        class="hide-menu">Sample - Shea</span></a>
                        </li>
                        <li class="hidden"><a href="sample_atome"><i class="fa-fw">A</i><span
                                        class="hide-menu">Sample - Atome</span></a>
                        </li>
						<li><a href="sample_haircare_2022"><i class="fa-fw">HC</i><span
                                        class="hide-menu">Sample - Haircare</span></a>
                        </li>
                        <li><a href="sample_almond_2022"><i class="fa-fw">A</i><span
                                        class="hide-menu">Sample - AlmondBC</span></a>
                        </li>
                        <li><a href="sample_reset5pcs_2022"><i class="fa-fw">R</i><span
                                        class="hide-menu">Sample - Reset5pcs</span></a>
                        </li>
                        <li><a href="sample_immortelleskincare_2023"><i class="fa-fw">I</i><span
                                        class="hide-menu">Sample - Immortelle</span></a>
                        </li>
                    </ul>
                </li>

                <!--<li><a href="sample_dyo" class="waves-effect "><i class="mdi mdi-view-list fa-fw"></i> <span
                                class="hide-menu">Sample - DYO<span class="fa arrow"></span></span></a></li>
                <li><a href="sample_reset" class="waves-effect "><i class="mdi mdi-view-list fa-fw"></i> <span
                                class="hide-menu">Sample - Reset<span class="fa arrow"></span></span></a></li>
                <li><a href="sample_shea" class="waves-effect "><i class="mdi mdi-view-list fa-fw"></i> <span
                                class="hide-menu">Sample - Shea<span class="fa arrow"></span></span></a></li>
                <li class="hidden"><a href="sample_atome" class="waves-effect "><i class="mdi mdi-view-list fa-fw"></i> <span
                                class="hide-menu">Sample - Atome<span class="fa arrow"></span></span></a></li>-->
            <?php } ?>
            
            <li><a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-file-excel fa-fw"></i> <span
                            class="hide-menu">Excel<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="excel/pos"><i class="fa-fw">P</i><span class="hide-menu">POS</span></a>
                    </li>
                    <?
                    if ($_SESSION['admin']['role'] == "1" || $_SESSION['admin']['id'] == "63") {
                        ?>
                        <li><a href="excel/warehouse"><i class="fa-fw">B</i><span class="hide-menu">BOLSG</span></a>
                        </li>
                        <li><a href="excel/urbanfox"><i class="fa-fw">U</i><span class="hide-menu">UrbanFox</span></a>
                        </li>
                        <li><a href="excel/tracking"><i class="fa-fw">U</i><span class="hide-menu">Update
                                Tracking</span></a></li>
                    <? } ?>
                </ul>
            </li>
            <li><a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-chart-bar fa-fw"></i> <span
                            class="hide-menu">Reports<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="report/order"><i class="fa-fw">O</i><span class="hide-menu">Order</span></a></li>
                    <li><a href="report/delivery"><i class="fa-fw">O</i><span class="hide-menu">Delivery</span></a></li>
                    <li><a href="report/pickup"><i class="fa-fw">O</i><span class="hide-menu">Pick-up</span></a></li>
                    <li><a href="report/game_voucher"><i class="fa-fw">G</i><span class="hide-menu">Game Voucher</span></a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            <?
            if ($_SESSION['admin']['role'] == "1") {
                ?>
                <li><a href="user" class="waves-effect "><i class="mdi fa-user fa-fw"></i> <span
                                class="hide-menu">Users<span class="fa arrow"></span></span></a></li>
            <? } ?>
            <?php if ($_SESSION['admin']['role'] == '3') { ?>
             <li><a href="javascript:void(0)" class="waves-effect"><i
                                class="mdi mdi-view-list fa-fw"></i> <span
                                class="hide-menu">Sampling<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="sample_loccitanepullman_2024"><i class="fa-fw">P</i><span
                                        class="hide-menu">Sample - Pullman</span></a>
                        </li>
                        <li><a href="sample_immortelle_skincare_sample_2024"><i class="fa-fw">IS</i><span
                                        class="hide-menu">Sample - Immortelle Skincare 2024</span></a>
                        </li>
                        <li><a href="sample_dyo"><i class="fa-fw">D</i><span
                                        class="hide-menu">Sample - DYO</span></a>
                        </li>
                        <li><a href="sample_reset"><i class="fa-fw">R</i><span
                                        class="hide-menu">Sample - Reset</span></a>
                        </li>
						<li><a href="sample_shea"><i class="fa-fw">S</i><span
                                        class="hide-menu">Sample - Shea</span></a>
                        </li>
                        <li class="hidden"><a href="sample_atome"><i class="fa-fw">A</i><span
                                        class="hide-menu">Sample - Atome</span></a>
                        </li>
						<li><a href="sample_haircare_2022"><i class="fa-fw">HC</i><span
                                        class="hide-menu">Sample - Haircare</span></a>
                        </li>
                        <li><a href="sample_almond_2022"><i class="fa-fw">A</i><span
                                        class="hide-menu">Sample - AlmondBC</span></a>
                        </li>
                        <li><a href="sample_reset5pcs_2022"><i class="fa-fw">R</i><span
                                        class="hide-menu">Sample - Reset5pcs</span></a>
                        </li>
                        <li><a href="sample_immortelleskincare_2023"><i class="fa-fw">I</i><span
                                        class="hide-menu">Sample - Immortelle</span></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
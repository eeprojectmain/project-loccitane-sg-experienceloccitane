<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['utm_source']) {
        $where_array[] = 'utm_source="' . mysqli_real_escape_string($GLOBALS["mysqli_conn"], $postfield['utm_source']) . '"';
    }

    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);

    if ($_SESSION['admin']['role'] != "1") {
        $where .= " order_id in (select pkid from order where outlet_id=" . $_SESSION['admin']['outlet_id'] . ")";
    }

    if (!empty($where)) {
        $where_query .= " and " . $where;
    }

    $resultArray = get_query_data($table['member'], "1 " . $where_query);
    while ($rs_array = $resultArray->fetchRow()) {
        if ($_SESSION['admin']['role'] == "1") {
            $all_button = $buttonClass->get_delete_button($this_folder, $rs_array['pkid']);
        }
        $dataTable['data'][] = array(
            $rs_array['order_id'],
            $rs_array['first_name'],
            $rs_array['last_name'],
            $rs_array['email'],
            $rs_array['mobile'],
            $rs_array['ic'],
            $rs_array['address'],
            $rs_array['dob'],
            $rs_array['race'],
            $rs_array['gender'],
            $rs_array['sms_status'] == 0 ? "No" : "Yes",
            $rs_array['email_status'] == 0 ? "No" : "Yes",
            $rs_array['created_date'],
            $all_button
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $query = get_query_delete($table['member'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Member Sign-Up</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Member Sign-Up</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>IC</th>
                                        <th>Address</th>
                                        <th>DOB</th>
                                        <th>Race</th>
                                        <th>Gender</th>
                                        <th>SMS PDPA</th>
                                        <th>Email PDPA</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    var table = $('#datatable').DataTable({
        "ajax": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        "order": [[0, "desc"]],
        responsive: true,
        dom: "<'row'<'col-sm-12'B>><'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>
</body>

</html>
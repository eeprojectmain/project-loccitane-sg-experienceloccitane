<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$currencyClass = new currency();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['member'], "pkid=$pkid");
$rs_array = $result->fetchRow();

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);
    unset($postfield['new_password']);

    $postfield['password'] = protect('encrypt', $_POST['password']);
    $postfield['updated_date'] = $time_config['now'];
    $postfield['updated_by'] = $_SESSION['user']['username'];

    $queryUpdate = get_query_update($table['member'], $pkid, $postfield);
    $database->query($queryUpdate);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Member <i class="fa fa-angle-right"></i> Edit</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a href="<?= $this_folder ?>">Member</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post"
                              action="<?= $this_folder ?>/edit?<?= http_build_query($_GET) ?>"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">First Name</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="name" maxlength="255"
                                                   value="<?= $rs_array['first_name'] ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Last Name</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="name" maxlength="255"
                                                   value="<?= $rs_array['last_name'] ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Email</label>
                                        <div class="col-sm-5">
                                            <input type="email" class="form-control" name="username" maxlength="255"
                                                   value="<?= $rs_array['email'] ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Contact</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="phone" maxlength="255"
                                                   value="<?= $rs_array['phone'] ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">City</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="city" maxlength="255"
                                                   value="<?= $rs_array['city'] ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Country</label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="country_id" required>
                                                <?
                                                $resultCountry = get_query_data($table['country'], "1 order by pkid asc");
                                                while ($rs_country = $resultCountry->fetchRow()) {
                                                    if ($rs_array['country_id'] == $rs_country['pkid'])
                                                        echo '<option value="' . $rs_country['pkid'] . '" selected>' . $rs_country['name'] . '</option>';
                                                    else
                                                        echo '<option value="' . $rs_country['pkid'] . '">' . $rs_country['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Created Date</label>
                                        <div class="col-sm-5">
                                            <label class="control-label"><?=$rs_array['created_date']?></label>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Password</label>
                                        <div class="col-sm-5">
                                            <input type="password" class="form-control" name="password" maxlength="255"
                                                   value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Confirm Password</label>
                                        <div class="col-sm-5">
                                            <input type="password" class="form-control" name="new_password"
                                                   maxlength="255"
                                                   value="">
                                        </div>
                                    </div>
                                    <hr>
                                    <h3>Address Book</h3>
                                    <?
                                    $resultAddress = get_query_data($table['member_address'], "member_id=$pkid");
                                    $row_address = $resultAddress->numRows();

                                    if ($row_address > 0) {
                                        ?>
                                        <div class="table-responsive">
                                            <table class="table table-hover manage-u-table" id="data-table">
                                                <thead>
                                                <tr>
                                                    <th>Address Line 1</th>
                                                    <th>Address Line 2</th>
                                                    <th>City</th>
                                                    <th>Postcode</th>
                                                    <th>Country</th>
                                                    <th>Date Added</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?
                                                while ($rs_address = $resultAddress->fetchRow()) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $rs_address['address1'] ?></td>
                                                        <td><?= $rs_address['address2'] ?></td>
                                                        <td><?= $rs_address['city'] ?></td>
                                                        <td><?= $rs_address['postcode'] ?></td>
                                                        <td><?= $rs_address['country'] ?></td>
                                                        <td><?= $rs_address['created_date'] ?></td>
                                                    </tr>
                                                <? } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <? } else {
                                        echo '<p>No address found</p>';
                                    } ?>
                                    <hr>
                                    <h3>Order History</h3>
                                    <?
                                    $resultOrder = get_query_data($table['order'], "payment_status=1 and member_id=$pkid");
                                    $row_order = $resultOrder->numRows();

                                    if ($row_order > 0) {
                                        ?>
                                        <div class="table-responsive">
                                            <table class="table table-hover manage-u-table" id="data-table">
                                                <thead>
                                                <tr>
                                                    <th>Order ID</th>
                                                    <th>Date Added</th>
                                                    <th>Status</th>
                                                    <th>Items</th>
                                                    <th>Total</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?
                                                while ($rs_order = $resultOrder->fetchRow()) {
                                                    $resultProductionOrder = get_query_data($table['production_order'], "order_id=" . $rs_order['pkid']);
                                                    $rs_productionOrder = $resultProductionOrder->fetchRow();

                                                    $product_array = explode(",", $rs_order['product_id']);

                                                    $product_image = '';
                                                    foreach ($product_array as $k => $v) {
                                                        $type = explode("-", $v)[0];
                                                        $id = explode("-", $v)[1];

                                                        if ($type == "C") {
                                                            $resultCustom = get_query_data($table['custom_result'], "pkid=$id");
                                                            $rs_custom = $resultCustom->fetchRow();

                                                            $product_image .= '<img src="../files/custom/thumbnail/' . $rs_custom['front_img_url'] . '" width="50px" style="float:left" />';
                                                            $product_image .= '<img src="../files/custom/thumbnail/' . $rs_custom['back_img_url'] . '" width="50px" />';
                                                        } elseif ($type == "D") {

                                                        }
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>#<?= $rs_order['pkid'] ?></td>
                                                        <td><?= $rs_order['created_date'] ?></td>
                                                        <td><label class="label label-<?=$order_status_class_array[$rs_productionOrder['status']]?>"><?= $order_status_array[$rs_productionOrder['status']] ?></label></td>
                                                        <td><?= $product_image ?></td>
                                                        <td><?= $_SESSION['currency'] ?> <?= $currencyClass->convert($rs_order['final_amount'], $rs_order['currency'], $_SESSION['currency']) ?></td>
                                                        <td>
                                                            <?= $buttonClass->get_edit_button("order", $rs_order['pkid']) ?>
                                                        </td>
                                                    </tr>
                                                <? } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <? } else {
                                        echo '<p>No order found</p>';
                                    } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif'],
    });
</script>
</body>

</html>
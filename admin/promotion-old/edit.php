<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['promotion'], "pkid=$pkid");
$rs_array = $result->fetchRow();

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);

    if ($postfield['type']=="1" || $postfield['type']=="3") {
        $postfield['product_id']=implode(",", $postfield['product_id']);
        $postfield['product_quantity']=implode(",", $postfield['product_quantity']);
        $postfield['product_discount']=implode(",", $postfield['product_discount']);
    }

    if ($postfield['free_gift']=="1") {
        $postfield['gift_product_id']=implode(",", $postfield['gift_product_id']);
    }

    $postfield['updated_date'] = $time_config['now'];
    $postfield['updated_by'] = $_SESSION['admin']['username'];

    $queryUpdate = get_query_update($table['promotion'], $pkid, $postfield);
    $database->query($queryUpdate);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <?php include('../pre-loader.php') ?>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('../nav.php') ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Promotion <i class="fa fa-angle-right"></i> Edit</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="dashboard">Dashboard</a></li>
                            <li><a
                                    href="<?= $this_folder ?>">Promotion</a>
                            </li>
                            <li class="active">Edit</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <form class="form-horizontal" method="post"
                                action="<?= $this_folder ?>/edit?id=<?=$pkid?>"
                                enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                    </div>
                                </div>
                                <div class="row m-t-20">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Status</label>
                                            <div class="col-sm-10">
                                                <div class="radio radio-inline radio-success">
                                                    <input type="radio" name="status" id="status_1" value="1" <?=$rs_array['status']=="1"?"checked":""?>>
                                                    <label for="status_1"> Active </label>
                                                </div>
                                                <div class="radio radio-inline radio-danger">
                                                    <input type="radio" name="status" id="status_0" value="0" <?=$rs_array['status']=="0"?"checked":""?>>
                                                    <label for="status_0"> Inactive </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Title</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="title" maxlength="255"
                                                    value="<?=$rs_array['title']?>"
                                                    required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Start Date</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control datetime" name="start_date"
                                                    value="<?=$rs_array['start_date']?>"
                                                    maxlength="255" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">End Date</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control datetime" name="end_date"
                                                    value="<?=$rs_array['end_date']?>"
                                                    maxlength="255" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Type</label>
                                            <div class="col-sm-5">
                                                <select class="form-control" name="type" required>
                                                    <option value="">---Please Select---</option>
                                                    <?php
                                                foreach ($promotion_array as $k=>$v) {
                                                    if ($k==$rs_array['type']) {
                                                        echo '<option value="'.$k.'" selected>'.$v.'</option>';
                                                    } else {
                                                        echo '<option value="'.$k.'">'.$v.'</option>';
                                                    }
                                                }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="type_product">
                                            <table class="table" style="min-width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <td>Product</td>
                                                        <td>Max Purchase Qty</td>
                                                        <td>Discount Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody class="order-table">
                                                    <?php
                                                $count=1;
                                                $product_id=explode(",", $rs_array['product_id']);
                                                $product_quantity=explode(",", $rs_array['product_quantity']);
                                                $product_discount=explode(",", $rs_array['product_discount']);

                                                foreach ($product_id as $k=>$v) {
                                                    ?>
                                                    <tr data-count="<?=$count?>">
                                                        <td>
                                                            <select class="form-control selectpicker"
                                                                name="product_id[]">
                                                                <?php
                                                        $resultProduct = get_query_data($table['product'], "1 order by title asc");
                                                    while ($rs_productList = $resultProduct->fetchRow()) {
                                                        ?>
                                                                <option <?=$v==$rs_productList['pkid']?"selected":""?>
                                                                    value="<?= $rs_productList['pkid'] ?>">
                                                                    <?= $rs_productList['title'] ?>
                                                                </option>
                                                                <?php
                                                    } ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control"
                                                                value="<?=$product_quantity[$k]?>"
                                                                name="product_quantity[]" />
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control order-price"
                                                                value="<?=$product_discount[$k]?>"
                                                                name="product_discount[]" />
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger btn-sm"
                                                                onclick="deletRow(<?=$count?>)"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $count++;
                                                }
                                            ?>
                                                </tbody>
                                            </table>
                                            <p style="margin-bottom:2em;">
                                                <button type="button" class="btn btn-success btn-add-row">Add
                                                    Product</button>
                                            </p>
                                        </div>

                                        <div id="type_cart">
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Discount Type</label>
                                                <div class="col-sm-5">
                                                    <select class="form-control" name="discount_type" required>
                                                        <option value="">---Please Select---</option>
                                                        <option value="f" <?=$rs_array['discount_type']=="f"?"selected":""?>>Fixed
                                                            Amount</option>
                                                        <option value="p" <?=$rs_array['discount_type']=="p"?"selected":""?>>Percentage
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Discount Value</label>
                                                <div class="col-sm-5">
                                                    <input type="number" class="form-control" name="discount_value"
                                                        value="<?=$rs_array['discount_value']?>"
                                                        maxlength="255" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Min Spend</label>
                                            <div class="col-sm-5">
                                                <input type="number" class="form-control" name="min_spend" maxlength="5"
                                                    value="<?=$rs_array['min_spend']?>"
                                                    required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Member Type</label>
                                            <div class="col-sm-10">
                                                <div class="radio radio-inline radio-success">
                                                    <input type="radio" name="member_only" id="member_1" value="1"
                                                        <?=$rs_array['member_only']=="1"?"checked":""?>>
                                                    <label for="member_1"> Member Only </label>
                                                </div>
                                                <div class="radio radio-inline radio-danger">
                                                    <input type="radio" name="member_only" id="member_2" value="2"
                                                        <?=$rs_array['member_only']=="2"?"checked":""?>>
                                                    <label for="member_2"> Non-member Only </label>
                                                </div>
                                                <div class="radio radio-inline radio-warning">
                                                    <input type="radio" name="member_only" id="member_0" value="0"
                                                        <?=$rs_array['member_only']=="0"?"checked":""?>>
                                                    <label for="member_0"> Both </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="div_member">
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Member Tier</label>
                                                <div class="col-sm-5">
                                                    <select class="form-control" name="member_tier" required>
                                                        <option value="">---Please Select---</option>
                                                        <option value="club" <?=$rs_array['member_tier']=="club"?"selected":""?>>Club
                                                        </option>
                                                        <option value="gold" <?=$rs_array['member_tier']=="gold"?"selected":""?>>Gold
                                                        </option>
                                                        <option value="both" <?=$rs_array['member_tier']=="both"?"selected":""?>>Both
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Free Courier Shipping</label>
                                            <div class="col-sm-10">
                                                <div class="radio radio-inline radio-success">
                                                    <input type="radio" name="free_courier" id="courier_1" value="1"
                                                        <?=$rs_array['free_courier']=="1"?"checked":""?>>
                                                    <label for="courier_1"> Yes </label>
                                                </div>
                                                <div class="radio radio-inline radio-danger">
                                                    <input type="radio" name="free_courier" id="courier_0" value="0"
                                                        <?=$rs_array['free_courier']=="0"?"checked":""?>>
                                                    <label for="courier_0"> No </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Free Lalamove Shipping</label>
                                            <div class="col-sm-10">
                                                <div class="radio radio-inline radio-success">
                                                    <input type="radio" name="free_lalamove" id="lalamove_1" value="1"
                                                        <?=$rs_array['free_lalamove']=="1"?"checked":""?>>
                                                    <label for="lalamove_1"> Yes </label>
                                                </div>
                                                <div class="radio radio-inline radio-danger">
                                                    <input type="radio" name="free_lalamove" id="lalamove_0" value="0"
                                                        <?=$rs_array['free_lalamove']=="0"?"checked":""?>>
                                                    <label for="lalamove_0"> No </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Free Gift</label>
                                            <div class="col-sm-10">
                                                <div class="radio radio-inline radio-success">
                                                    <input type="radio" name="free_gift" id="gift_1" value="1" <?=$rs_array['free_gift']=="1"?"checked":""?>>
                                                    <label for="gift_1"> Yes </label>
                                                </div>
                                                <div class="radio radio-inline radio-danger">
                                                    <input type="radio" name="free_gift" id="gift_0" value="0" <?=$rs_array['free_gift']=="0"?"checked":""?>>
                                                    <label for="gift_0"> No </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="div_gift_product">
                                            <table class="table" style="min-width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <td>Gift<br><small>(Auto add to cart when above condition matched)</small></td>
                                                    </tr>
                                                </thead>
                                                <tbody class="gift-table">
                                                    <?php
                                                $gift_count=1;
                                                $gift_product_id=explode(",", $rs_array['gift_product_id']);
                                                foreach ($gift_product_id as $v) {
                                                    ?>
                                                    <tr
                                                        data-gift-count="<?=$gift_count?>">
                                                        <td>
                                                            <select class="form-control selectpicker"
                                                                name="gift_product_id[]">
                                                                <?php
                                                        $resultProduct = get_query_data($table['product'], "1 order by title asc");
                                                    while ($rs_productList = $resultProduct->fetchRow()) {
                                                        ?>
                                                                <option <?=$v==$rs_productList['pkid']?"selected":""?>
                                                                    value="<?= $rs_productList['pkid'] ?>">
                                                                    <?= $rs_productList['title'] ?>
                                                                </option>
                                                                <?php
                                                    } ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger btn-sm"
                                                                onclick="deletGiftRow(<?=$gift_count?>)"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $gift_count++;
                                                } ?>
                                                </tbody>
                                            </table>
                                            <p style="margin-bottom:2em;">
                                                <button type="button" class="btn btn-success btn-add-gift">Add
                                                    Product</button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include('../footer.php') ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- /#wrapper -->
    <?php include('../js-script.php') ?>
    <script>
        $(document).ready(function() {
            $("#type_product,#type_cart,#div_member,#div_gift_product,#pwp_text").hide();

            <?php if ($rs_array['type']=="1") { ?>
            $("#type_product").show();
            <?php
            }

            if ($rs_array['type']=="2") {
                ?>
            $("#type_cart").show();
            <?php
            }

            if($rs_array['type']=="3"){
                ?>
            $("#pwp_text").show();
            $("#type_product").show();
            <?php
            }

            if ($rs_array['member_only']=="1") {
                ?>
            $("#div_member").show();
            <?php
            }

            if ($rs_array['free_gift']=="1") {
                ?>
            $("#div_gift_product").show();
            <?php
            }
            ?>

            var count = <?=$count?> ;
            var countGift = <?=$gift_count?>;
            var row_content = '';

            $("input[name='free_gift']").on('change', function(e) {
                if ($(this).val() == "1") {
                    $("#div_gift_product").show(500);
                } else {
                    $("#div_gift_product").hide(500);
                }
            });

            $("input[name='member_only']").on('change', function(e) {
                if ($(this).val() == "1") {
                    $("#div_member").show(500);
                } else {
                    $("#div_member").hide(500);
                }
            });

            $("select[name='type']").on('change', function(e) {
                if ($(this).val() == "1") {
                    $("#type_product").show(500);
                    $("#type_cart").hide(500);
                } else if ($(this).val() == "2") {
                    $("#type_cart").show(500);
                    $("#type_product").hide(500);
                }else if ($(this).val() == "3") {
                    $("#pwp_text").show(500);
                    $("#type_product").show(500);
                    $("#type_cart").hide(500);
                }else if($(this).val()=='4'){
                    $("#type_product").hide(500);
                    $("#type_cart").hide(500);
                }
            })

            $(".btn-add-row").on('click', function() {
                $.get('remote-view/promotion-individual-product?count=' + count, function(data) {
                    row_content = data;

                    $(".order-table").append(row_content);

                    jQuery('.select2-container').remove();
                    jQuery('.selectpicker').select2();
                    jQuery('.select2-container').css('width', '100%');

                    count++;
                });
            });

            $(".btn-add-gift").on('click', function() {
                $.get('remote-view/promotion-gift-product?count=' + countGift, function(data) {
                    row_content = data;

                    $(".gift-table").append(row_content);

                    jQuery('.select2-container').remove();
                    jQuery('.selectpicker').select2();
                    jQuery('.select2-container').css('width', '100%');

                    countGift++;
                });
            });
        });

        function deletRow(id) {
            $("tr[data-count='" + id + "']").remove();
        }

        function deletGiftRow(id) {
            $("tr[data-gift-count='" + id + "']").remove();
        }

        $("#fileinput").fileinput({
            showRemove: false,
            showUpload: false,
            showCancel: false,
            maxFileCount: 1,
            maxFileSize: 100000,
            allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif'],
        });
    </script>
</body>

</html>
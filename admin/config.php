<?php
error_reporting(0);
session_start();
date_default_timezone_set('Asia/Kuala_Lumpur');

include('environment/db.php');

$sql_details = array(
    'user' => DB_USERNAME,
    'pass' => DB_PASSWORD,
    'db' => DB_NAME,
    'host' => DB_HOST
);

$tablePrefix = 'dc_';

$table['holiday_ar'] = $tablePrefix . 'holiday_ar';
$table['sample_shea'] = $tablePrefix . 'sample_shea';
$table['sample_dyo'] = $tablePrefix . 'sample_dyo';
$table['sample_atome'] = $tablePrefix . 'sample_atome';
$table['sample_live'] = $tablePrefix . 'sample_live';
$table['sample_reset'] = $tablePrefix . 'sample_reset';
$table['sample_hair'] = $tablePrefix . 'sample_hair';
$table['sample_haircare'] = $tablePrefix . 'sample_haircare_2022';
$table['sample_almond'] = $tablePrefix . 'sample_almond_2022';
$table['sample_reset5pcs'] = $tablePrefix . 'sample_reset5pcs_2022';
$table['sample_immortelleskincare'] = $tablePrefix . 'sample_immortelleskincare_2023';
$table['sample_goldentrio'] = $tablePrefix . 'sample_goldentrio';
$table['sample_haircare_diagnosis'] = $tablePrefix . 'sample_haircarediagnosis_2023';
$table['sample_immortelle_skincare_sample'] = $tablePrefix . 'sample_immortelle_skincare_sample_2024';
$table['sample_loccitane_pullman'] = $tablePrefix . 'sample_loccitane_pullman_2024';

$table['otp'] = $tablePrefix . 'otp';
$table['promotion'] = $tablePrefix . 'promotion';
$table['sms'] = $tablePrefix . 'sms';
$table['inventory'] = $tablePrefix . 'inventory';
$table['member_voucher'] = $tablePrefix . 'member_voucher';
$table['game_voucher'] = $tablePrefix . 'game_voucher';
$table['banner'] = $tablePrefix . 'banner';
$table['campaign_product'] = $tablePrefix . 'campaign_product';
$table['campaign'] = $tablePrefix . 'campaign';
$table['lalamove'] = $tablePrefix . 'lalamove';
$table['outlet'] = $tablePrefix . 'outlet';
$table['payment'] = $tablePrefix . 'payment';
$table['order'] = $tablePrefix . 'order';
$table['cart'] = $tablePrefix . 'cart';
$table['product'] = $tablePrefix . 'product';
$table['product2'] = $tablePrefix . 'product2';
$table['product4'] = $tablePrefix . 'product4';
$table['product_category'] = $tablePrefix . 'product_category';
$table['country'] = $tablePrefix . 'country';

$table['module'] = $tablePrefix . 'module';
$table['setting'] = $tablePrefix . 'setting';
$table['tracking'] = $tablePrefix . 'tracking';
$table['user'] = $tablePrefix . 'user';
$table['user_logs'] = $tablePrefix . 'user_logs';

//include extra file
require('class/api.php');
require('class/order.php');
require('class/stock.php');
require('class/promotion.php');
require('class/email.php');
require('class/status.php');
require('class/button.php');
require('class/date.php');
require('class/login.php');
require('class/database.php');
require('include/Facebook/autoload.php');
require('include/PHPMailer-master/PHPMailerAutoload.php');
require('include/function.php');
require('include/PHPExcel/Classes/PHPExcel.php');
require('include/twilio-php-master/Twilio/autoload.php');
require('include/ssp.class.php');

//site config
$site_config['site_title'] = "L'OCCITANE EnDemande Singapore";
$site_config['admin_full_url'] = "https://" . $_SERVER['HTTP_HOST'] . "/admin/";
$site_config['full_url'] = "https://" . $_SERVER['HTTP_HOST'] . "/";
$site_config['404_url'] = $site_config['full_url'] . "404/";

$site_config['sample_shea'] = "shea-sample";
$site_config['sample_reset'] = "resetserum-sample";
$site_config['sample_dyo'] = "dyo-sample";
$site_config['sample_hair'] = "hair-sampling-live-aug";
$site_config['sample_live'] = "reset-live";
$site_config['sample_atome'] = "sample-atome";
$site_config['sample_haircare']="haircare-sample";
$site_config['sample_almond']="almondbc-sample";
$site_config['sample_reset5pcs']="reset5pcs";
$site_config['sample_immortelleskincare']="immortelleskincare_sample";
$site_config['sample_goldentrio'] = "immortelle_routine_sample";
$site_config['sample_haircare_diagnosis']="haircare_diagnosis";
$site_config['sample_immortelle_skincare_sample']="immortelle_skincare_sample";
$site_config['sample_loccitane_pullman']="loccitane_pullman";

if (preg_match("/admin/", $_SERVER['PHP_SELF'])) {
    $loginClass = new login();
    $loginClass->isLoggedIn();
}

//misc config
$green = "style=\"color:#5cb85c\"";
$red = "style=\"color:#c9302c\"";
$blue = "style=\"color:#428bca\"";
$yellow = "style=\"color:#f0ad4e\"";

$active = "<td $green><b>Active</b></td>";
$inactive = "<td $red><b>Inactive</b></td>";

$time_config['now'] = date("Y-m-d H:i:s");
$time_config['today'] = date("Y-m-d");

$user_role_array = array(1 => 'Super Admin', 2 => 'User');
$status_array = array(1 => 'Active', 2 => 'Inactive');
$month_array = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
$order_status_array = array(0 => 'Pending Payment', 1 => 'Payment Received', 2 => 'Order Received', 3 => 'Ready for pick-up', 4 => 'Shipped', 5 => 'Completed', 6 => 'Cancelled', 7 => 'Other');
$outlet_code_array = array(0 => 'S26000', 1 => 'S28000', 2 => 'S20', 3 => 'S21000', 4 => 'S15', 5 => 'S27000', 6 => 'S24000', 7 => 'S10');
$product_pwp_array = array('1070', '1284');
$product_holiday_featured_array = array('1071', '1058', '1057', '1042', '1067', '1070', '1048', '1045', '1072', '1075', '1087', '1077');
$product_price_filter_array = array(30 => 'Below $30', 50 => 'Below $50', 100 => '$50 - $100', 101 => '$100 and above');
$cprv_voucher_array = array(
    634 => 'Converted Cash Voucher',
    655 => 'Regular conversion',
    656 => '$20 off $130 nett spend Welcome to Club Voucher',
    657 => '$40 off $250 nett spend Welcome Gold Voucher',
    658 => 'Gold Thank you $40 off $250 voucher',
    651 => '10% Club Birthday Discount',
    652 => '20% Gold Birthday Discount',
    623 => 'Big Little Things $10 off $100 voucher',
    665 => 'Regular conversion $15 off $120',
    666 => 'Welcome to Club $20 off $130',
    667 => 'Welcome to Gold $40 off $250',
    707 => '2020 We Miss You - $10 off $70',
    719 => '2020 Big Little Things Eco-Tote bag - $50 Spend',
    737 => '2020 Holiday $10 off $70',
    738 => '2020 Holiday Gold $20 off $100',
    739 => '2020 Holiday $15 off $100',
    843 => 'Club Renewal Voucher – $20 off $110',
    844 => 'Gold Renewal Voucher – $40 off $250',
    845 => 'Upgrade to Gold – $40 off $250',
    980 => '$10 OFF with min. $80 nett spend',
    1038 => 'Club Renewal Voucher – $20 off $110',
    1039 => 'Gold Renewal Voucher – $40 off $250',
    1040 => 'Upgrade to Gold – $40 off $250',
);
$voucher_minspend_array = array(
    634 => '0',
    655 => '120',
    656 => '130',
    657 => '250',
    658 => '250',
    651 => '0',
    652 => '0',
    623 => '100',
    665 => '120',
    666 => '130',
    667 => '250',
    707 => '70',
    719 => '50',
    737 => '70',
    738 => '100',
    739 => '100',
    980 => '80',
);
$eco_refill_array = array(1007, 208, 207, 197, 82, 80, 78, 76, 73, 55, 46, 26, 21, 19, 17, 12, 7, 3);
$nov_promo_array = array(1015, 6, 2, 100, 39, 34, 33, 16, 11, 20, 81);
$promotion_array = array(
    1 => 'Product Discount',
    2 => 'Cart Discount',
    3 => 'PWP',
    4 => 'General',
);
$product_nov_promo = array(1015, 6, 2, 100, 39, 34, 33, 16, 11, 20, 81);
$game_discount_array = array(
    11 => array(1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1108, 1107, 1106, 1104, 1105, 1109, 1110, 1111),
    13 => array(1058, 1055, 1054, 1056, 1057, 188, 186, 1107, 1108, 1104),
    36 => array(347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 369, 370, 371, 372, 373)
);

if ($_GET['utm_source'] != "") {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
if ($_GET['utm_medium'] != "") {
    $_SESSION['utm_medium'] = $_GET['utm_medium'];
}
if ($_GET['utm_campaign'] != "") {
    $_SESSION['utm_campaign'] = $_GET['utm_campaign'];
}

if(preg_match("/shop-select/", $_SERVER['PHP_SELF']) && $_SESSION['campaign_id']!="") {
    unset($_SESSION['campaign_id']);
    unset($_SESSION['outlet_id']);
}

if ($_GET['campaign'] != "" || $_SESSION['campaign_id'] != "") {
    if ($_GET['campaign'] != "") {
        $resultCampaign = get_query_data($table['campaign'], "status=1 and code='" . $_GET['campaign'] . "'");
    } else {
        $resultCampaign = get_query_data($table['campaign'], "status=1 and pkid='" . $_SESSION['campaign_id'] . "'");
    }
    $rs_campaign = $resultCampaign->fetchRow();

    if ($rs_campaign['pkid'] != "") {
        if (strtotime($rs_campaign['start_date']) < strtotime('now') && strtotime($rs_campaign['end_date']) > strtotime('now')) {
            $_SESSION['campaign_id'] = $rs_campaign['pkid'];

            if ($rs_campaign['enable_outlet'] == "0") {
                $_SESSION['outlet_id'] = '999';
                if (preg_match("/shop-near/", $_SERVER['PHP_SELF']) || preg_match("/shop-select/", $_SERVER['PHP_SELF'])) {
                    header("Location: shop");
                    exit();
                }
            }
        } else {
            unset($_SESSION['campaign_id']);

            $resultCamProduct = get_query_data($table['campaign_product'], "campaign_id=" . $rs_campaign['pkid']);
            while ($rs_camProduct = $resultCamProduct->fetchRow()) {
                foreach ($_SESSION['cart'] as $k => $v) {
                    if ($v['product_id'] == $rs_camProduct['product_id']) {
                        unset($_SESSION['cart'][$k]);
                    }
                }
            }
        }
    }
}

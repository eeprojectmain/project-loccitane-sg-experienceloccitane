<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['order_id']);

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%"
       class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0"
                   width="100%" style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="300" style="width:300px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0"
                   style="max-width:300px;" width="100%"
                   class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                        <strong>DISCOUNT</strong>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="300" style="width:300px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0"
                   style="max-width:300px;" width="100%"
                   class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                        <div style="text-align: right;">- S$ <?=number_format($rs_order['discount_amount'],2)?></div>

                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
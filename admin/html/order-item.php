<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['order_id']);

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$product_id = explode(",", $rs_order['product_id']);
$quantity = explode(",", $rs_order['quantity']);

foreach ($product_id as $k => $v) {
    $resultProduct = get_query_data($table['product'], "pkid=$v");
    $rs_product = $resultProduct->fetchRow();
    ?>
    <tr>
        <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">
            <table border="0" cellpadding="0" cellspacing="0"
                   class="mcnCaptionRightContentOuter" width="100%">
                <tbody>
                <tr>
                    <td valign="top" class="mcnCaptionRightContentInner"
                        style="padding:0 9px ;">
                        <table align="left" border="0" cellpadding="0"
                               cellspacing="0"
                               class="mcnCaptionRightImageContentContainer"
                               width="176">
                            <tbody>
                            <tr>
                                <td class="mcnCaptionRightImageContent"
                                    align="center" valign="top">
                                    <img alt=""
                                         src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                                         width="176" style="max-width:500px;"
                                         class="mcnImage">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="mcnCaptionRightTextContentContainer"
                               align="right" border="0" cellpadding="0"
                               cellspacing="0" width="352">
                            <tbody>
                            <tr>
                                <td valign="top" class="mcnTextContent">
                                    <p><strong><?= strtoupper($rs_product['title']) ?></strong>
                                    </p>
                                    <p><?= $rs_product['size'] ?><br>
                                        x<?= $quantity[$k] ?> Qty<br>
                                        S$ <?= number_format($rs_product['price'], 2) ?></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                </tbody>
            </table>
        </td>
    </tr>
<?php } ?>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$dateClass = new date();

$this_folder = basename(__DIR__);

$month_start = $dateClass->getThisMonthStartAndEnd()['start'];
$month_end = $dateClass->getThisMonthStartAndEnd()['end'];

if (isset($_POST['submit_filter'])) {
    $date_range = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['date']);

    $month_start = date("Y-m-d", strtotime(explode(" - ", $date_range)[0]));
    $month_end = date("Y-m-d", strtotime(explode(" - ", $date_range)[1]));
}

$resultOrder = get_query_data($table['member_voucher'], "date(created_date) between date('$month_start') and date('$month_end') or date(updated_date) between date('$month_start') and date('$month_end') order by created_date asc");
while ($rs_order = $resultOrder->fetchRow()) {
    if ($rs_order['updated_date'] != "") {
        $date = substr($rs_order['updated_date'], 0, -9);
    } else {
        $date = substr($rs_order['created_date'], 0, -9);
        $voucher_game_array[$date][$rs_order['voucher_id']]++;
        if (!in_array($rs_order['voucher_id'], $voucher_type)) {
            $voucher_type[] = $rs_order['voucher_id'];
        }
    }
    if (!in_array($date, $data_array['date'])) {
        $data_array['date'][] = $date;
    }

    if ($rs_order['updated_date'] != "") {
        $data_array[$rs_order['updated_by']][$date]++;
        $total_redeem++;

        $redeem_array[substr($rs_order['updated_date'], 0, -9)][$rs_order['voucher_id']][$rs_order['updated_by']]++;
        if (!in_array($rs_order['voucher_id'], $redeem_type_array)) {
            $redeem_type_array[] = $rs_order['voucher_id'];
        }
    } else {
        $data_array['Total Issued'][$date]++;
        $total_issue++;
    }
}

asort($data_array['date']);
asort($voucher_game_array);

ksort($redeem_array);

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Game</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Game Voucher</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form action="<?= $this_folder ?>/game_voucher"
                              method="post" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Date Range</label>
                                    <input type="text" name="date" class="form-control daterange"
                                           value="<?= $month_start ?> - <?= $month_end ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left">
                                        <?= $buttonClass->get_submit_button() ?>
                                        <?php if ($postfield['submit_filter']) {
                                            echo $buttonClass->get_clear_button();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-4">
                                <div class="white-box" style="border:1px solid black;">
                                    <h3 class="box-title">Total Issued</h3>
                                    <ul class="list-inline two-part">
                                        <li style="width:auto;"><i class="fa fa-shopping-cart text-info"></i></li>
                                        <li class="text-right"><span
                                                    class="counter"><?= number_format($total_issue) ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="white-box" style="border:1px solid black;">
                                    <h3 class="box-title">Total Redeemed</h3>
                                    <ul class="list-inline two-part">
                                        <li style="width:auto;"><i class="fa fa-dollar text-warning"></i></li>
                                        <li class="text-right"><span
                                                    class="counter"><?= number_format($total_redeem) ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="white-box" style="border:1px solid black;">
                                    <h3 class="box-title">Average issued per day</h3>
                                    <ul class="list-inline two-part">
                                        <li style="width:auto;"><i class="fa fa-balance-scale text-danger"></i></li>
                                        <li class="text-right"><span
                                                    class="counter"><?= number_format($total_issue / count($data_array['date']), 2) ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>
                            <div class="col-sm-12" style="margin-top:3em;">
                                <h4>Issued Type by Date</h4>
                                <table class="table table-bordered" id="dataTable1">
                                    <thead>
                                    <tr>
                                        <td>Date</td>
                                        <?
                                        foreach ($voucher_type as $k => $v) {
                                            $resultVoucher = get_query_data($table['game_voucher'], "pkid=$v");
                                            $rs_voucher = $resultVoucher->fetchRow();

                                            echo '<td>' . $rs_voucher['title'] . '</td>';
                                        }
                                        ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?
                                    foreach ($voucher_game_array as $k => $v) {
                                        ?>
                                        <tr>
                                            <td><?= $k ?></td>
                                            <?
                                            foreach ($voucher_type as $k2 => $v2) {
                                                ?>
                                                <td><?= $voucher_game_array[$k][$v2] ?></td>
                                                <?
                                            }
                                            ?>
                                        </tr>
                                    <? } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-12" style="margin-top:3em;">
                                <h4>Redeemed Type by Date</h4>
                                <table class="table table-bordered" id="dataTable2">
                                    <thead>
                                    <tr>
                                        <td>Date</td>
                                        <?
                                        foreach ($redeem_type_array as $k => $v) {
                                            $resultVoucher = get_query_data($table['game_voucher'], "pkid=$v");
                                            $rs_voucher = $resultVoucher->fetchRow();

                                            echo "<td>" . $rs_voucher['title'] . "</td>";
                                        }
                                        ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?
                                    foreach ($redeem_array as $k => $v) {
                                        ?>
                                        <tr>
                                            <td><?= $k ?></td>
                                            <?
                                            foreach ($redeem_type_array as $k2 => $v2) {
                                                echo '<td>';
                                                foreach ($v[$v2] as $k3 => $v3) {
                                                    echo $k3 . ' : ' . $v3;
                                                    echo '<br />';
                                                }
                                                echo '</td>';
                                            }
                                            ?>
                                        </tr>
                                        <?
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script type="text/javascript">
    $('#dataTable1').DataTable({
        "order": [[0, "desc"]],
        responsive: true,
        dom: "<'row'<'col-sm-12'B>><'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $('#dataTable2').DataTable({
        "order": [[0, "desc"]],
        responsive: true,
        dom: "<'row'<'col-sm-12'B>><'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Game Voucher'
        },
        subtitle: {
            text: '<?=date("d F Y", strtotime($month_start)) . " - " . date("d F Y", strtotime($month_end))?>'
        },
        xAxis: {
            categories: [ <?php
                foreach ($data_array['date'] as $k => $v) {
                    echo "'" . date("d F", strtotime($v)) .
                        "',";
                } ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of Voucher'
            }
        },
        tooltip: {
            shared: true,
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            <?foreach($data_array as $k=>$v){
            if ($k == "date") {
                continue;
            }
            ?>
            {
                name: '<?=$k?>',
                data: [ <?php
                    foreach ($data_array[$k] as $k2 => $v2) {
                        if ($data_array[$k][$k2])
                            echo $data_array[$k][$k2] .
                                ",";
                        else
                            echo "0,";
                    } ?>
                ]
            },
            <?}?>
        ]
    });
</script>
</body>

</html>
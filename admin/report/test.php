<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$dateClass = new date();

$this_folder = basename(__DIR__);

$month_start = $dateClass->getThisMonthStartAndEnd()['start'];
$month_end = $dateClass->getThisMonthStartAndEnd()['end'];

if (isset($_POST['submit_filter'])) {
    $date_range = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['date']);

    $month_start = date("Y-m-d", strtotime(explode(" - ", $date_range)[0]));
    $month_end = date("Y-m-d", strtotime(explode(" - ", $date_range)[1]));
}

$resultOrder = get_query_data($table['order'], "date(created_date) between date('$month_start') and date('$month_end') order by created_date asc");
while ($rs_order = $resultOrder->fetchRow()) {
    $date = substr($rs_order['created_date'], 0, -9);

    $data_array[$date]['num']++;
    $data_array[$date]['value'] += ($rs_order['total_amount'] + $rs_order['shipping_amount']) - $rs_order['discount_amount'];
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Sales</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Orders</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form action="<?= $this_folder ?>/order" method="post" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Date Range</label>
                                    <input type="text" name="date" class="form-control daterange"
                                           value="<?= $month_start ?> - <?= $month_end ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left">
                                        <?= $buttonClass->get_submit_button() ?>
                                        <? if ($postfield['submit_filter']) {
                                            echo $buttonClass->get_clear_button();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script type="text/javascript">
    Highcharts.chart('container', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Average Monthly Temperature and Rainfall in Tokyo'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: [{
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            title: {
                text: 'Temperature',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Rainfall',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        series: [{
            name: 'Rainfall',
            type: 'column',
            yAxis: 1,
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],

        }, {
            name: 'Temperature',
            type: 'spline',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
        }]
    });
</script>
</body>

</html>
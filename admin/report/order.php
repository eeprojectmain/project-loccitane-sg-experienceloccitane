<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$dateClass = new date();

$this_folder = basename(__DIR__);

$month_start = $dateClass->getThisMonthStartAndEnd()['start'];
$month_end = $dateClass->getThisMonthStartAndEnd()['end'];

if (isset($_POST['submit_filter'])) {
    $date_range = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['date']);

    $month_start = date("Y-m-d", strtotime(explode(" - ", $date_range)[0]));
    $month_end = date("Y-m-d", strtotime(explode(" - ", $date_range)[1]));

    if ($_POST['game_voucher_id'] != "") {
        if ($_POST['game_voucher_id'] == "all") {
            $where_array[] = "game_voucher_id!='0'";
        } else {
            $resultVoucher = get_query_data($table['member_voucher'], "voucher_id=" . $_POST['game_voucher_id']);
            while ($rs_voucher = $resultVoucher->fetchRow()) {
                $where_voucher_array[] = $rs_voucher['pkid'];
            }
            $where_array[] = "game_voucher_id in (" . implode(",", $where_voucher_array) . ")";
        }
    }

    if ($_POST['type']) {
        $where_array[] = "type='" . $_POST['type']."'";
    }
}

$where_query = implode(" and ", $where_array);
if ($where_query != "") {
    $where_query = " and " . $where_query;
}

$resultOrder = get_query_data($table['order'], "payment_status=1 and date(created_date) between date('$month_start') and date('$month_end') $where_query order by created_date asc");
while ($rs_order = $resultOrder->fetchRow()) {
    $date = substr($rs_order['created_date'], 0, -9);

    $data_array[$date]['num']++;
    $data_array[$date]['value'] += ($rs_order['total_amount'] + $rs_order['shipping_amount']) - $rs_order['discount_amount'];

    $row_cc = get_query_data_row($table['payment'], "status=1 and order_id=" . $rs_order['pkid'] . " and method='PayDollar'");
    $row_pn = get_query_data_row($table['payment'], "status=1 and order_id=" . $rs_order['pkid'] . " and method='PayNow'");

    $data_array[$date]['cc'] += $row_cc;
    $data_array[$date]['pn'] += $row_pn;

    $total_value += ($rs_order['total_amount'] - $rs_order['discount_amount']);
    $total_order += 1;
}


?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Sales</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Orders</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form action="<?= $this_folder ?>/order"
                              method="post" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Date Range</label>
                                    <input type="text" name="date" class="form-control daterange"
                                           value="<?= $month_start ?> - <?= $month_end ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Game Voucher</label>
                                    <select name="game_voucher_id" class="form-control">
                                        <option value="">---Please Select---</option>
                                        <option value="all" <?=$_POST['game_voucher_id']=="all"?"selected":""?>>All Vouchers</option>
                                        <?
                                        $resultGame = get_query_data($table['game_voucher'], "1");
                                        while ($rs_game = $resultGame->fetchRow()) {
                                            if ($_POST['game_voucher_id'] == $rs_game['pkid']) {
                                                echo '<option value="' . $rs_game['pkid'] . '" selected>' . $rs_game['title'] . '</option>';
                                            } else {
                                                echo '<option value="' . $rs_game['pkid'] . '">' . $rs_game['title'] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Order Type</label>
                                    <select class="form-control" name="type">
                                        <option value="">---Please Select---</option>
                                        <option value="normal" <?=$_POST['type']=="normal"?"selected":""?>>Normal</option>
                                        <option value="gift" <?=$_POST['type']=="gift"?"selected":""?>>Gift</option>
<!--                                        <option value="sample" --><?//=$_POST['type']=="sample"?"selected":""?><!-->Sample</option>-->
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left">
                                        <?= $buttonClass->get_submit_button() ?>
                                        <?php if ($postfield['submit_filter']) {
                                            echo $buttonClass->get_clear_button();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-md-12">
                                <div class="white-box">
                                    <div class="row m-t-20">
                                        <div class="col-sm-4">
                                            <div class="white-box" style="border:1px solid black;">
                                                <h3 class="box-title">Total orders</h3>
                                                <ul class="list-inline two-part">
                                                    <li style="width:auto;"><i
                                                                class="fa fa-shopping-cart text-info"></i></li>
                                                    <li class="text-right"><span
                                                                class="counter"><?= number_format($total_order) ?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="white-box" style="border:1px solid black;">
                                                <h3 class="box-title">Total value (SGD)</h3>
                                                <ul class="list-inline two-part">
                                                    <li style="width:auto;"><i class="fa fa-dollar text-warning"></i>
                                                    </li>
                                                    <li class="text-right"><span
                                                                class="counter"><?= number_format($total_value, 2) ?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="white-box" style="border:1px solid black;">
                                                <h3 class="box-title">Average value per order (SGD)</h3>
                                                <ul class="list-inline two-part">
                                                    <li style="width:auto;"><i
                                                                class="fa fa-balance-scale text-danger"></i></li>
                                                    <li class="text-right"><span
                                                                class="counter"><?= number_format($total_value / $total_order, 2) ?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div id="container"
                                                 style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script type="text/javascript">
    Highcharts.chart('container', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Orders'
        },
        subtitle: {
            text: '<?=date("d F Y", strtotime($month_start)) . " - " . date("d F Y", strtotime($month_end))?>'
        },
        xAxis: {
            categories: [ <?php
                foreach ($data_array as $k => $v) {
                    echo "'" . date("d F", strtotime($k)) .
                        "',";
                } ?>
            ],
            crosshair: true
        },
        yAxis: [{ // Primary yAxis
            title: {
                text: 'No. of orders',
            }
        }, { // Secondary yAxis
            title: {
                text: 'Total sales',
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
        },
        series: [{
            name: 'No. of orders',
            type: 'column',
            data: [ <?php
                foreach ($data_array as $k => $v) {
                    if ($data_array[$k]['num'])
                        echo $data_array[$k]['num'] .
                            ",";
                    else
                        echo "0,";
                } ?>
            ]
        },
            {
                name: 'Total sales',
                type: 'spline',
                yAxis: 1,
                data: [ <?php
                    foreach ($data_array as $k => $v) {
                        if ($data_array[$k]['value'])
                            echo $data_array[$k]['value'] .
                                ",";
                        else
                            echo "0,";
                    } ?>
                ]
            },
            {
                name: 'Sales by Master/Visa',
                type: 'column',
                data: [ <?php
                    foreach ($data_array as $k => $v) {
                        if ($data_array[$k]['cc'])
                            echo $data_array[$k]['cc'] .
                                ",";
                        else
                            echo "0,";
                    } ?>
                ]
            },
            {
                name: 'Sales by PayNow',
                type: 'column',
                data: [ <?php
                    foreach ($data_array as $k => $v) {
                        if ($data_array[$k]['pn'])
                            echo $data_array[$k]['pn'] .
                                ",";
                        else
                            echo "0,";
                    } ?>
                ]
            },
        ]
    });
</script>
</body>

</html>
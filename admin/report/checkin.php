<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$dateClass = new date();

$this_folder = basename(__DIR__);

$month_start = $dateClass->getThisMonthStartAndEnd()['start'];
$month_end = $dateClass->getThisMonthStartAndEnd()['end'];

if (isset($_POST['submit_filter'])) {
    $date_range=mysqli_real_escape_string($GLOBALS["mysqli_conn"],$_POST['date']);

    $month_start=date("Y-m-d",strtotime(explode(" - ",$date_range)[0]));
    $month_end=date("Y-m-d",strtotime(explode(" - ",$date_range)[1]));
}

$resultOrder = get_query_data($table['checkin'], "date(created_date) between date('$month_start') and date('$month_end') order by created_date asc");
while ($rs_order = $resultOrder->fetchRow()) {
    $date = substr($rs_order['created_date'], 0, -9);

    $data_array[$date][$rs_order['station_id']]++;
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Check In</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Check In</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form action="<?= $this_folder ?>/checkin" method="post" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Date Range</label>
                                    <input type="text" name="date" class="form-control daterange" value="<?=$month_start?> - <?=$month_end?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left">
                                        <?= $buttonClass->get_submit_button() ?>
                                        <? if ($postfield['submit_filter']) {
                                            echo $buttonClass->get_clear_button();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script type="text/javascript">
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Check In'
        },
        subtitle: {
            text: '<?=date("d F Y",strtotime($month_start))." - ".date("d F Y",strtotime($month_end))?>'
        },
        xAxis: {
            categories: [
                <?
                foreach ($data_array as $k => $v) {
                    echo "'".date("d F",strtotime($k))."',";
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of Check In'
            }
        },
        tooltip: {
            shared: true,
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            <?foreach($station_array as $k2=>$v2){?>
            {
                name: '<?=mysqli_real_escape_string($GLOBALS["mysqli_conn"],$v2);?>',
                data: [
                    <?
                    foreach ($data_array as $k => $v) {
                        if ($data_array[$k][$k2])
                            echo $data_array[$k][$k2] . ",";
                        else
                            echo "0,";
                    }
                    ?>
                ]
            },
            <?}?>
        ]
    });
</script>
</body>

</html>
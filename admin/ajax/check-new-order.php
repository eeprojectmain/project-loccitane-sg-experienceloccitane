<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

if ($_SESSION['admin']['role']!="1") {
    $result = get_query_data($table['order'], "outlet_id=".$_SESSION['admin']['outlet_id']." and status=1 order by pkid asc");
    $row = $result->numRows();
    $rs_array=$result->fetchRow();

    if ($row > 0) {
        echo json_encode(array("print" => "true","order_id"=>$rs_array['pkid']));
    } else {
        echo json_encode(array("print" => "false"));
    }
}
exit();

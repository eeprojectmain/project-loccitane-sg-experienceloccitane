<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$product_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['product_id']);
$quantity = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['quantity']);

$resultProduct = get_query_data($table['product'], "pkid=$product_id");
$rs_product = $resultProduct->fetchRow();

if ($rs_product['type'] == "backend") {
    echo json_encode(array('price' => $rs_product['price']));
} else {
    echo json_encode(array('price' => $quantity * $rs_product['price']));
}
exit();
?>
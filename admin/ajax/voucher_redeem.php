<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['id']);

$queryUpdate = get_query_update($table['member_voucher'], $pkid, array('status' => '0', 'updated_date' => $time_config['now'], 'updated_by' => $_SESSION['admin']['username']));
$databaseClass->query($queryUpdate);

echo json_encode(array('result' => 'success', 'message' => 'successfully updated'));
exit();
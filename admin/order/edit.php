<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$stockClass = new stock();
$apiClass = new api();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['order'], "pkid=$pkid");
$rs_array = $result->fetchRow();

$mobile = str_replace("+65", "", $rs_array['mobile']);

$cprv_array = $apiClass->get_customer($mobile);
//print_r($cprv_array);

foreach ($cprv_array['vouchers'] as $k => $v) {
    if ($v['id'] == $rs_array['cprv_id']) {
        $voucher_url = $v['url'];
    }
}

if ($rs_array['campaign_id'] != "0") {
    $resultCampaign = get_query_data($table['campaign'], 'pkid=' . $rs_array['campaign_id']);
    $rs_campaign = $resultCampaign->fetchRow();

    $campaign_label = "<span class='badge badge-warning'>" . $rs_campaign['code'] . "</span>";
}


$resultOutlet = get_query_data($table['outlet'], "pkid=" . $rs_array['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

$resultLalamove = get_query_data($table['lalamove'], "order_id=$pkid order by pkid desc");
$row_lalamove = $resultLalamove->numRows();
$rs_lalamove = $resultLalamove->fetchRow();

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);
    unset($postfield['price']);

    $postfield['product_id'] = implode(",", $_POST['product_id']);
    $postfield['quantity'] = implode(",", $_POST['quantity']);
    $postfield['updated_date'] = $time_config['now'];
    $postfield['updated_by'] = $_SESSION['admin']['username'];

    $queryUpdate = get_query_update($table['order'], $pkid, $postfield);
    $database->query($queryUpdate);

    if ($rs_array['status'] != "6" && $postfield['status'] == "6") {
        $stockClass->add($rs_array['pkid']);
    }

    if ($rs_array['status'] != "2" && $postfield['status'] == "2" && $rs_array['email_status'] == "1") {
        //send email
        $content = file_get_contents("../html/order.html");
        $content = str_replace("@@name@@", $rs_array['name'], $content);
        $content = str_replace("@@order_id@@", "#LDSG20" . $rs_array['pkid'], $content);
        $content = str_replace("@@check_status_url@@", $site_config['full_url'] . 'check-order?m=' . protect('encrypt', $rs_array['mobile']), $content);
        $content = str_replace("@@sub_total@@", number_format($rs_array['total_amount'], 2), $content);
        $content = str_replace("@@shipping@@", number_format($rs_array['shipping_amount'], 2), $content);
        $content = str_replace("@@gst@@", ($rs_array['total_amount'] / 100) * 7, $content);
        $content = str_replace("@@total@@", number_format($rs_array['total_amount'] - $rs_array['discount_amount'] + $rs_array['shipping_amount'], 2), $content);
        $content = str_replace("@@item_table@@", file_get_contents($site_config['full_url'] . "admin/html/order-item?order_id=" . $rs_array['pkid']), $content);
        if ($rs_array['discount_amount'] != "") {
            $content = str_replace("@@discount_table@@", file_get_contents($site_config['full_url'] . "admin/html/discount-table?order_id=" . $rs_array['pkid']), $content);
        } else {
            $content = str_replace("@@discount_table@@", "", $content);
        }

        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->Host = "mail.experienceloccitane.com";
        $mail->SMTPAuth = true;
        $mail->Port = 587;
        $mail->Username = "no-reply@experienceloccitane.com";
        $mail->Password = "wowsome@888####!";
        $mail->SetFrom('no-reply@experienceloccitane.com', 'L\'OCCITANE Singapore');
        $mail->addAddress($rs_array['email']);
        $mail->isHTML(true);
        $mail->Subject = 'ORDER #LDSG20' . $rs_array['pkid'] . ' - L\'OCCITANE Singapore';

        $mail->Body = $content;
        $mail->send();
    }

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Order <i class="fa fa-angle-right"></i> Edit</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a
                                    href="<?= $this_folder ?>">Order</a>
                        </li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post"
                              action="<?= $this_folder ?>/edit?<?= http_build_query($_GET) ?>"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button($this_folder) . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Order ID</label>
                                        <div class="col-sm-5">
                                            <label class="control-label">#LDSG20<?= $pkid ?> <?= $campaign_label ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">BA Code</label>
                                        <div class="col-sm-5">
                                            <label class="control-label"><?= $rs_array['ba_code'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Order Type</label>
                                        <div class="col-sm-5">
                                            <label class="control-label"><?= $rs_array['type'] == "gift" ? "Gift" : "Normal" ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Order Date</label>
                                        <div class="col-sm-5">
                                            <label class="control-label"><?= $rs_array['created_date'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Order Status</label>
                                        <div class="col-sm-10">
                                            <?php
                                            foreach ($order_status_array as $k => $v) {
                                                ?>
                                                <div class="radio radio-inline">
                                                    <input type="radio" name="status"
                                                           id="status_<?= $k ?>"
                                                           value="<?= $k ?>"
                                                        <?= $rs_array['status'] == $k ? "checked" : "" ?>>
                                                    <label
                                                            for="status_<?= $k ?>">
                                                        <?= $v ?>
                                                    </label>
                                                </div>
                                                <?php
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Order Value</label>
                                        <div class="col-sm-5">
                                            <label class="control-label">S$ <?= $rs_array['total_amount'] - $rs_array['discount_amount'] + $rs_array['shipping_amount'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Type of Service</label>
                                        <div class="col-sm-5">
                                            <label class="control-label">
                                                <? if($rs_array['shipping_method'] != "courier") {
                                                    echo $rs_array['shipping_method'] == "pickup" ? "Click & Collect at " . $rs_outlet['title'] : "Delivery";
                                                } else {
                                                    echo "Courier";
                                                } ?>
                                            </label>
                                        </div>
                                    </div>
                                    <?php if ($rs_array['shipping_method'] == "pickup") { ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Pick-up Date</label>
                                            <div class="col-sm-5">
                                                <label class="control-label"><?= $rs_array['pickup_date'] ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Pick-up Time</label>
                                            <div class="col-sm-5">
                                                <label class="control-label"><?= $rs_array['pickup_time'] ?></label>
                                            </div>
                                        </div>
                                    <?php }elseif ($row_lalamove > 0) { ?>
                                        <h3>Lalamove</h3>
                                        <hr>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">ID</label>
                                            <div class="col-sm-5">
                                                <label class="control-label"><?= $rs_lalamove['lalamove_id'] ?></label>
                                                <br>
                                                <?php if ($rs_lalamove['status'] == "1") { ?>
                                                    <button type="button" class="btn btn-danger btn-lalamove-cancel"><i
                                                                class="fa fa-ban"></i> Cancel Delivery
                                                    </button>
                                                <?php } elseif ($rs_lalamove['status'] == "0" && strtotime('now') < strtotime($rs_array['pickup_date'] . ' ' . $rs_array['pickup_time'])) {
                                                    ?>
                                                    <button type="button" class="btn btn-warning btn-lalamove-request">
                                                        <i class="fa fa-motorcycle"></i> Request Again
                                                    </button>
                                                    <?php
                                                } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Status</label>
                                            <div class="col-sm-5">
                                                <label class="control-label lalamove_status"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Driver</label>
                                            <div class="col-sm-5">
                                                <label class="control-label lalamove_driver"
                                                       style="text-align: left;"></label>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                            <label class="control-label col-sm-2">Pick-up Date</label>
                                            <div class="col-sm-5">
                                                <input type="date" name="pickup_date" class="form-control"
                                                       value="<?= $rs_array['pickup_date'] ?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Delivery Time <br><small>Lalamove will pick-up 1 hour earlier</small></label>
                                            <div class="col-sm-5">
                                                <input type="text" name="pickup_time" class="form-control"
                                                       value="<?= $rs_array['pickup_time'] ?>"/>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <hr>
                                    <h3>Customer</h3>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Name</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['name'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Email</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['email'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Contact</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['mobile'] ?> <span
                                                        class="badge badge-warning"><?= $cprv_array['tier'] ?></span></label>
                                        </div>
                                    </div>
                                    <?php if ($rs_array['shipping_method'] == "delivery") { ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Address</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= nl2br($rs_array['address']) ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Unit No.</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= $rs_array['unit_no'] ?></label>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Customer Cegid Code</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $cprv_array['cid'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Call</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['call_status'] == "0" ? "No" : "Yes" ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">WhatsApp</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['whatsapp_status'] == "0" ? "No" : "Yes" ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Email</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['email_status'] == "0" ? "No" : "Yes" ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Email Marketing</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['email_marketing_status'] == "0" ? "No" : "Yes" ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">SMS Marketing</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['sms_marketing_status'] == "0" ? "No" : "Yes" ?></label>
                                        </div>
                                    </div>
                                    <?php
                                    if ($rs_array['type'] == "gift") {
                                        ?>
                                        <hr>
                                        <h3>Receiver</h3>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Receiver Name</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= $rs_array['receive_name'] ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Receiver Mobile</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= $rs_array['receive_mobile'] ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Receiver Message</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= $rs_array['receive_message'] ?></label>
                                            </div>
                                        </div>
                                        <? if ($rs_array['shipping_method'] == "delivery") { ?>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Receiver Address</label>
                                                <div class="col-sm-10">
                                                    <label class="control-label"><?= nl2br($rs_array['address']) ?></label>
                                                </div>
                                            </div>
                                        <? } ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Receiver Link</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><a target='_blank'
                                                                                class='btn btn-primary btn-sm'
                                                                                href="<?= $site_config['full_url'] . "gift-for-you?i=" . protect('encrypt', $rs_array['pkid']) ?>">Click
                                                        to view</a></label>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <hr>
                                    <h3>Order Details</h3>
                                    <table class="table" style="min-width: 100%">
                                        <thead>
                                        <tr>
                                            <td>Product</td>
                                            <td>Quantity</td>
                                            <td>Price (S$)</td>
                                        </tr>
                                        </thead>
                                        <tbody class="order-table">
                                        <?php
                                        $count = 1;
                                        $quantity_array = explode(",", $rs_array['quantity']);
                                        $product_array = explode(",", $rs_array['product_id']);
                                        $price_array = explode(",", $rs_array['price']);
                                        foreach ($product_array as $k => $v) {
                                            $resultProduct = get_query_data($table['product'], "pkid=" . $v);
                                            $rs_product = $resultProduct->fetchRow();

                                            if ($price_array[$k] != "") {
                                                $price = $price_array[$k];
                                            } else {
                                                $price = $quantity_array[$k] * $rs_product['price'];
                                            } ?>
                                            <tr
                                                    data-count="<?= $count ?>">
                                                <td>
                                                    <select class="form-control selectpicker" name="product_id[]">
                                                        <?php
                                                        $resultProduct = get_query_data($table['product'], "1 order by sort_order asc");
                                                        while ($rs_productList = $resultProduct->fetchRow()) {
                                                            ?>
                                                            <option
                                                                    value="<?= $rs_productList['pkid'] ?>"
                                                                <?= $v == $rs_productList['pkid'] ? "selected" : "" ?>><?= $rs_productList['title'] . ' ' . $rs_productList['size'] ?>
                                                            </option>
                                                            <?php
                                                        } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control selectpicker" name="quantity[]">
                                                        <?php
                                                        for ($i = 1; $i <= 10; $i++) {
                                                            ?>
                                                            <option
                                                                    value="<?= $i ?>"
                                                                <?= $quantity_array[$k] == $i ? "selected" : "" ?>><?= $i ?>
                                                            </option>
                                                            <?php
                                                        } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control order-price"
                                                           value="<?= number_format($price) ?>"
                                                           name="price[]"/>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                            onclick="deletRow(<?= $count ?>)"><i
                                                                class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                            $count++;
                                        } ?>
                                        </tbody>
                                    </table>
                                    <p>
                                        <button type="button" class="btn btn-success btn-add-row">Add Item</button>
                                    </p>
                                    <? if ($rs_array['free_giftbox'] == "1") { ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Free Paper Gift Bag</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><i
                                                            class="fa fa-warning text-danger animate__animated animate__flash animate__infinite"></i>
                                                    Yes</label>
                                            </div>
                                        </div>
                                    <? } ?>
                                    <?
                                    if ($rs_array['game_voucher_id'] != '' && $rs_array['game_voucher_id'] != '0') {
                                        $resultVoucher = get_query_data($table['member_voucher'], "pkid=" . $rs_array['game_voucher_id']);
                                        $rs_voucher = $resultVoucher->fetchRow();

                                        $resultGame = get_query_data($table['game_voucher'], "pkid=" . $rs_voucher['voucher_id']);
                                        $rs_game = $resultGame->fetchRow();
                                        ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Game Redemption</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><i
                                                            class="fa fa-warning text-danger animate__animated animate__flash animate__infinite"></i>
                                                    1x FREE <?= $rs_game['title'] ?></label>
                                            </div>
                                        </div>
                                    <? } ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Sub Total</label>
                                        <div class="col-sm-10">
                                            <label class="control-label sub-total">S$ <?= $rs_array['total_amount'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Shipping</label>
                                        <div class="col-sm-10">
                                            <label class="control-label">S$ <?= $rs_array['shipping_amount'] ?></label>
                                        </div>
                                    </div>
                                    <?
                                    $promo_ids = explode(",", $rs_array['promotion_id']);
                                    $promo_values = explode(",", $rs_array['promotion_value']);
                                    $promo_ids = array_filter($promo_ids);
                                    foreach ($promo_ids as $k => $v) {
                                        if ($promo_values[$k] == "0") {
                                            continue;
                                        }

                                        $resultPromo = get_query_data($table['promotion'], "pkid=$v");
                                        $rs_promo = $resultPromo->fetchRow();
                                        ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2"><?= $rs_promo['title'] ?></label>
                                            <div class="col-sm-10">
                                                <label class="control-label">- S$ <?= $promo_values[$k] ?></label>
                                            </div>
                                        </div>
                                        <?
                                    }
                                    if ($rs_array['cprv_id'] != "0") {
                                        ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Voucher Discount</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"> -
                                                    S$ <?= $rs_array['voucher_discount_amount'] ?></label>
                                                <br>
                                                <?php
                                                echo $cprv_voucher_array[$rs_array['cprv_id']];
                                                if ($voucher_url != "") {
                                                    echo " <a href='$voucher_url' target='_blank' class='btn btn-xs btn-success'>Redeem Voucher</a>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php }
                                    if ($rs_array['member_voucher_id'] != "0") {
                                        $resultMemberVoucher=get_query_data($table['member_voucher'],"pkid=".$rs_array['member_voucher_id']);
                                        $rs_memberVoucher=$resultMemberVoucher->fetchRow();
                                        ?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Voucher Discount</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"> -
                                                    S$ <?= $rs_array['voucher_discount_amount'] ?> <?=$rs_memberVoucher['remark']!=""?"(".$rs_memberVoucher['remark'].")":""?></label>
                                                <br>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Total</label>
                                        <div class="col-sm-10">
                                            <label class="control-label order-total">S$ <?= $rs_array['total_amount'] - $rs_array['discount_amount'] - $rs_array['promotion_discount_amount'] - $rs_array['voucher_discount_amount'] + $rs_array['shipping_amount'] ?></label>
                                        </div>
                                    </div>
                                    <hr>
                                    <h3>Payment History</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover manage-u-table" id="data-table">
                                            <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Method</th>
                                                <th>Amount</th>
                                                <th>Message</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $resultPaymentHistory = get_query_data($table['payment'], "order_id=$pkid order by pkid asc");
                                            while ($rs_paymentHistory = $resultPaymentHistory->fetchRow()) {
                                                ?>
                                                <tr>
                                                    <td><?= $rs_paymentHistory['status'] == "1" ? "Success" : "Failed" ?>
                                                    </td>
                                                    <td><?= $rs_paymentHistory['method'] ?>
                                                    </td>
                                                    <td><?= $rs_paymentHistory['amount'] ?>
                                                    </td>
                                                    <td><?= $rs_paymentHistory['remark'] ?>
                                                    </td>
                                                    <td><?= $rs_paymentHistory['created_date'] ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Last Update Date</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['updated_date'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Last Update By</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['updated_by'] ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button($this_folder) . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
</body>
<script>
    var count = <?=$count?>;
    var row_content = '';

    $(document).ready(function () {
        loadOrder();
        loadRider();

        $(".btn-lalamove-cancel").on('click', function (e) {
            swal({
                title: "Are you sure?",
                text: "Once cancel, you will need to request again / refund to customer!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: "POST",
                        url: "../ajax/lalamove-cancel-new?order_id=<?=$pkid?>",
                        dataType: 'json'
                    })
                        .done(function (data) {
                            console.log(data);
                            if (data === null || data.length == 0) {
                                swal({
                                    title: "Yay!",
                                    text: "Successfully canceled delivery",
                                    icon: "success",
                                });
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            } else {
                                swal({
                                    title: "Opps...",
                                    text: data.message,
                                    icon: "error",
                                });
                            }
                        });
                }
            });
        });

        $(".btn-lalamove-request").on('click', function (e) {
            swal({
                title: "Are you sure?",
                text: "Once request, driver will pick-up at <?=$rs_array['pickup_date']?>, <?=$rs_array['pickup_time']?>",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: "POST",
                        url: "../ajax/lalamove-quotation-renew?vktest=1&order_id=<?=$pkid?>",
                        dataType: 'json',
                        data: {order_id: <?=$pkid?>}
                    })
                        .done(function (data) {
                            console.log(data);
                            if (data.orderId) {
                                swal({
                                    title: "Yay!",
                                    text: "Delivery request submitted! Lalamove ID #" +
                                        data.orderId,
                                    icon: "success",
                                });
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            } else {
                                swal({
                                    title: "Opps...",
                                    text: data.errors[0].message,
                                    icon: "error",
                                });
                            }
                        });
                }
            });
        });
    });

    $('.selectpicker').select2();

    $(".btn-add-row").on('click', function () {
        $.get('remote-view/order-table-row?count=' + count, function (data) {
            row_content = data;

            $(".order-table").append(row_content);

            jQuery('.select2-container').remove();
            jQuery('.selectpicker').select2();
            jQuery('.select2-container').css('width', '100%');

            getProductPrice(count);
            count++;
        });
    });

    $("body").on('change', '.selectpicker', function (e) {
        var row = $(this).closest("tr").data('count');
        getProductPrice(row);
    });

    $("body").on('keyup', '.order-price', function (e) {
        getTotal();
    });

    function getProductPrice(row) {
        var product_id = $('tr[data-count="' + row + '"] td').children('select[name="product_id[]"]').val();
        var quantity = $('tr[data-count="' + row + '"] td').children('select[name="quantity[]"]').val();

        $.ajax({
            method: "POST",
            url: "ajax/get-product-price",
            dataType: 'json',
            data: {
                product_id: product_id,
                quantity: quantity
            }
        })
            .done(function (data) {
                $('tr[data-count="' + row + '"] td').children('input.order-price').val(data.price);
                getTotal()
            });
    }

    function getTotal() {
        var total = 0;
        var
            discount = <?=$rs_array['discount_amount'] != "" ? $rs_array['discount_amount'] : "0"?>;
        var
            shipping = <?=$rs_array['shipping_amount'] != "" ? $rs_array['shipping_amount'] : "0"?>;
        $(".order-price").each(function (index, element) {
            total += parseFloat($(element).val(), 10);
        });
        $(".sub-total").html("S$ " + total.toFixed(2));
        total = total - discount + shipping;
        $(".order-total").html("S$ " + total.toFixed(2));
    }

    function deletRow(id) {
        $("tr[data-count='" + id + "']").remove();
        getTotal();
    }

    function loadOrder() {
        $.ajax({
            method: "GET",
            url: "../ajax/lalamove-order-details-new?order_id=<?=$pkid?>",
            dataType: 'json',
        })
            .done(function (data) {
                $(".lalamove_status").html(data.status);
            });
    }

    function loadRider() {
        $.ajax({
            method: "POST",
            url: "../ajax/lalamove-driver-new?order_id=<?=$pkid?>",
            dataType: 'json',
        })
            .done(function (data) {
                if (data != null) {
                    $(".lalamove_driver").html("<i class='fa fa-user'></i> " + data.name +
                        "<br><i class='fa fa-phone'></i> " + data.phone +
                        "<br><i class='fa fa-motorcycle'></i> " + data.plateNumber);
                    if (data.photo != "") {
                        $(".img_pic").attr('src', data.photo);
                    }
                }
            });
    }
</script>

</html>
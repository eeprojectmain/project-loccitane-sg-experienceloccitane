<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);

    if ($postfield['type'] == "1") {
        $postfield['product_id'] = implode(",", $postfield['product_id'][1]);
        $postfield['product_quantity'] = implode(",", $postfield['product_quantity'][1]);
        $postfield['product_discount'] = implode(",", $postfield['product_discount'][1]);
    }

    if($postfield['type']=="3"){
        $postfield['product_id']=implode(",", $postfield['product_id'][3]);
        $postfield['product_quantity']=implode(",", $postfield['product_quantity'][3]);

        $postfield['pwp_product_id']=implode(",", $postfield['pwp_product_id'][3]);
        $postfield['pwp_product_quantity']=implode(",", $postfield['pwp_product_quantity'][3]);
        $postfield['pwp_product_discount']=implode(",", $postfield['pwp_product_discount'][3]);
    }

    if($postfield['type']=="5"){
        $postfield['product_id'] = implode(",", $postfield['product_id'][5]);
        $postfield['min_product_quantity'] = implode(",", $postfield['min_product_quantity'][5]);
        $postfield['max_product_quantity'] = implode(",", $postfield['max_product_quantity'][5]);
        $postfield['product_discount'] = implode(",", $postfield['product_discount'][5]);
    }

    if ($postfield['free_gift'] == "1") {
        $postfield['gift_product_id'] = implode(",", $postfield['gift_product_id']);
    }

    $postfield['created_date'] = $time_config['now'];
    $postfield['created_by'] = $_SESSION['admin']['username'];

    $queryInsert = get_query_insert($table['promotion'], $postfield);
    $database->query($queryInsert);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Promotion <i class="fa fa-angle-right"></i> New</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a
                                    href="<?= $this_folder ?>">Promotion</a>
                        </li>
                        <li class="active">New</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post"
                              action="<?= $this_folder ?>/new"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Status</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="status" id="status_1" value="1" checked>
                                                <label for="status_1"> Active </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="status" id="status_0" value="0">
                                                <label for="status_0"> Inactive </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Title</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="title" maxlength="255"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Start Date</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control datetime" name="start_date"
                                                   maxlength="255" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">End Date</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control datetime" name="end_date"
                                                   maxlength="255" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Type</label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="type" required>
                                                <option value="">---Please Select---</option>
                                                <?php
                                                foreach ($promotion_array as $k => $v) {
                                                    echo '<option value="' . $k . '">' . $v . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="pwp_type">
                                        <label class="control-label col-sm-2">PWP Type</label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="pwp_type" required>
                                                <option value="">---Please Select---</option>
                                                <option value="1">BUY Any</option>
                                                <option value="2">BUY All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="type_1">
                                        <table class="table" style="min-width: 100%;">
                                            <thead>
                                            <tr>
                                                <td>Product</td>
                                                <td>Max Purchase Qty</td>
                                                <td>Discount Amount</td>
                                            </tr>
                                            </thead>
                                            <tbody class="order-table">
                                            <tr data-count="1">
                                                <td>
                                                    <select class="form-control selectpicker"
                                                            name="product_id[1][]">
                                                        <?php
                                                        $resultProduct = get_query_data($table['product'], "1 order by title asc");
                                                        while ($rs_productList = $resultProduct->fetchRow()) {
                                                            ?>
                                                            <option
                                                                    value="<?= $rs_productList['pkid'] ?>">
                                                                <?= $rs_productList['title'] ?> | <?=$rs_productList['size']?> | S$<?=$rs_productList['price']?>
                                                            </option>
                                                            <?php
                                                        } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="5"
                                                           name="product_quantity[1][]"/>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control order-price" value=""
                                                           name="product_discount[1][]"/>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                            onclick="deletRow(1)"><i
                                                                class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style="margin-bottom:2em;">
                                            <button type="button" class="btn btn-success btn-add-row">Add
                                                Product
                                            </button>
                                        </p>
                                    </div>
                                    <div id="type_2">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Discount Type</label>
                                            <div class="col-sm-5">
                                                <select class="form-control" name="discount_type" required>
                                                    <option value="">---Please Select---</option>
                                                    <option value="f">Fixed Amount</option>
                                                    <option value="p">Percentage</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Discount Value</label>
                                            <div class="col-sm-5">
                                                <input type="number" class="form-control" name="discount_value"
                                                       maxlength="255" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="type_3">
                                        <blockquote>BUY</blockquote>
                                        <table class="table" style="min-width: 100%;">
                                            <thead>
                                            <tr>
                                                <td>Product</td>
                                                <td>Min Purchase Qty</td>
                                            </tr>
                                            </thead>
                                            <tbody class="order-table-pwp-buy">
                                            <?php
                                            $count=1;
                                            $product_id=explode(",", $rs_array['product_id']);
                                            $product_quantity=explode(",", $rs_array['product_quantity']);

                                            foreach ($product_id as $k=>$v) {
                                                ?>
                                                <tr data-count="<?=$count?>">
                                                    <td>
                                                        <select class="form-control selectpicker"
                                                                name="product_id[3][]">
                                                            <?php
                                                            $resultProduct = get_query_data($table['product'], "1 order by title asc");
                                                            while ($rs_productList = $resultProduct->fetchRow()) {
                                                                ?>
                                                                <option <?=$v==$rs_productList['pkid']?"selected":""?>
                                                                        value="<?= $rs_productList['pkid'] ?>">
                                                                    <?= $rs_productList['title'] ?> | <?=$rs_productList['size']?> | S$<?=$rs_productList['price']?>
                                                                </option>
                                                                <?php
                                                            } ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control"
                                                               value="<?=$product_quantity[$k]?>"
                                                               name="product_quantity[3][]" />
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-sm"
                                                                onclick="deletRow(<?=$count?>)"><i
                                                                    class="fa fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                                <?php
                                                $count++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <p style="margin-bottom:2em;">
                                            <button type="button" class="btn btn-success btn-add-pwp-buy">Add
                                                Product</button>
                                        </p>
                                        <blockquote>GET</blockquote>
                                        <table class="table" style="min-width: 100%;">
                                            <thead>
                                            <tr>
                                                <td>Product</td>
                                                <td>Max Purchase Qty</td>
                                                <td>Discount Amount (Each)</td>
                                            </tr>
                                            </thead>
                                            <tbody class="order-table-pwp-get">
                                            <?php
                                            $count=1;
                                            $product_id=explode(",", $rs_array['pwp_product_id']);
                                            $product_quantity=explode(",", $rs_array['pwp_product_quantity']);
                                            $product_discount=explode(",", $rs_array['pwp_product_discount']);

                                            foreach ($product_id as $k=>$v) {
                                                ?>
                                                <tr data-pwp-count="<?=$count?>">
                                                    <td>
                                                        <select class="form-control selectpicker"
                                                                name="pwp_product_id[3][]">
                                                            <?php
                                                            $resultProduct = get_query_data($table['product'], "1 order by title asc");
                                                            while ($rs_productList = $resultProduct->fetchRow()) {
                                                                ?>
                                                                <option <?=$v==$rs_productList['pkid']?"selected":""?>
                                                                        value="<?= $rs_productList['pkid'] ?>">
                                                                    <?= $rs_productList['title'] ?> | <?=$rs_productList['size']?> | S$<?=$rs_productList['price']?>
                                                                </option>
                                                                <?php
                                                            } ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control"
                                                               value="<?=$product_quantity[$k]?>"
                                                               name="pwp_product_quantity[3][]" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control order-price"
                                                               value="<?=$product_discount[$k]?>"
                                                               name="pwp_product_discount[3][]" />
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-sm"
                                                                onclick="deletRowPWP(<?=$count?>)"><i
                                                                    class="fa fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                                <?php
                                                $count++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <p style="margin-bottom:2em;">
                                            <button type="button" class="btn btn-success btn-add-pwp-get">Add
                                                Product</button>
                                        </p>
                                    </div>
                                    <div id="type_5">
                                        <table class="table" style="min-width: 100%;">
                                            <thead>
                                            <tr>
                                                <td>Product</td>
                                                <td>Min Purchase Qty</td>
                                                <td>Max Purchase Qty</td>
                                                <td>Discount Amount</td>
                                            </tr>
                                            </thead>
                                            <tbody class="order-table">
                                            <tr data-count="1">
                                                <td>
                                                    <select class="form-control selectpicker"
                                                            name="product_id[5][]">
                                                        <?php
                                                        $resultProduct = get_query_data($table['product'], "1 order by title asc");
                                                        while ($rs_productList = $resultProduct->fetchRow()) {
                                                            ?>
                                                            <option
                                                                    value="<?= $rs_productList['pkid'] ?>">
                                                                <?= $rs_productList['title'] ?> | <?=$rs_productList['size']?> | S$<?=$rs_productList['price']?>
                                                            </option>
                                                            <?php
                                                        } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="5"
                                                           name="min_product_quantity[5][]"/>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="5"
                                                           name="max_product_quantity[5][]"/>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control order-price" value=""
                                                           name="product_discount[5][]"/>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                            onclick="deletRow(1)"><i
                                                                class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style="margin-bottom:2em;">
                                            <button type="button" class="btn btn-success btn-add-row">Add
                                                Product
                                            </button>
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Special Label</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="special_label" maxlength="50"
                                                   >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Min Spend</label>
                                        <div class="col-sm-5">
                                            <input type="number" class="form-control" name="min_spend" maxlength="5"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Member Type</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="member_only" id="member_1" value="1">
                                                <label for="member_1"> Member Only </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="member_only" id="member_2" value="2">
                                                <label for="member_2"> Non-member Only </label>
                                            </div>
                                            <div class="radio radio-inline radio-warning">
                                                <input type="radio" name="member_only" id="member_0" value="0" checked>
                                                <label for="member_0"> Both </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_member">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Member Tier</label>
                                            <div class="col-sm-5">
                                                <select class="form-control" name="member_tier" required>
                                                    <option value="">---Please Select---</option>
                                                    <option value="club">Club</option>
                                                    <option value="gold">Gold</option>
                                                    <option value="both">Both</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Game Player Only</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="game_user_only" id="game_user_only_1" value="1">
                                                <label for="game_user_only_1"> Yes </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="game_user_only" id="game_user_only_0" value="0"
                                                       checked>
                                                <label for="game_user_only_0"> No </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Free Courier Shipping</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="free_courier" id="courier_1" value="1">
                                                <label for="courier_1"> Yes </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="free_courier" id="courier_0" value="0"
                                                       checked>
                                                <label for="courier_0"> No </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Free Lalamove Shipping</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="free_lalamove" id="lalamove_1" value="1">
                                                <label for="lalamove_1"> Yes </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="free_lalamove" id="lalamove_0" value="0"
                                                       checked>
                                                <label for="lalamove_0"> No </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Free Gift</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="free_gift" id="gift_1" value="1">
                                                <label for="gift_1"> Yes </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="free_gift" id="gift_0" value="0" checked>
                                                <label for="gift_0"> No </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_gift_product">
                                        <table class="table" style="min-width: 100%;">
                                            <thead>
                                            <tr>
                                                <td>Gift<br><small>(Auto add to cart when above condition
                                                        matched)</small></td>
                                            </tr>
                                            </thead>
                                            <tbody class="gift-table">
                                            <tr data-gift-count="1">
                                                <td>
                                                    <select class="form-control selectpicker"
                                                            name="gift_product_id[]">
                                                        <?php
                                                        $resultProduct = get_query_data($table['product'], "1 order by title asc");
                                                        while ($rs_productList = $resultProduct->fetchRow()) {
                                                            ?>
                                                            <option
                                                                    value="<?= $rs_productList['pkid'] ?>">
                                                                <?= $rs_productList['title'] ?>
                                                            </option>
                                                            <?php
                                                        } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                            onclick="deletGiftRow(1)"><i
                                                                class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style="margin-bottom:2em;">
                                            <button type="button" class="btn btn-success btn-add-gift">Add
                                                Product
                                            </button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    $(document).ready(function () {
        $("#type_1,#type_2,#type_3,#type_5,#div_member,#div_gift_product,#pwp_text,#pwp_type").hide();

        var count = 2;
        var countGift = 2;
        var row_content = '';

        $("input[name='free_gift']").on('change', function (e) {
            if ($(this).val() == "1") {
                $("#div_gift_product").show(500);
            } else {
                $("#div_gift_product").hide(500);
            }
        });

        $("input[name='member_only']").on('change', function (e) {
            if ($(this).val() == "1") {
                $("#div_member").show(500);
            } else {
                $("#div_member").hide(500);
            }
        });

        $("select[name='type']").on('change', function (e) {
            if ($(this).val() == "1") {
                $("#type_1").show(500);
                $("#type_2").hide(500);
                $("#type_5").hide(500);
            } else if ($(this).val() == "2") {
                $("#type_2").show(500);
                $("#type_1").hide(500);
                $("#type_5").hide(500);
            }else if ($(this).val() == "3") {
                $("#pwp_text").show(500);
                $("#pwp_type").show(500);
                $("#type_3").show(500);
                $("#type_1").hide(500);
                $("#type_2").hide(500);
                $("#type_5").hide(500);
            }else if($(this).val()=='4'){
                $("#type_1").hide(500);
                $("#type_2").hide(500);
                $("#type_5").hide(500);
            }
        })

        $(".btn-add-row").on('click', function () {
            $.get('remote-view/promotion-individual-product?count=' + count, function (data) {
                row_content = data;

                $(".order-table").append(row_content);

                jQuery('.select2-container').remove();
                jQuery('.selectpicker').select2();
                jQuery('.select2-container').css('width', '100%');

                count++;
            });
        });

        $(".btn-add-pwp-buy").on('click', function() {
            $.get('remote-view/promotion-pwp-buy-product?count=' + count, function(data) {
                row_content = data;

                $(".order-table-pwp-buy").append(row_content);

                jQuery('.select2-container').remove();
                jQuery('.selectpicker').select2();
                jQuery('.select2-container').css('width', '100%');

                count++;
            });
        });

        $(".btn-add-pwp-get").on('click', function() {
            $.get('remote-view/promotion-pwp-get-product?count=' + count, function(data) {
                row_content = data;

                $(".order-table-pwp-get").append(row_content);

                jQuery('.select2-container').remove();
                jQuery('.selectpicker').select2();
                jQuery('.select2-container').css('width', '100%');

                count++;
            });
        });

        $(".btn-add-gift").on('click', function () {
            $.get('remote-view/promotion-gift-product?count=' + countGift, function (data) {
                row_content = data;

                $(".gift-table").append(row_content);

                jQuery('.select2-container').remove();
                jQuery('.selectpicker').select2();
                jQuery('.select2-container').css('width', '100%');

                countGift++;
            });
        });
    });

    function deletRow(id) {
        $("tr[data-count='" + id + "']").remove();
    }

    function deletGiftRow(id) {
        $("tr[data-gift-count='" + id + "']").remove();
    }

    function deletRowPWP(id){
        $("tr[data-pwp-count='" + id + "']").remove();
    }

    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif'],
    });
</script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if ($_SESSION['admin']['role'] != "1" && $_SESSION['admin']['role'] != "3") {
    header("Location: /admin/order");
    exit();
}

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['machine_id']) {
        $where_array[] = "find_in_set(" . $postfield['machine_id'] . ", cast(key_id as char)) > 0";
    }

    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);

    $resultVoucher = get_query_data($table['game_voucher'], "1");
    while ($rs_voucher = $resultVoucher->fetchRow()) {
        $db_game_voucher[$rs_voucher['pkid']] = $rs_voucher['title'];
    }

    $columns = array(
        array('db' => 'pkid', 'dt' => 0, 'formatter' => function ($d, $row) {
            return "#" . $d;
        }),
        array('db' => 'status', 'dt' => 1, 'formatter' => function ($d, $row) {
            $statusClass=new status();
            
            return $statusClass->getStatusLabel($row['status']);
        }),
        array('db' => 'mobile', 'dt' => 2),
        array('db' => 'voucher_id', 'dt' => 3, 'formatter' => function ($d, $row) {
            global $db_game_voucher;
            return $db_game_voucher[$d];
        }),
        array('db' => 'ba_code', 'dt' => 4),
        array('db' => 'created_date', 'dt' => 5),
        array('db' => 'updated_date', 'dt' => 6),
        array('db' => 'updated_by', 'dt' => 7),
        array('db' => 'pkid', 'dt' => 8, 'formatter' => function ($d, $row) {
            global $this_folder;

            $buttonClass=new button();

            $all_button = "";

            if ($row['voucher_id'] == "8099" && $row['status'] == "1") {
                $all_button .= '<button type="button" class="btn btn-primary" onclick="voucher_resend(' . $row['pkid'] . ')"><i class="fa fa-paper-plane"></i> Resend</button> ';
            }

            if ($row['status'] == "1") {
                $all_button .= '<button type="button" class="btn btn-warning" onclick="voucher_redeem(' . $row['pkid'] . ')"><i class="fa fa-check-circle-o"></i> Mark as redeemed</button> ';
            }

            $all_button .= $buttonClass->get_delete_button($this_folder, $row['pkid']);

            return $all_button;
        }),
    );

    echo json_encode(
        SSP::complex($_GET, $sql_details, $table['member_voucher'], 'pkid', $columns, null, $where)
    );
    exit();
}

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $query = get_query_delete($table['member_voucher'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">User Voucher</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">User Voucher</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $buttonClass->get_new_button($this_folder) ?>
                            </div>
                        </div>
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Status</th>
                                        <th>Mobile</th>
                                        <th>Voucher</th>
                                        <th>BA Code</th>
                                        <th>Issued Date</th>
                                        <th>Redemption Date</th>
                                        <th>Redemption By</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    var table = $('#datatable').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        "order": [[0, "desc"]],
        responsive: true,
    });

    function voucher_resend(pkid) {
        swal({
            title: "Are you sure?",
            text: "This will trigger SMS to user's mobile",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "ajax/voucher_resend",
                        type: 'POST',
                        data: {id: pkid},
                        dataType: 'json',
                        success: function (data) {
                            if (data.result == "success") {
                                table.ajax.reload(null, false);
                                swal("Bravo!", "Successfully sent", {
                                    icon: "success",
                                    timer: 1000,
                                });
                            } else {
                                swal("Opps...", "Something went wrong, please try again.", {
                                    icon: "error",
                                });
                            }
                        }
                    });
                }
            }
        )
    }

    function voucher_redeem(pkid) {
        swal({
            title: "Are you sure?",
            text: "This will mark the voucher to redeemed",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "ajax/voucher_redeem",
                        type: 'POST',
                        data: {id: pkid},
                        dataType: 'json',
                        success: function (data) {
                            if (data.result == "success") {
                                table.ajax.reload(null, false);
                                swal("Bravo!", "Successfully marked as redeemed", {
                                    icon: "success",
                                    timer: 1000,
                                });
                            } else {
                                swal("Opps...", "Something went wrong, please try again.", {
                                    icon: "error",
                                });
                            }
                        }
                    });
                }
            }
        )
    }
</script>
</body>

</html>
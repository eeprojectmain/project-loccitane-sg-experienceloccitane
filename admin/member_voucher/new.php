<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);

    $postfield['from_backend']="1";
    $postfield['created_date'] = $time_config['now'];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $site_config['full_url'] . "api/put_voucher");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfield);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);
    $server_output = json_decode($server_output, true);

    curl_close($ch);

    $swal['title'] = $server_output['result'];
    $swal['msg'] = $server_output['message'];
    $swal['type'] = $server_output['result'];
    if ($server_output['result'] == "success") {
        $swal['url'] = "$this_folder/index";
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">User Voucher <i class="fa fa-angle-right"></i> New</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a href="<?= $this_folder ?>">User Voucher</a></li>
                        <li class="active">New</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post" action="<?= $this_folder ?>/new"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Status</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="status" id="status_1" value="1" checked>
                                                <label for="status_1"> Active </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="status" id="status_0" value="0">
                                                <label for="status_0"> Inactive </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Mobile</label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">+60</span>
                                                <input type="number" class="form-control" name="mobile" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Voucher</label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="voucher_id" required>
                                                <option value="">---Please Select---</option>
                                                <?
                                                $resultVoucher = get_query_data($table['game_voucher'], "1");
                                                while ($rs_voucher = $resultVoucher->fetchRow()) {
                                                    echo '<option value="' . $rs_voucher['pkid'] . '">' . $rs_voucher['title'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif'],
    });
</script>
</body>

</html>
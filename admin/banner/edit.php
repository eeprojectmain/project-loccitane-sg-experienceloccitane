<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['banner'], "pkid=$pkid");
$rs_array = $result->fetchRow();

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);

    if ($_FILES['img_url']['tmp_name']!="") {
        $path = $_FILES['img_url']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $file_name=uniqid().".".$ext;

        move_uploaded_file($_FILES['img_url']['tmp_name'], "../../assets/banner/$file_name");
        $postfield['img_url']=$file_name;
    }

    if ($_FILES['mobile_img_url']['tmp_name']!="") {
        $path = $_FILES['mobile_img_url']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $file_name=uniqid().".".$ext;

        move_uploaded_file($_FILES['mobile_img_url']['tmp_name'], "../../assets/banner/$file_name");
        $postfield['mobile_img_url']=$file_name;
    }

    $postfield['updated_date'] = $time_config['now'];
    $postfield['updated_by'] = $_SESSION['admin']['username'];

    $queryUpdate = get_query_update($table['banner'], $pkid, $postfield);
    $database->query($queryUpdate);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <?php include('../pre-loader.php') ?>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('../nav.php') ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Banner <i class="fa fa-angle-right"></i> Edit</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="dashboard">Dashboard</a></li>
                            <li><a href="<?= $this_folder ?>">Banner</a>
                            </li>
                            <li class="active">Edit</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <form class="form-horizontal" method="post"
                                action="<?= $this_folder ?>/edit?<?=http_build_query($_GET)?>"
                                enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                    </div>
                                </div>
                                <div class="row m-t-20">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Status</label>
                                            <div class="col-sm-10">
                                                <div class="radio radio-inline radio-success">
                                                    <input type="radio" name="status" id="status_1" value="1"
                                                        <?= $rs_array['status'] == "1" ? "checked" : "" ?>>
                                                    <label for="status_1"> Active </label>
                                                </div>
                                                <div class="radio radio-inline radio-danger">
                                                    <input type="radio" name="status" id="status_0" value="0"
                                                        <?= $rs_array['status'] == "0" ? "checked" : "" ?>>
                                                    <label for="status_0"> Inactive </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Hidden Campaign</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" name='campaign_id'>
                                                    <option value=''>---Please Select---</option>
                                                    <?php
                                                    $resultCampaign=get_query_data($table['campaign'], "1 order by pkid desc");
                                                    while($rs_campaign=$resultCampaign->fetchRow()){
                                                        if($rs_array['campaign_id']==$rs_campaign['pkid']){
                                                            $selected='selected';
                                                        }else{
                                                            $selected='';
                                                        }
                                                        echo '<option value="'.$rs_campaign['pkid'].'" '.$selected.'>'.$rs_campaign['code'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Image (Desktop)</label>
                                            <div class="col-sm-6">
                                                <input type="file" id="fileinput" name="img_url">
                                                <?php
                                                if ($rs_array['img_url']) {
                                                    if (!preg_match("/mp4/", $rs_array['img_url'])) {
                                                        ?>
                                                <img src="../assets/banner/<?=$rs_array['img_url']?>" width="150px" />
                                                <?php
                                                    }else{
                                                    ?>
                                                <video width='300px' autoplay loop muted>
                                                    <source src='../assets/banner/<?=$rs_array['img_url']?>'
                                                        type='video/mp4'>
                                                </video>
                                                <?php
                                                }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Image (Mobile)</label>
                                            <div class="col-sm-6">
                                                <input type="file" id="fileinput2" name="mobile_img_url">
                                                <?php
                                                if ($rs_array['img_url']) {
                                                    if (!preg_match("/mp4/", $rs_array['mobile_img_url'])) {
                                                        ?>
                                                <img src="../assets/banner/<?=$rs_array['mobile_img_url']?>" width="150px" />
                                                <?php
                                                    } else {
                                                        ?>
                                                <video width='300px' autoplay loop muted>
                                                    <source src='../assets/banner/<?=$rs_array['mobile_img_url']?>'
                                                        type='video/mp4'>
                                                </video>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Text</label>
                                            <div class="col-sm-6">
                                                <textarea id="editor" name="text"><?=$rs_array['text']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Start Date</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="start_date" class="form-control date" required value='<?=$rs_array['start_date']?>'>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">End Date</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="end_date" class="form-control date" required value='<?=$rs_array['end_date']?>'>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Display Order</label>
                                            <div class="col-sm-6">
                                                <input type="number" name="sort_order" class="form-control"
                                                    value="<?=$rs_array['sort_order']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include('../footer.php') ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- /#wrapper -->
    <?php include('../js-script.php') ?>
    <script>
    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif', 'mp4'],
        initialPreview: []
    });

    $("#fileinput2").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif', 'mp4'],
    });
    </script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();

$dateClass = new date();
$start_date = $dateClass->getWeekStartAndEndDate(date("W"), date("Y"))['week_start'];
$end_date = $dateClass->getWeekStartAndEndDate(date("W"), date("Y"))['week_end'];

$month_start=$dateClass->getThisMonthStartAndEnd()['start'];
$month_end=$dateClass->getThisMonthStartAndEnd()['end'];
?>

<!DOCTYPE html>
<html lang="en">

<?php include('head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <?php include('nav.php') ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- ============================================================== -->
            <!-- Other sales widgets -->
            <!-- ============================================================== -->
            <!-- .row -->
            <div class="row">
                <div class="col-lg-3 col-sm-6 col-xs-12 hidden">
                    <div class="white-box">
                        <h3 class="box-title">sign up</h3>
                        <p class="box-date"><?= date("d M", strtotime($month_start)) . " - " . date("d M", strtotime($month_end)) ?></p>
                        <ul class="list-inline two-part">
                            <li><i class="fa fa-users text-info"></i></li>
                            <?
                            $resultSignup=get_query_data($table['member'],"date(created_date) between date('$month_start') and date('$month_end')");
                            $row_signup=$resultSignup->numRows();
                            ?>
                            <li class="text-right"><span class="counter"><?=number_format($row_signup)?></span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12 hidden">
                    <div class="white-box">
                        <h3 class="box-title">unique visitors</h3>
                        <p class="box-date"><?= date("d M", strtotime($month_start)) . " - " . date("d M", strtotime($month_end)) ?></p>
                        <ul class="list-inline two-part">
                            <li><i class="fa fa-user text-warning"></i></li>
                            <?
                            $resultSignup=get_query_data($table['member'],"pkid in (select member_id from dc_checkin group by member_id)");
                            $row_signup=$resultSignup->numRows();
                            ?>
                            <li class="text-right"><span class="counter"><?=number_format($row_signup)?></span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12 hidden">
                    <div class="white-box">
                        <h3 class="box-title">check in</h3>
                        <p class="box-date"><?= date("d M", strtotime($month_start)) . " - " . date("d M", strtotime($month_end)) ?></p>
                        <ul class="list-inline two-part">
                            <li><i class="fa-check-circle-o fa text-success"></i></li>
                            <?
                            $resultSignup=get_query_data($table['checkin'],"date(created_date) between date('$month_start') and date('$month_end')");
                            $row_signup=$resultSignup->numRows();
                            ?>
                            <li class="text-right"><span class="counter"><?=number_format($row_signup)?></span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">today's order</h3>
                        <p class="box-date"><?= $time_config['today'] ?></p>
                        <ul class="list-inline two-part">
                            <li><i class="fa-shopping-cart fa text-purple"></i></li>
                            <?
                            if ($_SESSION['admin']['role']=="1") {
                                $resultSignup=get_query_data($table['order'], "payment_status=1 and date(created_date) like '%".$time_config['today']."%'");
                            }else{
                                $resultSignup=get_query_data($table['order'], "payment_status=1 and date(created_date) like '%".$time_config['today']."%' and outlet_id=".$_SESSION['admin']['outlet_id']);
                            }
                                $row_signup=$resultSignup->numRows();
                            ?>
                            <li class="text-right"><span class="counter"><?=number_format($row_signup)?></span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-xs-12 hidden">
                    <div class="white-box">
                        <h3 class="box-title">share</h3>
                        <p class="box-date"><?= date("d M", strtotime($month_start)) . " - " . date("d M", strtotime($month_end)) ?></p>
                        <ul class="list-inline two-part">
                            <li><i class="fa-share fa text-danger"></i></li>
                            <?
                            $row_share=get_query_data_row($table['share'],"date(created_date) between date('$month_start') and date('$month_end')")
                            ?>
                            <li class="text-right"><span class="counter"><?=number_format($row_share)?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr>
            <!-- /.row -->
            <!-- ============================================================== -->
        </div>
        <!-- /.container-fluid -->
        <?php include('footer.php') ?>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php include('js-script.php') ?>
<script src="js/dashboard3.js"></script>

</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$count = $_GET['count'];
?>
<tr data-count="<?= $count ?>">
    <td>
        <select class="form-control selectpicker"
                name="pwp_product_id[3][]">
            <?php
            $resultProduct = get_query_data($table['product'], "1 order by title asc");
            while ($rs_productList = $resultProduct->fetchRow()) {
                ?>
                <option value="<?= $rs_productList['pkid'] ?>"><?= $rs_productList['title'] ?> | <?=$rs_productList['size']?> | S$<?=$rs_productList['price']?></option>
                <?php
            }
            ?>
        </select>
    </td>
    <td>
        <input class="form-control" name="pwp_product_quantity[3][]" value="5">
    </td>
    <td>
        <input class="form-control" name="pwp_product_discount[3][]" value="">
    </td>
    <td>
        <button type="button" class="btn btn-danger btn-sm" onclick="deletRowPWP(<?= $count ?>)"><i
                class="fa fa-trash"></i></button>
    </td>
</tr>

<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$count = $_GET['count'];
?>
<tr data-count="<?= $count ?>">
    <td>
        <select class="form-control selectpicker"
                name="product_id[]">
            <?
            $resultProduct = get_query_data($table['product'], "1 order by sort_order asc");
            while ($rs_productList = $resultProduct->fetchRow()) {
                ?>
                <option value="<?= $rs_productList['pkid'] ?>"><?= $rs_productList['title'].' '.$rs_productList['size'] ?></option>
                <?
            }
            ?>
        </select>
    </td>
    <td>
        <select class="form-control selectpicker" name="quantity[]">
            <?
            for ($i = 1; $i <= 10; $i++) {
                ?>
                <option value="<?= $i ?>"><?= $i ?></option>
            <? } ?>
        </select>
    </td>
    <td>
        <input class="form-control order-price" name="price[]" value="<?= number_format($rs_productList['price']) ?>">
    </td>
    <td>
        <button type="button" class="btn btn-danger btn-sm" onclick="deletRow(<?= $count ?>)"><i
                    class="fa fa-trash"></i></button>
    </td>
</tr>

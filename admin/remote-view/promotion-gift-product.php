<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$count = $_GET['count'];
?>
<tr data-gift-count="<?= $count ?>">
    <td>
        <select class="form-control selectpicker"
                name="gift_product_id[]">
            <?php
            $resultProduct = get_query_data($table['product'], "1 order by title asc");
            while ($rs_productList = $resultProduct->fetchRow()) {
                ?>
                <option value="<?= $rs_productList['pkid'] ?>"><?= $rs_productList['title'] ?> | <?=$rs_productList['size']?> | S$<?=$rs_productList['price']?></option>
                <?php
            }
            ?>
        </select>
    </td>
    <td>
        <button type="button" class="btn btn-danger btn-sm" onclick="deletGiftRow(<?= $count ?>)"><i
                    class="fa fa-trash"></i></button>
    </td>
</tr>

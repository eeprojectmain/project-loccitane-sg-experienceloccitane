<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['inventory'], "pkid=$pkid");
$rs_array = $result->fetchRow();

$resultOutlet=get_query_data($table['outlet'],"pkid=".$rs_array['outlet_id']);
$rs_outlet=$resultOutlet->fetchRow();

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);

    $postfield['updated_date'] = $time_config['now'];

    $queryUpdate = get_query_update($table['inventory'], $pkid, $postfield);
    $database->query($queryUpdate);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <?php include('../pre-loader.php') ?>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('../nav.php') ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Inventory <i class="fa fa-angle-right"></i> Edit</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="dashboard">Dashboard</a></li>
                            <li><a href="<?= $this_folder ?>">Inventory</a>
                            </li>
                            <li class="active">Edit</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <form class="form-horizontal" method="post"
                                action="<?= $this_folder ?>/edit?<?=http_build_query($_GET)?>"
                                enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                    </div>
                                </div>
                                <div class="row m-t-20">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Outlet</label>
                                            <div class="col-sm-6">
                                                <label class="control-label"><?=$rs_outlet['title']?></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Product</label>
                                            <div class="col-sm-6">
                                                <select class="form-control selectpicker" disabled name='product_id'>
                                                    <option value=''>---Please Select---</option>
                                                    <?php
                                                    $resultProduct=get_query_data($table['product'], "1 order by pkid desc");
                                                    while($rs_product=$resultProduct->fetchRow()){
                                                        if($rs_array['product_id']==$rs_product['pkid']){
                                                            $selected='selected';
                                                        }else{
                                                            $selected='';
                                                        }
                                                        echo '<option value="'.$rs_product['pkid'].'" '.$selected.'>'.$rs_product['title'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Quantity</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="quantity" class="form-control" required value='<?=$rs_array['quantity']?>'>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Last Update Date</label>
                                            <div class="col-sm-6">
                                                <label class="control-label"><?=$rs_array['updated_date']?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include('../footer.php') ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- /#wrapper -->
    <?php include('../js-script.php') ?>
    <script>
    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif', 'mp4'],
        initialPreview: []
    });

    $("#fileinput2").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png', 'jpg', 'jpeg', 'gif', 'mp4'],
    });
    </script>
</body>

</html>
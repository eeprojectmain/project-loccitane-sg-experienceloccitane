<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['outlet_id']) {
        $where_array[] = "outlet_id=" . $postfield['outlet_id'];
    }

    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);

    if (!empty($where)) {
        $where_query .= " and " . $where;
    }

    if ($_SESSION['admin']['role'] != "1") {
        $where_query .= " and outlet_id=" . $_SESSION['admin']['outlet_id'];
    }

    $resultOutlet = get_query_data($table['outlet'], "1 order by pkid asc");
    while ($rs_outlet = $resultOutlet->fetchRow()) {
        $outlet_array[$rs_outlet['pkid']] = $rs_outlet['title'];
    }

    $resultProduct = get_query_data($table['product'], "1 order by pkid asc");
    while ($rs_product = $resultProduct->fetchRow()) {
        $product_array[$rs_product['pkid']] = $rs_product['title'];
    }

    $resultArray = get_query_data($table['inventory'], "1" . $where_query);
    while ($rs_array = $resultArray->fetchRow()) {
        $dataTable['data'][] = array(
            $rs_array['pkid'],
            $outlet_array[$rs_array['outlet_id']],
            $product_array[$rs_array['product_id']],
            $rs_array['product_code'],
            $rs_array['quantity'],
            $rs_array['updated_date'],
            $buttonClass->get_edit_button($this_folder, $rs_array['pkid'])
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $query = get_query_delete($table['inventory'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Inventory</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Inventory</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <?
                if($_SESSION['admin']['role']=="1"){
                ?>
                <div class="col-md-12">
                    <div class="white-box">
                        <form action="<?= $this_folder ?>/index" method="post" class="form-vertical">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Outlet</label>
                                    <select class="form-control" name="outlet_id">
                                        <option value="">---Please Select---</option>
                                        <?
                                        $resultOutlet = get_query_data($table['outlet'], "1 order by sort_order asc");
                                        while ($rs_outlet = $resultOutlet->fetchRow()) {
                                            if ($rs_outlet['pkid'] == $postfield['outlet_id']) {
                                                echo "<option value='" . $rs_outlet['pkid'] . "' selected>" . $rs_outlet['title'] . "</option>";
                                            } else {
                                                echo "<option value='" . $rs_outlet['pkid'] . "'>" . $rs_outlet['title'] . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="margin-left:1em;margin-top:1em;">
                                    <div class="pull-left">
                                        <?= $buttonClass->get_submit_button() ?>
                                        <? if ($postfield['submit_filter']) {
                                            echo $buttonClass->get_clear_button();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?}?>
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Outlet</th>
                                        <th>Product</th>
                                        <th>Code</th>
                                        <th>Qty</th>
                                        <th>Last Update</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    var table = $('#datatable').DataTable({
        "ajax": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        "order": [[0, "desc"]],
        responsive: true,
    });
</script>
</body>

</html>
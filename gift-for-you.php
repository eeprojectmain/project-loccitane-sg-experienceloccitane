<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id=$_GET['i'];
$order_id=protect('decrypt', $order_id);

$resultOrder=get_query_data($table['order'], "pkid=$order_id");
$rs_order=$resultOrder->fetchRow();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg bg-light bg-xmas">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h5 class="text-center w-100 mt-5">Bonjour <?=$rs_order['receive_name']?>!<br><?=$rs_order['name']?> has
                    bought a gift for you!</h5>
                <?php if ($rs_order['receive_message']!="") { ?>
                <h5 class="text-center p-4 m-4 mt-3 card-bg w-80 mx-auto"><?=nl2br($rs_order['receive_message'])?><br><br><span
                        class="float-right">- <?=$rs_order['name']?></span><br></h5>
                <?php } ?>
            </div>
        </div>
        <div class="row row-gift">
            <div class="col-12">
                <img src="assets/img/Receiver.gif" class="img-fluid d-block mx-auto" />
            </div>
        </div>
        <div class="row row-gift-button">
            <div class="col-12 text-center">
                <p>Redeem your gift!</p>
                <a href="gift-redeem?i=<?=$_GET['i']?>" class="btn btn-red">NEXT</a>
            </div>
        </div>
    </div>

    <?php include('footer.php') ?>
    <?php include('js-script.php') ?>
    <script>
    $(".row-gift-button").hide(500);

    $(window).on('load', function(e) {
        setTimeout(() => {
            $(".row-gift-button").show(500);
        }, 2000);
    });
    </script>
</body>

</html>
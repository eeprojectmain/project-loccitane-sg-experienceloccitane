<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

//$row_check = get_query_data_row($table['sample_live'], "date(created_date) >= date('2021-08-20')");

if (strtotime('now') > strtotime('2021-08-20 23:59:59')) {
    $row_check = 999999;
}

if ($_GET['vk'] == '1') {
    $row_check = 0;
}

if ($_SESSION['sample_live']['done'] != "") {
    header("Location: " . $site_config['sample_live']);
    exit();
}

if ($_POST) {
    $postfield = $_POST;

    $mobile = str_replace("-", "", $postfield['mobile']);
    $mobile = str_replace(" ", "", $mobile);

    $mobile = "+65" . $mobile;
    $_SESSION['iglive']['mobile'] = $mobile;

    $resultOtp = get_query_data($table['otp'], "mobile='$mobile' order by pkid desc limit 1");
    $rs_otp = $resultOtp->fetchRow();

    $to_time = strtotime("now");
    $from_time = strtotime($rs_otp['created_date']);

    if (round(abs($to_time - $from_time) / 60, 2) >= 1) {
        $otp = rand('100', '999');

        $content = "L'OCCITANE: OTP code: $otp. NEVER share this code with others.";

        $ch = curl_init();
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
        );
        curl_setopt($ch, CURLOPT_URL, "http://www.etracker.cc/bulksms/mesapi.aspx?user=davino&pass=Wowsome%40820%23%23%23%23%21&type=0&to=$mobile&from=EnDemande&text=" . urlencode($content) . "&servid=MES01&title=EnDemande_SG_SampleReset");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $server_respond = curl_exec($ch);
        curl_close($ch);

        $postfield = array(
            'status' => '0',
            'type' => 'sample_live',
            'mobile' => $mobile,
            'otp' => $otp,
            'created_date' => $time_config['now'],
        );

        $queryInsert = get_query_insert($table['otp'], $postfield);
        $databaseClass->query($queryInsert);

        $_SESSION['iglive']['otp_request'] = "true";

        header("Location: " . $site_config['sample_live'] . "-otp");
        exit();
    } else {
        $swal['title'] = 'Opps...';
        $swal['msg'] = 'You\'ve just request an OTP, please try again later.';
        $swal['icon'] = 'error';
    }
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">IMMORTELLE SKINCARE SAMPLING KIT</h4>
                <div class="col-12">
                    <img src="assets/img/reset_sample_kit.png" class="img-fluid"/>
                </div>
                <p>Pamper yourself with our Skincare Sample Kit featuring our best selling Immortelle Range!<br>
                    Sign up to redeem your free sample kit today.</p>
            </div>
        </div>
    </div>
    <? if ($row_check < 300) { ?>
        <div class="col-12 mt-4">
            <label class="w-100 text-center">Please fill in your mobile number.</label>
            <form action="<?= $site_config['sample_live'] ?>" method="post" class="w-80 mx-auto formIglive">
                <div class="form-group">
                    <input type="tel" class="form-control" placeholder="MOBILE NUMBER" name="mobile"
                           required
                           minlength="8" maxlength="9">
                </div>
                <div class="form-group mt-5 pb-5 text-center">
                    <button type="submit" name="submit_mobile" value="true" class="btn btn-darkblue w-50">NEXT
                    </button>
                </div>
            </form>
        </div>
    <? } else { ?>
        <div class="col-12 mt-4 text-center">
            <p>Thank you for your interest. Sample Kit have been fully redeemed.</p>
        </div>
    <? } ?>
</div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(".formIglive").each(function (index, element) {
        form = $(".formIglive")[index];
        fv = FormValidation.formValidation(
            form, {
                fields: {
                    mobile: {
                        message: 'This field is required',
                        validators: {
                            remote: {
                                message: 'This mobile number already sign-up',
                                url: '/ajax/sample-mobile-check',
                                method: 'POST',
                            }
                        }
                    }
                },
                plugins: {
                    declarative: new FormValidation.plugins.Declarative({
                        html5Input: true,
                    }),
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    excluded: new FormValidation.plugins.Excluded(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    icon: new FormValidation.plugins.Icon({
                        valid: 'fal fa-check',
                        invalid: 'fal fa-times',
                        validating: 'fal fa-refresh'
                    }),
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                },
            }
        ).on('core.form.valid', function () {
            $("button[type='submit']").attr('disabled', 'disabled');
        });
    });
</script>
</body>
</html>
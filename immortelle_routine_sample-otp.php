<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

/*$row_check = get_query_data_row($table['sample_goldentrio'], "date(created_date) >= date('2021-08-19')");
if ($row_check >= 300) {
    header("Location: " . $site_config['sample_goldentrio']);
    exit();
}*/

if ($_SESSION['iglive']['mobile'] == "" || $_SESSION['iglive']['otp_request'] == "") {
    header("Location: " . $site_config['sample_goldentrio']);
    exit();
}

if ($_POST) {
    header("Location: " . $site_config['sample_goldentrio'] . "-details");
    exit();
}

$mobile = $_SESSION['iglive']['mobile'];

$resultOtp = get_query_data($table['otp'], "mobile='$mobile' order by pkid desc limit 1");
$rs_otp = $resultOtp->fetchRow();

?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="my-5">
        <div class="d-flex justify-content-center mt-3">
            <div class="col-12 col-lg-6" style="line-height:1.2em;">
                <div class="title">
                    <h4 class="w-100 mb-3">Immortelle Skin Care Sample Kit</h4>
                    <p>Discover the secret to stronger, firmer skin, and unleash a captivating skin radiance today!
                        Sign up to redeem your free sample kit today.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="my-5">
        <div class="d-flex justify-content-center mt-3">
            <div class="col-12 col-lg-6">
                <label class="w-100 text-center" style="line-height:1.2em; font-size: 14px;">An SMS verification code has been sent to your mobile. Please key in the code to proceed: </label>
                <form action="<?= $site_config['sample_goldentrio'] ?>-otp" method="post" class="w-80 mx-auto formIglive">
                    <div class="form-group">
                        <input type="number" class="form-control" placeholder="SMS verification code" name="otp_code"
                            required
                            minlength="3" maxlength="3">
                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-blue btn-resend">Resend Code <span
                                    id="timer"></span></button>
                    </div>
                    <div class="form-group mt-5 pb-5 text-center">
                        <button type="submit" name="submit_mobile" value="true" class="btn btn-yellow w-80">Next <i class="me-3 fa fa-arrow-right"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(document).ready(function () {
        var time = '<?=date('Y-m-d\TH:i:s', strtotime($rs_otp['created_date'] . ' +60 sec'));?>';
        var countDownDate = new Date(time).getTime();

        $(".btn-resend").attr('disabled', 'disabled');

        var x = setInterval(function () {
            var now = new Date().getTime();

            var timeCD = countDownDate - now;
            timeCD = parseInt(timeCD) || 0;
            var seconds = moment.duration(timeCD).seconds();

            if (timeCD <= 1) {
                clearInterval(x);
                $(".btn-resend").attr('disabled', false);
                $("#timer").hide();
            } else {
                $("#timer").text("(" + seconds + ")");
            }
        }, 1000);

        $(".btn-resend").on('click', function (e) {
            $.ajax({
                method: "POST",
                url: "ajax/iglive-otp-resend",
                data: {'type': 'goldentrio'},
                dataType: "json",
            })
                .done(function (data) {
                    Swal.fire({
                        title: data.title,
                        text: data.message,
                        icon: data.result,
                    });
                });
        });

        $(".formIglive").each(function (index, element) {
            form = $(".formIglive")[index];
            fv = FormValidation.formValidation(
                form, {
                    fields: {
                        otp_code: {
                            message: 'This field is required',
                            validators: {
                                remote: {
                                    message: 'Incorrect code',
                                    url: '/ajax/iglive-otp-check',
                                    method: 'POST',
                                }
                            }
                        }
                    },
                    plugins: {
                        declarative: new FormValidation.plugins.Declarative({
                            html5Input: true,
                        }),
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        excluded: new FormValidation.plugins.Excluded(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fal fa-check',
                            invalid: 'fal fa-times',
                            validating: 'fal fa-refresh'
                        }),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                    },
                }
            ).on('core.form.valid', function () {
                $("button[type='submit']").attr('disabled', 'disabled');
            });
        });
    });
</script>
</body>
</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$promotionClass = new promotion();
$stockClass = new stock();
$orderClass = new order();

$nav_step = '2';

$_SESSION['member']['mobile']=$_GET['mobile'];

//inventory check
$ofs_product = $stockClass->check_summary();

$ofs_product = array_filter($ofs_product);
if (count($ofs_product) > 0) {
    foreach ($ofs_product as $v) {
        $resultProduct = get_query_data($table['product'], "pkid=$v");
        $rs_product = $resultProduct->fetchRow();

        $ofs_title[] = $rs_product['title'];
    }
    $swal['icon'] = 'warning';
    $swal['title'] = 'Sorry...';
    $swal['msg'] = implode(",", $ofs_title) . ' had removed from your cart due to low stock';
}
//end inventory check

$orderClass->update();

$promotion_array = $promotionClass->discount();
foreach ($promotion_array as $k => $v) {
    $promotion_total_amount += $v['discount'];
    $promotion_discount_array[] = $v['discount'];
}
$promotionClass->freegift();
foreach ($promotionClass->special_label() as $k => $v) {
    $promo_product_order[] = $v['product_id'];
}

$resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['member']['order_id']);
$rs_order = $resultOrder->fetchRow();

$cprv_mobile = $_SESSION['member']['mobile'];
include $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/cprv.php";

print_r($cprv_array);

$_SESSION['member']['otp_request'] = '1';
$_SESSION['member']['otp'] = '1';

//vincent voucher
if ($_SESSION['member']['mobile'] == "+65182268875") {
    $_SESSION['member']['otp_request'] = '1';
    $_SESSION['member']['otp'] = '1';
    $cprv_array['tier'] = 'gold';
    $voucher_array['vouchers'][] = array('id' => '655');
    $voucher_array['vouchers'][] = array('id' => '656');
    $voucher_array['vouchers'][] = array('id' => '657');
    $voucher_array['vouchers'][] = array('id' => '658');
    $voucher_array['vouchers'][] = array('id' => '651');
    $voucher_array['vouchers'][] = array('id' => '652');
    $voucher_array['vouchers'][] = array('id' => '653');
    $voucher_array['vouchers'][] = array('id' => '665');
    $voucher_array['vouchers'][] = array('id' => '666');
    $voucher_array['vouchers'][] = array('id' => '667');
}

if ($_GET['test'] == 'vk') {
    $_SESSION['member']['otp_request'] = '1';
    $_SESSION['member']['otp'] = '1';
}

$voucher_array = $cprv_array;

foreach ($voucher_array['vouchers'] as $k => $v) {
    if ($v['id'] == "403") {
        unset($voucher_array['vouchers'][$k]);
    }
}

//gold campaign
if ($rs_campaign['pkid'] == '5' && $cprv_array['tier'] == '') {
    $ofs_product = array();

    $resultCampaignProduct = get_query_data($table['campaign_product'], "campaign_id=" . $rs_campaign['pkid']);
    while ($rs_campaignProduct = $resultCampaignProduct->fetchRow()) {
        foreach ($_SESSION['cart'] as $k2 => $v2) {
            if ($rs_campaignProduct['product_id'] == $v2['product_id']) {
                unset($_SESSION['cart'][$k2]);
                $ofs_product[] = $rs_campaignProduct['product_id'];
            }
        }
    }

    $ofs_product = array_filter($ofs_product);
    if (count($ofs_product) > 0) {
        $ofs_title = array();
        foreach ($ofs_product as $v) {
            $resultProduct = get_query_data($table['product'], "pkid=$v");
            $rs_product = $resultProduct->fetchRow();

            $ofs_title[] = $rs_product['title'];
        }
        $swal['icon'] = 'warning';
        $swal['title'] = 'Sorry...';
        $swal['msg'] = implode(",", $ofs_title) . ' has been removed from your cart as it is exclusively to member on 1 Sep.';

        $orderClass->update();
        $promotionClass->freegift();
    }
}

//member voucher
$row_check_voucher = get_query_data_row($table['member_voucher'], "status=1 and type='' and mobile='" . $_SESSION['member']['mobile'] . "' and date(created_date) >= date('2021-03-31')");
if ($row_check_voucher > 0) {
    $resultVoucher = get_query_data($table['member_voucher'], "status=1 and type='' and mobile='" . $_SESSION['member']['mobile'] . "' and date(created_date) >= date('2021-03-31')");
    while ($rs_voucher = $resultVoucher->fetchRow()) {
        $resultGame = get_query_data($table['game_voucher'], "pkid=" . $rs_voucher['voucher_id']);
        $rs_game = $resultGame->fetchRow();

        $game_voucher_array['vouchers'][] = array('id' => $rs_voucher['pkid'], 'voucher_id' => $rs_voucher['voucher_id'], 'url' => '#', 'name' => $rs_game['title'], 'type' => $rs_voucher['type']);
    }
}

$resultVoucher = get_query_data($table['member_voucher'], "status=1 and type!='' and mobile='" . $_SESSION['member']['mobile'] . "'");
while ($rs_voucher = $resultVoucher->fetchRow()) {
    if ($rs_voucher['type'] == "free_shipping") {
        $voucher_title = 'Free Shipping';
    } elseif ($rs_voucher['type'] == "cash") {
        $voucher_title = '$' . $rs_voucher['value'] . ' Off';
    }

    if ($rs_voucher['expiry_date'] != "" && strtotime('now') <= strtotime($rs_voucher['expiry_date'])) {
        $member_voucher_array['vouchers'][] = array('id' => $rs_voucher['pkid'], 'voucher_id' => $rs_voucher['voucher_id'], 'url' => '#', 'name' => $voucher_title, 'type' => $rs_voucher['type'], 'value' => $rs_voucher['value']);
    } elseif ($rs_voucher['expiry_date'] == "") {
        $member_voucher_array['vouchers'][] = array('id' => $rs_voucher['pkid'], 'voucher_id' => $rs_voucher['voucher_id'], 'url' => '#', 'name' => $voucher_title, 'type' => $rs_voucher['type'], 'value' => $rs_voucher['value']);
    }
}

$row_voucher = count($voucher_array['vouchers']);
$row_member_voucher = count($member_voucher_array['vouchers']);
$row_game_voucher = count($game_voucher_array['vouchers']);

$resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['member']['order_id']);
$rs_order = $resultOrder->fetchRow();

//$total_non_promo = $rs_order['total_amount'] - $promotion_total_amount;
$total_non_promo = $rs_order['total_amount'];

if ($_POST) {
    $postfield = $_POST;

    unset($postfield['submit_form']);
    unset($postfield['rdmpcode']);
    unset($postfield['voucher_id']);
    unset($postfield['voucher_value']);
    unset($postfield['voucher']);
    unset($postfield['point_value']);

    if (preg_match("/game_/", $_POST['voucher_id'])) {
        $postfield['game_voucher_id'] = explode("_", $_POST['voucher_id'])[1];
        $voucher_discount_amount = $_POST['voucher_value'];

        $postfield['cprv_id'] = "0";
        $postfield['member_voucher_id'] = "0";
    } elseif (preg_match("/member_/", $_POST['voucher_id'])) {
        $postfield['member_voucher_id'] = explode("_", $_POST['voucher_id'])[1];
        $voucher_discount_amount = $_POST['voucher_value'];

        $postfield['cprv_id'] = "0";
        $postfield['game_voucher_id'] = "0";
    } elseif ($_POST['voucher_id'] >= "482") {
        $postfield['cprv_id'] = $_POST['voucher_id'];
        $voucher_discount_amount = $_POST['voucher_value'];

        $postfield['game_voucher_id'] = "0";
        $postfield['member_voucher_id'] = "0";
    }

    $postfield['voucher_discount_amount'] = $voucher_discount_amount;

    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], $postfield);
    $databaseClass->query($queryUpdate);

    if ($_SESSION['type'] == "gift") {
        header("Location: checkout-friend");
    } else {
        header("Location: checkout-payment");
    }
    exit();
}

?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <form class="mx-auto formValidation w-90" action="checkout-summary?" method="post" id="form_voucher">
            <?php
            if ($_SESSION['campaign_id'] != '8') {
                if ($row_voucher > 0 || $row_check_voucher > 0 || count($game_voucher_array) > 0 || count($member_voucher_array) > 0) {
                    ?>
                    <div class="col-12 text-center p-0">
                        <div class="title">
                            <h4 class="w-100">E-VOUCHER REDEMPTION</h4>
                        </div>
                    </div>
                    <? if ($_SESSION['member']['otp_request'] == "") {
                        ?>
                        <div class="form-group pb-4">
                            <p class="w-100">In order to redeem your points and voucher, please request an OTP.</p>
                            <button type="button" class="btn btn-darkblue w-50 btn_otp mx-auto d-block">REQUEST</button>
                        </div>
                        <?php
                    } elseif ($_SESSION['member']['otp_request'] != "" && $_SESSION['member']['otp'] == "") {
                        ?>
                        <div class="form-group">
                            <p class="w-100">Please enter OTP code to continue</p>
                            <input type="tel" class="form-control" maxlength="3" minlength="3" name="otp" required>
                        </div>
                        <div class="form-group pb-4">
                            <button type="button" class="btn btn-darkblue w-80 mx-auto d-block" name="submit_otp"
                                    value="true">SUBMIT
                            </button>
                            <button type="button" class="btn btn-blue w-80 btn_otp mx-auto d-block mt-3">REQUEST
                                AGAIN
                            </button>
                        </div>
                        <?php
                    } elseif ($_SESSION['member']['otp_request'] != "" && $_SESSION['member']['otp'] != "") {
                        ?>
                        <div class="col-12 text-center p-0">
                            <div class="title">
                                <p class="w-100">You have <?= $row_voucher + $row_check_voucher ?>
                                    e-voucher(s) available for redemption. Please
                                    select the voucher you will like to utilize in this transaction.</p>
                            </div>
                        </div>
                        <div class="col-12 mt-4 mx-auto">
                            <div class="form-group">
                                <?php foreach ($voucher_array['vouchers'] as $k => $v) {
                                    if ($v['id'] == '623') {
                                        $value = 10;
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>"
                                                <?= $total_non_promo < 100 ? "disabled" : "" ?>>
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Big little things completed card – $10 off $100
                                            </label>
                                        </div>
                                        <?php
                                    } elseif ($v['id'] == '651') {
                                        $value = ($total_non_promo / 100 * 10);
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>">
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Birthday Voucher – Club 10%
                                            </label>
                                        </div>
                                        <?php
                                    } elseif ($v['id'] == '652') {
                                        $value = ($total_non_promo / 100 * 15);
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>">
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Birthday Voucher – Gold 15%
                                            </label>
                                        </div>
                                        <?php
                                    } elseif ($v['id'] == '665') {
                                        $value = 15;
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>"
                                                <?= $total_non_promo < 120 ? "disabled" : "" ?>>
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Regular conversion – $15 off $120
                                            </label>
                                        </div>
                                        <?php
                                    } elseif ($v['id'] == '666') {
                                        $value = 20;
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>"
                                                <?= $total_non_promo < 130 ? "disabled" : "" ?>>
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Welcome to Club – $20 off $130
                                            </label>
                                        </div>
                                        <?php
                                    } elseif ($v['id'] == '667') {
                                        $value = 40;
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>"
                                                <?= $total_non_promo < 250 ? "disabled" : "" ?>>
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Welcome to Gold – $40 off $250
                                            </label>
                                        </div>
                                        <?php
                                    } elseif ($v['id'] == '843') {
                                        $value = 20;
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>"
                                                <?= $total_non_promo < 110 ? "disabled" : "" ?>>
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Club Renewal Voucher – $20 off $110
                                            </label>
                                        </div>
                                        <?php
                                    } elseif ($v['id'] == '844') {
                                        $value = 40;
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>"
                                                <?= $total_non_promo < 250 ? "disabled" : "" ?>>
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Gold Renewal Voucher – $40 off $250
                                            </label>
                                        </div>
                                        <?php
                                    } elseif ($v['id'] == '845') {
                                        $value = 40;
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="voucher_<?= $v['id'] ?>"
                                                   value="<?= $v['id'] ?>" data-url="<?= $v['url'] ?>"
                                                   data-name="<?= $v['name'] ?>"
                                                   data-id="<?= $v['id'] ?>" data-value="<?= $value ?>"
                                                <?= $total_non_promo < 250 ? "disabled" : "" ?>>
                                            <label class="form-check-label" for="voucher_<?= $v['id'] ?>">
                                                Upgrade to Gold – $40 off $250
                                            </label>
                                        </div>
                                        <?php
                                    }
                                }
                                if ($row_check_voucher > 0) {
                                    foreach ($game_voucher_array['vouchers'] as $k => $v) {
                                        $value = 0;
                                        if ($v['voucher_id'] == "10") {
                                            foreach ($_SESSION['cart'] as $k2 => $v2) {
                                                if ($v2['product_id'] == "32") {
                                                    $resultProduct = get_query_data($table['product'], "pkid=" . $v2['product_id']);
                                                    $rs_product = $resultProduct->fetchRow();
                                                    $value += ($rs_product['price'] * $v2['quantity']) * 0.05;
                                                }
                                            }
                                        } elseif ($v['voucher_id'] == "11") {
                                            foreach ($_SESSION['cart'] as $k2 => $v2) {
                                                if (in_array($v2['product_id'], $game_discount_array[11])) {
                                                    $resultProduct = get_query_data($table['product'], "pkid=" . $v2['product_id']);
                                                    $rs_product = $resultProduct->fetchRow();
                                                    $value += ($rs_product['price'] * $v2['quantity']) * 0.05;
                                                }
                                            }
                                        } elseif ($v['voucher_id'] == "12") {
                                            $value = 35;
                                        } elseif ($v['voucher_id'] == "13") {
                                            if (strtotime('now') >= strtotime('2020-12-26')) {
                                                continue;
                                            }
                                            foreach ($_SESSION['cart'] as $k2 => $v2) {
                                                if (in_array($v2['product_id'], $game_discount_array[11])) {
                                                    $resultProduct = get_query_data($table['product'], "pkid=" . $v2['product_id']);
                                                    $rs_product = $resultProduct->fetchRow();
                                                    $value += ($rs_product['price'] * $v2['quantity']) * 0.05;
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="game_<?= $k ?>"
                                                   value="game_<?= $v['id'] ?>"
                                                   data-id="game_<?= $v['id'] ?>" data-name="<?= $v['name'] ?>"
                                                   data-type="game"
                                                   data-value="<?= $value ?>">
                                            <label class="form-check-label" for="game_<?= $k ?>">
                                                <?= $v['name'] ?>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                }
                                if ($row_member_voucher > 0) {
                                    foreach ($member_voucher_array['vouchers'] as $k => $v) {
                                        $value = 0;
                                        if ($v['type'] == "free_shipping") {
                                            $value = $rs_order['shipping_amount'];
                                        } elseif ($v['type'] == "cash") {
                                            $value = $v['value'];
                                        }
                                        ?>
                                        <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                            <input class="form-check-input" type="radio" name="voucher"
                                                   id="member_<?= $k ?>"
                                                   value="member_<?= $v['id'] ?>"
                                                   data-id="member_<?= $v['id'] ?>" data-name="<?= $v['name'] ?>"
                                                   data-type="member"
                                                   data-value="<?= $value ?>">
                                            <label class="form-check-label" for="member_<?= $k ?>">
                                                <?= $v['name'] ?>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="form-check w-100 mb-3 btn btn-blue btn-sm">
                                    <input class="form-check-input" type="radio" name="voucher" id="voucher_0" value="0"
                                           data-url="#" data-name="None" data-id="0" data-value="0">
                                    <label class="form-check-label" for="voucher_0">
                                        None
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <small>*All vouchers can only be redeemed once on regular-priced items. Any unused
                                    balance
                                    of
                                    voucher will be forfeited. Other T&Cs apply.
                                </small>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
            <hr class="w-90 mx-auto mb-4">
            <div class="col-12 text-center p-0 mb-4">
                <div class="title">
                    <h4 class="w-100">ORDER SUMMARY</h4>
                </div>
            </div>
            <div class="col-12">
                <table class="table table-cart" style="min-width: 100%">
                    <tbody>
                    <?php
                    $row_cart = count($_SESSION['cart']);

                    if ($row_cart > 0) {
                        foreach ($_SESSION['cart'] as $k => $v) {
                            $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();

                            if ($v['free'] == "true") {
                                $rs_product['price'] = "0";
                                $badge = "<span class='badge badge-danger'>Free</span>";
                            } else {
                                $badge = "";
                            }
                            ?>
                            <tr class="p-3">
                                <td width="30%" class="text-center pl-0 pr-0">
                                    <div class="product-card m-0">
                                        <img <?php if ($rs_product['img_url'] == "") { ?>
                                            src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                                        <?php } else { ?> src="assets/product/<?= $rs_product['img_url'] ?>" <?php } ?>
                                                class="img-fluid product-img">
                                    </div>
                                </td>
                                <td><?= $badge ?>
                                    <?
                                    if (in_array($rs_product['pkid'], $promo_product_order)) {
                                        foreach ($promotionClass->special_label() as $k2 => $v2) {
                                            if ($v2['product_id'] == $rs_product['pkid']) {
                                                echo '<span class="badge badge-danger">' . $v2['special_label'] . '</span><br>';
                                            }
                                        }
                                    }
                                    ?>
                                    <b><?= strtoupper($rs_product['title']); ?></b>
                                    <br>
                                    <p>
                                        S$ <?= number_format($v['quantity'] * $rs_product['price']); ?>
                                        <span class="float-right">x<?= $v['quantity'] ?></span>
                                    </p>
                                </td>
                            </tr>
                            <?php
                        }
                    } else { ?>
                        <tr>
                            <td colspan="4" class="text-center">YOUR CART IS EMPTY</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-center"><a href="shop" class="btn btn-blue w-80">BACK TO
                                    SHOP</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <?php if ($row_cart > 0) { ?>
                    <div class="mt-2 mb-2 d-inline-block w-100">
                        <p>SUBTOTAL <span
                                    class="float-right price-subtotal">S$ <?= number_format($rs_order['total_amount'], 2) ?></span>
                        </p>
                        <p>SHIPPING FEES <span
                                    class="float-right">S$ <?= number_format($rs_order['shipping_amount'], 2) ?></span>
                        </p>
                        <?
                        foreach ($promotion_array as $k => $v) {
                            $resultPromo = get_query_data($table['promotion'], "pkid=" . $v['promo_id']);
                            $rs_promo = $resultPromo->fetchRow();
                            $promotion_id_array[] = $v['promo_id'];

                            if ($v['type'] == "free_shipping") {
                                $free_shipping = true;
                                $min_spend[] = $v['min_spend'];
                            }

                            if ($v['discount'] > 0) {
                                ?>
                                <p class="<?= $v['type'] == "free_shipping" ? "div_freeshipping" : "" ?>"><?= $v['label'] != "" ? $v['label'] : $rs_promo['title'] ?>
                                    <span class="float-right">- S$ <?= number_format($v['discount'], 2) ?></span></p>
                                <?php
                            }
                        } ?>
                        <div class="div_discount">
                            <p><span id="div_discount_name"></span> <span class="float-right"
                                                                          id="div_discount_value"></span></p>
                        </div>
                        <p> 9% GST INCLUDED IN TOTAL
                            <span class="float-right">S$ <?= number_format((9/109) * $rs_order['total_amount'], 2) ?></span>
                        </p>
                    </div>
                    <hr class="border-yellow mb-4 mt-2">
                    <div class="float-left"><b>TOTAL</b></div>
                    <div class="float-right price-total">
                        <b>S$ <?= number_format($_SESSION['shipping_method'] == 'delivery' ? $rs_order['total_amount'] + $rs_order['shipping_amount'] - $promotion_total_amount : $rs_order['total_amount'] - $promotion_total_amount, 2) ?></b>
                    </div>
                    <br>
                    <div class="col-12 text-center mt-5 mb-3">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" name="free_giftbox"
                                   id="defaultCheck1">
                            <label class="form-check-label text-left" for="defaultCheck1">
                                Tick for complimentary paper gift bag.
                            </label>
                        </div>
                    </div>
                    <div class="col-12 text-center mt-5">
                        <p>Tell us which Beauty Advisor<br>assisted you</p>
                        <input type="text" name="ba_code" class="form-control w-80 mx-auto d-block"
                               placeholder="BA CODE (IF ANY)"/>
                    </div>
                    <div class="col-12 mt-5 pb-5 text-center">
                        <button type="submit" name="submit_form" value="true"
                                class="btn btn-darkblue w-80">CHECK OUT
                        </button>
                    </div>
                <?php } ?>
            </div>
            <input type="hidden" name="promotion_discount_amount" value="<?= $promotion_total_amount ?>"/>
            <input type="hidden" name="promotion_id" value="<?= implode(',', $promotion_id_array) ?>"/>
            <input type="hidden" name="promotion_value" value="<?= implode(',', $promotion_discount_array) ?>"/>
            <input type="hidden" name="voucher_id" value="0"/>
            <input type="hidden" name="voucher_value" value="0"/>
        </form>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(document).ready(function () {
        $(".div-discount").hide();

        $("button[name='submit_otp']").on('click', function (e) {
            if ($("input[name='otp']").val().length == 3) {
                $.ajax({
                    method: "POST",
                    url: "ajax/check-otp",
                    dataType: 'json',
                    data: {
                        otp: $("input[name='otp']").val()
                    }
                })
                    .done(function (data) {
                        if (data.result == "success") {
                            window.location.reload();
                        } else {
                            Swal.fire({
                                text: data.msg,
                                icon: data.result,
                            });
                        }
                    });
            }
        });

        $(".btn_otp").on('click', function (e) {
            $(this).attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "/ajax/send-otp",
                dataType: 'json',
            })
                .done(function (data) {
                    Swal.fire({
                        text: data.msg,
                        icon: data.result,
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 1500);
                });
        });

        <?
        if($promotion_total_amount > 0){
        foreach($disable_voucher_array as $v){
        ?>
        $("#voucher_<?=$v?>").attr('disabled', 'disabled');
        <?
        }
        }
        ?>

        $("input[name='voucher']").on('click', function (e) {
            var name = $(this).data('name');
            var value = $(this).data('value');

            if (name == '') {
                name = 'E-VOUCHER';
            }

            $("input[name='voucher_id']").val($(this).data('id'));
            $("input[name='voucher_value']").val($(this).data('value'));

            if (name == "None") {
                $(".div_discount").hide(500);
            } else {
                $(".div_discount").show(500);
                $("#div_discount_name").text(name);
                $("#div_discount_value").text("- S$ " + value.toFixed(2));

                $.each($("select"), function (key, value) {
                    $(value).val('0');
                    $(value).trigger('change');
                });
            }

            showDiscount();
        });
    });

    function showDiscount() {
        var value = parseFloat($("input[name='voucher_value']").val());
        var total = '<?=$rs_order['total_amount'] + $rs_order['shipping_amount'] - $promotion_total_amount?>';
        var shipping = '<?=$rs_order['shipping_amount']?>';
        var min_spend = [<?=implode(",", $min_spend != "" ? $min_spend : "0")?>];
        var total_nett = '<?=$rs_order['total_amount'] - $promotion_total_amount?>' - value;
        var free_shipping = '<?=$free_shipping == true ? "1" : "0"?>';
        var apply_shipping = 0;

        shipping = parseFloat(shipping);

        var after_discount = total - value;

        if (after_discount < 0) {
            after_discount = 0;
        }

        if (free_shipping == "1") {
            total_nett += shipping;
            $.each(min_spend, function (k, v) {
                if (total_nett >= v) {
                    apply_shipping++;
                }
            });
            if (apply_shipping == 0) {
                after_discount = after_discount + shipping;
                $(".div_freeshipping").hide(500);
            } else {
                // after_discount = after_discount - shipping;
                $(".div_freeshipping").show(500);
            }
        } else if (after_discount == 0) {
            after_discount = shipping;
        }

        after_discount = parseFloat(after_discount);

        $(".price-total").html("S$ " + after_discount.toFixed(2));
    }

</script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg bg-light bg-xmas">
    <div class="container-fluid">
        <div class="row row-gift">
            <div class="col-12">
                <img src="assets/img/Giver.gif" class="img-fluid d-block mx-auto" />
            </div>
        </div>
        <div class="row row-gift-button">
            <div class="col-12 text-center">
                <p>Your gift is wrapped now!</p>
                <a href="checkout-payment" class="btn btn-red">CONTINUE</a>
            </div>
        </div>
    </div>

    <?php include('footer.php') ?>
    <?php include('js-script.php') ?>
    <script>
        $(".row-gift-button").hide(500);

        $(window).on('load', function(e) {
            setTimeout(() => {
                $(".row-gift-button").show(500);
            }, 2000);
        });
    </script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

//$row_check = get_query_data_row($table['sample_loccitane_pullman'], "date(created_date) >= date('2022-01-14')");


//21-2-2023 3.57pm client request to live it, initial live date 2023-04-01 00:00:00

//23-2-2023 8.17pm client request to close it
if (strtotime('now') < strtotime('2024-05-12 23:59:59')) {
    $row_check = 999999;
}

if ($_GET['vk'] == '1') {
    $row_check = 0;
}

// if ($_SESSION['sample_loccitane_pullman']['done'] != "") {
//     header("Location: " . $site_config['sample_loccitane_pullman']);
//     exit();
// }

if ($_POST) {
    $postfield = $_POST;

    $mobile = str_replace("-", "", $postfield['mobile']);
    $mobile = str_replace(" ", "", $mobile);

    $mobile = "+65" . $mobile;
    $_SESSION['iglive']['mobile'] = $mobile;

    $resultOtp = get_query_data($table['otp'], "mobile='$mobile' and type='sample_loccitane_pullman' order by pkid desc limit 1");
    $rs_otp = $resultOtp->fetchRow();

    $to_time = strtotime("now");
    $from_time = strtotime($rs_otp['created_date']);

    if (round(abs($to_time - $from_time) / 60, 2) >= 1) {
        $otp = rand('100', '999');

        $content = "L'OCCITANE: OTP code: $otp. NEVER share this code with others.";

        $ch = curl_init();
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
        );
        curl_setopt($ch, CURLOPT_URL, "http://www.etracker.cc/bulksms/mesapi.aspx?user=davino&pass=Wowsome%40820%23%23%23%23%21&type=0&to=$mobile&from=EnDemande&text=" . urlencode($content) . "&servid=MES01&title=EnDemande_SG_SampleLoccitanePullman2024");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $server_respond = curl_exec($ch);
        curl_close($ch);

        $postfield = array(
            'status' => '0',
            'type' => 'sample_loccitane_pullman',
            'mobile' => $mobile,
            'otp' => $otp,
            'created_date' => $time_config['now'],
        );

        $queryInsert = get_query_insert($table['otp'], $postfield);
        $databaseClass->query($queryInsert);

        $_SESSION['iglive']['otp_request'] = "true";

        header("Location: " . $site_config['sample_loccitane_pullman'] . "-otp");
        exit();
    } else {
        $swal['title'] = 'Opps...';
        $swal['msg'] = 'You\'ve just request an OTP, please try again later.';
        $swal['icon'] = 'error';
    }
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4 d-flex justify-content-center">
        <div class="col-12 col-lg-6 p-0">
            <div class="title">
                <div class="col-12">
                    <img src="assets/img/sample-pullman-1.png" class="img-fluid" width="100%" style="max-width:640px;"/>
                </div>
                <div class="mt-3 px-3 text-center">
                    <p><b>Welcome aboard Le Grand Voyage</b>
                    <br><br>

                    We're delighted to have you with us as we embark on this holiday season.
                    Join us on a grand voyage for the senses and transform your festive getaway into a luxurious sensory exploration.  
                    <br><br>

                    <!-- YouTube Video Embed -->
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/7stYUKf32xc?start=30" 
                            title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen></iframe>
                    <br><br>

                    At L'OCCITANE, we believe every gift should be a journey and every product a discovery, ensuring your holidays are filled with delight and unforgettable memories.
                    <br><br><br>

                    <b>Sign up to redeem these exclusive guest privileges* at L'OCCITANE boutiques:</b><br><br>
                    - 6-piece L'OCCITANE bestseller sample kit<br>
                    - $10 voucher (valid with a minimum spend of $50)
                    <hr>
                    *Valid until 31st December 2024.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php
    $start_date = '2024-11-06';
    $end_date = '2025-01-01';

    $current_date = date('Y-m-d');

    $start_timestamp = strtotime($start_date);
    $end_timestamp = strtotime($end_date);
    $current_timestamp = time();

        if ($current_timestamp >= $start_timestamp && $current_timestamp <= $end_timestamp) {
            ?>
            <? if ($row_check < 5000) { ?>
                <div class="col-12 mt-4">
                    <label class="w-100 text-center">Please fill in your mobile number.</label>
                    <form action="<?= $site_config['sample_loccitane_pullman'] ?>" method="post" class="w-80 mx-auto formIglive">
                        <div class="form-group">
                            <input type="tel" class="form-control" placeholder="MOBILE NUMBER" name="mobile"
                                required
                                minlength="8" maxlength="9">
                        </div>
                        <div class="form-group mt-5 pb-5 text-center">
                            <button type="submit" name="submit_mobile" value="true" class="btn btn-darkblue w-50">SIGN UP NOW
                            </button>
                        </div>
                    </form>
                </div>
            <? } else { ?>
                <div class="col-12 mt-4 text-center">
                    <p>Thank you for your interest. Sample Kit have been fully redeemed at the moment.</p>
                </div>
            <? } ?>
        <?php
        } else {
            ?>
            <div class="col-12 mt-4">
                <label class="w-100 text-center font-weight-bold">Kindly be informed that the sign-up form is closed. We invite you to explore our wide range of products at our L'OCCITANE boutique stores, where you can discover more about what we offer.</label>
            </div>
        <?php
        }
    ?>
    <div class="text-center">
    <p>Discover our Holiday Collection here: <a href="https://sg.loccitane.com/holiday_collection.html" target="_blank">https://sg.loccitane.com/holiday_collection.html</a></p>
    </div>

</div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(".formIglive").each(function (index, element) {
        form = $(".formIglive")[index];
        fv = FormValidation.formValidation(
            form, {
                fields: {
                    mobile: {
                        message: 'This field is required',
                        validators: {
                            remote: {
                                message: 'Mobile number has already been registered',
                                url: '/ajax/sample-loccitanepullman-2024-check',
                                method: 'POST',
                                data: {
                                    type: 'loccitanepullman_2024',
                                },
                            }
                        }
                    }
                },
                plugins: {
                    declarative: new FormValidation.plugins.Declarative({
                        html5Input: true,
                    }),
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    excluded: new FormValidation.plugins.Excluded(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    icon: new FormValidation.plugins.Icon({
                        valid: 'fal fa-check',
                        invalid: 'fal fa-times',
                        validating: 'fal fa-refresh'
                    }),
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                },
            }
        ).on('core.form.valid', function () {
            $("button[type='submit']").attr('disabled', 'disabled');
        });
    });
</script>
</body>
</html>
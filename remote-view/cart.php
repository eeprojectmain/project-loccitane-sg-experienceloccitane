<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$member_id = $_SESSION['member']['id'];
?>
<div class="modal-body">
    <table class="table table-bordered" style="min-width: 100%">
        <thead class="thead-dark">
        <tr>
            <th>PRODUCT</th>
            <th>QTY</th>
            <th width="30%">PRICE</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?
        $resultCart = get_query_data($table['cart'], "member_id=$member_id");
        $row_cart = $resultCart->numRows();

        if ($row_cart > 0) {
            while ($rs_cart = $resultCart->fetchRow()) {
                $resultProduct = get_query_data($table['product'], "pkid=" . $rs_cart['product_id']);
                $rs_product = $resultProduct->fetchRow()
                ?>
                <tr>
                    <td><?= $rs_product['title'] ?></td>
                    <td><?= $rs_cart['quantity'] ?></td>
                    <td>S$ <?= number_format($rs_cart['quantity'] * $rs_product['price']) ?></td>
                    <td>
                        <button type="button" class="btn btn-xs btn-danger"
                                onclick="delete_cart(<?= $rs_cart['product_id'] ?>)"><i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
            <? }
        } else { ?>
            <tr>
                <td colspan="4" class="text-center">YOUR CART IS EMPTY</td>
            </tr>
        <? } ?>
        </tbody>
    </table>
</div>

<div class="modal-footer">
    <?if($row_cart>0){?>
    <button class="btn btn-warning btn-yellow" type="submit" name="submit_checkout" value="true">
        CHECK
        OUT
    </button>
    <?}?>
</div>

<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$promotionClass=new promotion();

foreach ($promotionClass->special_label() as $k => $v) {
    $promo_product_order[] = $v['product_id'];
}
?>
<div class="modal-body">
    <table class="table m-0" style="min-width: 100%">
        <tbody>
        <?php
        $row_cart = count($_SESSION['cart']);

        $total = 0;

        if ($row_cart > 0) {
            foreach ($_SESSION['cart'] as $k => $v) {
                $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
                $rs_product = $resultProduct->fetchRow();

                if ($v['free'] == "true") {
                    $rs_product['price'] = "0";
                    $badge = "<span class='badge badge-danger'>Free</span>";
                }else{
                    $badge="";
                }

                $total += $v['quantity'] * $rs_product['price']; ?>
                <tr>
                    <td width="30%"><img <?php if ($rs_product['img_url'] == "") { ?>
                            src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                        <?php } else { ?> src="assets/product/<?= $rs_product['img_url'] ?>" <?php } ?>
                                class="img-fluid">
                    </td>
                    <td><?= $badge ?>
                        <?
                        if (in_array($rs_product['pkid'], $promo_product_order)) {
                            foreach ($promotionClass->special_label() as $k2 => $v2) {
                                if ($v2['product_id'] == $rs_product['pkid']) {
                                    echo '<span class="badge badge-danger">' . $v2['special_label'] . '</span><br>';
                                }
                            }
                        }
                        ?>
                        <b><?= strtoupper($rs_product['title']) ?></b>
                        <br>
                        <p>S$ <?= number_format($v['quantity'] * $rs_product['price'], 2) ?> <span class="float-right"><b>x<?=$v['quantity']?></b></span></p>
                    </td>
                </tr>
                <?php
            }
        } else { ?>
            <tr>
                <td colspan="4" class="text-center">YOUR CART IS EMPTY</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<div class="modal-footer text-center b-0">
    <?php if ($row_cart > 0) { ?>
        <a href="checkout-cart" class="mx-auto">
            <button class="btn btn-blue btn-sm mx-auto d-block" type="button" name="submit_checkout" value="true">
                CHECK OUT
            </button>
        </a>
    <?php } ?>
</div>
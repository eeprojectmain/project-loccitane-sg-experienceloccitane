<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$member_id = session_id();
?>

<table class="table table-bordered" style="min-width: 100%;font-size:14px;">
    <thead class="thead-dark">
    <tr>
        <th>ORDER ID</th>
        <th>TOTAL</th>
        <th>PRODUCTS</th>
        <th>DATE</th>
    </tr>
    </thead>
    <tbody>
    <?
    $resultOrder = get_query_data($table['order'], "member_id='".$member_id."' order by pkid desc");
    while ($rs_order = $resultOrder->fetchRow()) {
        $product_id_array = explode(",", $rs_order['product_id']);
        $product_qty_array = explode(",", $rs_order['quantity']);
        $product_text_array = array();

        foreach ($product_id_array as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=$v");
            $rs_product = $resultProduct->fetchRow();

            $product_text_array[] = "- ".$rs_product['title']." (x".$product_qty_array[$k].")";
        }
        ?>
        <tr>
            <td>#LDSG20<?= $rs_order['pkid'] ?></td>
            <td>S$ <?= number_format($rs_order['total_amount']) ?></td>
            <td><?= implode("<br>", $product_text_array) ?></td>
            <td><?= $rs_order['created_date'] ?></td>
        </tr>
        <?
    }
    ?>
    </tbody>
</table>

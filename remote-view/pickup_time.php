<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$date = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['date']);

if ($date == date("Y-m-d", strtotime("today")) && (strtotime('now') < strtotime('today 2pm'))) {
    $time_array = array('5pm - 8pm');
} else {
    $time_array = array('12pm - 2pm', '2pm - 5pm', '5pm - 8pm');
}

$count = 0;
foreach ($time_array as $k => $v) {
    $row_check = get_query_data_row($table['order'], "method='pickup' and outlet_id=".$_SESSION['outlet_id']." and pickup_date='$date' and pickup_time='$v' and status!=5");

    if ($k == 0 && $row_check >= 10) {
        continue;
    } else if ($row_check >= 15) {
        continue;
    }
    $count++;
    ?>
    <div class="form-check btn btn-blue btn-sm mx-auto d-block w-80 mb-3">
        <input class="form-check-input" type="radio" name="pickup_time"
               id="time_<?= $k ?>" value="<?= $v ?>" required>
        <label class="form-check-label" for="time_<?= $k ?>">
            <?= $v ?>
        </label>
    </div>
<? }

if ($count == 0) {
    ?>
    <p>All time slot are full, please select another date.</p>
    <?
}
?>
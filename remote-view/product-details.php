<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$product_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['product_id']);

$result = get_query_data($table['product'], "pkid=$product_id");
$rs_array = $result->fetchRow();

if($rs_array['img_url']=="") {
    echo '<img src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id='.$rs_array['item_code'].'&v=2" class="img-fluid" />';
}else{
    echo '<img src="assets/product/'.$rs_array['img_url'].'" class="img-fluid" />';
}
echo '<br />';
echo nl2br($rs_array['details']);
?>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass = new stock();
$promotionClass = new promotion();

foreach ($promotionClass->special_label() as $k => $v) {
    $promo_product_order[] = $v['product_id'];
}

$where = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['where']);
$where = urldecode($where);
$where = stripcslashes($where);
$c_index = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['c_index']);
$page = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['page']);
$order_by = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['order_by']);
$order_by = urldecode($order_by);

if ($rs_campaign['pkid'] != "") {
    $resultCampaignProduct = get_query_data($table['campaign_product'], "campaign_id=" . $rs_campaign['pkid']);
    while ($rs_campaignProduct = $resultCampaignProduct->fetchRow()) {
        $campaign_product_array[] = $rs_campaignProduct['product_id'];
        $campaign_label_array[$rs_campaignProduct['product_id']]=$rs_campaignProduct['special_label'];
    }
}

$i = 0;
$resultCategory = get_query_data($table['product_category'], "status=1 and holiday_status=0 order by sort_order asc");
while ($rs_category = $resultCategory->fetchRow()) {
    $i++;

    if ($i == $c_index) {
        $cat_id = $rs_category['pkid'];
    }
}

if ($cat_id != "") {
    $where .= " and find_in_set($cat_id,cat_id)";
}

$adjacents = 1;
$targetpage = "shop";
$limit = 10;

if ($page != 'undefined') {
    $start = ($page - 1) * $limit;
} else {
    $start = '0';
}

$queryProduct = "select * from " . $table['product'] . " where status=1 and holiday_status=0 $where";
$queryProduct .= " order by $order_by sort_order asc,title asc,price asc LIMIT $start,$limit";
$resultProduct = $databaseClass->query($queryProduct);

$recordsQuery = get_query_data_row($table['product'], "status=1 $where");
if ($page == 0) {
    $page = 1;
}
$prev = $page - 1;
$next = $page + 1;
$lastpage = ceil($recordsQuery / $limit);
$lpm1 = $lastpage - 1;
$pagination = "";
if ($lastpage >= 1) {
    $pagination .= "<nav><ul class=\"pagination mt-5 pb-4\">";
    if ($page > 1) {
        $pagination .= "<li class='page-item'><a class='page-link' href=\"$prev\">&laquo;</a></li>";
    } else {
        $pagination .= "<li class='page-item'><a class=\"disabled page-link\">&laquo;</a></li>";
    }
    if ($lastpage < 7 + ($adjacents * 2)) {
        for ($counter = 1; $counter <= $lastpage; $counter++) {
            if ($counter == $page) {
                $pagination .= "<li class=\"active page-item\"><a class=\"active page-link\">$counter</a></li>";
            } else {
                $pagination .= "<li class='page-item'><a class='page-link' href=\"$counter\">$counter</a></li>";
            }
        }
    } elseif ($lastpage > 5 + ($adjacents * 2)) {
        if ($page < 1 + ($adjacents * 2)) {
            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                if ($counter == $page) {
                    $pagination .= "<li class=\"active page-item\"><a class='page-link'>$counter</a></li>";
                } else {
                    $pagination .= "<li class='page-item'><a class='page-link' href=\"$counter\">$counter</a></li>";
                }
            }
            $pagination .= "<li class='page-item'><a class='page-link'>...</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"$lpm1\">$lpm1</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"$lastpage\">$lastpage</a></li>";
        } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
            $pagination .= "<li class='page-item'><a class='page-link' href=\"1\">1</a><li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"2\">2</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link'>...</a></li>";
            for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                if ($counter == $page) {
                    $pagination .= "<li class=\"active page-item\"><a class='page-link'>$counter</a></li>";
                } else {
                    $pagination .= "<li class='page-item'><a class='page-link' href=\"$counter\">$counter</a></li>";
                }
            }
            $pagination .= "<li class='page-item'><a class='page-link'>...</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"$lpm1\">$lpm1</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"$lastpage\">$lastpage</a></li>";
        } else {
            $pagination .= "<li class='page-item'><a class='page-link' href=\"1\">1</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"2\">2</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link'>...</a></li>";
            for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                if ($counter == $page) {
                    $pagination .= "<li class=\"active page-item\"><a class='page-link'>$counter</a></li>";
                } else {
                    $pagination .= "<li class='page-item'><a class='page-link' href=\"$counter\">$counter</a></li>";
                }
            }
        }
    }
    if ($page < $counter - 1) {
        $pagination .= "<li class='page-item'><a class='page-link' href=\"$next\">&raquo;</a></li>";
    } else {
        $pagination .= "<li class='page-item'><a class=\"disabled page-link\">&raquo;</a></li>";
    }
    $pagination .= "</ul></nav>";
}


?>
<?php
while ($rs_product = $resultProduct->fetchRow()) {
    ?>
    <div class="col-5 col-sm-5 col-md-3 product-card text-center same-height">
        <div class="card">
            <a href="product-inner?id=<?= $rs_product['pkid'] ?>">
                <div class="card-body text-center mb-3" id="product_<?= $rs_product['pkid'] ?>">
                    <h4 class="card-title"><img class="img-fluid product-img"
                            <?php if ($rs_product['img_url'] == "") { ?>
                                src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                            <?php } else { ?> src="assets/product/<?= $rs_product['img_url'] ?>" <?php } ?>>
                    </h4>
                    <?
                    if (in_array($rs_product['pkid'], $promo_product_order)) {
                        foreach ($promotionClass->special_label() as $k2 => $v2) {
                            if ($v2['product_id'] == $rs_product['pkid']) {
                                echo '<span class="badge badge-danger">' . $v2['special_label'] . '</span><br><br>';
                            }
                        }
                    }

                    if(in_array($rs_product['pkid'],$campaign_product_array)){
                        echo '<span class="badge badge-danger">' . $campaign_label_array[$rs_product['pkid']] . '</span><br><br>';
                    }
                    ?>
                    <p class="card-text text-center product-title m-0">
                        <?= strtoupper($rs_product['title']) ?>
                    </p>
                    <p class="card-text text-center product-price m-0">
                        <?= $rs_product['size'] != "" ? $rs_product['size'] . " | " : "" ?>
                        <b>S$ <?= $rs_product['price'] ?></b>
                    </p>
                </div>
            </a>
            <div class="card-footer">
                <div class="row row-product-button">
                    <div class="col product-button text-center">
                        <?
                        if (strtotime($rs_product['start_date']) > strtotime($time_config['today'])) {
                            ?>
                            <span class="badge">AVAILABLE ON <?= strtoupper(date('d M', strtotime($rs_product['start_date']))) ?></span>
                            <?
                        } else {
                            if ($stockClass->check($rs_product['pkid']) === true) {
                                ?>
                                <button class="btn btn-sm btn-blue" type="button"
                                        onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                                </button>
                            <? } else {
                                ?>
                                <button class="btn btn-sm btn-blue" type="button"
                                        onclick="low_stock('<?= mysqli_real_escape_string($GLOBALS["mysqli_conn"], $rs_product['title']); ?>');">
                                    <i class="fa fa-exclamation-circle"></i> LOW STOCK
                                </button>
                                <?
                            }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
} ?>

<div class="col-12">
    <?= $pagination ?>
</div>

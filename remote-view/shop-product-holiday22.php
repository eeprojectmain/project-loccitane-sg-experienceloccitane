<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass = new stock();
$promotionClass = new promotion();

foreach ($promotionClass->special_label() as $k => $v) {
    $promo_product_order[] = $v['product_id'];
}

$where = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['where']);
$where = urldecode($where);
$where = stripcslashes($where);
$c_index = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['c_index']);
$page = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['page']);
$order_by = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['order_by']);
$price = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['price']);
$order_by = urldecode($order_by);

if ($rs_campaign['pkid'] != "") {
    $resultCampaignProduct = get_query_data($table['campaign_product'], "campaign_id=" . $rs_campaign['pkid']);
    while ($rs_campaignProduct = $resultCampaignProduct->fetchRow()) {
        $campaign_product_array[] = $rs_campaignProduct['product_id'];
        $campaign_label_array[$rs_campaignProduct['product_id']] = $rs_campaignProduct['special_label'];
    }
}

$i = 0;
$resultCategory = get_query_data($table['product_category'], "status=1 or holiday_status=1 order by sort_order asc");
while ($rs_category = $resultCategory->fetchRow()) {
    if($rs_category['pkid'] == '34' && $_SESSION['outlet_id'] != '1' && $_SESSION['outlet_id'] != '4') {// Rose pear only appear when chosen branch are ION Orchard and Raffles City
        continue;
    }
    $i++;

    if ($i == $c_index) {
        $cat_id = $rs_category['pkid'];
    }
}

if ($cat_id != "") {
    $where .= " and find_in_set($cat_id,cat_id)";
}

$adjacents = 1;
$targetpage = "shop";
$limit = 10;

if ($price != "") {
    //$limit = '6';
    //$queryProduct = "select * from " . $table['product'] . " where holiday_status=1 $where";
    //$recordsQuery = get_query_data_row($table['product'], "holiday_status=1 $where");

    $queryProduct = "select * from " . $table['product'] . " where holiday_status=1 and status=1 $where";
    $recordsQuery = get_query_data_row($table['product'], "holiday_status=1 and status=1 $where");
} else {
    $queryProduct = "select * from " . $table['product'] . " where (holiday_status=1 or status=1) and status=1 $where";
    $recordsQuery = get_query_data_row($table['product'], "(holiday_status=1 or status=1) and status=1 $where");
}

if ($page != 'undefined') {
    $start = ($page - 1) * $limit;
} else {
    $start = '0';
}

if ($cat_id == "15") {
    $queryProduct .= " order by CAST(price AS DECIMAL(10,2)) asc LIMIT $start,$limit";
}else{
    $queryProduct .= " order by holiday_status desc,$order_by CAST(price AS DECIMAL(10,2)) asc,sort_order asc,title asc LIMIT $start,$limit";
}
$resultProduct = $databaseClass->query($queryProduct);
//print_r($queryProduct);
if ($page == 0) {
    $page = 1;
}
$prev = $page - 1;
$next = $page + 1;
$lastpage = ceil($recordsQuery / $limit);
$lpm1 = $lastpage - 1;
$pagination = "";
if ($lastpage >= 1) {
    $pagination .= "<nav><ul class=\"pagination mt-5 pb-4\">";
    if ($page > 1) {
        $pagination .= "<li class='page-item'><a class='page-link' href=\"$prev\">&laquo;</a></li>";
    } else {
        $pagination .= "<li class='page-item'><a class=\"disabled page-link\">&laquo;</a></li>";
    }
    if ($lastpage < 7 + ($adjacents * 2)) {
        for ($counter = 1; $counter <= $lastpage; $counter++) {
            if ($counter == $page) {
                $pagination .= "<li class=\"active page-item\"><a class=\"active page-link\">$counter</a></li>";
            } else {
                $pagination .= "<li class='page-item'><a class='page-link' href=\"$counter\">$counter</a></li>";
            }
        }
    } elseif ($lastpage > 5 + ($adjacents * 2)) {
        if ($page < 1 + ($adjacents * 2)) {
            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                if ($counter == $page) {
                    $pagination .= "<li class=\"active page-item\"><a class='page-link'>$counter</a></li>";
                } else {
                    $pagination .= "<li class='page-item'><a class='page-link' href=\"$counter\">$counter</a></li>";
                }
            }
            $pagination .= "<li class='page-item'><a class='page-link'>...</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"$lpm1\">$lpm1</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"$lastpage\">$lastpage</a></li>";
        } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
            $pagination .= "<li class='page-item'><a class='page-link' href=\"1\">1</a><li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"2\">2</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link'>...</a></li>";
            for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                if ($counter == $page) {
                    $pagination .= "<li class=\"active page-item\"><a class='page-link'>$counter</a></li>";
                } else {
                    $pagination .= "<li class='page-item'><a class='page-link' href=\"$counter\">$counter</a></li>";
                }
            }
            $pagination .= "<li class='page-item'><a class='page-link'>...</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"$lpm1\">$lpm1</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"$lastpage\">$lastpage</a></li>";
        } else {
            $pagination .= "<li class='page-item'><a class='page-link' href=\"1\">1</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link' href=\"2\">2</a></li>";
            $pagination .= "<li class='page-item'><a class='page-link'>...</a></li>";
            for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                if ($counter == $page) {
                    $pagination .= "<li class=\"active page-item\"><a class='page-link'>$counter</a></li>";
                } else {
                    $pagination .= "<li class='page-item'><a class='page-link' href=\"$counter\">$counter</a></li>";
                }
            }
        }
    }
    if ($page < $counter - 1) {
        $pagination .= "<li class='page-item'><a class='page-link' href=\"$next\">&raquo;</a></li>";
    } else {
        $pagination .= "<li class='page-item'><a class=\"disabled page-link\">&raquo;</a></li>";
    }
    $pagination .= "</ul></nav>";
}

if ($cat_id == "" && $price == "" && $where == "") {
    ?>
    <div style="background-image:url('assets/holiday22/title-bg.png'); background-size:100%; position:relative; width:100%;">
        <div class="holiday-shortcut">
            <button class="btn btn-holiday-shortcut" type="button" data-toggle="collapse" data-target="#holiday-shortcut"
                    aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-bars text-light"></i>
            </button>
            <div class="collapse" id="holiday-shortcut">
                <div class="holiday-shortcut-body">
                    <div class="holiday-shortcut-item" onclick="goCategory(15)">
                        <img src="assets/holiday/icon-menu-1.png" class="holiday-shortcut-icon"> Holiday Collection
                    </div>
                    <!--<div class="holiday-shortcut-item">
                        <a href="#section-lifestyle">
                            <img src="assets/holiday/icon-menu-2.png" class="holiday-shortcut-icon"> Shop by Lifestyle
                        </a>
                    </div>-->
                    <div class="holiday-shortcut-item">
                        <a href="#section-value">
                            <img src="assets/holiday/icon-menu-3.png" class="holiday-shortcut-icon"> Shop by Value
                        </a>
                    </div>
                    <div class="holiday-shortcut-item" data-toggle="modal" data-target="#modalFinder">
                        <img src="assets/holiday/icon-menu-4.png" class="holiday-shortcut-icon"> Find The Perfect Gift
                    </div>
                </div>
            </div>
        </div>
        <div class="py-2">
            <h4 class="holiday-title green w-100 text-center p-0 m-0">Holiday Exclusive</h4>
            <div class="w-100 text-center green font-locci-serif p-0">Made and inspired by Provence</div>
        </div>
    </div>
    <div class="row justify-content-md-center w-100">
        <div class="col-3 text-center mt-3">
            <img src="assets/holiday22/icon-shea-golden-latte-min.png" class="img-fluid pb-2" onclick="goCategory(32)">
            <br>
            <div class="brown font-locci-sans-bl">Shea Golden Latte</div>
        </div>
        <div class="col-3 text-center mt-3">
            <img src="assets/holiday22/icon-shea-green-chestnut-min.png" class="img-fluid pb-2" onclick="goCategory(33)">
            <br>
            <div class="brown font-locci-sans-bl">Shea Green Chestnut</div>
        </div>
        <div class="col-3 text-center mt-3">
            <img src="assets/holiday22/icon-rose-pear-min.png" class="img-fluid pb-2" 
            <?if($_SESSION['outlet_id'] == '1' || $_SESSION['outlet_id'] == '4') {// Rose pear only appear when chosen branch are ION Orchard and Raffles City
                echo "onclick='goCategory(34)'";
            }?>>
            <br>
            <div class="brown font-locci-sans-bl">Rose Pear</div>
            <small class="font-locci-sans-bl">* only available at ION & Raffles City</small>
        </div>
        <div class="col-3 text-center mt-3">
            <img src="assets/holiday22/icon-holiday-edition-min.png" class="img-fluid pb-2" onclick="goCategory(35)">
            <br>
            <div class="brown font-locci-sans-bl">Holiday Edition</div>
        </div>
    </div>

    <div class="my-5 py-4 holiday-title-bg-2 w-100 text-center">
        <button type="button" class="btn btn-yellow font-locci-serif brown" data-toggle="modal" data-target="#modalFinder" style="outline: 2px dashed #a9302e; outline-offset: 8px; border-radius: 0;"><b>Find Gift
            in 60 Seconds!</b>
        </button>
    </div>

    <div style="background-image:url('assets/holiday22/title-bg.png'); background-size:100%; position:relative; width:100%;">
        <h4 class="holiday-title holiday-title-bg green w-100 text-center py-3 m-0">Advent Calendar</h4>
    </div>
    <!--<small class="w-100 text-center green">24 surprises, made by nature!
        We have wrapped up 24 of nature’s best gifts for you in this year’s edition of our bestselling Advent calendar.
    </small>-->
    <img src="assets/holiday22/banner-advent-calendar-min.png" class="img-fluid" onclick="goCategory(31)">
    
    <!--<h5 class="holiday-title green w-100 text-center" id="section-lifestyle">Shop by lifestyle</h5>
    <small class="w-100 text-center green">Made for everyone</small>
    <div class="row w-100 text-center">
        <div class="col-4 mt-3">
            <img src="assets/holiday/icon-go-getter.png" class="img-fluid" onclick="goCategory(25)">
        </div>
        <div class="col-4 mt-3">
            <img src="assets/holiday/icon-self-care.png" class="img-fluid" onclick="goCategory(26)">
        </div>
        <div class="col-4 mt-3">
            <img src="assets/holiday/icon-nature.png" class="img-fluid" onclick="goCategory(27)">
        </div>
        <div class="col-4 mt-3">
            <img src="assets/holiday/icon-fit.png" class="img-fluid" onclick="goCategory(28)">
        </div>
        <div class="col-4 mt-3">
            <img src="assets/holiday/icon-home-body.png" class="img-fluid" onclick="goCategory(29)">
        </div>
        <div class="col-4 mt-3">
            <img src="assets/holiday/icon-trend.png" class="img-fluid" onclick="goCategory(30)">
        </div>
    </div>-->
    <div style="background-image:url('assets/holiday22/title-bg.png'); background-size:100%; position:relative; width:100%;">
        <h4 class="holiday-title green w-100 text-center py-3 m-0" id="section-value">Shop by Value</h4>
    </div>
    <div class="row w-100 text-center">
        <div class="col-4 py-5">
            <a href="shop-holiday22?price=50" class="btn btn-price font-locci-sans-bl d-flex align-items-center" style="outline: 2px dashed #a9302e; outline-offset: 10px; border-radius: 0; height:100%;"><div class="mx-auto"><b>Below<br>SGD 50</b></div></a>
        </div>
        <div class="col-4 py-5">
            <a href="shop-holiday22?price=100" class="btn btn-price font-locci-sans-bl d-flex align-items-center" style="outline: 2px dashed #a9302e; outline-offset: 10px; border-radius: 0; height:100%;"><div class="mx-auto"><b>Between<br>SGD 50-100</b></div></a>
        </div>
        <div class="col-4 py-5">
            <a href="shop-holiday22?price=101" class="btn btn-price font-locci-sans-bl d-flex align-items-center" style="outline: 2px dashed #a9302e; outline-offset: 10px; border-radius: 0; height:100%;"><div class="mx-auto"><b>Above<br>SGD 100</b></div></a>
        </div>
    </div>
    <div style="background-image:url('assets/holiday22/title-bg.png'); background-size:100%; position:relative; width:100%;">
        <h4 class="holiday-title green w-100 text-center py-3 m-0">Shop by Categories</h4>
    </div>
    <div class="row w-100 text-center">
        <div class="col-4 mt-4 px-2">
            <!--<img src="assets/holiday22/icon-face-care-min.png" class="img-fluid" onclick="loadProduct(7)">-->
            <img src="assets/holiday22/icon-face-care-min.png" class="img-fluid" onclick="goCategory(1)">
        </div>
        <div class="col-4 mt-4 px-2">
            <!--<img src="assets/holiday22/icon-bath-body-min.png" class="img-fluid" onclick="loadProduct(9)">-->
            <img src="assets/holiday22/icon-bath-body-min.png" class="img-fluid" onclick="goCategory(3)">
        </div>
        <div class="col-4 mt-4 px-2">
            <!--<img src="assets/holiday22/icon-hair-care-min.png" class="img-fluid" onclick="loadProduct(8)">-->
            <img src="assets/holiday22/icon-hair-care-min.png" class="img-fluid" onclick="goCategory(2)">
        </div>
        <div class="col-4 mt-4 px-2">
            <!--<img src="assets/holiday22/icon-hand-care-min.png" class="img-fluid" onclick="loadProduct(10)">-->
            <img src="assets/holiday22/icon-hand-care-min.png" class="img-fluid" onclick="goCategory(4)">
        </div>
        <div class="col-4 mt-4 px-2">
            <!--<img src="assets/holiday22/icon-home-min.png" class="img-fluid" onclick="loadProduct(12)">-->
            <img src="assets/holiday22/icon-home-min.png" class="img-fluid" onclick="goCategory(16)">
        </div>
        <div class="col-4 mt-4 px-2">
            <!--<img src="assets/holiday22/icon-men-min.png" class="img-fluid" onclick="loadProduct(13)">-->
            <img src="assets/holiday22/icon-men-min.png" class="img-fluid" onclick="goCategory(24)">
        </div>
    </div>
    <div class="mt-5 py-4 holiday-title-bg-3 w-100 text-center">
        <button type="button" class="btn btn-yellow font-locci-serif brown" onclick="goCategory(15)" style="outline: 2px dashed #a9302e; outline-offset: 8px; border-radius: 0;">Explore the entire holiday
            range
        </button>
    </div>
    <?php
} else {

    if ($cat_id != "") {
        if (file_exists('../assets/holiday22/banner-' . $cat_id . '-min.png')) {
            ?>
            <div class="item">
                <img src="assets/holiday22/banner-<?=$cat_id?>-min.png?v=2"
                     class="img-fluid w-100 img-banner"/>
            </div>
            <?
        }
    }

    ?>
    
    <div class="w-100 text-center my-4">
        <a href="shop-holiday22" class="btn btn-yellow btn-sm mx-auto font-locci-serif brown" style="border-radius: 0;">Back to Home Page</a>
    </div>
    <?php
    $row_product = $resultProduct->numRows();
    while ($rs_product = $resultProduct->fetchRow()) {
        ?>
        <div class="col-5 col-sm-5 col-md-3 product-card text-center same-height">
            <div class="card">
                <a href="product-inner?id=<?= $rs_product['pkid'] ?>">
                    <div class="card-body text-center mb-3" id="product_<?= $rs_product['pkid'] ?>">
                        <h4 class="card-title"><img class="img-fluid product-img"
                                <?php if ($rs_product['img_url'] == "") { ?>
                                    src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                                <?php } else { ?> src="assets/product/<?= $rs_product['img_url'] ?>" <?php } ?>>
                        </h4>
                        <?
                        if (in_array($rs_product['pkid'], $promo_product_order)) {
                            foreach ($promotionClass->special_label() as $k2 => $v2) {
                                if ($v2['product_id'] == $rs_product['pkid']) {
                                    echo '<span class="badge badge-danger">' . $v2['special_label'] . '</span><br><br>';
                                }
                            }
                        }

                        if (in_array($rs_product['pkid'], $campaign_product_array)) {
                            echo '<span class="badge badge-danger">' . $campaign_label_array[$rs_product['pkid']] . '</span><br><br>';
                        }
                        ?>
                        <p class="card-text text-center product-title m-0">
                            <?= strtoupper($rs_product['title']) ?>
                        </p>
                        <p class="card-text text-center product-price m-0">
                            <?= $rs_product['size'] != "" ? $rs_product['size'] . " | " : "" ?>
                            <b>S$ <?= $rs_product['price'] ?></b>
                        </p>
                    </div>
                </a>
                <div class="card-footer">
                    <div class="row row-product-button">
                        <div class="col product-button text-center">
                            <?
                            if (strtotime($rs_product['start_date']) > strtotime($time_config['today']) && $rs_product['holiday_status'] = '1' && $rs_product['start_date'] != '2021-10-28') {
                                ?>
                                <span class="badge">AVAILABLE ON <?= strtoupper(date('d M', strtotime($rs_product['start_date']))) ?></span>
                                <br>
                                <?
                            } else {
                                if ($stockClass->check($rs_product['pkid']) === true) {
                                    if ($rs_product['pkid'] != '560') { //advent calendar only for pwp
                                        ?>
                                        <button class="btn btn-sm btn-blue" type="button"
                                                onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                                        </button>
                                    <? }
                                } else {
                                    if ($rs_product['pkid'] != '560') { //advent calendar only for pwp
                                    ?>
                                    <button class="btn btn-sm btn-blue" type="button"
                                            onclick="low_stock('<?= mysqli_real_escape_string($GLOBALS["mysqli_conn"], $rs_product['title']); ?>');">
                                        <i class="fa fa-exclamation-circle"></i> LOW STOCK
                                    </button>
                                    <?
                                    }
                                }
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }


    if ($row_product == 0) {
        ?>
        <p>No product available.</p>
        <br>
        <div class="pt-4 pb-4 holiday-title-bg-2 w-100 text-center">
            <button type="button" class="btn btn-red font-gandhi" data-toggle="modal" data-target="#modalFinder">Find
                other gift
            </button>
        </div>
        <?php
    }

    if ($cat_id == "31") { // advent calendar ?>
        <br>
        <div class="col-12 pt-4 pb-4">
            <div class="mx-auto" style="max-width:640px;">
            <small>
            ** Advent calendar is only valid with purchase of $45 nett spend.<br>
            ** Limited to 2 redemption per transaction.<br>
            ** Please note that Purchase With Purchase will not be entitled for gift with purchases.<br>
            ** Other T&amp;Cs apply.<br>
            </small>
            </div>
        </div>
        <? } 
    ?>
    <div class="col-12">
        <?= $pagination ?>
    </div>
    <?php
}
?>
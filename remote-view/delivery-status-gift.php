<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['oid']);

$resulOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resulOrder->fetchRow();

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $rs_order['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();
?>
    <style>
        table.mcnCaptionRightTextContentContainer {
            width: 50%;
        }
    </style>
    <p>Order ID #LDSG20<?= $order_id ?></p>
    <p>Placed on <?= $rs_order['created_date'] ?></p>
    <p>Order Value S$<?= number_format($rs_order['total_amount'], 2) ?></p>
    <p>Type of
        Service: <?= $rs_order['shipping_method'] == "pickup" ? "Click & Collect at " . $rs_outlet['title'] : "Delivery" ?></p>
<? if ($rs_order['shipping_method'] == "delivery") { ?>
    <p>Lalamove will pick up your order on <?= $rs_order['pickup_date'] . " " . $rs_order['pickup_time'] ?></p>

    <ul id="progressbar" class="mb-5 mt-5">
        <li class="<?= $rs_order['status'] >= 1 ? "active" : "" ?>"><i class="fa fa-credit-card"></i><strong>Payment
                received</strong></li>
        <li class="<?= $rs_order['status'] >= 2 ? "active" : "" ?>"><i class="fa fa-server"></i><strong>Order
                received</strong></li>
        <li class="<?= $rs_order['status'] >= 4 ? "active" : "" ?>"><i class="fa fa-truck"></i><strong>Shipped</strong>
        </li>
        <li class="<?= $rs_order['status'] >= 5 ? "active" : "" ?>"><i
                    class="fa fa-check"></i><strong>Completed</strong></li>
    </ul>
    <div class="col-12 mt-3 text-center mx-auto">
        <?php //echo file_get_contents($site_config['full_url']."admin/html/order-item?order_id=".$rs_order['pkid']) ?>
    </div>
    <?php if ($rs_order['campaign_id'] == "0") { ?>
        <div class="col-12 mt-3 text-center mx-auto">
            <h4 class="h_status"></h4>
            <div class="div_rider">
                <hr>
                <h4>Driver Info</h4>
                <img src="https://www.rightlydigital.com/wp-content/uploads/2019/08/Lalamove.png"
                     class="img-fluid rounded-circle img_pic w-25"/>
                <p class="p_name mt-3 m-0"></p>
                <p class="p_mobile m-0"></p>
                <p class="p_plate"></p>
            </div>
            <div class="div_map">
                <div id="map" style="height: 50vh"></div>
            </div>
        </div>
    <?php } ?>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQEDktCHtvfMoFOFEQx_7Z1vIrgFVewFM&libraries=places&callback=initMap"
            async defer></script>
    <script>
        var rider_location, map, icons, status, marker, markers = [];
        var status_text = {
            ASSIGNING_DRIVER: 'Finding driver',
            ON_GOING: 'Driver assigned, your order will get pick-up on <?= $rs_order['pickup_date'] . " " . $rs_order['pickup_time'] ?>',
            CANCELED: 'Delivery cancelled, please contact us for assistant',
            PICKED_UP: 'Driver has picked up your item and heading to you',
            REJECTED: 'Driver has rejected, we will find you another driver',
            COMPLETED: 'Successfully delivered'
        };

        $(document).ready(function () {
            loadOrder();
            loadRider();
            setInterval(function () {
                loadOrder();
                loadRider();
            }, 5000);
        });

        function loadOrder() {
            $.ajax({
                method: "GET",
                url: "ajax/lalamove-order-details-new?order_id=<?=$order_id?>",
                dataType: 'json',
            })
                .done(function (data) {
                    console.log(data);

                    if (status != "" && data.status.replace("_", " ") != status) {
                        Swal.fire({
                            title: data.status.replace("_", " "),
                            text: status_text[data.status],
                        })
                    }

                    status = data.status.replace("_", " ");

                    if (status == "ON GOING") {
                        $(".h_status").html("DRIVER ASSIGNED");
                    } else {
                        $(".h_status").html(status);
                    }
                    status = status;

                    if (status == "CANCELED" || status == "REJECTED" || status == "COMPLETED" || status == "EXPIRED") {
                        $(".div_map").hide();
                        $(".div_rider").hide();
                    }
                });
        }

        function loadRider() {
            if (status == "CANCELED" || status == "REJECTED" || status == "COMPLETED" || status == "EXPIRED") {
                $(".div_map").hide();
                $(".div_rider").hide();
                return false;
            }

            $.ajax({
                method: "POST",
                url: "ajax/lalamove-driver-new?order_id=<?=$order_id?>",
                dataType: 'json',
            })
                .done(function (data) {
                    if (data === null) {
                        $(".div_rider").hide();
                    } else {
                        $(".div_rider").show();
                        $(".p_name").html(data.name);
                        $(".p_mobile").html(data.phone);
                        $(".p_plate").html(data.plateNumber);
                        if (data.photo != "") {
                            $(".img_pic").attr('src', data.photo);
                        }

                        if (markers.length == 0) {
                            marker = new google.maps.Marker({
                                position: {lat: parseFloat(data.location.lat), lng: parseFloat(data.location.lng)},
                                map: map,
                                icon: icons.rider,
                                animation: google.maps.Animation.DROP,
                            });
                            marker.setMap(map);
                            markers.push(marker);
                        }

                        marker.setPosition(new google.maps.LatLng(parseFloat(data.location.lat), parseFloat(data.location.lng)));
                        // map.panTo(new google.maps.LatLng(parseFloat(data.location.lat), parseFloat(data.location.lng)));

                        if (status == "ON GOING") {
                            var latlng = [
                                new google.maps.LatLng(<?=$rs_outlet['lat']?>, <?=$rs_outlet['lng']?>),
                                new google.maps.LatLng(parseFloat(data.location.lat), parseFloat(data.location.lng)),
                            ];
                        } else {
                            var latlng = [
                                new google.maps.LatLng(<?=$rs_order['lat']?>, <?=$rs_order['lng']?>),
                                new google.maps.LatLng(parseFloat(data.location.lat), parseFloat(data.location.lng)),
                            ];
                        }
                        var latlngbounds = new google.maps.LatLngBounds();
                        for (var i = 0; i < latlng.length; i++) {
                            latlngbounds.extend(latlng[i]);
                        }
                        map.fitBounds(latlngbounds);
                    }
                });
        }

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                mapTypeControl: false,
                center: {lat: 1.3519852, lng: 103.7495453},
                zoom: 15,
                disableDefaultUI: true
            });

            map.setZoom(map.getZoom());

            icons = {
                start: new google.maps.MarkerImage(
                    'assets/img/store-icon.png',
                    // (width,height)
                    new google.maps.Size(50, 50),
                    // The origin point (x,y)
                    new google.maps.Point(0, 0),
                    // The anchor point (x,y)
                    new google.maps.Point(22, 32)
                ),
                end: new google.maps.MarkerImage(
                    'assets/img/house-icon.png',
                    // (width,height)
                    new google.maps.Size(50, 50),
                    // The origin point (x,y)
                    new google.maps.Point(0, 0),
                    // The anchor point (x,y)
                    new google.maps.Point(22, 32)
                ),
                rider: new google.maps.MarkerImage(
                    'assets/img/icon-rider.png',
                    // (width,height)
                    new google.maps.Size(50, 50),
                    // The origin point (x,y)
                    new google.maps.Point(0, 0),
                    // The anchor point (x,y)
                    new google.maps.Point(22, 32)
                )
            };

            new AutocompleteDirectionsHandler(map);
        }

        function AutocompleteDirectionsHandler(map) {
            this.map = map;
            this.travelMode = 'DRIVING';
            this.directionsService = new google.maps.DirectionsService;
            this.directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true});
            this.directionsRenderer.setMap(map);

            var me = this;

            this.directionsService.route(
                {
                    origin: {'lat': <?=$rs_outlet['lat']?>, 'lng': <?=$rs_outlet['lng']?>},
                    destination: {'lat': <?=$rs_order['lat']?>, 'lng': <?=$rs_order['lng']?>},
                    travelMode: 'DRIVING'
                },
                function (response, status) {
                    if (status === 'OK') {
                        me.directionsRenderer.setDirections(response);
                        var leg = response.routes[0].legs[0];
                        makeMarker(leg.start_location, icons.start);
                        makeMarker(leg.end_location, icons.end);
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                });
        }

        function makeMarker(position, icon) {
            var marker = new google.maps.Marker({
                position: position,
                map: map,
                icon: icon,
                animation: google.maps.Animation.DROP,
            });
        }
    </script>
<? } else {
    ?>
    <ul id="progressbar" class="pickup mb-5 mt-5">
        <li class="<?= $rs_order['status'] >= 1 ? "active" : "" ?>"><i class="fa fa-credit-card"></i><strong>Payment
                received</strong></li>
        <li class="<?= $rs_order['status'] >= 3 ? "active" : "" ?>"><i class="fa fa-shopping-bag"></i><strong>Ready for
                pick-up
            </strong></li>
        <li class="<?= $rs_order['status'] >= 5 ? "active" : "" ?>"><i class="fa fa-check"></i><strong>Pick-up completed
            </strong>
        </li>
    </ul>
    <?
} ?>
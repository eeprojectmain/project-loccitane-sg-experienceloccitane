<?php

function countDeletion($str)
{
    $count = 0;
    $prevChar = "";
    $length = strlen($str);
    $strArr = str_split($str);

    for($i=0; $i<$length; $i++)
    {
        $tmp = $strArr[$i];
        if($tmp == $prevChar)
        {
            $count++;
        }
        $prevChar = $tmp;
    }

    return $count;
}

$inputs = [
    "AAAA",
    "BBBBB",
    "ABABABAB",
    "AAABBB",
    "AABBAABB",
];

foreach ($inputs as $v) {
    echo countDeletion($v.'<br>');
    echo '<br>';
}
?>
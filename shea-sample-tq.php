<?php

require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;

$databaseClass = new database();
$apiClass = new api();

$pkid = $_SESSION['sample_shea']['done'];

if ($pkid == "") {
    header("Location: ".$site_config['sample_shea']);
    exit();
}

$resultSample = get_query_data($table['sample_shea'], "pkid=$pkid");
$rs_sample = $resultSample->fetchRow();

$postfield['first_name'] = $rs_sample['first_name'];
$postfield['last_name'] = $rs_sample['last_name'];
$postfield['mobile'] = $rs_sample['mobile'];
$postfield['email'] = $rs_sample['email'];
$postfield['sms_status'] = $rs_sample['sms_status'];
$postfield['email_status'] = $rs_sample['email_status'];
$postfield['call_status'] = $rs_sample['call_status'];
$postfield['whatsapp_status'] = $rs_sample['whatsapp_status'];
$postfield['source_id'] = '4936';
// $postfield['source_id'] = '4516';

$apiClass->create_sample_profile($postfield);

unset($_SESSION['iglive']);

?>

<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">SHEA SAMPLE KIT</h4>
                <p>Thank you for signing up!</p>
                <p>Please present a screenshot of this confirmation page to redeem your sample kit in stores.</p>
            </div>
        </div>
        <div class="col-12">
            <img src="assets/img/shea-sample.jpg" class="img-fluid" style="max-width: 600px;" />
        </div>
        <div class="col-12">
            <br><br>
        </div>
        <div class="col-12">
            <b>First Name: </b> <?= $rs_sample['first_name'] ?><br><br>
            <b>Last Name: </b> <?= $rs_sample['last_name'] ?><br><br>
            <b>Mobile: </b> <?= $rs_sample['mobile'] ?><br><br>
            <b>Redeem by: </b> <?= date('d M Y', strtotime($rs_sample['created_date'] . ' +2 week')) ?>
        </div>
        <div class="col-12 mt-4">
            <div class="border border-dark p-2">
                <p><b><u>Terms & Conditions for Sampling</u></b></p>

                <ol>
                    <li>Limited to one sample kit per customer, while stocks last.</li><br>

                    <li>Valid for redemption at all L'OCCITANE boutiques except L'OCCITANE web store, Lazada, Shopee, Krisshop and Duty-Free Stores.</li><br>

                    <li>Non-transferrable and no collection is allowed on behalf of others. OTP verification to registered phone number is required during collection of sample kit in stores.</li><br>

                    <li>Not exchangeable for cash.</li><br>

                    <li>Only applicable for customers who have not redeemed this sample kit.</li><br>

                    <li>L'OCCITANE Singapore reserves the final right to alter and/or withdraw the items, terms & conditions without prior notice.</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
<div class="row mt-3">
    <div class="col-12 pl-4 pr-4">
        <div class="checkin-progress">
            <div class="line-container">
                <div class="progress-line">
                    <div class="progressbar" style="width: calc( 100% / 2 * 1 )">
                    </div>
                    <div class="status <?=$nav_step>="1"?"completed":""?>">
                        <div class="dot">
                            1
                        </div>
                        <span>Details</span>
                    </div>
                    <div class="status <?=$nav_step>="2"?"completed":""?>">
                        <div class="dot">
                            2
                        </div>
                        <span>Delivery Method</span>
                    </div>
                    <div class="status <?=$nav_step>="3"?"completed":""?>">
                        <div class="dot">
                            3
                        </div>
                        <span>Payment</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
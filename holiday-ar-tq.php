<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$pkid = $_SESSION['holiday_ar']['id'];

$resultHoliday = get_query_data($table['holiday_ar'], "pkid=$pkid");
$rs_holiday = $resultHoliday->fetchRow();

if($_POST){
    $postfield=$_POST;

    unset($postfield['submit_form']);

    if ($postfield['gift_message'] != "" && $postfield['gift_mobile'] != "") {
        $queryUpdate=get_query_update($table['holiday_ar'],$rs_holiday['pkid'],$postfield);
        $databaseClass->query($queryUpdate);

        $msg = $postfield['gift_message'];
        $mobile = $postfield['gift_mobile'];
        if (preg_match("/^6/", $mobile)) {
            $mobile = ltrim("65", $mobile);
        } elseif (preg_match("/^+/", $mobile)) {
            $mobile = str_replace("+", "", $mobile);
        }
        $mobile = str_replace("-", "", $mobile);
        $mobile = str_replace(" ", "", $mobile);
        $mobile = "65" . $mobile;

        echo '<script>';
        echo 'window.top.location.href="https://api.whatsapp.com/send/?phone='.$mobile.'&text='.$postfield['gift_message'].'";';
        echo '</script>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <div class="row mt-4">
        <div class="col-12 text-center">
            <p>Redeem your treat with the special gift code sent to your email!</p>
        </div>
        <div class="col-12">
        <form action="holiday-ar-tq" method="post" class="mx-auto formValidation">
            <label class="w-100 text-center">Send a Thank you note to your gifter via WhatsApp!</label>
            <div class="form-group">
                    <textarea name="gift_message" class="form-control" rows="3" placeholder="Thank you message"
                              maxlength="153"></textarea>
            </div>
            <div class="form-group">
                <input type="tel" class="form-control" placeholder="MOBILE SEND TO" name="gift_mobile"
                       minlength="8"
                       maxlength="9">
            </div>
            <div class="form-group mt-5 pb-5 text-center">
                <button type="submit" name="submit_form" value="online" class="btn btn-darkblue w-50">SEND
                </button>
            </div>
        </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
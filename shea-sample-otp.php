<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

/*$row_check = get_query_data_row($table['sample_shea'], "date(created_date) >= date('2021-08-19')");
if ($row_check >= 300) {
    header("Location: " . $site_config['sample_shea']);
    exit();
}*/

if ($_SESSION['iglive']['mobile'] == "" || $_SESSION['iglive']['otp_request'] == "") {
    header("Location: " . $site_config['sample_shea']);
    exit();
}

if ($_POST) {
    header("Location: " . $site_config['sample_shea'] . "-details");
    exit();
}

$mobile = $_SESSION['iglive']['mobile'];

$resultOtp = get_query_data($table['otp'], "mobile='$mobile' order by pkid desc limit 1");
$rs_otp = $resultOtp->fetchRow();

?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">SHEA SAMPLE KIT</h4>
                <p>Made with 25% Shea Butter, the L’OCCITANE Shea Butter Ultra Rich Body Cream is dermatologist-approved to be suitable for sensitive skin and children over 3.</p>
            </div>
        </div>
        <div class="col-12 mt-4">
            <label class="w-100 text-center">A SMS verification code has been sent to your mobile.<br/>Please key in the
                verification code to proceed. </label>
            <form action="<?= $site_config['sample_shea'] ?>-otp" method="post" class="w-80 mx-auto formIglive">
                <div class="form-group">
                    <input type="number" class="form-control" placeholder="SMS verification code" name="otp_code"
                           required
                           minlength="3" maxlength="3">
                </div>
                <div class="form-group text-center">
                    <button type="button" class="btn btn-blue btn-resend">Click to resend code <span
                                id="timer"></span></button>
                </div>
                <div class="form-group mt-5 pb-5 text-center">
                    <button type="submit" name="submit_mobile" value="true" class="btn btn-darkblue w-50">NEXT
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(document).ready(function () {
        var time = '<?=date('Y-m-d\TH:i:s', strtotime($rs_otp['created_date'] . ' +60 sec'));?>';
        var countDownDate = new Date(time).getTime();

        $(".btn-resend").attr('disabled', 'disabled');

        var x = setInterval(function () {
            var now = new Date().getTime();

            var timeCD = countDownDate - now;
            timeCD = parseInt(timeCD) || 0;
            var seconds = moment.duration(timeCD).seconds();

            if (timeCD <= 1) {
                clearInterval(x);
                $(".btn-resend").attr('disabled', false);
                $("#timer").hide();
            } else {
                $("#timer").text("(" + seconds + ")");
            }
        }, 1000);

        $(".btn-resend").on('click', function (e) {
            $.ajax({
                method: "POST",
                url: "ajax/iglive-otp-resend",
                data: {'type': 'shea'},
                dataType: "json",
            })
                .done(function (data) {
                    Swal.fire({
                        title: data.title,
                        text: data.message,
                        icon: data.result,
                    });
                });
        });

        $(".formIglive").each(function (index, element) {
            form = $(".formIglive")[index];
            fv = FormValidation.formValidation(
                form, {
                    fields: {
                        otp_code: {
                            message: 'This field is required',
                            validators: {
                                remote: {
                                    message: 'Incorrect code',
                                    url: '/ajax/iglive-otp-check',
                                    method: 'POST',
                                }
                            }
                        }
                    },
                    plugins: {
                        declarative: new FormValidation.plugins.Declarative({
                            html5Input: true,
                        }),
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        excluded: new FormValidation.plugins.Excluded(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fal fa-check',
                            invalid: 'fal fa-times',
                            validating: 'fal fa-refresh'
                        }),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                    },
                }
            ).on('core.form.valid', function () {
                $("button[type='submit']").attr('disabled', 'disabled');
            });
        });
    });
</script>
</body>
</html>
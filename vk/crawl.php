<?php

echo scandir("http://18.231.158.99/resources/metronic/plugins/formvalidation/dist/");
exit();
/**
 * Grab pictures
 * Class download_image
 * Time: 2019/1/31 13:49
 * Author Jiangnan Geek
 */
class download_image
{
    public $save_path; //Grab the saved address of the image
    //Grab the size limit of the image (unit: byte). Only capture images larger than this limit.
    public $img_size = 0;
    public static $a_url_arr = array();
    // Is it from WeChat?
    public $is_from_wx = false;

    /**
     * @param String $save_path The saved address of the captured image
     * @param Int $img_size The saved address of the captured image
     */
    public function __construct($save_path, $img_size)
    {
        $this->save_path = $save_path;
        $this->img_size = $img_size;
    }

    /**
     * Recursive download method for crawling the home page and its subpage images (recursive recursion)
     *
     * @param String $capture_url URL to fetch images
     *
     */
    public function recursive_download_images($capture_url)
    {
        if (!in_array($capture_url, self::$a_url_arr)) //Not crawled
        {
            Self::$a_url_arr[] = $capture_url; //count in a static array
        } else //Crawled, exit the function directly
        {
            return;
        }
        $this->download_current_page_images($capture_url); //Download all images of the current page
        $content = @file_get_contents($capture_url);
        $a_pattern = "|<a[^>]+href=['\" ]?([^ '\"?]+)['\" >]|U";
        preg_match_all($a_pattern, $content, $a_out, PREG_SET_ORDER);
        $tmp_arr = array(); //Define an array to store the hyperlink address of the captured image under the current loop
        foreach ($a_out as $k => $v) {
            /**
             * Remove empty '', '#', '/' and duplicate values ​​in the hyperlink
             * 1: The value of the hyperlink address cannot be equal to the url of the currently crawled page, otherwise it will fall into an infinite loop.
             * 2: The hyperlink is '' or '#', '/' is also this page, which will also fall into an infinite loop.
             * 3: Sometimes a hyperlink address will be repeated multiple times in a web page. If it is not removed, a subpage will be repeatedly downloaded.
             */
            if ($v[1] && !in_array($v[1], self::$a_url_arr) && !in_array($v[1], array('#', '/', $capture_url))) {
                $tmp_arr[] = $v[1];
            }
        }
        foreach ($tmp_arr as $k => $v) {
            // hyperlink path address
            if (strpos($v, 'http://') !== false || strpos($v, 'https://') !== false) //If the url contains http://, you can access it directly
            {
                $a_url = $v;
            } else // Otherwise the proof is a relative address, you need to re-paste the access address of the hyperlink
            {
                $domain_url = substr($capture_url, 0, strpos($capture_url, '/', 8) + 1);
                $a_url = $domain_url . $v;
            }
            $this->recursive_download_images($a_url);
        }
    }

    /**
     * Download all images under the current page
     *
     * @param String $capture_url The web address used to fetch the image
     * @return Array An array of all image img tag url addresses on the current page
     */
    public function download_current_page_images($capture_url)
    {
        if (strpos($capture_url, 'mp.weixin.qq.com')) {
            $this->is_from_wx = true;
        }
        $content = @file_get_contents($capture_url); //block warning error
        $img_pattern = "|<img[^>]+src=['\" ]?([^ '\"?]+)['\" >]|U";
        if ($this->is_from_wx) {
            $img_pattern = "/<[img|IMG].*?data-src=[\'|\"](.*?)[\'|\"].*?[\/]?>/";
        }
        preg_match_all($img_pattern, $content, $img_out, PREG_SET_ORDER);
        $photo_num = count($img_out);
        echo '<h1>' . $capture_url . "Total found " . $photo_num . " Image </h1>";
        foreach ($img_out as $k => $v) {
            $this->save_one_img($capture_url, $v[1]);
        }
    }

    /**
     * How to save a single image
     *
     * @param String $capture_url The web address used to fetch the image
     * @param String $img_url The url of the image to be saved
     *
     */
    public function save_one_img($capture_url, $img_url)
    {
        //Image path address
        if (strpos($img_url, 'http://') !== false || strpos($img_url, 'https://') !== false) {
            // $img_url = $img_url;
        } else {
            $domain_url = substr($capture_url, 0, strpos($capture_url, '/', 8) + 1);
            $img_url = $domain_url . $img_url;
        }
        $pathinfo = pathinfo($img_url); //Get image path information
        $pic_name = $pathinfo['basename']; //Get the name of the image
        if ($this->is_from_wx) {
            //https://mmbiz.qpic.cn/mmbiz_png/ibTbTlOaBfVCHKn0j4SJicuOa6HPSBXoebtp2h9cvpevf3Z3icrQzibNkc5txErJY1GwkSEsOklnl7sLzUl6oOds6Q/640?wx_fmt=png
            //Image suffix
            $file_ext = strrchr($pic_name, '=');
            $img_ext = ltrim($file_ext, '=');

            //Image name
            $dir_name = $pathinfo['dirname'];
            $img_name = substr(strrchr($dir_name, "/"), 1);
            $pic_name = $img_name . '.' . $img_ext;
        }
        if (file_exists($this->save_path . $pic_name)) //If the image exists, the proof has been fetched, exit function
        {
            echo $img_url . '<span style="color:red;margin-left:80px">This image has been crawled!</span><br/>';
            return;
        }
        $img_data = @file_get_contents($img_url); //block out warning errors caused by unreadable image addresses
        if (strlen($img_data) > $this->img_size) //Download images larger than the limit
        {
            $img_size = file_put_contents($this->save_path . $pic_name, $img_data);
            if ($img_size) {
                echo $img_url . '<span style="color:green;margin-left:80px">Image saved successfully!</span><br/>';
            } else {
                echo $img_url . '<span style="color:red;margin-left:80px">Image save failed!</span><br/>';
            }
        } else {
            echo $img_url . '<span style="color:red;margin-left:80px">Image reading failed!</span><br/>';
        }
    }
} // END

Set_time_limit(120);

$save_path = __DIR__ . '/formv/'; //Image local save path
//$capture_url = 'https://ewei.shop/'; //URL to crawl
$capture_url = 'http://18.231.158.99/resources/metronic/plugins/formvalidation/dist/'; //Public Article
$download_img = new download_image($save_path, 0); //Instantiate download image object
$download_img->recursive_download_images($capture_url); //Recursively grab image method
print_r($download_img);
//$download_img->download_current_page_images($capture_url); //Only grab the current page image method
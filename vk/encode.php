<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/vendor/autoload.php";
global $table;
$databaseClass = new database();

use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\Algorithm\RS256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128GCM;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEBuilder;
use Jose\Component\Signature\Serializer\CompactSerializer;

$key_private = JWKFactory::createFromKeyFile(
	__DIR__ . '/../vendor/vk_private.key'
);

$payload=json_decode('{
  "auth_user_app_version": "3.0.43",
  "auth_user_os": "android",
  "auth_user_device_version": "9",
  "logged_user": "0148567624",
  "user_accounts": [
    "0148567624",
    "0136484772",
    "0198172016",
    "0136759018",
    "0148567624"
  ],
  "prepos_indicator": "Prepaid",
  "is_Corporate": false,
  "auth_user_device_id": "1d343b8356d269f0",
  "iss": "https://securetoken.google.com/celcom-life-app-production",
  "aud": "celcom-life-app-production",
  "auth_time": 1670666900,
  "user_id": "H5ATy69OYrcQHHPb5bOCoOSgaYb2",
  "sub": "H5ATy69OYrcQHHPb5bOCoOSgaYb2",
  "iat": 1670666900,
  "exp": 1670670500,
  "phone_number": "+60148567624",
  "firebase": {
    "identities": {
      "phone": [
        "+60148567624"
      ]
    },
    "sign_in_provider": "custom"
  }
}', true);

$payload=json_encode($payload);
// The algorithm manager with the HS256 algorithm.
$algorithmManager = new AlgorithmManager([
	new RS256(),
]);

// We instantiate our JWS Builder.
$jwsBuilder = new JWSBuilder($algorithmManager);

$jws = $jwsBuilder
	->create()// We want to create a new JWS
	->withPayload($payload)// We set the payload
	->addSignature($key_private, ['alg' => 'RS256', 'kid' => '951c08c516ae352b89e4d2e0e14096f734942a87', 'typ' => 'JWT'])// We add a signature with a simple protected header
	->build();
$serializer = new CompactSerializer(); // The serializer
$token = $serializer->serialize($jws, 0);

echo $token;
?>
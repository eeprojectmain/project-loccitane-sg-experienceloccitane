<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$api_url = 'https://api.apaylater.net/v2';

$amount = '52';

$ref_id = uniqid();

$postfield = array(
    'referenceId' => $ref_id,
    'currency' => 'SGD',
    'amount' => $amount * 100,
    'callbackUrl' => 'https://sg-new.experienceloccitane.com/payment/atome-callback',
    'paymentResultUrl' => 'https://sg-new.experienceloccitane.com/payment-success',
    'paymentCancelUrl' => 'https://sg-new.experienceloccitane.com/payment-fail',
    'merchantReferenceId' => 'LSG256',
    'customerInfo' => array(
        'mobileNumber' => "+6587654321",
        'fullName' => 'Vincent Test',
        'email' => 'vincentlee@wowsome.com.my'
    ),
    'shippingAddress' => array(
        'countryCode' => 'SG',
        'lines' => array(
            '100 Woodlands Ave 5',
            'Singapore 739010'
        ),
        'postCode' => '739010'
    ),
    'billingAddress' => array(
        'countryCode' => 'SG',
        'lines' => array(
            '100 Woodlands Ave 5',
            'Singapore 739010'
        ),
        'postCode' => '739010'
    ),
    'taxAmount' => (($amount * 100) / 100) * 7,
    'shippingAmount' => 1200,
    'originalAmount' => $amount * 100,
    'items' => array(
        array(
            'itemId' => 'D123',
            'name' => 'Reset kit',
            'quantity' => 1,
            'price' => $amount * 100
        )
    )
);

/*$ch = curl_init($api_url . '/payments');
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postfield));
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_USERPWD, '28551ece56fb4274ad634444201a8289:fc57552c3712402e81ae869c5d06a54e');
curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

$result = json_decode($result, true);

header("Location: " . $result['redirectUrl']);
exit();*/

$api_url = 'https://api.apaylater.net/v2';

$ch = curl_init($api_url . '/payments/294LOSG610b5a881315e');
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_USERPWD, '28551ece56fb4274ad634444201a8289:fc57552c3712402e81ae869c5d06a54e');
curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

$result=json_decode($result,true);

print_r($result);
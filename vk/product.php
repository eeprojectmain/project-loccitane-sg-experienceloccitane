<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$resultProduct=get_query_data($table['product'],"title like '%ML%'");
while($rs_product=$resultProduct->fetchRow()){
    $size=explode(" ",$rs_product['title']);
    if(preg_match("/ML/",end($size))){
        $db_size=end($size);
        $db_title=str_replace(" ".$db_size,"",$rs_product['title']);

        $queryUpdate=get_query_update($table['product'],$rs_product['pkid'],array('title'=>$db_title,'size'=>$db_size));
        $databaseClass->query($queryUpdate);
    }
}
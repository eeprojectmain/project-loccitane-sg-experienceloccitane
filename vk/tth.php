<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$cegid_mobile = file_get_contents("tth.txt");
$cegid_mobile = explode(PHP_EOL, $cegid_mobile);
$cegid_mobile = array_filter($cegid_mobile);

foreach ($cegid_mobile as $v) {
    $mobile = str_replace("+", "", $v);
    $mobile = ltrim($mobile, '65');
    $mobile = "+65" . $mobile;

    if (!in_array($mobile, $sms_array)) {
        $sms_array[] = $mobile;
    }
}

foreach ($sms_array as $v) {
    //35
    $postfield = array(
        'status' => '1',
        'type' => 'game',
        'mobile' => $v,
        'voucher_id' => '35',
        'created_date' => date('Y-m-d H:i:s'),
    );
    $queryInsert = get_query_insert($table['member_voucher'], $postfield);
//    $databaseClass->query($queryInsert);
}

echo 'done - ' . count($sms_array);
exit();
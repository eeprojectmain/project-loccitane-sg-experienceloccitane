<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$promotionClass = new promotion();
$stockClass = new stock();

$nav_step='1';

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $_SESSION['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

$pwp_array = $promotionClass->pwp();
$got_product=false;
foreach ($pwp_array as $k=>$v){
    $v['product_id']=array_filter($v['product_id']);
    if(count($v['product_id'])!="0"){
        $got_product=true;
    }
}
if($got_product==false){
    header("Location: checkout-shipping");
    exit();
}

foreach($promotionClass->special_label() as $k=>$v){
    $promo_product_order[]=$v['product_id'];
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4>Add ons!</h4>
                <p class="m-0">Only applicable with purchase of $45 nett spend </p>
                <p>Limited to 2 purchase with purchase per transaction/customer.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-4">
            <div class="row justify-content-center">
                <?php
                foreach ($pwp_array as $k => $v) {
                    foreach ($v['product_id'] as $k2 => $v2) {
                        $resultProduct = get_query_data($table['product'], "pkid=" . $v2);
                        $rs_product = $resultProduct->fetchRow();
                        ?>
                        <div class="col-5 col-sm-5 col-md-3 product-card text-center same-height">
                            <div class="card">
                                <a href="product-inner?id=<?= $rs_product['pkid'] ?>">
                                    <div class="card-body text-center mb-3" id="product_<?= $rs_product['pkid'] ?>">
                                        <h4 class="card-title"><img class="img-fluid product-img"
                                                <?php if ($rs_product['img_url'] == "") { ?>
                                                    src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                                                <?php } else { ?> src="assets/product/<?= $rs_product['img_url'] ?>" <?php } ?>>
                                        </h4>
                                        <?
                                        if (in_array($rs_product['pkid'], $promo_product_order)) {
                                            foreach ($promotionClass->special_label() as $k2 => $v2) {
                                                if ($v2['product_id'] == $rs_product['pkid']) {
                                                    echo '<span class="badge badge-danger">' . $v2['special_label'] . '</span><br><br>';
                                                }
                                            }
                                        }

                                        if (in_array($rs_product['pkid'], $campaign_product_array)) {
                                            echo '<span class="badge badge-danger">' . $campaign_label_array[$rs_product['pkid']] . '</span><br><br>';
                                        }
                                        ?>
                                        <p class="card-text text-center product-title m-0">
                                            <?= strtoupper($rs_product['title']) ?>
                                        </p>
                                        <p class="card-text text-center product-price m-0">
                                            <?= $rs_product['size'] != "" ? $rs_product['size'] . " | " : "" ?>
                                            <b>S$ <?= $rs_product['price'] ?></b>
                                        </p>
                                    </div>
                                </a>
                                <!--<div class="card-footer">
                                    <div class="row row-product-button">
                                        <div class="col product-button text-center">
                                            <?
                                            if (strtotime($rs_product['start_date']) > strtotime($time_config['today']) && $_SESSION['campaign_id']=="8" && $rs_product['start_date']!='2021-10-28' && $rs_product['start_date']!='2021-12-01' && $rs_product['holiday_status']='1') {
                                                ?>
                                                <span class="badge">AVAILABLE ON <?= strtoupper(date('d M', strtotime($rs_product['start_date']))) ?></span>
                                                <?
                                            } else {
                                                if ($stockClass->check($rs_product['pkid']) === true) {
                                                    ?>
                                                    <button class="btn btn-sm btn-blue" type="button"
                                                            onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                                                    </button>
                                                <? } else {
                                                    ?>
                                                    <button class="btn btn-sm btn-blue" type="button"
                                                            onclick="low_stock('<?=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $rs_product['title']); ?>');">
                                                        <i class="fa fa-exclamation-circle"></i> LOW STOCK
                                                    </button>
                                                    <?
                                                }
                                            } ?>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="card-footer">
                                    <div class="row row-product-button">
                                        <div class="col product-button text-center">
                                            <?
                                            if (strtotime($rs_product['start_date']) > strtotime($time_config['today']) && $rs_product['start_date']!='2023-10-26' && $rs_product['start_date']!='2023-10-30' && $rs_product['holiday_status']='1') {
                                                ?>
                                                <span class="badge">AVAILABLE ON <?= strtoupper(date('d M', strtotime($rs_product['start_date']))) ?></span>
                                                <?
                                            } else {
                                                if ($stockClass->check($rs_product['pkid']) === true) {
                                                    ?>
                                                    <button class="btn btn-sm btn-blue" type="button"
                                                            onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                                                    </button>
                                                <? } else {
                                                    ?>
                                                    <!--<button class="btn btn-sm btn-blue" type="button"
                                                            onclick="low_stock('<?=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $rs_product['title']); ?>');">
                                                        <i class="fa fa-exclamation-circle"></i> LOW STOCK
                                                    </button> cegid-hezril-bypass -->
                                                    <button class="btn btn-sm btn-blue" type="button"
                                                            onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                                                    </button>
                                                    <?
                                                }
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? }
                } ?>
            </div>
            <div class="form-group mt-5 pb-5 text-center">
                <a href="checkout-shipping" class="btn btn-darkblue w-50">CONTINUE
                </a>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>

</html>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body>
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100 text-center">HOW TO SEND A GIFT?</h4>
                <p>Follow the steps below</p>
            </div>
        </div>
        <div class="col-12 mt-4 text-center p-0">
            <div class=" border-yellow p-2 m-2">
                <b>Step 1</b>
                <p>Select the store nearest to your friend.</p>
                <img src="assets/img/step-1.png" class="img-fluid"/>
            </div>
        </div>

        <div class="col-12 text-center mt-4 p-0">
            <div class=" border-yellow p-2 m-2">
                <b>Step 2</b>
                <p>Add your gift to cart.</p>
                <img src="assets/img/step-2.png" class="img-fluid"/>
            </div>
        </div>
        <div class="col-12 text-center mt-4 p-0">
            <div class=" border-yellow p-2 m-2">
                <b>Step 3</b>
                <p>Proceed to check out to key in yours and your friends details.</p>
                <img src="assets/img/step-3.png" class="img-fluid"/>
            </div>
        </div>
        <div class="col-12 text-center mt-4 p-0">
            <div class=" border-yellow p-2 m-2">
                <b>Step 4</b>
                <p>Have your friend receive the gift at a selected store, or send it over through express
                    delivery!</p>
                <img src="assets/img/step-4.png" class="img-fluid"/>
            </div>
        </div>
        <div class="col-12 text-center mt-4 p-0">
            <div class=" border-yellow p-2 m-2">
                <b>Step 5</b>
                <p>Make payment and share the details of the gift with your friend over WhatsApp or SMS!</p>
                <img src="assets/img/step-5.png" class="img-fluid"/>
            </div>
        </div>
        <div class="col-12 text-center mt-4 p-0">
            <div class=" border-yellow p-2 m-2">
                <b>Step 6</b>
                <p>Track the order status here</p>
                <p><a href="/check-order" class="btn btn-blue w-80">ORDER STATUS</a></p>
            </div>
        </div>
        <div class="col-12 text-center p-0 mt-4">
        <a href="shop-select?t=gift" class="btn btn-blue w-80">SEND GIFT TO FRIEND</a>
        <br/>
        <br/>
        <a href="<?=$_SERVER['HTTP_REFERER']?>" class="btn btn-blue w-80">BACK</a>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>

</html>
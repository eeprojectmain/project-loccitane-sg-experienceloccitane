$(document).ready(function() {
    // mobile menu slide from the left
    $('[data-toggle="slide-collapse"]').on('click', function() {
        $navMenuCont = $($(this).data('target'));
        $navMenuCont.animate({'width':'toggle'}, 280);
    });

    $(".status.completed .dot").html('<i class="fal fa-check"></i>');

    //sidebar
    $('.dismiss, .overlay').on('click', function() {
        $('.sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('.open-menu').on('click', function(e) {
        e.preventDefault();
        $('.sidebar').addClass('active');
        $('.overlay').addClass('active');
        // close opened sub-menus
        $('.collapse.show').toggleClass('show');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    /* replace the default browser scrollbar in the sidebar, in case the sidebar menu has a height that is bigger than the viewport */
    $('.sidebar').mCustomScrollbar({
        theme: "minimal-dark"
    });
});

function copyText(id) {
    $("#" + id).attr('disabled', false);
    var range = document.createRange();
    var tmpElem = $('<div>');
    tmpElem.css({
        position: "absolute",
        left: "-1000px",
        top: "-1000px",
    });
    tmpElem.text($("#" + id).html());
    $("body").append(tmpElem);
    range.selectNodeContents(tmpElem.get(0));
    selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);

    document.execCommand("copy");
    $("#" + id).attr('disabled', 'disabled');

    Swal.fire({
        icon: 'success',
        title: 'Yay!',
        text: 'Copied to clipboard!',
        timer:1000
    });
}
<?php
require_once "../admin/config.php";
global $table;
$databaseClass = new database();
$emailClass = new email();

$resultOutlet = get_query_data($table['outlet'], "1 order by pkid asc");
while ($rs_outlet = $resultOutlet->fetchRow()) {
    $outlet_array[$rs_outlet['code']] = $rs_outlet['pkid'];
}

$resultProduct = get_query_data($table['product'], "1 order by pkid asc");
while ($rs_product = $resultProduct->fetchRow()) {
    $product_array[$rs_product['item_code']] = $rs_product['pkid'];
}
/*
$ftp_user = 'wowsome';
$ftp_password = 'wow12345';
$ftp_server = 'ftps://sftp.ap.loccitane.com/MY/';

$ch = curl_init($ftp_server);
curl_setopt($ch, CURLOPT_USERPWD, $ftp_user . ':' . $ftp_password);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'LIST -t');
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
curl_setopt($ch, CURLOPT_FTPPORT, '43.252.212.83:48238');
curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_ALL);
curl_setopt($ch, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_TLS);
curl_setopt($ch, CURLOPT_PORT, 990);
curl_setopt($ch, CURLOPT_TIMEOUT, 300);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$curl_result = trim(curl_exec($ch));
curl_close($ch);

foreach (explode("\n", trim($curl_result, "\n")) as $line) {
    $line = preg_split('/\s+/', $line, 9);

    $ts = strtotime(implode(' ', array_slice($line, -4, 3)));
    $files[end($line)] = $ts;
}


krsort($files);

foreach ($files as $k => $v) {
    if (preg_match("/MY_Stock/", $k)) {
//        $server_filename = explode(" ", $v);
//        $server_filename = end($server_filename);
        $server_filename=$k;
        break;
    }
}
*/

$filename = '2023-03-24_09.csv';
/*$fp = fopen('../files/stock/' . $filename, 'w+');

$ch = curl_init($ftp_server . $server_filename);
curl_setopt($ch, CURLOPT_USERPWD, $ftp_user . ':' . $ftp_password);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
curl_setopt($ch, CURLOPT_FTPPORT, '43.252.212.83:48238');
curl_setopt($ch, CURLOPT_PORT, 990);
curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_ALL);
curl_setopt($ch, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_TLS);
curl_setopt($ch, CURLOPT_TIMEOUT, 300);
curl_setopt($ch, CURLOPT_FILE, $fp);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_exec($ch);
fclose($fp);

if (curl_error($ch)) {
    $email_postfield = array(
        'title' => "MY Inventory Curl Error",
        'body' => 'Error Msg (Files) : ' . (curl_error($ch))
    );
    $emailClass->send('vincentlee@wowsome.com.my', $email_postfield);
}
curl_close($ch);*/

/*
//delete file
$ch = curl_init("ftps://sftp.ap.loccitane.com");
curl_setopt($ch, CURLOPT_USERPWD, $ftp_user . ':' . $ftp_password);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
curl_setopt($ch, CURLOPT_FTP_USE_EPSV, 0);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_FTPPORT, '43.252.212.83:48238');
curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
curl_setopt($ch, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_DEFAULT);
curl_setopt($ch, CURLOPT_PORT, 990);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_QUOTE, array("DELE /MY/".$server_filename));
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_exec($ch);

if (curl_error($ch)) {
    $email_postfield = array(
        'title' => "MY Inventory Curl Error",
        'body' => 'Error Msg (Command) : ' . (curl_error($ch))
    );
    $emailClass->send('vincentlee@wowsome.com.my', $email_postfield);
}
curl_close($ch);
*/

$error_log = "";
$row = 1;
if (($handle = fopen("../files/stock/$filename", "r")) !== FALSE) {
    echo "open stock file<br/>";
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

        $row++;
        echo $row;
        echo '<br />';

        if ($row <= 6) {
            continue;
        }

        foreach ($data as $k => $v) {
            $data[$k] = mb_convert_encoding($v, 'UTF-8', 'UTF-16');
        }

        $num = count($data);
        if ($num > 0 && $outlet_array[$data[0]] != "") {
            $outlet_id = $outlet_array[$data[0]];
            $product_id = $product_array[$data[1]];
            $quantity = $data[2];

            if ($outlet_id == "") {
                $error_log .= $data[0] . '-' . $data[1] . '-' . $data[2];
            }

            $row_check = get_query_data_row($table['inventory'], "outlet_id=$outlet_id and product_code='" . $data[1] . "'");

            if ($row_check == 0) {
                $postfield = array(
                    'outlet_id' => $outlet_id,
                    'outlet_code' => $data[0],
                    'product_id' => $product_id,
                    'product_code' => $data[1],
                    'quantity' => $quantity,
                    'created_date' => $time_config['now']
                );
                $queryInsert = get_query_insert($table['inventory'], $postfield);
                $databaseClass->query($queryInsert);
            } else {
                $queryUpdate = "update " . $table['inventory'] . " set product_id='$product_id',quantity='$quantity',updated_date='" . $time_config['now'] . "' where outlet_id='$outlet_id' and product_code='" . $data[1] . "'";
                $databaseClass->query($queryUpdate);
            }
        }
    }
    fclose($handle);
}

if ($error_log != '') {
    $email_postfield = array(
        'title' => "MY Inventory Outlet Error",
        'body' => 'Error Msg: ' . $error_log
    );
    $emailClass->send('vincentlee@wowsome.com.my', $email_postfield);
}

echo $queryUpdate;

$queryUpdate = "update " . $table['inventory'] . " set quantity=0 where updated_date!='' and updated_date not like '%" . date('Y-m-d H') . "%'";
$databaseClass->query($queryUpdate);

//temp
//$queryUpdate="UPDATE `dc_inventory` SET quantity=100 where product_code='27OR030I21' or product_code='27OR050I21'";
//$databaseClass->query($queryUpdate);
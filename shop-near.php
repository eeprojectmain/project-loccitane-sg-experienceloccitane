<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$resultOutlet = get_query_data($table['outlet'], "status=1 order by pkid asc");
$outlet_text = "{";
while ($rs_outlet = $resultOutlet->fetchRow()) {
    $outlet_text .= $rs_outlet['pkid'] . ":'" . $rs_outlet['title'] . "',";
    $gmap_outlet_text .= "'" . $rs_outlet['title'] . "',";
}
$outlet_text = substr($outlet_text, 0, -1);
$gmap_outlet_text = substr($gmap_outlet_text, 0, -1);
$outlet_text .= "}";

if ($_POST) {
    $outlet_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['outlet_id']);

    $_SESSION['outlet_id'] = $outlet_id;

    header("Location: shop");
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0">
                <h4 class="bold text-white text-center bg-dark pt-4 pb-3"><?=$_SESSION['campaign_id']==""?"EXPRESS":""?> DELIVERY</h4>
            </div>
        </div>
        <div class="row mt-3 mb-5">
            <div class="col-12 mb-5">
                <form action="shop-near" method="post" class="w-80 mx-auto">
                    <div class="form-group">
                        <label>Please key in your postal code to locate the nearest boutique from your destination:
                        </label>
                    </div>
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input id="autocomplete" class="form-control" name="address"
                                placeholder="ADDRESS / POSTAL CODE" required type="text" />
                            <div class="input-group-prepend">
                                <button type="button" class="btn btn-default btn_clear"><i
                                        class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="map" style="height: 250px"></div>
                    </div>
                    <div class="form-group text-center">
                        <p><span class="span_distance"></p>
                    </div>
                    <div class="form-group mt-5 div_btn">
                        <button type="submit" name="submit_outlet"
                            class="btn btn-black w-50 mx-auto d-block">NEXT</button>
                    </div>

                    <input type="hidden" name="outlet_id" />
                </form>
            </div>
        </div>
    </div>
    <?php include('footer.php') ?>
    <?php include('js-script.php') ?>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-meP8O4H3yLtVBG1XuxLq-2MH3tc-gG0&libraries=places&callback=initMap"
        async defer></script>
    <script>
        var destinationAutocomplete, map, icons, markers = [];
        var distance_array = new Array();
        var outlet_array = <?=$outlet_text?> ;
        var gmap_outlet_array = [ <?=$gmap_outlet_text?> ];
        var outlet_id;

        $(document).ready(function() {
            $(".btn-whatsapp").hide();

            $(".p_price").hide();
            $(".p_warning").hide();
            $(".div_btn").hide();
            $(".div_time").hide();
            $(".div_receive").hide();

            $(".btn_clear").on('click', function(e) {
                $(".div_btn").hide(500);
                $(".div_time").hide(500);
                $(".p_price").hide(500);
                $(".span_price").hide(500);
                $("#autocomplete").val("").focus();
            });

            $("#autocomplete").on('keyup', function(e) {
                $(".span_distance").hide(500);
                $(".div_btn").hide(500);
            });
        });

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                mapTypeControl: false,
                center: {
                    lat: 1.3519852,
                    lng: 103.7495453
                },
                zoom: 13,
                disableDefaultUI: true
            });

            icons = {
                start: new google.maps.MarkerImage(
                    'assets/img/store-icon.png',
                    // (width,height)
                    new google.maps.Size(44, 44),
                    // The origin point (x,y)
                    new google.maps.Point(0, 0),
                    // The anchor point (x,y)
                    new google.maps.Point(22, 32)
                ),
                end: new google.maps.MarkerImage(
                    'assets/img/house-icon.png',
                    // (width,height)
                    new google.maps.Size(44, 44),
                    // The origin point (x,y)
                    new google.maps.Point(0, 0),
                    // The anchor point (x,y)
                    new google.maps.Point(22, 32)
                )
            };

            new AutocompleteDirectionsHandler(map);
        }

        function AutocompleteDirectionsHandler(map) {
            this.map = map;
            this.destinationPlaceId = null;
            this.travelMode = 'DRIVING';
            this.directionsService = new google.maps.DirectionsService;
            this.directionsRenderer = new google.maps.DirectionsRenderer({
                suppressMarkers: true
            });
            this.directionsRenderer.setMap(map);

            var destinationInput = document.getElementById('autocomplete');

            destinationAutocomplete =
                new google.maps.places.Autocomplete(destinationInput);
            destinationAutocomplete.setComponentRestrictions({
                'country': ['sg']
            });

            this.setupPlaceChangedListener(destinationAutocomplete);
        }

        AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(
            autocomplete) {
            var me = this;
            autocomplete.bindTo('bounds', this.map);

            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();

                if (!place.place_id) {
                    window.alert('Please select an option from the dropdown list.');
                    return;
                }
                me.destinationPlaceId = place.place_id;
                me.route();
            });
        };

        AutocompleteDirectionsHandler.prototype.route = function() {
            if (!this.destinationPlaceId) {
                return;
            }
            var me = this;

            //find nearest
            var place = destinationAutocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();

            var origin = {
                lat: lat,
                lng: lng
            };
            var geocoder = new google.maps.Geocoder;
            var service = new google.maps.DistanceMatrixService;
            service.getDistanceMatrix({
                origins: [origin],
                destinations: gmap_outlet_array,
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function(response, status) {
                if (status !== 'OK') {
                    alert('Error was: ' + status);
                } else {
                    var originList = response.originAddresses;
                    var destinationList = response.destinationAddresses;

                    var showGeocodedAddressOnMap = function(asDestination) {
                        return function(results, status) {};
                    };

                    var results = response.rows[0].elements;
                    for (var j = 0; j < results.length; j++) {
                        geocoder.geocode({
                                'address': destinationList[j]
                            },
                            showGeocodedAddressOnMap(true));
                        distance_array.push(results[j].distance.value);
                    }

                    var distance = Math.min.apply(Math, distance_array);
                    outlet_id = getOutlet(distance);

                    var request = {
                        radius: 500,
                        location: map.getCenter(),
                        query: outlet_array[outlet_id]
                    };

                    var service = new google.maps.places.PlacesService(map);
                    service.textSearch(request, function(results, status) {
                        me.destinationPlaceId = results[0].place_id;
                        me.directionsService.route({
                                origin: {
                                    'lat': lat,
                                    'lng': lng
                                },
                                destination: {
                                    'placeId': me.destinationPlaceId
                                },
                                travelMode: me.travelMode
                            },
                            function(response, status) {
                                if (status === 'OK') {
                                    removeMarker();
                                    me.directionsRenderer.setDirections(response);
                                    var leg = response.routes[0].legs[0];
                                    makeMarker(leg.start_location, icons.end);
                                    makeMarker(leg.end_location, icons.start);

                                    $("input[name='outlet_id']").val(outlet_id);
                                    $(".span_distance").html(outlet_array[outlet_id] +
                                        "<br />" + response.routes[0].legs[0].distance
                                        .text + ", " + response.routes[0].legs[0].duration
                                        .text);
                                    $(".span_distance").show(500);
                                    $(".div_btn").show(500);
                                } else {
                                    window.alert('Directions request failed due to ' + status);
                                }
                                distance_array = new Array();
                            });
                    });
                }
            });
        };

        function getOutlet(v) {
            var i;
            $(distance_array).each(function(index, element) {
                if (element == v) {
                    i = index;
                }
            });

            var c = 0;
            var oid;
            $.each(outlet_array, function(key, value) {
                if (c == i) {
                    oid = key;
                }
                c++;
            });

            return oid;
        }

        function removeMarker() {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
        }

        function makeMarker(position, icon) {
            var marker = new google.maps.marker.AdvancedMarkerElement({
                position: position,
                map: map,
                icon: icon,
                animation: google.maps.Animation.DROP,
            });
            markers.push(marker);
        }
    </script>
</body>

</html>
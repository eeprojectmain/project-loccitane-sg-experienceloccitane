<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$apiClass = new api();

$pkid = $_SESSION['sample_hair']['done'];

if($pkid==""){
    header("Location: hair-sampling-live-aug");
    exit();
}

$resultSample = get_query_data($table['sample_hair'], "pkid=$pkid");
$rs_sample = $resultSample->fetchRow();

$postfield['first_name'] = $rs_sample['first_name'];
$postfield['last_name'] = $rs_sample['last_name'];
$postfield['mobile'] = $rs_sample['mobile'];
$postfield['email'] = $rs_sample['email'];
$postfield['sms_status'] = $rs_sample['sms_status'];
$postfield['email_status'] = $rs_sample['email_status'];
$postfield['call_status'] = $rs_sample['call_status'];
$postfield['whatsapp_status'] = $rs_sample['whatsapp_status'];
$postfield['source_id'] = '4525';
//$postfield['source_id'] = '4516';

$apiClass->create_sample_profile($postfield);

unset($_SESSION['iglive']);
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">BEST SELLER SAMPLE KIT</h4>
                <p>Thank you for signing up!<br>
                    Please present a screenshot of this confirmation to redeem your best seller sample kit in stores.</p>
            </div>
        </div>
        <div class="col-12">
            <img src="assets/img/LSGG218EDS10E - GWP Free 10pc sampling kit.png" class="img-fluid"/>
        </div>
        <div class="col-12">
            <b>First Name: </b> <?= $rs_sample['first_name'] ?><br><br>
            <b>Last Name: </b> <?= $rs_sample['last_name'] ?><br><br>
            <b>Mobile: </b> <?= $rs_sample['mobile'] ?><br><br>
            <b>Redeem by: </b> <?=date('d M Y',strtotime($rs_sample['created_date'].' +2 week'))?>
        </div>
        <div class="col-12 mt-4">
            <div class="border border-dark p-2">
                <p>- Limited to 1 sample kit per customer, while stocks last.</p>
                <p>- Valid for redemption at all L'OCCITANE boutiques except L'OCCITANE web store, Lazada, Shopee and Duty Free Stores.</p>
                <p>- Non-transferrable and no collection is allowed on behalf of others. OTP verification to registered phone number is required during collection of sample kit in stores.</p>
                <p>- Not exchangeable for cash.</p>
                <p>- L'OCCITANE Singapore reserves the final right to alter and/or withdraw the items, terms & conditions without prior notice.</p>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
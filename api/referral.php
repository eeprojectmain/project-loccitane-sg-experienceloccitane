<?
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$emailClass = new email();

/*$email_postfield = array(
	'title' => "Tap Tap Harvest - Your invitation rewards!",
	'body' => 'You have just gained 1000 coins by referring a friend to join Tap Tap Harvest! Please enter the game to check your coins wallet!'
);
$emailClass->send($_POST['email'], $email_postfield);*/

$postfield = array(
    'campaignId' => 3232217,
    'recipientEmail' => $_POST['email'],
    'sendAt' => date("Y-m-d H:i:s", strtotime('yesterday'))
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.iterable.com/api/email/target");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postfield));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$headers = [
    'Api-Key: e20f64a864954eb58c565cfde6439a63',
];
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result=curl_exec($ch);
//var_dump($result);
curl_close($ch);

/*$raw_post = file_get_contents("php://input");
file_put_contents("referral_log.txt", json_encode($raw_post) . PHP_EOL, FILE_APPEND);
file_put_contents("referral_log.txt", json_encode($_POST) . PHP_EOL, FILE_APPEND);*/

echo 'SUCCESS';
exit();
?>
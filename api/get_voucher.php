<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

header('Content-Type: application/json');

if ($_SERVER['REMOTE_ADDR'] != "43.252.215.111") {
//    echo json_encode(array('result'=>'error','message'=>'you do not have access to this API'));
//    exit();
}

$resultVoucher = get_query_data($table['game_voucher'], "status=1 and quantity>0 and (start_date='' or date(start_date) <= date('" . $time_config['today'] . "')) order by coin asc");
while ($rs_voucher = $resultVoucher->fetchRow()) {
    $return_array[] = array(
        'id' => $rs_voucher['pkid'],
        'title' => $rs_voucher['title'],
        'details' => $rs_voucher['details'],
        'bottle_to_redeem' => $rs_voucher['bottle'],
        'coin_to_redeem' => $rs_voucher['coin'],
        'level_to_redeem' => $rs_voucher['level'],
        'start_date' => $rs_voucher['start_date'],
        'end_date' => $rs_voucher['end_date'],
        'country' => 'SG'
    );
}

echo json_encode(array('result' => 'success', 'message' => 'sucessfully queries', 'voucher' => $return_array));
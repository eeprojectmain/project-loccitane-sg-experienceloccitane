<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
//error_reporting(E_ALL);
global $table;
$databaseClass = new database();

header('Content-Type: application/json');

$mobile = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['mobile']);
$voucher_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['voucher_id']);

$mobile = str_replace(" ", "", $mobile);
$mobile = str_replace("-", "", $mobile);

if ($mobile == "") {
    echo json_encode(array('result' => 'error', 'message' => 'mobile number cannot be empty'));
    exit();
}

if ($voucher_id == "") {
    echo json_encode(array('result' => 'error', 'message' => 'voucher_id cannot be empty'));
    exit();
}

$resultVoucher = get_query_data($table['game_voucher'], "pkid=$voucher_id");
$rs_voucher = $resultVoucher->fetchRow();

if ($rs_voucher['pkid'] == "") {
    echo json_encode(array('result' => 'error', 'message' => 'this voucher_id is invalid'));
    exit();
}

if ($rs_voucher['status'] == "0") {
    echo json_encode(array('result' => 'error', 'message' => 'this voucher has been disabled'));
    exit();
}

if ($voucher_id == "8099" && strtotime('now') > strtotime('2021-01-01 00:00:00')) {
    echo json_encode(array('result' => 'error', 'message' => 'this voucher has run out of quantity'));
    exit();
}

if ($rs_voucher['quantity'] == "0") {
    echo json_encode(array('result' => 'error', 'message' => 'this voucher has run out of quantity'));
    exit();
}

$queryUpdate = get_query_update($table['game_voucher'], $voucher_id, array('quantity' => ($rs_voucher['quantity'] - 1)));
$databaseClass->query($queryUpdate);

$mobile = str_replace("+65", "", $mobile);
$mobile = "+65" . $mobile;

$queryInsert = get_query_insert($table['member_voucher'], array('mobile' => $mobile, 'status' => '1', 'voucher_id' => $voucher_id, 'created_date' => $time_config['now']));
$resultInsert = $databaseClass->query($queryInsert);
$genID = $resultInsert->insertID();

if ($voucher_id == "8100") {
    $resultGame = get_query_data($table['game_voucher'], "pkid=$voucher_id");
    $rs_game = $resultGame->fetchRow();

    $url = $site_config['full_url'] . "game-redeem?i=" . protect('encrypt', $genID);

    $json = file_get_contents("https://cutt.ly/api/api.php?key=aaaaaf44e3186cde7c5f3cff51fa365ba9965&short=" . urlencode($url));
    $result = json_decode($json, true);
    $short_url = $result['url']['shortLink'];

    $msg = 'Here\'s your FREE ' . $rs_game['title'] . '! Redeem in-store ' . $short_url . ' OR when you checkout on L\'OCCITANE En Demande https://bit.ly/34RmtrM';

    $mobile=str_replace("+","",$mobile);
    $ch = curl_init();
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json',
    );
    curl_setopt($ch, CURLOPT_URL, "http://www.etracker.cc/bulksms/mesapi.aspx?user=davino&pass=Wowsome%40820%23%23%23%23%21&type=0&to=$mobile&from=TapTapHarvest&text=" . urlencode($msg) . "&servid=MES01&title=TTH_SG_Sample");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $server_respond = curl_exec($ch);
    curl_close($ch);
}

echo json_encode(array('result' => 'success', 'message' => 'successfully inserted'));
exit();
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$dateClass = new date();

$resultVoucher = get_query_data($table['game_voucher'], "1 order by pkid asc");
while ($rs_voucher = $resultVoucher->fetchRow()) {
    $voucher_title_array[$rs_voucher['pkid']] = $rs_voucher['title'];
}

if ($_POST['from_date'] != "") {
    $from_date = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['from_date']);
} else {
    $from_date = $dateClass->getThisMonthStartAndEnd()['start'];
}

if ($_POST['to_date'] != "") {
    $to_date = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['to_date']);
} else {
    $to_date = $dateClass->getThisMonthStartAndEnd()['end'];
}

if ($_POST['type'] == "issue") {
    $resultArray = get_query_data($table['member_voucher'], "date(created_date) between date('$from_date') and date('$to_date') order by created_date asc");
} else {
    $resultArray = get_query_data($table['member_voucher'], "updated_date!='' and date(updated_date) between date('$from_date') and date('$to_date') order by updated_date asc");
}
while ($rs_array = $resultArray->fetchRow()) {
    if ($_POST['type'] == "issue") {
        $date = substr($rs_array['created_date'], 0, -9);
    } else {
        $date = substr($rs_array['updated_date'], 0, -9);
    }

    $json_array[$date]['total']++;
    $json_array[$date]['vouchers'][$voucher_title_array[$rs_array['voucher_id']]]++;
    if($_POST['type']=="redeem") {
        $json_array[$date]['redeem_type'][$rs_array['updated_by']]++;
    }
}

echo json_encode($json_array);
exit();
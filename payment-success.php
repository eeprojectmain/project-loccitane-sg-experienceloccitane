<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$nav_step = '3';

$resultOrder = get_query_data($table['order'], "pkid=" . mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['Ref']));
$rs_order = $resultOrder->fetchRow();

$mobile = $rs_order['receive_mobile'];
if (preg_match("/^\+/", $mobile)) {
    $mobile = str_replace("+", "", $mobile);
}
$mobile = "65" . $mobile;

if ($rs_order['type'] == "gift" && $rs_order['shipping_method'] == "delivery") {
    $sms_link = "Surprise! I've bought you a gift on L'OCCITANE EnDemande! The delivery is scheduled on " . date('M d', strtotime($rs_order['pickup_date'])) . " at " . $rs_order['pickup_time'] . " and will be sent to you at " . ($rs_order['unit_no'] != "" ? $rs_order['unit_no'] . ", " : "") . $rs_order['address'] . ". Click this link to find out more " . $site_config['full_url'] . "gift-for-you?i=" . protect('encrypt', $rs_order['pkid']);
    $whatsapp_link = "https://wa.me/+$mobile?text=" . urlencode($sms_link);
} elseif ($rs_order['type'] == "gift" && $rs_order['shipping_method'] == "pickup") {
    $resultOutlet = get_query_data($table['outlet'], "pkid=" . $rs_order['outlet_id']);
    $rs_outlet = $resultOutlet->fetchRow();

    $sms_link = "Surprise! I've bought you a gift on L'OCCITANE EnDemande! You can pick-up your gift anytime after " . date('M d', strtotime($rs_order['pickup_date'])) . ", " . explode(" - ", $rs_order['pickup_time'])[0] . " at L'OCCITANE " . $rs_outlet['title'] . ". Click this link to find out more " . $site_config['full_url'] . "gift-for-you?i=" . protect('encrypt', $rs_order['pkid']);
    $whatsapp_link = "https://wa.me/+$mobile?text=" . urlencode($sms_link);
}

unset($_SESSION['cart']);
unset($_SESSION['pickup_time']);
unset($_SESSION['pickup_date']);
unset($_SESSION['member']['order_id']);
unset($_SESSION['member']['voucher_value']);
unset($_SESSION['member']['voucher_id']);
unset($_SESSION['member']['voucher_name']);
unset($_SESSION['member']['count']);
unset($_SESSION['member']['total_promo_discount']);
unset($_SESSION['member']['total_after_promo']);
unset($_SESSION['type']);

?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <img src="assets/img/icon-check.png" class="w-25"/>
                <h4 class="w-100 mt-3">PAYMENT SUCCESSFUL</h4>
                <p class="">Thank you for your order.</p>
            </div>
        </div>
        <div class="col-12 mt-5 text-center">
            <p class="w-100"><b>Your order ID is #LDSG20<?= $_GET['Ref'] ?></b></p>
            <p class="w-100">We are now processing your order. Check your order status below.</p>
            <?php
            if ($rs_order['type'] == "gift" && $rs_order['shipping_method'] == "delivery") {
                ?>
                <hr class="border-yellow">
                <div class="border-white pb-3 mb-5">
                    <p class="w-100 mt-4">
                        Let your friend know a gift is coming!
                    </p>
                    <a href="<?= $whatsapp_link ?>" target="_blank" class="btn btn-blue w-80 mt-3">WHATSAPP</a>
                    <button type="button" class="btn btn-blue w-80 mt-2 btn-sms">SMS</button>
                </div>
                <p class="mb-5">
                <?php
            } elseif ($rs_order['type'] == "gift" && $rs_order['shipping_method'] == "pickup") {
                ?>
                <hr class="border-yellow">
                <div class="border-white pb-3 mb-5">
                    <p class="w-100 mt-4">Let your friend know you bought a gift!</p>
                    <p>Share the redemption link through WhatsApp or SMS so your friend can redeem the gift in
                        store.</p>
                    <a href="<?= $whatsapp_link ?>" target="_blank" class="btn btn-blue w-80 mt-3">WHATSAPP</a>
                    <button type="button" class="btn btn-blue w-80 mt-2 btn-sms">SMS</button>
                </div>
                <?php
            } ?>
            <hr class="border-yellow">
            <a href="check-order" class="btn btn-darkblue w-80 mb-3 mb-md-0">CHECK ORDER STATUS</a>
            <a href="shop" class="btn btn-blue w-80">BACK TO HOME</a>
            <!--holiday2023?campaign=GMHoliday23';-->
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    fbq('track', 'Purchase', {
        value: <?=$rs_order['total_amount'] + $rs_order['shipping_amount'] - $rs_order['discount_amount']?>,
        currency: 'SGD'
    });

    $(".btn-sms").on('click', function (e) {
        $(this).attr('disabled', 'disabled');
        $.ajax({
            method: "POST",
            url: "ajax/send-sms",
            dataType: 'json',
            data: {
                id: <?=$rs_order['pkid']?>
            }
        })
            .done(function (data) {
                Swal.fire({
                    text: data.message,
                    icon: data.result,
                });
            });
    });
</script>
</body>

</html>
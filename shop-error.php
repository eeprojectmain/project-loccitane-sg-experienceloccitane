<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body style="font-family: Gill Sans, serif;padding-bottom: 150px;" class="page-bg">
<div class="container">
    <img class="img-fluid d-block mx-auto" src="assets/img/logo-white.png" width="50%">
    <div style="background-color:white;margin-left: 5%;margin-right:5%;">
        <div class="login-clean pb-4">
            <form method="post" style="padding:20px 20px 0 20px;" action="login">
                <div class="illustration">
                    <h4>ERROR</h4>
                </div>
                <p>Seem like your've accessed a invalid link. Please try again.</p>
            </form>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
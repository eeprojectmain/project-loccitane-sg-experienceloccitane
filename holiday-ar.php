<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$apiClass = new api();

if ($_POST) {
    $postfield = $_POST;
    unset($postfield['submit_form']);
    unset($postfield['tnc2']);

    $postfield['created_date'] = $time_config['now'];

    $queryInsert = get_query_insert($table['holiday_ar'], $postfield);
    $resultInsert = $databaseClass->query($queryInsert);
    $genID = $resultInsert->insertID();

    $_SESSION['holiday_ar']['id'] = $genID;

    $postfield_db['first_name'] = $postfield['first_name'];
    $postfield_db['last_name'] = $postfield['last_name'];
    $postfield_db['mobile'] = $postfield['mobile'];
    $postfield_db['email'] = $postfield['email'];
    $postfield_db['sms_status'] = '0';
    $postfield_db['email_status'] = '0';
    $postfield_db['call_status'] = '0';
    $postfield_db['whatsapp_status'] = '0';
    $postfield_db['source_id'] = '4745';

    $apiClass->create_sample_profile($postfield_db);

    $postfield_iterable = array(
        'campaignId' => 3048898,
        'recipientEmail' => $postfield['email'],
        'sendAt' => date("Y-m-d H:i:s", strtotime('yesterday'))
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.iterable.com/api/email/target");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postfield_iterable));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $headers = [
        'Api-Key: e20f64a864954eb58c565cfde6439a63',
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    curl_close($ch);

    header("Location: holiday-ar-tq");
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <!--                <h4 class="w-100">RESET SERUM SAMPLE KIT</h4>-->
                <p>Sign up to receive your treat!</p>
            </div>
        </div>
        <div class="col-12 mt-4">
            <form action="holiday-ar" method="post" class="mx-auto formValidation">
                <div class="form-group">
                    <input type="text" name="first_name" class="form-control" maxlength="100" placeholder="FIRST NAME"
                           required/>
                </div>
                <div class="form-group">
                    <input type="text" name="last_name" class="form-control" maxlength="100" placeholder="LAST NAME"
                           required/>
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control" placeholder="MOBILE NUMBER" name="mobile" required
                           minlength="8"
                           maxlength="9">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" maxlength="100" placeholder="EMAIL ADDRESS"
                           required/>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="tnc2" id="tnc_2" required value="1">
                        <label class="form-check-label" for="tnc_2">*I hereby consent to the Processing of my Personal
                            Data for the above Purpose and agree to the terms in the <a
                                    href="https://sg.loccitane.com/pages?fdid=terms-conditions" target="_blank"><u>Data
                                    Protection</u></a> and <a href="https://sg.loccitane.com/private-policy"
                                                              target="_blank"><u>Privacy
                                    Policy Notice</u></a>.</label>
                    </div>
                </div>
                <div class="form-group mt-5 pb-5 text-center">
                    <button type="submit" name="submit_form" value="online" class="btn btn-darkblue w-50">SIGN UP
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
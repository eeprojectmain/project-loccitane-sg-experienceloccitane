<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<style>
    footer,footer a{
        color:white !important;
    }
</style>
<body class="bg-index">
<div class="container">
    <div class="row pt-3">
        <div class="col-12">
            <img src="assets/img/logo-white.png" class="logo"/>
        </div>
    </div>
    <div class="row mt-3">
        <div class="card bg-light">
            <div class="card-body text-center m-5">
            <h3>Thank You for Visiting En Demande!</h3><br>
            This service is currently closed, but you can continue shopping for all your favourite L'OCCITANE products on our official Singapore website: <a href="https://sg.loccitane.com" target="_blank"><b>sg.loccitane.com</b></a>.<br><br>
            We appreciate your understanding and look forward to serving you there!
            </div>
        </div>
    </div>
    <div class="row d-flex justify-content-center mt-5">
        <!-- <div class="col-4">
            <a href="shop-select?t=normal" class="btn btn-icon same-height"><i
                        class="fal fa-shopping-bag fa-2x"></i><br>START
                SHOPPING</a>
        </div>
        <div class="col-4">
            <a href="how-to-gift" class="btn btn-icon same-height"><i
                        class="fal fa-gift fa-2x"></i><br>SEND
                A GIFT</a>
        </div> -->
        <div class="d-flex justify-content-center">
            <a href="check-order" class="btn btn-icon same-height"><i class="fal fa-truck-loading fa-2x"></i><br>TRACK ORDER</a>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
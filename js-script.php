<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-shim.min.js"></script>

<script src="assets/formvalidation/js/FormValidation.full.min.js"></script>

<script src="assets/formvalidation/js/plugins/Bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.all.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/4.1.1/less.min.js" data-env="development"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.2.4/pace.min.js"></script>

<script src="assets/js/mCustomScrollbar.concat.min.js"></script>

<script src="assets/js/jquery.smartWizard.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<script src="assets/js/main.js"></script>



<div class="modal fade" role="dialog" tabindex="-1" id="modal-cart">

    <form action="ajax/checkout" method="post">

        <div class="modal-dialog modal-dialog-centered" role="document">

            <div class="modal-content dark-bg">

                <div class="modal-header b-0">

                    <h4 class="modal-title text-center w-100 font-title">SHOPPING BAG</h4>

                    <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span

                                aria-hidden="true"><i class="fal fa-times"></i></span></button>

                </div>

                <div id="modal-cart-body">

                    <i class="fa fa-cog fa-spin fa-3x fa-fw"

                       style="top: 0;left: 0;right: 0;position: absolute;margin: auto;"></i>

                </div>

            </div>

        </div>

        <input type="hidden" name="token" value="<?= protect('encrypt', session_id()) ?>">

    </form>

</div>



<div class="modal fade" role="dialog" tabindex="-1" id="modal-product">

    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content dark-bg">

            <div class="modal-header b-0">

                <h4 class="modal-title w-100 text-center" data-type="product-name">

                    DESCRIPTION</h4>

                <button type="button" class="close p-absolute r-5 w-100 text-right" data-dismiss="modal"

                        aria-label="Close"><span aria-hidden="true"><i class="fal fa-times"></i></span></button>

            </div>

            <div class="modal-body text-center" id="modal-product-body">

                <i class="fa fa-cog fa-spin fa-3x fa-fw"

                   style="top: 0;left: 0;right: 0;position: absolute;margin: auto;"></i>

            </div>

        </div>

    </div>

</div>



<div id="search">

    <button type="button" class="close" style="color:white !important;"><i class="fal fa-times"></i></button>

    <?php
        if($_SESSION["campaign_id"] == '9'){ // if is campaign, search by campaign, elase search as usual shop
            $searchPath = "GMHoliday22";
        }else{
            $searchPath = "shop";
        }
    ?>
    <?php
        if($_SESSION["campaign_id"] == '10'){ // if is campaign, search by campaign, elase search as usual shop
            $searchPath = "GMHoliday23";
        }else{
            $searchPath = "shop";
        }
    ?>
    <form action="<?=$searchPath?>" method="get">

        <div class="input-group mb-3">

            <div class="input-group-prepend">

                <i class="fal fa-search"></i>

            </div>

            <input type="text" name="search" placeholder="SEARCH"/>

        </div>

        <button type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;">SUBMIT</button>

    </form>

</div>



<!-- Modal -->

<div class="modal fade" id="popup_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"

     aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">

            <div class="modal-body p-0">

                <img src="assets/img/free_shipping.jpeg" class="img-fluid"/>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

    var form, fv;



    $(document).ready(function () {

        update_cart_price();



        $('a[href="#search"]').on('click', function (event) {

            event.preventDefault();

            $('#search').addClass('open');

            $('#search form input[type="text"]').focus();

        });



        $('.close').on('click', function (event) {

            $("#search").removeClass('open');

        });



        $(".same-height").matchHeight();



        <?if(preg_match("/" . $site_config['sample_hair'] . "/", $_SERVER['PHP_SELF'])){?>

        $(".btn-menu,.btn-cart,.btn-search").hide();

        <?}?>



        <?if(preg_match("/" . $site_config['sample_reset'] . "/", $_SERVER['PHP_SELF'])){?>

        $(".btn-menu,.btn-cart,.btn-search").hide();

        <?}?>

    });



    function add_to_cart(id) {

        fbq('track', 'AddToCart');



        $.ajax({

            method: "POST",

            url: "ajax/add-to-cart",

            dataType: 'json',

            data: {

                id: id,

                token: "<?=protect("encrypt", session_id())?>"

            }

        })

            .done(function (data) {

                if (data.function == 'low_stock') {

                    low_stock(data.title);

                } else {

                    Swal.fire({

                        title: data.title,

                        text: data.msg,

                        showCloseButton: true,

                        showCancelButton: true,

                        confirmButtonText: 'CONTINUE SHOPPING',

                        cancelButtonText: '<span onclick="open_cart_modal();">VIEW CART</span>',

                        customClass: {

                            confirmButton: 'btn btn-black float-left',

                            cancelButton: 'btn btn-black btn-cart float-right',

                        }

                    });

                    update_cart_price();

                    $(".checkout").show(500);

                }

            });

    }



    function delete_cart(id) {

        Swal.fire({

            title: "Are you sure?",

            text: "It's such a great deal!",

            icon: "warning",

            showCloseButton: true,

            showCancelButton: true,

        }).then((willDelete) => {

            if (willDelete.value) {

                $.ajax({

                    method: "POST",

                    url: "ajax/delete-cart",

                    data: {

                        id: id

                    }

                })

                    .done(function (data) {

                        update_cart_price();

                        open_cart_modal();

                        Swal.fire("Successfully removed from cart", {

                            icon: "success",

                            timer: 1000,

                        });

                    });

            }

        });

    }



    function update_cart_price() {

        $.ajax({

            method: "POST",

            url: "ajax/cart-value",

            dataType: 'json'

        })

            .done(function (data) {

                $("#cart-price").text(data.total_price);

                $("#cart-qty").text(data.total_qty);

            });

    }



    function open_cart_modal() {

        $("#modal-cart-body").load("remote-view/cart2", function (e) {

            $("#modal-cart").modal('show');

        });

    }



    function open_product_modal(id) {

        $("#modal-product-body").load("remote-view/product-details?product_id=" + id, function (e) {

            $("#modal-product").modal('show');

        });

    }



    function low_stock(title) {

        Swal.fire({

            title: "Sorry...",

            text: title + " is running out of stock, would you like to contact store for assist?",

            icon: "warning",

            showCloseButton: true,

            showCancelButton: true,

        }).then((willDelete) => {

            if (willDelete.value) {

                window.open("https://api.whatsapp.com/send?phone=<?= str_replace("+", "", $rs_outlet['whatsapp_no']) ?>&text=Hi L'OCCITANE <?= urlencode($rs_outlet['title']) ?>, " + title + " is showing low stock, can you assist?");

            }

        });

    }



    <? if($swal){?>

    Swal.fire({

        title: "<?=$swal['title']?>",

        text: "<?=$swal['msg']?>",

        icon: "<?=$swal['type']?>",

        <?php if($swal['pwp'] != ""){?>

        showCloseButton: true,

        showCancelButton: true,

        confirmButtonText: '<span onclick="add_to_cart(1040);">SURE!</span>',

        cancelButtonText: 'No, Thanks.',

        customClass: {

            confirmButton: 'btn btn-black float-left',

            cancelButton: 'btn btn-black btn-cart float-right',

        }

        <?php } ?>

    })

    <?}?>



    <?if($_GET['a']=='swal'){?>

    Swal.fire({

        title: "<?=$_GET['t']?>",

        text: "<?=$_GET['m']?>",

        icon: "<?=$_GET['i']?>",

    })

    <?}?>



    function confirmCheckout(e) {

        e.preventDefault();

        swal({

            title: "Are you sure?",

            text: "By confirm this order, our staff will start to pack your order.",

            icon: "warning",

            buttons: true,

            dangerMode: true,

        })

            .then((value) => {

                if (value) {

                    $(".checkoutForm")[0].submit();

                } else {

                    return false;

                }

            });

    }



    $(window).on("load", function (e) {

        $(".formValidation").each(function (index, element) {

            form = $(".formValidation")[index];

            fv = FormValidation.formValidation(

                form, {

                    fields: {

                        reset_email: {

                            message: 'This field is required',

                            validators: {

                                remote: {

                                    message: 'The email did not register before',

                                    url: 'ajax/reset-email',

                                    method: 'POST',

                                }

                            }

                        },

                    },

                    plugins: {

                        declarative: new FormValidation.plugins.Declarative({

                            html5Input: true,

                        }),

                        trigger: new FormValidation.plugins.Trigger(),

                        bootstrap: new FormValidation.plugins.Bootstrap(),

                        excluded: new FormValidation.plugins.Excluded(),

                        submitButton: new FormValidation.plugins.SubmitButton(),

                        icon: new FormValidation.plugins.Icon({

                            valid: 'fal fa-check',

                            invalid: 'fal fa-times',

                            validating: 'fal fa-refresh'

                        }),

                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),

                    },

                }

            ).on('core.form.valid', function () {

                $("button[type='submit']").attr('disabled', 'disabled');

            });

        });

    });



    if ('serviceWorker' in navigator) {

        navigator.serviceWorker.register('service-worker.js')

            .then(function (reg) {

            }).catch(function (err) {

        });

    }

</script>

<!-- Facebook Pixel Code -->

<script>

    !function (f, b, e, v, n, t, s) {

        if (f.fbq) return;

        n = f.fbq = function () {

            n.callMethod ?

                n.callMethod.apply(n, arguments) : n.queue.push(arguments)

        };

        if (!f._fbq) f._fbq = n;

        n.push = n;

        n.loaded = !0;

        n.version = '2.0';

        n.queue = [];

        t = b.createElement(e);

        t.async = !0;

        t.src = v;

        s = b.getElementsByTagName(e)[0];

        s.parentNode.insertBefore(t, s)

    }(window, document, 'script',

        'https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '800121447587288');

    fbq('track', 'PageView');

</script>

<noscript><img height="1" width="1" style="display:none"

                 src="https://www.facebook.com/tr?id=800121447587288&ev=PageView&noscript=1"

    /></noscript>

<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123164816-1"></script>

<script>

    window.dataLayer = window.dataLayer || [];



    function gtag() {

        dataLayer.push(arguments);

    }



    gtag('js', new Date());



    gtag('config', 'UA-123164816-1');

</script>

<script>

    window.fbAsyncInit = function () {

        FB.init({

            appId: '<?=$site_config['fb_app_id']?>',

            autoLogAppEvents: true,

            xfbml: true,

            version: 'v3.1'

        });

    };



    (function (d, s, id) {

        var js, fjs = d.getElementsByTagName(s)[0];

        if (d.getElementById(id)) {

            return;

        }

        js = d.createElement(s);

        js.id = id;

        js.src = "https://connect.facebook.net/en_US/sdk.js";

        fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'facebook-jssdk'));

</script>

<script>

    fbq('track', 'ViewContent');

</script>
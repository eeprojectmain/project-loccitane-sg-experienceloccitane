<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$orderClass = new order();
$stockClass = new stock();

$nav_step = '2';

$orderClass->check();
$orderClass->update_final();
$orderClass->check_final();

if (isset($_POST['submit_paynow'])) {
    header("Location: checkout-paynow");
    exit();
}

if (isset($_POST['submit_visa'])) {
    header("Location: payment/cc");
    // header("Location: payment/cc-callback?vktest=1&order_id=".$_SESSION['member']['order_id']);
    exit();
}

if (isset($_POST['submit_atome'])) {
    header("Location: payment/atome");
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<body>
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">PAYMENT</h4>
                <p class="text-center">Please select your mode of payment</p>
            </div>
        </div>
        <div class="col-12">
            <form action="checkout-payment" method="post" class="mx-auto">
                <button type="submit" name="submit_paynow" value="1" class="btn btn-blue w-80 d-block mx-auto mt-5 same-height">
                    <img src="assets/img/logo-paynow.png" class="w-50"/>
                </button>
                <button type="submit" name="submit_visa" value="1" class="btn btn-blue w-80 d-block mx-auto mt-3 same-height">
                    <img src="assets/img/logo-visa.png" class="w-25"/>
                    <img src="assets/img/logo-master.png" class="w-25"/>
                </button>
                <button type="submit" name="submit_atome" value="1" class="btn btn-blue w-80 d-block mx-auto mt-3 same-height">
                    <img src="assets/img/logo-atome.png" class="w-50"/>
                    <br>
                    <p class="m-0" style="font-size: 60%;">3 monthly payments, 0% interest. Enjoy S$10 off your first order.</p>
                </button>
            </form>
        </div>
        <div class="col-12 mt-5">
            <p class="w-100 text-center"><i class="fal fa-lock"></i> Safe and secure payment</p>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
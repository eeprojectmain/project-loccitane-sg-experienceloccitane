<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$member_id = session_id();

$total_price=0;

foreach($_SESSION['cart'] as $k=>$v){
    $resultProduct=get_query_data($table['product'],"pkid=".$v['product_id']);
    $rs_product=$resultProduct->fetchRow();

    $total_price+=($rs_product['price']*$v['quantity']);
}

echo number_format($total_price,2);
?>
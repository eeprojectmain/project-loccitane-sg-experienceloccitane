<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$member_id = $_SESSION['member']['id'];

if (!is_numeric($member_id)) {
    $return_result = array("result" => "error", "message" => "Invalid QR Code");
    echo json_encode($return_result);
    exit();
}

$resultCheckin = get_query_data($table['checkin'], "member_id=$member_id");
$row_checkin = $resultCheckin->numRows();
while ($rs_checkin = $resultCheckin->fetchRow()) {
    $completed_array[] = $rs_checkin['station_id'];
}

$resultRedeem = get_query_data($table['redeem'], "member_id=$member_id");
$row_redeem = $resultRedeem->numRows();

$return_result = array("result" => "success", "checkin" => $row_checkin, "completed" => $completed_array, 'redeem' => $row_redeem);

echo json_encode($return_result);
exit();
?>
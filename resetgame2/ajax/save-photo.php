<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['id']);
$member_id = $_SESSION['member']['id'];

$id = protect('decrypt', $id);

if (session_id() != $id) {
    exit();
}

$imagedata = base64_decode($_POST['imgdata']);
$filename = md5(uniqid(rand(), true));
$file = '../files/facebook/' . $filename . '.png';
file_put_contents($file, $imagedata);

$queryUpdate="update ".$table['member']." set share_thumbnail='$filename.png' where pkid=$member_id";
$databaseClass->query($queryUpdate);

echo 'success';
?>
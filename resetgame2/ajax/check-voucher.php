<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$member_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['member_id']);
$member_id = protect('decrypt', $member_id);

if (!is_numeric($member_id)) {
    $return_result = array("result" => "error", "message" => "Invalid QR Code");
    echo json_encode($return_result);
    exit();
}

$resultMember = get_query_data($table['member'], "pkid=$member_id");
$rs_member = $resultMember->fetchRow();

$resultVoucher = get_query_data($table['cafe_voucher'], "member_id=$member_id and status=0");
$row_voucher = $resultVoucher->numRows();
$rs_voucher = $resultVoucher->fetchRow();

$row_check = get_query_data_row($table['share'], "page='share-cafe' and member_id=$member_id");

if ($row_check == 0) {
    $return_result = array("result" => "error", "message" => "You haven't share on Facebook");
} elseif ($row_voucher == 0) {
    $resultRedeem = get_query_data($table['cafe_voucher'], "member_id=$member_id and status=1");
    $rs_redeem = $resultRedeem->fetchRow();

    $return_result = array("result" => "error", "message" => "Already redeemed on " . $rs_redeem['created_date']);
} elseif ($row_voucher > 0) {
    $postfield = array(
        'status' => '1',
        'updated_date' => $time_config['now']
    );

    $queryUpdate = get_query_update($table['cafe_voucher'], $rs_voucher['pkid'], $postfield);
    $databaseClass->query($queryUpdate);

    $return_result = array("result" => "success", "message" => "Successfully redeem");
}

echo json_encode($return_result);
exit();
?>
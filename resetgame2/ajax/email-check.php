<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();

$email = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['email']);

$row = get_query_data_row($table['member'], "email='$email'");

if ($row > 0) {
    echo json_encode(array(
        'valid' => false,
    ));
} else {
    echo json_encode(array(
        'valid' => true,
    ));
}
exit();
?>
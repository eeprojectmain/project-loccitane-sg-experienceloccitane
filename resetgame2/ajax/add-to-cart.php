<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$product_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['id']);

foreach ($_SESSION['cart'] as $k => $v) {
    if ($v['product_id']==$product_id) {
        $_SESSION['cart'][$k]['quantity'] += 1;
        $added=true;
    }
}

if(!$added) {
    $_SESSION['cart'][] = array(
        'product_id' => $product_id,
        'quantity' => 1
    );
}

echo "success";
?>
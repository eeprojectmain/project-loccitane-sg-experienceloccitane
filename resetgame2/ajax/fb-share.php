<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['id']);
$page = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['page']);
$member_id = $_SESSION['member']['id'];

$id = protect('decrypt', $id);

if (session_id() != $id) {
    exit();
}

$postfield = array(
    'media' => 'facebook',
    'member_id' => $member_id,
    'page' => $page,
    'created_date' => $time_config['now']
);

$queryInsert = get_query_insert($table['share'], $postfield);
$resultInsert = $databaseClass->query($queryInsert);
$genID = $resultInsert->insertID();

if ($page == "share-cafe") {
    $today = date("Y-m-d");
    $row_check = get_query_data_row($table['cafe_voucher'], "member_id=$member_id and created_date like '%$today%'");
    if ($row_check == 0) {
        $postfield = array(
            'member_id' => $member_id,
            'share_id' => $genID,
            'created_date' => $time_config['now']
        );
        $queryInsert = get_query_insert($table['cafe_voucher'], $postfield);
        $databaseClass->query($queryInsert);
    }
}

echo 'success';
exit();
?>
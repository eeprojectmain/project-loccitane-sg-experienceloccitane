<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";

if ($_POST) {
    $slot_date = $_POST['slot_date'];
    $slot_time = $_POST['slot_time'];

    // Retrieve the quota for the selected slot
    $stmt = $databaseClass->query("SELECT quota FROM dc_slot WHERE sdate='$slot_date' AND stime='$slot_time'");
    $quota = $stmt->fetchRow()['quota'];

    // Set status text and color based on quota
    if ($quota <= 0) {
        $status_text = 'Fully booked';
        $status_color = 'red';
    } elseif ($quota <= 5) {
        $status_text = 'Low availability';
        $status_color = 'yellow';
    } else {
        $status_text = 'Available';
        $status_color = 'white';
    }

    // Output status text and color as JSON
    $response = array(
        'status_text' => $status_text,
        'status_color' => $status_color,
    );
    echo json_encode($response);
} else {
    echo 'Error: No post data received';
}
?>

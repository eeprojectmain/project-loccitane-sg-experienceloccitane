<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$redeem_id=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['redeem_id']);

$queryUpdate=get_query_update($table['redeem'],$redeem_id,array('one_card'=>'1'));
$databaseClass->query($queryUpdate);

echo json_encode(array('result'=>'success','message'=>'Successfully Updated'));
exit();
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$today=$time_config['today'];
$diff=0;

function secondsToTime($inputSeconds)
{
    $secondsInAMinute = 60;
    $secondsInAnHour = 60 * $secondsInAMinute;
    $secondsInADay = 24 * $secondsInAnHour;

    // Extract days
    $days = floor($inputSeconds / $secondsInADay);

    // Extract hours
    $hourSeconds = $inputSeconds % $secondsInADay;
    $hours = floor($hourSeconds / $secondsInAnHour);

    // Extract minutes
    $minuteSeconds = $hourSeconds % $secondsInAnHour;
    $minutes = floor($minuteSeconds / $secondsInAMinute);

    // Extract the remaining seconds
    $remainingSeconds = $minuteSeconds % $secondsInAMinute;
    $seconds = ceil($remainingSeconds);

    // Format and return
    $timeParts = [];
    $sections = [
        'day' => (int)$days,
        'hour' => (int)$hours,
        'minute' => (int)$minutes,
        // 'second' => (int)$seconds,
    ];

    foreach ($sections as $name => $value) {
        if ($value > 0) {
            $timeParts[] = $value. ' '.$name.($value == 1 ? '' : 's');
        }
    }

    return implode(' ', $timeParts);
}

$resultRedeem=get_query_data($table['redeem'], "created_date like '%$today%' group by member_id limit 20");
$row_redeem=$resultRedeem->numRows();
while ($rs_redeem=$resultRedeem->fetchRow()) {
    $resultCheckin=get_query_data($table['checkin'], "member_id=".$rs_redeem['member_id']." and created_date like '%$today%' order by pkid asc limit 1");
    $rs_checkin=$resultCheckin->fetchRow();

    $date1 = strtotime($rs_checkin['created_date']);
    $date2 = strtotime($rs_redeem['created_date']);
  
    $diff += abs($date2 - $date1);
}

$wait_time=secondsToTime($diff/$row_redeem);

echo json_encode(array('result'=>'success','time'=>$wait_time));
exit();
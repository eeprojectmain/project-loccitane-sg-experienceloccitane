<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$visitorClass=new visitor();

$member_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['member_id']);
$station_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['station_id']);
$member_id = protect('decrypt', $member_id);

if (!is_numeric($member_id)) {
    $return_result = array("result" => "error", "message" => "Invalid QR Code");
    echo json_encode($return_result);
    exit();
}

$resultMember = get_query_data($table['member'], "pkid=$member_id");
$rs_member = $resultMember->fetchRow();

$resultCheckin = get_query_data($table['checkin'], "member_id=$member_id and station_id=$station_id");
$row_checkin = $resultCheckin->numRows();
$rs_checkin=$resultCheckin->fetchRow();

if ($row_checkin > 0) {
    if (substr($rs_checkin['created_date'], 0, -9)==$time_config['today']) {
        $return_result = array("title" => "Opps...", "result" => "error", "message" => "You've already checked in at this station");
    } else {
        $return_result = array("title" => "Opps...", "result" => "error", "message" => "You've already checked in this station on ".$rs_checkin['created_date']);
    }
} else {
    $postfield = array(
        "member_id" => $member_id,
        'station_id' => $station_id,
        'created_date' => $time_config['now']
    );

    $queryInsert = get_query_insert($table['checkin'], $postfield);
    $databaseClass->query($queryInsert);

    $visitorClass->add($member_id);

    $total_visitor=$visitorClass->get();

    $return_result = array("title" => "Yay!", "result" => "success", "message" => "COMPLETED - " . $station_array[$station_id],"total"=>$total_visitor,"name"=>$rs_member['first_name']." ".$rs_member['last_name']);
}

echo json_encode($return_result);
exit();

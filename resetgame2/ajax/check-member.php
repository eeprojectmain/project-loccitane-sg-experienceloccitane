<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$pkid=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
$pkid=protect('decrypt',$pkid);

$resultMember=get_query_data($table['member'],"pkid=$pkid");
$rs_member=$resultMember->fetchRow();

$row_onecard=get_query_data_row($table['redeem'],'one_card=1');

if($row_onecard<1000 && strtotime($rs_member['created_date']) < strtotime('2021-04-05')){
    echo json_encode(array('show_onecard'=>'1'));
}else{
    echo json_encode(array('show_onecard'=>'0'));
}
exit();
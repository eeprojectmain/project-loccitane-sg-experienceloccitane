<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$token = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['token']);
$member_id = $_SESSION['member']['id'];

if (protect('decrypt', $token) != session_id()) {
    exit();
}

$total_amount = 0;

foreach ($_SESSION['cart'] as $k => $v) {
    $product_id[] = $v['product_id'];
    $product_quantity[] = $v['quantity'];

    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    $total_amount += ($v['quantity'] * $rs_product['price']);
}

$product_id_text = implode(",", $product_id);
$product_quantity_text = implode(",", $product_quantity);

$postfield = array(
    'member_id' => $member_id,
    'product_id' => $product_id_text,
    'quantity' => $product_quantity_text,
    'total_amount' => $total_amount,
    'created_date' => $time_config['now']
);

$queryInsert = get_query_insert($table['order'], $postfield);
$resultInsert = $databaseClass->query($queryInsert);
$genID = $resultInsert->insertID();

$queryDelete = get_query_delete_all($table['cart'], "member_id=$member_id");
$databaseClass->query($queryDelete);

unset($_SESSION['cart']);

header("Location: ../checkout?order_id=$genID");
exit();

?>
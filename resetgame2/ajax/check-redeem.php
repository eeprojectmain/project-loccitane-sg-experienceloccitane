<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$visitorClass = new visitor();

$member_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['member_id']);
$member_id = protect('decrypt', $member_id);
$one_card = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['one_card']);

if (!is_numeric($member_id)) {
    $return_result = array("title" => "Opps...", "result" => "error", "message" => "Invalid QR Code");
    echo json_encode($return_result);
    exit();
}

$resultMember = get_query_data($table['member'], "pkid=$member_id");
$rs_member = $resultMember->fetchRow();

$resultRedeem = get_query_data($table['redeem'], "member_id=$member_id");
$row_redeem = $resultRedeem->numRows();
$rs_redeem = $resultRedeem->fetchRow();

$resultCheckin = get_query_data($table['checkin'], "member_id=$member_id and station_id<=7");
$row_checkin = $resultCheckin->numRows();

if ($row_redeem > 0) {
    $return_result = array("title" => "Opps...", "result" => "error", "message" => "Already redeemed on " . $rs_redeem['created_date']);
} elseif ($row_checkin < 4) {
    $return_result = array("title" => "Opps...", "result" => "error", "message" => "You need to check in " . (4 - $row_checkin) . " more station");
} else {
//    $row_checkin = $row_checkin - 1;
    $postfield = array(
        "member_id" => $member_id,
        'station' => $row_checkin,
        'one_card' => $one_card,
        'created_date' => $time_config['now']
    );

    $queryInsert = get_query_insert($table['redeem'], $postfield);
    $resultInsert = $databaseClass->query($queryInsert);
    $genID = $resultInsert->insertID();

    $row_onecard = get_query_data_row($table['redeem'], "one_card=1");

    $visitorClass->minus($member_id);
    $visitor_total = $visitorClass->get();

    $extra_message = "";
    if ($one_card == "1" && $row_onecard < 1000) {
        //$extra_message .= "<br><br>Extra Gift:<br>First 1000 One Card Member";
    }

    $blt_json = file_get_contents("https://blt.experienceloccitane.com/api/get_member?mobile=" . urlencode($rs_member['mobile']) . "&api_key=" . urlencode(protect('encrypt', 'vk')));
    $blt_json = json_decode($blt_json, true);

    if ($blt_json['result'] == 'success') {
        $extra_message .= "<br>BLT Member";
    }

    $resultGame = get_query_data($table['score'], "member_id=$member_id and type='blt' order by score desc limit 1");
    $row_game = $resultGame->numRows();

    if ($row_game > '0') {
        $rs_game = $resultGame->fetchRow();

        if ($rs_game['score'] <= '100') {
            $medal = 'BRONZE';
        } elseif ($rs_game['score'] <= '150') {
            $medal = 'GOLD';
        } elseif ($rs_game['score'] > '150') {
            $medal = 'PLATINUM';
        }

        $extra_message .= "<br>BLT Game $medal medal";
    }

    $return_result = array("title" => "Yay!", "result" => "success", "message" => "Successfully redeem ($row_checkin check-in)", 'admin_message' => "Successfully redeem ($row_checkin check-in) $extra_message", 'total' => $visitor_total, 'redeem_id' => $genID);
}

echo json_encode($return_result);
exit();
?>
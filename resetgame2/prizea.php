<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/resetgame/admin/config.php";
global $table;
$databaseClass = new database();

$althead=1;
/*
$pkid=trim($_SESSION['pkid']);
if ($pkid=="") {
    $pkid=rand(100,9999);
    $_SESSION['pkid']=$pkid;
}
*/
$score=$_SESSION['gscore'];
$v1=500; $v2=5000;
if ($score>=150){
    $v1=700; $v2=7000;
}
$_SESSION['smobileok']="";

$pkid=$_SESSION['genid'];
if ($pkid=="") {
    header("Location: /resetgame/game/?language=".$gamelang);
    exit();
}
?>
<?php include('head.php') ?>

<table width="100%" style="background-image:url('/resetgame/bg.jpg'); background-position:center; background-repeat:repeat-y;">
<tr><td align="center" style="padding:20px;">
    <table border=0 style="max-width:600px;color:#253783; font-size:12px;font-family:locci"><tr>
        <!--
        <tr><td style="padding:0px;">
            <img src="assets/img/register-1.png" class="img-fluid">
        -->
        <tr>

            <td><br>
            <div align="center">
                <a href="/resetgame/event-index"><img src="assets/img/logo-title-blue.png" class="img-fluid" style="max-height:60px; margin:2px;"></a>
            </div>
            <div class="pgtitle" style="color:#253783;font-size:34px; text-align:center; font-family:locci-serif-bold"><br>It's time to RESET!</div>
            
            <div class="pgtitle" style="color:#253783;font-size:18px; text-align:center; font-weight:bold;">
                <br><br><b style='font-size:24px; font-weight:bold; font-family:locci-bold'>Congratulations!</b>
                <br><br>You won<br>
                <i><b style='font-size:48px; font-weight:bold; font-family:locci-bold'>&#8377;<?=$v1?> off</b></i><br>
                on purchase of &#8377;<?=$v2?> & above
                <br><br>
                <br><br>
                It’s time to get your GLOW back!
                <br><br>
                <br><br>Capture this screen and show this to our beauty advisor at the cashier to collect your voucher and unlock glowing skin
                <br><br>
                <br><br><b style="font-family:locci-bold;">Score: <?=$score?></b>
                <br><br>&nbsp;
            </div>
            <div>

            </div>
        <tr><td style="padding-top:580px;">&nbsp;</td></tr>    
    </table>
</table>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>

</body>
</html>
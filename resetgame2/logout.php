<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

session_destroy();
setcookie('auth', '', time() - 3600, '/');
setcookie('pkid', '', time() - 3600, '/');
header("Location: login");
exit();
?>
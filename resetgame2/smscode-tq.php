<?php
session_start();

require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<table width="100%"><tr><td align="center">
<tr><td align="center">
    <table border=0 style="max-width:600px;"><tr>
        <tr><td style="padding:15px;">
            <img src="assets/img/register-1.png" class="img-fluid">
        <tr><td style="padding:30px;">    
            <br>
            <div class="pgtitle">
                Thank you for<Br> pre-registering
            </div>
            <br>
            Our butlers are putting on some final magical touches.
            <br><br>
            L’OCCITANE Hotel welcomes you <br>from <b class="loccib">26 May – 11 June 2023</b><br>at <b class="loccib">Pavilion Bukit Jalil, <br>Level 3 (Orange Zone), Lot 3.28.00</b>.
            <br><br>
            <b class="loccib">L'OCCITANE Gold members can enjoy priority<br> check-in from 26 – 28 May 2023.</b>
            <br><br>
            <p style="color;red">Reserve your timeslot below for priority check-in.</p> <br><br>
        
        
        <table width="100%">
        <tr>
            <td align="center"><br>
            <a href="https://calendar.google.com/calendar/render?action=TEMPLATE&dates=20230526T020000Z%2F20230611T100000Z&location=Pavilion%20Bukit%20Jalil%2C%20Level%203%20%28Orange%20Zone%29%2C%20Lot%203.28.00&text=L%E2%80%99OCCITANE%20Hotel%20"><div style="max-width:250px;" class="yellowb btn btn-block btn-yellow">Save the date</div></a>
        </td>
        </tr>
        <?php 
        if (isset($_SESSION['pkid'])){ ?>
        <tr>
            <td align="center"><br>
                <a href="room-key"><div style="max-width:250px;" class="blueb btn btn-block btn-secondary">Check Room Key</div></a>
            </td>
        </tr>
        <?php } ?>
            
        </table>
    </table>
</table>


<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    function share_fb() {
        FB.ui({
            method: 'share',
            href: 'https://my.loccitanehotel.com/event-index',
            hashtag: '#loccitanehotel'
        }, function (response) {
            if (typeof response != "undefined") {
                $.ajax({
                    method: "POST",
                    url: "ajax/fb-share",
                    data: {id: "<?=protect('encrypt', session_id())?>", page: "event-index"}
                })
                    .done(function (data) {
                        swal("Bravo!", "Successfully shared to Facebook", "success");
                    });
            } else {
                swal("", "The more the merrier, share this event with your friends & family!");
            }
        });
    }
</script>
</body>

</html>
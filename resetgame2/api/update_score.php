<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/vendor/autoload.php";
global $table;
$databaseClass = new database();

use PubNub\PubNub;
use PubNub\PNConfiguration;

$qr_code = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['qr_code']);
$pkid = protect('decrypt', $qr_code);
$score = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['score']);

$resultMember = get_query_data($table['member'], "pkid=$pkid");
$rs_member = $resultMember->fetchRow();

if ($rs_member['pkid'] == "") {
    echo json_encode(array('result' => 'error', 'message' => 'user not found'));
    exit();
}

$postfield = array(
    'member_id' => $pkid,
    'type' => 'blt',
    'score' => $score,
    'created_date' => $time_config['now']
);

$queryInsert = get_query_insert($table['score'], $postfield);
$databaseClass->query($queryInsert);

$resultCheckin = get_query_data($table['checkin'], "member_id=$pkid and station_id=5");
$row_checkin = $resultCheckin->numRows();

if ($row_checkin == 0) {
    $postfield = array(
        'member_id' => $pkid,
        'station_id' => '5',
        'created_date' => $time_config['now']
    );

    $queryInsert = get_query_insert($table['checkin'], $postfield);
    $databaseClass->query($queryInsert);

    $pnconf = new PNConfiguration();
    $pubnub = new PubNub($pnconf);

// Use the publish command separately from the Subscribe code shown above.
// Subscribe is not async and will block the execution until complete.
    $result = $pubnub->publish()
        ->channel(protect('encrypt',$pkid))
        ->message(array('title'=>'Yay!','result'=>'success','message'=>'COMPLETED - '.$station_array[5],'station'=>'5'))
        ->sync();
}

echo json_encode(array('result' => 'success', 'message' => 'score saved'));
exit();
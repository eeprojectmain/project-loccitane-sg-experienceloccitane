<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<script src="js/formValidation.min.js?v=2"></script>
<script src="js/framework/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="assets/js/script.min.js"></script>
<script src="assets/js/jquery.session.js"></script>
<!-- sweetalert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<!-- Instantiate PubNub -->
<script src="js/pubnub.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        if ($.session.get("QrSection") == "hide") {
            $(".qr-section").slideUp('fast');
            $(".qr-arrow").removeClass("fa-angle-double-up");
            $(".qr-arrow").addClass("fa-angle-double-down");
        } else {
            $(".qr-section").slideDown('fast');
            $(".qr-arrow").removeClass("fa-angle-double-down");
            $(".qr-arrow").addClass("fa-angle-double-up");
        }

        $(window).scroll(function () {
            var height = $(window).scrollTop();

            if (height <= 60) {
                $(".float-icon").css('top', 'auto');
            } else {
                $(".float-icon").css('top', '0');
            }
        });
    });

    var checkins = 0;

    $("form").formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            /*
            email: {
                message: 'This field is required',
                validators: {
                    remote: {
                        message: 'The email address already registered',
                        url: 'ajax/email-check',
                        type: 'POST',
                        delay: 500
                    }
                }
            },
            */
            mobile: {
                message: 'This field is required',
                validators: {
                    remote: {
                        message: 'This phone number is already registered.',
                        url: 'ajax/mobile-check',
                        type: 'POST',
                        delay: 500
                    }
                }
            }
        }
    });
    
    function checkfrm(){
        var tmpcc=document.getElementById('country').value;
        if (tmpcc=='+65') {
            var tmpmm=document.getElementById('mobile').value;    
            if (tmpmm.length>=8){
                if (tmpmm.substring(0,1)!='8' && tmpmm.substring(0,1)!='9') {
                    document.getElementById('ttmobile').style.display='';
                    return false;
                }
                else {
                    document.getElementById('ttmobile').style.display='none';
                    return true;
                }
            }
        }
        document.getElementById('ttmobile').style.display='none';
        return true;
    }
    

    <? if($swal){?>
    swal("<?=$swal['title']?>", "<?=$swal['msg']?>", "<?=$swal['type']?>");
    <?}?>

    function confirmCheckout(e) {
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "By confirm this order, our staff will start to pack your order.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((value) => {
                if (value) {
                    $(".checkoutForm")[0].submit();
                } else {
                    return false;
                }
            });
    }

    /*$("input").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
        }
    });*/

    function check_station() {
        $.ajax({
            method: "POST",
            url: "ajax/check-station",
            dataType: 'json',
            data: {'token': '<?=protect('encrypt', $_SESSION['member']['id'])?>'},
        })
            .done(function (data) {
                var required = 0;
                checkins = data.checkin;
                $("#total-checkin").html(checkins);

                $(data.completed).each(function (index, value) {
                    var str = $(".station-" + value).attr('src');
                    var patt = new RegExp("done");
                    var res = patt.test(str);

                    required += 1;

                    if (!res) {
                        var img = $(".station-" + value).attr('src');
                        $(".station-" + value).attr('src', img.split(".")[0] + "-done." + img.split(".")[1]);
                    }
                });

                if (required >= 4) {
                    $(".station-8").css('opacity', '1');
                    $(".station-8").attr("data-toggle", "modal");
                }

                if (data.redeem > 0) {
                    $(".station-8").attr("src", "assets/img/icon-checkout-done.png");
                }
            });
    }

    function hideQR() {
        if ($(".qr-arrow").hasClass("fa-angle-double-up")) {
            $(".qr-section").slideUp('500');
            $(".qr-arrow").removeClass("fa-angle-double-up");
            $(".qr-arrow").addClass("fa-angle-double-down");
            $.session.set("QrSection", "hide");
        } else {
            $(".qr-section").slideDown('500');
            $(".qr-arrow").removeClass("fa-angle-double-down");
            $(".qr-arrow").addClass("fa-angle-double-up");
            $.session.set("QrSection", "show");
        }
    }

    $(window).bind('load', function () {
        $('.page-loader').addClass('loaded');
        check_station();
    });
</script>
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '800121447587288');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=800121447587288&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123164816-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    //gtag('config', 'UA-123164816-1');
	gtag('config', 'UA-151327741-2');
</script>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?=$site_config['fb_app_id']?>',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v3.1'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

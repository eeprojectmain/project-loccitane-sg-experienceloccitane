<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/resetgame/admin/config.php";
global $table;
$databaseClass = new database();

$althead=1;
?>
<?php include('head.php') ?>

<table width="100%" style="background-image:url('/resetgame/bg.jpg'); background-position:top; background-repeat:repeat-y; pointer:cursor;" onclick="window.location.href='/resetgame/ready'">
<tr><td align="center" style="padding:20px;">
    <table border=0 style="max-width:600px;color:#253783; font-size:12px;font-family:locci; pointer:cursor;"><tr>
        <tr>

            <td><br>
            <div align="center">
                <a href="/resetgame/event-index"><img src="assets/img/logo-title-blue.png" class="img-fluid" style="max-height:60px; margin:2px;"></a>
            </div>
            <div class="pgtitle" style="color:#253783;font-size:34px; text-align:center; font-family:locci-serif"><br><i>What is <b style="font-family:locci-serif-bold">"RESET"?</b></i></div>
            
            <div class="pgtitle" style="color:#253783;font-size:18px; text-align:center; font-weight:bold;">
                
<br><br>
Our bestselling Immortelle Reset Serum does exactly what it's name says: it acts like a reset button for your skin. With the power of Immortelle and soothing  botanical extracts, you can 
<br><br>
<b style="font-family:locci-bold;font-weight:bold;font-size:20px;">
Reset your glow<br>
Reset your hydration<br>
Reset your signs of stress</b>
<br><br>
It's as easy as pressing a reset button!
    <br><br>
    <br><a href="/resetgame/ready"><img src="flower.png"></a>
    <br><br>
<div class="form-group mt-3 text-center" align="center"><table width="100%"><tr><td align="center">
                        <button style="max-width:250px;" onclick="window.location.href='/resetgame/ready';" class="blueb btn btn-block" type="button"
                                 value="true">
                            Next
                        </button>
                        </td></tr></table>
                    </div>
    <br><br>&nbsp;

            </div>
            <div>

            </div>
        <tr><td style="padding-top:430px;">&nbsp;</td></tr>    
    </table>
</table>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>

</body>
</html>
<?php
session_start();

require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
/*if(strtotime('now') > strtotime('2022-02-20')){
	header("Location: event-index");
    exit();
}*/

$mobile="";
$email="";
$country2="";
$mobile2="";
$error="";

// $aaa = trim(get('aaa'));
// if ($aaa=="") {$aaa=0;}
// $aaa++;
// set('aaa',$aaa);

// $bbb = trim(getc('bbb'));
// if ($bbb=="") {$bbb=0;}
// $bbb++;
// setc('bbb',$bbb);

if (isset($_COOKIE['auth'])) {
    $authToken = $_COOKIE['auth'];
    $salt = 'hotelloccitane';
    $authTokenWithSalt = $authToken . $salt;
    $hashedAuthToken = hash('sha256', $authTokenWithSalt);
    $pkid = $_COOKIE['pkid'];
    $sql = "SELECT pkid FROM dc_member WHERE pkid = '$pkid' AND auth_token = '$hashedAuthToken'";
    $rs = $databaseClass->query($sql);
    $result = $rs->fetchRow();
    
    if ($result) {
        set('pkid',$result['pkid']);
        header("Location: room-key");
        exit;
    } 
    
    else {
        // Unauthorized user, delete cookie and redirect to login page
        setcookie('auth', '', time() - 3600, '/');
        setcookie('pkid', '', time() - 3600, '/');
        // header("Location: login");
        // exit;
    }
}

if ($_POST['submit_login']){
    unset($postfield['submit_login']);
    $postfield['mobile'] = updateMobileFormat(frmp('mobile'),frmp('country'));
    $postfield['email'] = frmp('email');
    $mobile = trim($postfield['mobile']);
    $email = ($postfield['email']);

    $sql="SELECT * FROM dc_member WHERE (mobile='$mobile' AND email = '$email') AND sms_status=1";
    $rslogin = $databaseClass->query($sql);
    $login = $rslogin->fetchRow();
    if ($login['mobile'] == $postfield['mobile'] && $login['email'] == $postfield['email']){
        $pkid = $login['pkid'];
        $salt = 'hotelloccitane';
        $authToken = bin2hex(random_bytes(16));
        $authTokenWithSalt = $authToken . $salt;
        $hashedAuthToken = hash('sha256', $authTokenWithSalt);
        
        $sql = "UPDATE dc_member SET auth_token = '$hashedAuthToken' WHERE pkid = '$pkid'";
        if ($databaseClass->query($sql)){
            setc('auth', $authToken, time() + (86400 * 60), '/');
            setc('pkid',$pkid, time() + (86400 * 60), '/');
            set('pkid',$pkid);
            header("Location: room-key");
            exit;
        }
    }
    $country2=frmp("country");
    $mobile2=frmp("mobile");
    $error="<br><b style='color:red;'>Login is invalid / inactive</b>";
}
?>
<?php include('head.php') ?>
<table width="100%" style="">
<tr><td align="center" style="padding:20px;">
    <table border=0 style="max-width:600px;"><tr>
        <tr><td style="padding:0px;">
            <img src="assets/img/register-1.png" class="img-fluid">
        <tr>
            <td >
            <div class="pgtitle" font-family: 'Brush Script MT', cursive;>
                Check-in
            </div>
            <div>
                Start your stay in <strong>L'OCCITANE Hotel</strong> and enjoy the magical experiences. 
            </div style="padding:30px;">
            <div style="text-align:center;">
                <?=$error?>
                <form method="post" action="login" style="margin-top:15px;" class="">
                    <div class="form-group">
                        <div class="input-group">
                                <select id="country" name="country" class="fginput" style="margin-right:10px;">
                                <option value="+60" <?=($country2=='+60' ? "selected":"")?>>+60</option>
                                <option value="+65" <?=($country2=='+65' ? "selected":"")?>>+65</option>
                                </select>
                            <input class="fginput form-control" type="tel"  name="mobile" required=""
                                    value="<?=($mobile2 ? $mobile2:"")?>"
                                   placeholder="Mobile (eg.12987XXXX)" maxlength="10" minlength="8">
                            </div>
                    </div>
                    <div class="form-group">
                        <input class="fginput form-control justify-content-center align-items-center align-content-center align-self-center"
                            type="email" name="email" required="" placeholder="Email Address" maxlength="150"
                            value="<?= ($email ? $email:$_SESSION['facebook']['email']) ?>">
                    </div>
                    <div class="form-group mt-3 text-center" align="center"><table width="100%"><tr><td align="center"><br>
                        <button id="btnsubmit" style="max-width:250px;" class="yellowb btn btn-block btn-yellow" type="submit"
                                name="submit_login" value="true">
                            Check-in
                        </button>
                        </td></tr></table>
                    </div>
                </form>
            </div>
    </table>
</table>
<?php include('footer.php') ?>

</body>
</html>
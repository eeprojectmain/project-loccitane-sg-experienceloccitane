<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$merchantClass = new merchant();
$emailClass = new email();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['order'], "pkid=$pkid");
$rs_array = $result->fetchRow();

$resultProduction = get_query_data($table['production_order'], "order_id=$pkid");
$rs_production = $resultProduction->fetchRow();

$resultProductionRecord = get_query_data($table['production_order_history'], "order_id=" . $rs_production['pkid'] . " order by pkid desc");
$rs_productionRecord = $resultProductionRecord->fetchRow();

$resultPaymentMethod = get_query_data($table['payment_method'], "pkid=" . $rs_array['payment_method_id']);
$rs_paymentMethod = $resultPaymentMethod->fetchRow();

$resultPaymentRecord = get_query_data($table['payment_record'], "order_id=$pkid order by pkid desc limit 1");
$rs_paymentRecord = $resultPaymentRecord->fetchRow();

$type = explode("-", $rs_array['product_id'])[0];
$id = explode("-", $rs_array['product_id'])[1];

if ($type == "C") {
    $resultCustom = get_query_data($table['custom_result'], "pkid=$id");
    $rs_custom = $resultCustom->fetchRow();

    $resultBuilder = get_query_data($table['custom_builder'], "pkid=" . $rs_custom['builder_id']);
    $rs_builder = $resultBuilder->fetchRow();

    $options = json_decode($rs_custom['options'], true);

    $shirt_type = $options['type'];
    $color_type = $options['color'];
    $size_type = $options['size'];
    $graphic_type = $options['graphic'];
    $text_type = $options['text'];
    $number_type = $options['number'];

    $seller_name = $merchantClass->getMerchantDetails($rs_builder['merchant_id'])['business_name'];
}

if ($rs_array['type'] == "2") {
    $customer_name = $options['name'];
    $customer_contact = $options['contact'];
    $customer_email = $options['email'];
} else {
    $resultMember = get_query_data($table['member'], "pkid=" . $rs_array['member_id']);
    $rs_member = $resultMember->fetchRow();

    $customer_name = $rs_member['first_name'] . " " . $rs_member['last_name'];
    $customer_contact = $rs_member['phone'];
    $customer_email = $rs_member['email'];
}

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);

    $postfield['updated_date'] = $time_config['now'];
    $postfield['updated_by'] = $_SESSION['admin']['username'];

    $row_production = get_query_data_row($table['production_order'], "order_id=$pkid");

    if ($row_production == 0) {
        $product_id_array = explode(",", $rs_array['product_id']);

        foreach ($product_id_array as $k => $v) {
            $postfield_production = array(
                'order_id' => $pkid,
                'product_id' => $v,
                'created_date' => $time_config['now']
            );

            $queryInsert = get_query_insert($table['production_order'], $postfield_production);
            $database->query($queryInsert);
        }
    }

    //update production order
    $queryUpdate = "update " . $table['production_order'] . " set status=" . $postfield['production_status'] . ",shipping_id=" . $postfield['shipping_id'] . ",shipping_code='" . $postfield['shipping_code'] . "',updated_date='" . $time_config['now'] . "',updated_by='" . $_SESSION['admin']['name'] . "' where order_id=$pkid";
//    $queryUpdate = get_query_update($table['production_order'], $rs_production['pkid'], array('status' => $postfield['production_status'], 'shipping_id' => $postfield['shipping_id'], 'shipping_code' => $postfield['shipping_code'], 'updated_date' => $time_config['now'], 'updated_by' => $_SESSION['admin']['name']));
    $database->query($queryUpdate);

    $postfield_record = array(
        'order_id' => $rs_production['pkid'],
        'admin_user_id' => $_SESSION['admin']['id'],
        'status' => $postfield['production_status'],
        'created_date' => $time_config['now'],
    );

    //update shipping
    if ($postfield['shipping_id'] != "" && $postfield['shipping_code'] != "" && $rs_array['shipping_id'] == "2") {
        $postfield_email = array(
            'product_items' => file_get_contents($site_config['full_url'] . "email-templates/payment-success-item.php?order_id=" . $rs_array['pkid']),
            'tracking_url' => "https://www.tracking.my/externalcall?style=iframebox&lang=en&tracking_no=" . $postfield['shipping_code'],
            'first_name' => $customer_name,
            'order_id' => "#" . $rs_array['pkid'],
            'order_date' => date("d F Y", strtotime($rs_array['created_date'])),
            'order_details_url' => $site_config['full_url'] . "member/order/view?id=" . $rs_array['pkid'],
            'collect_address' => $site_config['collect_address']
        );
        $emailClass->sendEmail('order_shipped', $customer_email, $postfield_email);
    }

    $queryInsert = get_query_insert($table['production_order_history'], $postfield_record);
    $database->query($queryInsert);

    //insert payment record
    $postfield_payment = array(
        'order_id' => $rs_array['pkid'],
        'payment_method_id' => $rs_array['payment_method_id'],
        'payment_amount' => $postfield['payment_amount'],
        'trans_id' => $postfield['payment_trans_id'],
        'created_date' => $postfield['payment_created_date'],
        'created_by' => $_SESSION['admin']['username']
    );

    $queryInsert = get_query_insert($table['payment_record'], $postfield_payment);
    $database->query($queryInsert);

    unset($postfield['payment_created_date']);
    unset($postfield['payment_trans_id']);
    unset($postfield['payment_amount']);

    unset($postfield['production_status']);
    unset($postfield['shipping_code']);

    //update main order
    $queryUpdate = get_query_update($table['order'], $pkid, $postfield);
    $database->query($queryUpdate);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Order <i class="fa fa-angle-right"></i> Edit</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a href="<?= $this_folder ?>">Order</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post"
                              action="<?= $this_folder ?>/edit?<?= http_build_query($_GET) ?>"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Order ID</label>
                                        <div class="col-sm-5">
                                            <label class="control-label">#<?= $pkid ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Order Date</label>
                                        <div class="col-sm-5">
                                            <label class="control-label"><?= $rs_array['created_date'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Order Type</label>
                                        <div class="col-sm-5">
                                            <label class="control-label"><?= $rs_array['type'] == "1" ? "ONLINE" : "IN-STORE" ?></label>
                                        </div>
                                    </div>
                                    <? if ($type == "C") { ?>
                                        <hr>
                                        <h3>Product</h3>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Product Type</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= $type == "C" ? "Customized" : "Seller Design" ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Shirt</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= $shirt_type ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Color</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= $color_type ?></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Size</label>
                                            <div class="col-sm-10">
                                                <label class="control-label"><?= $size_type ?></label>
                                            </div>
                                        </div>
                                    <? } ?>
                                    <hr>
                                    <h3>Customer</h3>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Customer Name</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $customer_name ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Customer Email</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $customer_email ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Customer Contact</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $customer_contact ?></label>
                                        </div>
                                    </div>
                                    <hr>
                                    <h3>Order Details</h3>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-6">
                                            <?= file_get_contents($site_config['full_url'] . "email-templates/payment-success-item.php?order_id=$pkid") ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Sub Total</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['currency'] . " " . $rs_array['product_amount'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Shipping</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['currency'] . " " . $rs_array['shipping_amount'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Discount</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['currency'] . " " . $rs_array['discount_amount'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Grand Total</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['currency'] . " " . $rs_array['final_amount'] ?></label>
                                        </div>
                                    </div>
                                    <?
                                    if ($rs_array['type'] == "1") {
                                        ?>
                                        <hr>
                                        <h3>Payment</h3>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Payment Status</label>
                                            <div class="col-sm-10">
                                                <div class="radio radio-inline radio-success">
                                                    <input type="radio" name="payment_status" id="status_1"
                                                           value="1" <?= $rs_array['payment_status'] == "1" ? "checked" : "" ?>>
                                                    <label for="status_1"> Paid </label>
                                                </div>
                                                <div class="radio radio-inline radio-danger">
                                                    <input type="radio" name="payment_status" id="status_0"
                                                           value="0" <?= $rs_array['payment_status'] == "0" ? "checked" : "" ?>>
                                                    <label for="status_0"> Unpaid </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Payment Method</label>
                                            <div class="col-sm-10">
                                                <?
                                                $resultPaymentMethod = get_query_data($table['payment_method'], "");
                                                while ($rs_paymentMethod = $resultPaymentMethod->fetchRow()) {
                                                    ?>
                                                    <div class="radio radio-inline">
                                                        <input type="radio" name="payment_method_id"
                                                               id="production_status_<?= $rs_paymentMethod['pkid'] ?>"
                                                               value="<?= $rs_paymentMethod['pkid'] ?>" <?= $rs_array['payment_method_id'] == $rs_paymentMethod['pkid'] ? "checked" : "" ?>>
                                                        <label for="production_status_<?= $rs_paymentMethod['pkid'] ?>"> <?= $rs_paymentMethod['title'] ?> </label>
                                                    </div>
                                                <? } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Payment Date</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control date" name="payment_created_date"
                                                       value="<?= $rs_paymentRecord['created_date'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Transaction ID</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="payment_trans_id"
                                                       value="<?= $rs_paymentRecord['trans_id'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Payment Amount</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="payment_amount"
                                                       value="<?= $rs_paymentRecord['payment_amount'] ?>">
                                            </div>
                                        </div>
                                        <!--<div class="form-group">
                                            <label class="control-label col-sm-2">Payment Gateway Charge</label>
                                            <div class="col-sm-5">
                                                <label class="control-label"><? /*= $rs_paymentRecord['payment_gateway_charge'] */ ?></label>
                                            </div>
                                        </div>-->
                                        <hr>
                                        <h3>Payment History</h3>
                                        <div class="table-responsive">
                                            <table class="table table-hover manage-u-table" id="data-table">
                                                <thead>
                                                <tr>
                                                    <th>Status</th>
                                                    <th>Method</th>
                                                    <th>Amount</th>
                                                    <th>Message</th>
                                                    <th>Bank Slip</th>
                                                    <th>Date</th>
                                                    <th>Created By</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?
                                                $resultPaymentHistory=get_query_data($table['payment_record'],"order_id=$pkid order by pkid asc");
                                                while ($rs_paymentHistory = $resultPaymentHistory->fetchRow()) {
                                                    $resultPaymentMethod=get_query_data($table['payment_method'],"pkid=".$rs_paymentRecord['payment_method_id']);
                                                    $rs_paymentMethod=$resultPaymentMethod->fetchRow();
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?=$statusClass->getPaymentStatusLabel($rs_paymentHistory['status'],$rs_array['type'])?>
                                                        </td>
                                                        <td><?= $rs_paymentMethod['title'] ?></td>
                                                        <td><?= $rs_paymentHistory['payment_amount'] ?></td>
                                                        <td><?= $rs_paymentHistory['message'] ?></td>
                                                        <td><?= $rs_paymentHistory['bank_slip_url']!=""?"<a href='../files/bankin/".$rs_paymentHistory['bank_slip_url']."' target='_blank'>".$rs_paymentHistory['bank_slip_url']."</a>":"-" ?></td>
                                                        <td><?= $rs_paymentHistory['created_date'] ?></td>
                                                        <td><?= $rs_paymentHistory['created_by'] ?></td>
                                                    </tr>
                                                <? } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <hr>
                                        <h3>Shipping</h3>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Tracking Code</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="shipping_code"
                                                       value="<?= $rs_production['shipping_code'] ?>">
                                            </div>
                                        </div>
                                    <? } ?>
                                    <hr>
                                    <h3>Production</h3>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Production Status</label>
                                        <div class="col-sm-10">
                                            <?
                                            foreach ($order_status_array as $k => $v) {
                                                ?>
                                                <div class="radio radio-inline">
                                                    <input type="radio" name="production_status"
                                                           id="production_status_<?= $k ?>"
                                                           value="<?= $k ?>" <?= $rs_production['status'] == $k ? "checked" : "" ?>>
                                                    <label for="production_status_<?= $k ?>"> <?= $v ?> </label>
                                                </div>
                                            <? } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Processed By</label>
                                        <div class="col-sm-5">
                                            <label class="control-label"><?= $rs_production['updated_by'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Production Time</label>
                                        <div class="col-sm-5">
                                            <label class="control-label"><?= timeLength($rs_array['created_date'], $rs_productionRecord['created_date']) ?></label>
                                        </div>
                                    </div>
                                    <?
                                    $resultHistory = get_query_data($table['production_order_history'], "order_id=" . $rs_production['pkid'] . " order by pkid asc");
                                    $row_history = $resultHistory->numRows();

                                    if ($row_history > 0) {
                                        ?>
                                        <hr>
                                        <h3>Production History</h3>
                                        <div class="table-responsive">
                                            <table class="table table-hover manage-u-table" id="data-table">
                                                <thead>
                                                <tr>
                                                    <th>Status</th>
                                                    <th>Remark</th>
                                                    <th>User</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?
                                                while ($rs_history = $resultHistory->fetchRow()) {
                                                    if ($rs_history['user_id'] == "0") {
                                                        $resultHistoryUser = get_query_data($table['user'], "pkid=" . $rs_history['admin_user_id']);
                                                        $rs_historyUser = $resultHistoryUser->fetchRow();
                                                    } else {
                                                        $resultHistoryUser = get_query_data($table['production_user'], "pkid=" . $rs_history['user_id']);
                                                        $rs_historyUser = $resultHistoryUser->fetchRow();
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <label class="btn btn-<?= $order_status_class_array[$rs_history['status']] ?>"><?= $order_status_array[$rs_history['status']] ?></label>
                                                        </td>
                                                        <td><?= str_replace("\\r\\n", "<br>", $rs_history['message']) ?></td>
                                                        <td><?= $rs_historyUser['name'] ?></td>
                                                        <td><?= $rs_history['created_date'] ?></td>
                                                    </tr>
                                                <? } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <? } ?>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Last Update Date</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['updated_date'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Last Update By</label>
                                        <div class="col-sm-10">
                                            <label class="control-label"><?= $rs_array['updated_by'] ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png'],
    });
</script>
</body>

</html>
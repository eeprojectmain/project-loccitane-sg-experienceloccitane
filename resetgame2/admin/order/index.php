<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$currencyClass = new currency();
$merchantClass = new merchant();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['machine_id']) {
        $where_array[] = "find_in_set(" . $postfield['machine_id'] . ", cast(key_id as char)) > 0";
    }
    if ($postfield['type_id']) {
        $where_array[] = "type=" . $postfield['type_id'];
    }

    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);

    if (!empty($where)) {
        $where_query .= " and " . $where;
    }

    $resultOrder = get_query_data($table['order'], "1" . $where_query);
    while ($rs_order = $resultOrder->fetchRow()) {
        $product_array = explode(",", $rs_order['product_id']);
        $quantity_array = explode(",", $rs_order['quantity']);

        $product_text = "";

        foreach ($product_array as $k => $v) {
            $resultProduct = get_query_data($table['product'], "pkid=$v");
            $rs_product = $resultProduct->fetchRow();

            $product_text .= $rs_product['title'] . " <label class='label label-warning'>x" . $quantity_array[$k] . "</label><br>";
        }

        $resultMember = get_query_data($table['member'], "pkid=" . $rs_order['member_id']);
        $rs_member = $resultMember->fetchRow();

        $all_buttons = "";
        $all_buttons .= '<a href="remote-view/order-receipt?order_id='.$rs_order['pkid'].'" target="_blank" class="btn btn-primary waves-effect waves-light"><i class="fa fa-print fa-fw"></i> <span>Print</span></a> ';
        if ($rs_order['status']=="0") {
            $all_buttons .= '<button type="button" onclick="markComplete(' . $rs_order['pkid'] . ')"  class="btn btn-success waves-effect waves-light"><i class="fa fa-check fa-fw"></i> <span>Mark as complete</span></button> ';
        }

        if ($_SESSION['admin']['role'] == "1") {
            $all_buttons .= $buttonClass->get_delete_button($this_folder, $rs_order['pkid']);
        }

        if ($rs_order['status'] == "1") {
            $order_id = '#' . $rs_order['pkid'] . " <label class='label label-success'>Completed</label>";
        } else {
            $order_id = '<i class="fa fa-exclamation-triangle fa-spin text-danger"></i> #' . $rs_order['pkid'];
        }

        $resultOutlet=get_query_data($table['outlet'], "pkid=".$rs_order['outlet_id']);
        $rs_outlet=$resultOutlet->fetchRow();

        $dataTable['data'][] = array(
            $order_id,
            time_ago($rs_order['created_date'])."<br>".$rs_order['created_date'],
            $rs_member['first_name']." ".$rs_member['last_name'] . "<br>" . $rs_member['mobile'],
            $product_text,
            number_format($rs_order['total_amount']),
            $all_buttons
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $resultDelete = get_query_data($table['order'], "pkid=$pkid");
    $rs_delete = $resultDelete->fetchRow();

    $query = get_query_delete($table['order'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">

    <audio controls id="notification" style="width: 1px;height: 1px">
        <source src="/admin/noti.mp3" type="audio/mpeg">
    </audio>
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <?php include('../pre-loader.php') ?>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include('../nav.php') ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Orders</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="dashboard">Dashboard</a></li>
                            <li class="active">Orders</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Order ID</th>
                                                <th>Date</th>
                                                <th>Customer</th>
                                                <th>Product</th>
                                                <th>Amount</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include('../footer.php') ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- /#wrapper -->
    <?php include('../js-script.php') ?>
    <script>
        var autoprint;

        var table = $('#datatable').DataTable({
            "ajax": {
                "url": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
            },
            "order": [
                [0, "asc"]
            ],
            responsive: true,
            dom: "<'row'<'col-sm-12'B>><'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
            $(this)[0].onchange = function() {
                autoprint = $(this)[0].checked;
                $.ajax({
                        method: "POST",
                        url: "ajax/save-auto-print",
                        data: {
                            status: autoprint
                        }
                    })
                    .done(function(data) {
                        console.log(data);
                    });
            };
        });

        setInterval(function(e) {
            table.ajax.reload(null, false);
            $.ajax({
                    method: "POST",
                    url: "ajax/check-new-order",
                    dataType: "json"
                })
                .done(function(data) {
                    if (data.print == 'true') {
                        $("#notification")[0].play();
                        if (data.print_receipt=="0") {
                            window.open('remote-view/order-receipt?order_id=' + data.order_id);
                        }
                    }
                });
        }, 30000);

        setInterval(function(e) {
            window.location.reload();
        }, 900000);

        function markComplete(id) {
            swal({
                title: "Are you sure?",
                text: "Once marked as complete, you will not be able to unmark it.",
                icon: "info",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "ajax/order-complete",
                        type: 'POST',
                        data: {
                            id: id
                        },
                        dataType: 'json',
                        success: function(data) {
                            if (data.result == "success") {
                                table.ajax.reload(null, false);
                                swal("Bravo!", "Successfully updated", {
                                    icon: "success",
                                    timer: 1000,
                                });
                            } else {
                                swal("Opps...", "Something went wrong, please try again.", {
                                    icon: "error",
                                });
                            }
                        }
                    });
                }
            })
        }
    </script>
</body>

</html>
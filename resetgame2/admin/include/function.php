<?php
function time_ago($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function timeLength($from,$to, $full = false) {
    $now = new DateTime($from);
    $ago = new DateTime($to);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ' : ' ';
}

function get_module_by_id($pkid)
{
    global $table;
    $database = new database();

    $query = "select * from " . $table['module'] . " where pkid='$pkid'";
    $result = $database->query($query);
    $rs_array = $result->fetchRow();

    return $rs_array;
}

function get_module($folder)
{
    global $table;
    $database = new database();

    $query = "select * from " . $table['module'] . " where folder='$folder'";
    $result = $database->query($query);
    $rs_array = $result->fetchRow();

    return $rs_array;
}

function mail_template_replace($content, $value)
{
    foreach ($value as $k => $v) {
        $content = str_replace("@@$k@@", $v, $content);
    }

    return $content;
}

function send_email($value, $postfields)
{
    $database = new database();
    global $table, $admin_site_title;

    $admin_email = get_site_admin_email();

    $query = 'select * from ' . $table['setting'] . " where type='email_template' and value='$value' and status=1";
    $result = $database->query($query);
    $rs_array = $result->fetchRow();
    $row = $result->numRows();

    if ($row > 0) {
        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "tls";
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 587;
        $mail->Username = "default.system.notification@gmail.com";
        $mail->Password = "Dunksme@1";
        $mail->SetFrom('no-reply@byonline.com', $admin_site_title);
        $mail->addReplyTo('no-reply@byonline.com', 'By You Online');
        foreach ($admin_email as $k => $v) {
            $mail->addAddress($v);
        }
        $mail->isHTML(true);
        $mail->Subject = $rs_array['subject'];

        $mail_content = mail_template_replace($rs_array['content'], $postfields);

        $mail->Body = '<body>' . $mail_content . '</body>';
        $mail->send();
    }
}

function get_site_admin_email()
{
    $database = new database();
    global $table;

    $query = "select * from " . $table['setting'] . " where type='admin_email' and status=1";
    $result = $database->query($query);
    while ($rs_array = $result->fetchRow()) {
        $admin_email[] = $rs_array['email'];
    }

    return $admin_email;
}

function is_active($target)
{
    if (count(explode(",", $target)) > 0) {
        $array_target = explode(",", $target);
        foreach ($array_target as $k => $v) {
            if (preg_match("/" . $v . "/", $_SERVER['PHP_SELF'])) {
                return 'active';
            }
        }

    } else if (preg_match("/" . $target . "/", $_SERVER['PHP_SELF'])) {
        return 'active';
    }
}

function get_query_data($table, $where)
{
    $database = new database();

    if (!empty($where)) {
        $query = "select * from " . $table . " where " . $where;
    } else {
        $query = "select * from " . $table;
    }
    $result = $database->query($query);

    return $result;
}

function get_query_data_row($table, $where)
{
    $database = new database();

    if (!empty($where)) {
        $query = "select * from " . $table . " where " . $where;
    } else {
        $query = "select * from " . $table;
    }
    $result = $database->query($query);
    $row = $result->numRows();

    return $row;
}

function get_query_insert($table, $db_data)
{
    foreach ($db_data as $k => $v) {
        $db_data[$k] = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $v);
    }

    $db_field = implode(',', array_keys($db_data));
    $db_value = implode("','", array_values($db_data));

    $query = "insert into " . $table . " ($db_field) values ('$db_value')";

    return $query;
}

function get_query_update($table, $pkid, $db_data)
{
    $query = "update " . $table . " set ";
    foreach ($db_data as $k => $v) {
        $v = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $v);
        $query .= $k . "='" . $v . "',";
    }
    $query = substr($query, 0, -1);
    $query .= " where pkid=$pkid";
    return $query;
}

function get_query_delete($table, $pkid)
{
    $query = "delete from " . $table . " where pkid=$pkid";
    return $query;
}

function get_query_delete_all($table, $where)
{
    $query = "delete from " . $table;

    if ($where != "") {
        $query .= " where " . $where;
    }
    return $query;
}

function get_button($folder, $type, $pkid, $url)
{
    switch ($type) {
        case 'new':
            return '<a href="' . $folder . '/new.php"><button type="button" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> New</button></a>';
            break;

        case 'edit':
            return '<a href="' . $folder . '/edit.php?id=' . $pkid . '"><button type="button" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></button></a>';
            break;

        case 'enter':
            return '<a href="' . $folder . '/edit.php?id=' . $pkid . '"><button type="button" class="btn btn-default btn-xs"><i class="fa fa-sign-in"></i></button></a>';
            break;

        case 'view':
            return '<a href="' . $folder . '/view.php?id=' . $pkid . '"><button type="button" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></button></a>';
            break;

        case 'delete':
            return '<button type="button" class="btn btn-danger btn-xs" onclick="modal_delete_listing(\'' . $folder . '\',\'' . $pkid . '\')"><i class="fa fa-trash"></i></button>';
            break;

        case 'save':
            return '<button type="submit" name="submit_save" value="true" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Save</button>';
            break;

        case 'submit':
            return '<button type="submit" name="submit_save" value="true" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Submit</button>';
            break;

        case 'clear':
            return '<button type="submit" name="submit_clear" value="true" class="btn btn-danger btn-xs"><i class="fa fa-close"></i> Clear</button>';
            break;

        case 'next':
            return '<button type="submit" name="submit_save" value="true" class="btn btn-primary btn-xs"><i class="fa fa-arrow-right"></i> Next</button>';
            break;

        case 'cancel':
            return '<a href="' . $folder . '/listing.php"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-close"></i> Cancel</button></a>';
            break;

        case 'export':
            return '<a href="' . $folder . '/export.php"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-file-excel-o"></i> Export</button></a>';
            break;
    }
}

function protect($action, $string)
{
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'b557c005cb757674879e9d0aefb13d6f';
    $secret_iv = 'wowsome820';

    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
}

function randomString($length = 15)
{
    $characters = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function get_device_id()
{
    $cookie = $_COOKIE[protect('encrypt', 'deviceid')];

    if (preg_match('/www/', $_SERVER['HTTP_HOST'])) {
        $domain = str_replace("www", '', $_SERVER['HTTP_HOST']);
    } else {
        $domain = '.' . $_SERVER['HTTP_HOST'];
    }

    if ($cookie != "") {
        return $cookie;
    } else {
        //setcookie(protect('encrypt', 'deviceid'), uniqid(), time() + 12441600, '/', $domain);
        setcookie(protect('encrypt', 'deviceid'), uniqid(), time() + 12441600, '/');
        return $_COOKIE[protect('encrypt', 'deviceid')];
    }
}

function get_balance()
{
    $xml_array['sClientUserName'] = $_SESSION['fe']['username'];
    $xml_array['sClientPassword'] = protect('decrypt', $_SESSION['fe']['password']);
    $xml_array['dBalance'] = 0.00;

    $client = new SoapClient($_SESSION['fe']['api_url'], array('trace' => true, 'exceptions' => true));
    $result = $client->CheckBalance($xml_array);

    return $result->dBalance;
}

function do_tracking($user_username, $action)
{
    $ip_address = get_ipaddress();
    global $table;
    $database = new database();
    $query = "insert into " . $table['tracking'] . " (username,action,ip,created_date) values ('$user_username','$action','$ip_address',now())";
    $database->query($query);
}

function is_between_date($start, $end)
{
    $date = date('Y-m-d');
    $today = date('Y-m-d', strtotime($date));
    $DateBegin = date('Y-m-d', strtotime($start));
    $DateEnd = date('Y-m-d', strtotime($end));

    if ($today >= $DateBegin && $today <= $DateEnd) {
        return true;
    } else {
        return false;
    }
}

function is_mobile()
{
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

function get_ipaddress()
{
    global $_SERVER;

    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $clientIP = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) AND preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
        // check for internal ips by looking at the first octet
        foreach ($matches[0] AS $ip) {
            if (!preg_match("#^(10|172\.16|192\.168)\.#", $ip)) {
                $clientIP = $ip;
                break;
            }
        }
    } elseif (isset($_SERVER['HTTP_FROM'])) {
        $clientIP = $_SERVER['HTTP_FROM'];
    } else {
        $clientIP = $_SERVER['REMOTE_ADDR'];
    }
    return $clientIP;
}

function do_badwords_filter($text)
{
    $bad_words = 'ass,asshole,anus,anal,arse,arsehole,bodoh,blow job,boobs,bobbie,boops,buntut,butt,' .
        'bugger,bastard,bustard,busterd,bitch,b!tch,b1tch,biatch,ceebai,cibai,cibet,cock,' .
        'cheebye,cocksuck,crap,cum,cunt,cilaka,damade,dick,dickhead,dumbass,dildo,diu,' .
        'fuck,fucker,f u c k,fark,faggard,faggot,fagtart,farhai,fu2k,uckin,fag,fatass,' .
        'gay,hooker,h00ker,idiot,jackoff,jackass,jesus christ,konkek,kongket,kanasai,lancau,' .
        'motherfucker,mothersucker,nigga,nigger,niamah,pecker,pig,piss,pussy,pukimak,porn,p0rn,' .
        'racist,sohai,sorhai,s0hai,s0rhai,sh!at,sh1at,shit,sh!t,sh1t,slut,stupid,sex,suck,tit,' .
        'tui nia mah,tiu,whore,wh0re';

    $white_words = 'assumption,assume,assassin,assignment,assure,assurance,assert,asset';

    $arr_bad_words = explode(',', $bad_words);
    $arr_white_words = explode(',', $white_words);

    foreach ($arr_white_words as $white_word) {
        $text = preg_replace("/\b$white_word/i", '_' . $white_word, $text);
    }

    foreach ($arr_bad_words as $bad_word) {
        $text = preg_replace("/\b$bad_word/i", do_mask_text($bad_word), $text);
    }

    foreach ($arr_white_words as $white_word) {
        $text = preg_replace('/\b_' . $white_word . '/', $white_word, $text);
    }


    return $text;
}

function do_mask_text($text)
{
    for ($i = 0; $i < strlen($text); $i++) {
        $return .= "*";
    }
    return $return;
}

function compt($query)
{
    $database = new database();

    $result = $database->query($query);
    $row = $result->numRows();

    return $row;
}

function updateMobileFormat($mobile, $countryCode=0)
{
    if ($countryCode=='+91') {
        if (preg_match("/^91/", $mobile)){
            $mobile = ltrim($mobile, '91');
        }
    
        if (preg_match("/^\+91/", $mobile)){
            $mobile = ltrim($mobile, '+91');
        }
    }
    /*
    if ($countryCode=='+60') {
        if (preg_match("/^60/", $mobile)){
            $mobile = ltrim($mobile, '60');
        }
    
        if (preg_match("/^\+60/", $mobile)){
            $mobile = ltrim($mobile, '+60');
        }
    
        if (preg_match("/^01/", $mobile)){
            $mobile = ltrim($mobile, '0');
        }
    }
    if ($countryCode=='+65') {
        if (preg_match("/^65/", $mobile)){
            $mobile = ltrim($mobile, '65');
        }
    
        if (preg_match("/^\+65/", $mobile)){
            $mobile = ltrim($mobile, '+65');
        }
    }
    */

        // $mobile = ltrim($mobile, '60');
        // $mobile = ltrim($mobile, '+60');
        // $mobile = ltrim($mobile, '0');
    

    $mobile = preg_replace('/\D/', '', $mobile);

    if($countryCode)
    {
        if($countryCode != 0)
        {
            $mobile = $countryCode.''.$mobile;
        }
        else
        {
            $mobile = '+91'.$mobile;
        }
    }
    
    return $mobile;
}




function hsc($s)
{
	return htmlspecialchars($s, ENT_QUOTES);
}
function frmp($s)
{
	if (isset($_POST[$s])) return hsc($_POST[$s]);
	else return "";
}
function frmg($s)
{
	if (isset($_GET[$s])) return hsc($_GET[$s]);
	else return "";
}
function frm($s)
{
	if (frmp($s) != "") return frmp($s);
	else if (frmg($s) != "") return frmg($s);
	else return "";
}
function frmr($s)
{
	if (isset($_POST[$s]) && $_POST[$s] != "") return $_POST[$s];
	else return "";
}
function clean($s)
{
	global $db;
	return mysqli_real_escape_string($db, stripslashes($s));
}

//miscellany
function cbx($s)
{
	return ($s == "on" ? 1 : 0);
}
function rvd($s)
{
	$d = "-";
	if ($s == "") return "";
	$r = explode($d, $s);
	if (sizeof($r) != 3) return "";
	return $r[2] . $d . $r[1] . $d . $r[0];
}
function pad($s, $c, $l)
{
	while (strlen($s) < $l) {
		$s = $c . $s;
	}
	return $s;
}
function xxx($s)
{
	return preg_replace('[a-zA-Z0-9]', 'x', $s);
}
function rnd($l)
{
	$s = "abcdefghjkmnpqrstuvwxyz0123456789";
	$p = "";
	for ($i = 0; $i < $l; $i++) {
		$p .= substr($s, rand(0, strlen($s) - 1), 1);
	}
	return $p;
}
function lnbr($s)
{
	return str_replace("\n", "<br/>", $s);
}
function go($u)
{
	echo "<meta http-equiv='REFRESH' content='0; URL=" . $u . "'>";
	exit();
}
function pageurl()
{
	return "http" . ($_SERVER["HTTPS"] == "on" ? "s" : "") . "://" . $_SERVER["SERVER_NAME"] . ($_SERVER["SERVER_PORT"] != "80" ? ":" . $_SERVER["SERVER_PORT"] : "") . $_SERVER["REQUEST_URI"];
}
function genap($s)
{
	$d = $s * 100;
	$m = $d % 10;
	if ($m == 1 || $m == 6) {
		$d -= 1;
	}
	if ($m == 2 || $m == 7) {
		$d -= 2;
	}
	if ($m == 3 || $m == 8) {
		$d += 2;
	}
	if ($m == 4 || $m == 9) {
		$d += 1;
	}
	return dfd($d / 100);
}
function img($s)
{
	return str_replace('../', '', $s);
}

//mysql shortcuts
function mq($s)
{
	global $db;
	return mysqli_query($db, $s);
}
function mfa($s)
{
	return mysqli_fetch_array($s);
}
function mnr($s)
{
	return mysqli_num_rows($s);
}

//session handling
function set($s, $v)
{
	global $session_pfx;
	$_SESSION[$session_pfx . $s] = $v;
}
function get($s)
{
	global $session_pfx;
	if (isset($_SESSION[$session_pfx . $s])) return $_SESSION[$session_pfx . $s];
	else return "";
}

//cookie handling
function setc($s, $v)
{
	global $session_pfx;
	setcookie($session_pfx . $s, $v, (time() + 60 * 60 * 24 * 365));
}
function getc($s)
{
	global $session_pfx;
	if (isset($_COOKIE[$session_pfx . $s])) return $_COOKIE[$session_pfx . $s];
	else return "";
}

//time formating
function now()
{
	return date('Y-m-d H:i:s');
}
function sdf($t)
{
	return date('d-M-Y', strtotime($t));
}
function sdtf($t)
{
	return date('d M Y, h:i a', strtotime($t));
}
function sdf3($t)
{
	return date('d M Y', strtotime($t));
}
function sdfnm($t)
{
	return date('d-m-Y', strtotime($t));
}
function stf($t)
{
	return date('h:i a', strtotime($t));
}
function sdfd($t)
{
	return date('d', strtotime($t));
}
function sdfm($t)
{
	return date('m', strtotime($t));
}
function sdfy($t)
{
	return date('Y', strtotime($t));
}

//date manipulation - only accept / return Y-m-d format. e.g. moddate('2008-01-01','+3 day')
function moddate($s, $v)
{
	return date('Y-m-d', strtotime($v, strtotime($s)));
}

//number formating
function dfi($s)
{
	return number_format($s, 0);
}
function dfd($s)
{
	return number_format($s, 2);
}

//validation
function isalphanum($s)
{
	return !preg_match('/[^a-zA-Z0-9_]/', $s);
}
function chk($s)
{
	return (strlen($s) >= 4 && strlen($s) <= 16 && isalphanum($s));
}
function isdate($s)
{
	$d = "-";
	if ($s == "") return 0;
	$r = explode($d, $s);
	if (sizeof($r) != 3) return 0;
	return checkdate($r[1], $r[0], $r[2]);
}
function isnulldate($s)
{
	return (!isset($s) || $s == "" || $s == "00-00-0000" || $s == "0000-00-00" || $s == "00-00-0000 00:00:00" || $s == "0000-00-00 00:00:00");
}
function isemail($s)
{
	$a = "a-z0-9-";
	$r = "/^[_" . $a . "]+(\.[_" . $a . "]+)*@[" . $a . "]+(\.[" . $a . "]+)*(\.[a-z]{2,10})$/i";
	return (preg_match($r, $s));
}
function isphone($s)
{
	return !(preg_match('/[^0-9()\-]/', $s));
}
//function isurl($s) {return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i',$s);}
function isurl($s)
{
	return preg_match('|^[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $s);
}
function isint($s)
{
	return !preg_match('/[^0-9,]/', $s);
}
function isdbl($s)
{
	return (!preg_match('/[^0-9.,]/', $s) && substr_count($s, ".") < 2);
}
function cint($s)
{
	return (isint($s) ? trim(str_replace(",", "", $s)) + 0 : 0);
}
function cdbl($s)
{
	return (isdbl($s) ? trim(str_replace(",", "", $s)) + 0 : 0.00);
}
function cbool($s)
{
	$s = strtolower($s);
	return (($s == 'y' || $s == 'yes' || $s == 'true' || $s == '1' || $s == 'on') ? 1 : 0);
}
function epfx($e, $s)
{
	return (($e != "" ? "<br/>" : "") . "&#149;&nbsp; " . $s);
}
function upfx($u, $s)
{
	return (($u != "" ? "<br/>" : "") . "&#149;&nbsp; " . $s);
}

function getimg($s)
{
	return str_replace("../", "", $s);
}

function pr($s)
{
	echo "<pre>";
	print_r($s);
	echo "</pre>";
}

$this_file = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], "/") + 1);
if (strpos($this_file, "?") !== false) {
	$this_file = substr($this_file, 0, strpos($this_file, "?"));
}
//if ($z=="" && $this_file=="index.php") {go("home.php?z=1");}
//set("z",1);


?>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$countryClass = new country();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['date_selected']) {
        $where_array[] = 'sdate="' . mysqli_real_escape_string($GLOBALS["mysqli_conn"], $postfield['date_selected']) . '"';
    }
    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);
    
    if (!empty($where)) {
        $where_query .= " and " . $where;
    }
    //$where_query .= " AND sms_status=1 ";
    //$where_query .= " AND ( (sms_status=1 AND created_date>'2023-05-05 23:59:59') OR created_date<='2023-05-05 23:59:59') ";
    $where_query .= " ";
    $resultArray = get_query_data($table['slot'], "1 " . $where_query);
    while ($rs_array = $resultArray->fetchRow()) {
        $dataTable['data'][] = array(
            $rs_array['pkid'],
            $rs_array['sdate'],
            $rs_array['stime'],
            $rs_array['quota'],
            $rs_array['booked'],
            $rs_array['quota']-$rs_array['booked'],
            // $buttonClass->get_edit_button($this_folder, $rs_array['pkid']) . " " . $buttonClass->get_delete_button($this_folder, $rs_array['pkid'])
            //$buttonClass->get_delete_button($this_folder, $rs_array['pkid'])
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $query = get_query_delete($table['slot'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Slot</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Sign Up</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="slot/<?= http_build_query($_GET) ?>" method="post"
                                      class="form-horizontal">
                                    <h4 class="page-title">Filter</h4>
                                    <div class="form-group">
                                        <label class="control-label col-sm-1">Filter Date</label>
                                        <div class="col-sm-3">
                                            <select class="form-control" name="date_selected">
                                                <option value="">---Please Select---</option>
                                                <?
                                                $resultslot = get_query_data($table['slot'], '1 GROUP BY sdate');
                                                while ($rs_slot = $resultslot->fetchRow()) {
                                                    if($rs_slot['sdate']==$postfield['date_selected'])
                                                    echo '<option value="' . $rs_slot['sdate'] . '" selected>' . $rs_slot['sdate'] . '</option>';
                                                    else
                                                    echo '<option value="' . $rs_slot['sdate'] . '">' . $rs_slot['sdate'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" name="submit_filter" value="true" class="btn btn-success">
                                        SUBMIT
                                    </button>
                                    <? if ($postfield['submit_filter']) { ?>
                                        <button type="submit" name="submit_clear" value="true" class="btn btn-danger">
                                            CLEAR
                                        </button>
                                    <? } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Max Quota</th>
                                        <th>Booked</th>
                                        <th>Left</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    var table = $('#datatable').DataTable({
        "ajax": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        "order": [[0, "asc"]],
        responsive: true,
        dom: "<'row'<'col-sm-12'B>><'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/resetgame/admin/config.php";
global $table;
$database = new database();
$loginClass = new login();

if (isset($_POST['submit_login'])) {
    $postfield = $_POST;
    unset($_POST);


    if ($loginClass->checkLogin($postfield['email'], $postfield['password'])) {
        $resultUser = get_query_data($table['user'], "username='" . $postfield['email'] . "' and password='" . protect('encrypt', $postfield['password']) . "'");
        $rs_user = $resultUser->fetchRow();

        $_SESSION['admin']['id'] = $rs_user['pkid'];
        $_SESSION['admin']['username'] = $rs_user['username'];
        $_SESSION['admin']['name'] = $rs_user['name'];
        $_SESSION['admin']['role'] = $rs_user['role'];
        $_SESSION['admin']['outlet_id'] = $rs_user['outlet_id'];

        header("Location: member/");
        exit();
    } else {
        $swal['title'] = "Error";
        $swal['msg'] = "Incorrect username / password";
        $swal['type'] = "error";
    }
}

if (isset($_POST['submit_login'])) {
    $swal['title'] = "Opps...";
    $swal['msg'] = "Something went wrong, please try again.";
    $swal['type'] = "error";
}
?>
<!DOCTYPE html>
<html lang="en">
<?php include('head.php') ?>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="new-login-register">
    <div class="lg-info-panel">
        <div class="inner-panel">
            <div class="lg-content">
                <img src="plugins/images/loccitane-logo-white.png" class="img-responsive logo-center">
                <hr>
                <h2>ADMIN SYSTEM</h2>
            </div>
        </div>
    </div>
    <div class="new-login-box">
        <div class="white-box">
            <h3 class="box-title m-b-0">Sign In to Admin</h3>
            <small>Enter your details below</small>
            <form class="form-horizontal new-lg-form" id="loginform" action="login" method="post">
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Username</label>
                        <input class="form-control" type="text" required placeholder="Username" name="email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Password</label>
                        <input class="form-control" type="password" required placeholder="Password" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <a href="javascript:void(0)" id="to-recover" class="text-dark"><i
                                    class="fa fa-lock m-r-5"></i> Forgot password?</a></div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light"
                                type="submit" name="submit_login" value="true">Log In
                        </button>
                    </div>
                </div>
                <div class="form-group m-b-0 hidden">
                    <div class="col-sm-12 text-center">
                        <p>Don't have an account? <a href="register.html" class="text-primary m-l-5"><b>Sign Up</b></a>
                        </p>
                    </div>
                </div>
            </form>
            <form class="form-horizontal" id="recoverform" action="login">
                <div class="form-group ">
                    <div class="col-xs-12">
                        <h3>Recover Password</h3>
                        <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" required="" placeholder="Email" name="email">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light"
                                name="submit_forgot" value="true"
                                type="submit">Reset
                        </button>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <a href="javascript:void(0)" id="to-login" class="text-dark text-center"><i
                                    class="fa fa-chevron-circle-left m-r-5"></i> Back to login</a>
                    </div>
                </div>
        </div>
        </form>
    </div>


</section>
<?php include('js-script.php') ?>
<script>
    $.ajax({
        method: "GET",
        url: "../ajax/get_banner.php",
    })
        .done(function (data) {
            console.log(data);
            $(".lg-info-panel").attr('style', 'background-image:url("../../assets/banner/' + data + '") !important');
        });
</script>
</body>
</html>

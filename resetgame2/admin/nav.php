<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <div class="top-left-part">
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i
                            class="ti-close ti-menu"></i></a></li>
            </ul>
            <a class="logo hidden-xs" href="/resetgame/admin/index.php"> <img src="plugins/images/loccitane-logo.png" alt="home"
                    class="light-logo" /> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown"><a class="dropdown-toggle profile-pic" data-toggle="dropdown"
                    href="javascript:void(0)"> <span class="chat-img bg-danger"><b><?= substr(strtoupper($_SESSION['admin']['username']), 0, 1) ?></b></span><b
                        class="hidden-xs"><?= $_SESSION['admin']['name'] ?></b><span
                        class="caret"></span> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-text">
                                <h4><?= $_SESSION['admin']['name'] ?>
                                </h4>
                                <p class="text-muted"><?= $_SESSION['admin']['username'] ?>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a
                            href="user/edit?id=<?= $_SESSION['admin']['id'] ?>"><i
                                class="ti-settings"></i> Account
                            Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i
                        class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3>
        </div>
        <ul class="nav" id="side-menu">
            <li class="user-pro"><a href="javascript:void(0)" class="waves-effect"><span
                        class="chat-img bg-danger"><b><?= substr(strtoupper($_SESSION['admin']['username']), 0, 1) ?></b></span>
                    <span class="hide-menu"> <?= $_SESSION['admin']['name'] ?><span
                            class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                    <li><a href="javascript:void(0)"><i class="ti-settings"></i> <span class="hide-menu">Account
                                Setting</span></a></li>
                    <li><a href="logout"><i class="fa fa-power-off"></i> <span class="hide-menu">Logout</span></a></li>
                </ul>
            </li>
            <!-- <li><a href="dashboard" class="waves-effect"><i class="mdi mdi-av-timer fa-fw"
                                                            data-icon="v"></i> <span class="hide-menu"> Dashboard <span
                                class="fa arrow"></span></span></a></li> -->
            <li><a href="/resetgame/admin/member" class="waves-effect "><i class="mdi mdi-view-list fa-fw"></i> <span
                        class="hide-menu">Sign-up<span class="fa arrow"></span></span></a></li>
            <!--
            <li><a href="order" class="waves-effect "><i class="mdi mdi-view-list fa-fw"></i> <span
                        class="hide-menu">Order<span class="fa arrow"></span></span></a></li>
            -->
            <?php
            if ($_SESSION['admin']['role'] == "1") {
                ?>
            
            <? if (1==0){ ?>
            <li><a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-chart-bar fa-fw"></i> <span
                        class="hide-menu">Reports<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <!--
                    <li><a href="report/order"><i class="fa-fw">O</i><span class="hide-menu">Order</span></a></li>
                    <li><a href="report/checkin"><i class="fa-fw">C</i><span class="hide-menu">Check in</span></a></li>
                    -->
                    <li><a href="/resetgame/admin/report/member"><i class="fa-fw">S</i><span class="hide-menu">Sign-up</span></a></li>
                    <!--
                    <li><a href="report/redeem"><i class="fa-fw">R</i><span class="hide-menu">Redeem</span></a></li>
                    <li><a href="report/share"><i class="fa-fw">S</i><span class="hide-menu">Share</span></a></li>
                    -->
                </ul>
            </li>
            <? } ?>
            <li><a href="/resetgame/admin/user" class="waves-effect "><i class="mdi fa-user fa-fw"></i> <span
                        class="hide-menu">Users<span class="fa arrow"></span></span></a></li>
            <?php
            } ?>
        </ul>
    </div>
</div>
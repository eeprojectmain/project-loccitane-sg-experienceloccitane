<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);
$pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$result = get_query_data($table['production_user'], "pkid=$pkid");
$rs_array = $result->fetchRow();

if (isset($_POST['submit_save'])) {
    $postfield = $_POST;

    unset($postfield['submit_save']);
    unset($postfield['new_password']);

    $postfield['password']=protect('encrypt',$_POST['password']);
    $postfield['updated_date'] = $time_config['now'];
    $postfield['updated_by'] = $_SESSION['user']['username'];

    $queryUpdate = get_query_update($table['production_user'], $pkid, $postfield);
    $database->query($queryUpdate);

    header("Location: ../$this_folder");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Production User <i class="fa fa-angle-right"></i> Edit</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li><a href="<?= $this_folder ?>">Production User</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form class="form-horizontal" method="post" action="<?= $this_folder ?>/edit?<?=http_build_query($_GET)?>"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Status</label>
                                        <div class="col-sm-10">
                                            <div class="radio radio-inline radio-success">
                                                <input type="radio" name="status" id="status_1"
                                                       value="1" <?= $rs_array['status'] == "1" ? "checked" : "" ?>>
                                                <label for="status_1"> Active </label>
                                            </div>
                                            <div class="radio radio-inline radio-danger">
                                                <input type="radio" name="status" id="status_0"
                                                       value="0" <?= $rs_array['status'] == "0" ? "checked" : "" ?>>
                                                <label for="status_0"> Inactive </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Name</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="name" maxlength="255"
                                                   value="<?= $rs_array['name'] ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Username</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="username" maxlength="255" value="<?=$rs_array['username']?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Password</label>
                                        <div class="col-sm-5">
                                            <input type="password" class="form-control" name="password" maxlength="255" value="<?=protect('decrypt',$rs_array['password'])?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Confirm Password</label>
                                        <div class="col-sm-5">
                                            <input type="password" class="form-control" name="new_password" maxlength="255" value="<?=protect('decrypt',$rs_array['password'])?>"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $buttonClass->get_save_button() . " " . $buttonClass->get_cancel_button($this_folder) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    $("#fileinput").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 100000,
        allowedFileExtensions: ['png','jpg','jpeg','gif'],
    });
</script>
</body>

</html>
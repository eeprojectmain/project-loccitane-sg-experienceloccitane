<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();

if ($_POST['method'] == "add_sticker") {
    $result = get_query_data($table['sticker_lib'], "pkid=" . mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']));
    $rs_array = $result->fetchRow();

    $postfield = array(
        'lib_id' => $_POST['pkid'],
        'company_id' => $_SESSION['user']['company_id'],
        'name' => $rs_array['name'],
        'img_url' => $rs_array['img_url'],
        'created_date' => $time_config['now'],
        'created_by' => $_SESSION['user']['username']
    );

    $queryInsert = get_query_insert($table['sticker'], $postfield);
    $database->query($queryInsert);

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
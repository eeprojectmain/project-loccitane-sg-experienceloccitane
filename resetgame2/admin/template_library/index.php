<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();

$this_folder = basename(__DIR__);

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['machine_id']) {
        $where_array[] = "find_in_set(" . $postfield['machine_id'] . ", cast(key_id as char)) > 0";
    }
    if ($postfield['type_id']) {
        $where_array[] = "find_in_set(" . $postfield['type_id'] . ", cast(type_id as char)) > 0";
    }

    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);

    if (!empty($where)) {
        $where_query .= " and " . $where;
    }

    $resultTemplate = get_query_data($table['template_lib'],"status=1 ".$where_query);
    while ($rs_template = $resultTemplate->fetchRow()) {

        $array_template_type = explode(",", $rs_template['type_id']);
        unset($text_template_type);
        foreach ($array_template_type as $v) {
            $text_template_type .= $template_type_array[$v] . "<br>";
        }

        $button_add = '<button type="button" class="btn btn-warning waves-effect waves-light" onclick="ajax_add_template(' . $rs_template['pkid'] . ')"><i class="fa fa-download fa-fw"></i> <span>Get this</span></button>';
        $button_added = '<button type="button" disabled class="btn btn-warning waves-effect waves-light"><i class="fa fa-download fa-fw"></i> <span>Already added</span></button>';

        $row_check = get_query_data_row($table['template'], "lib_id=" . $rs_template['pkid'] . " and company_id=" . $_SESSION['user']['company_id']);

        if ($row_check == 0) {
            $button_all = $button_add;
        } else {
            $button_all = $button_added;
        }

        $dataTable['data'][] = array(
            $rs_template['pkid'],
            '<a href="files/template/' . $rs_template['img_url'] . '" data-toggle="lightbox"
                                           data-title="#' . $rs_template['pkid'] . ' - ' . $rs_template['name'] . '">
                                            <img src="files/template/' . $rs_template['img_url'] . '"
                                                 class="img-responsive" width="100px">
                                        </a>',
            $rs_template['name'],
            $text_template_type,
            $rs_template['created_date'],
            $button_all,
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php') ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Templates Library</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Templates Library</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <form action="<?= $this_folder ?>/" method="post" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Type</label>
                                    <select class="form-control" name="type_id">
                                        <option value="">---Please Select---</option>
                                        <?
                                        foreach ($template_type_array as $k => $v) {
                                            if ($postfield['type_id'] == $k) {
                                                echo "<option value='$k' selected>$v</option>";
                                            } else {
                                                echo "<option value='$k'>$v</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <div class="col-sm-12 col-md-3">
                                    <label class="control-label">Machine</label>
                                    <select class="form-control selectpicker" name="machine_id">
                                        <option value="">---Please Select---</option>
                                        <?
                                        $resultKey = get_query_data($table['license'], "company_id=" . $_SESSION['user']['company_id']);
                                        while ($rs_key = $resultKey->fetchRow()) {
                                            if ($postfield['machine_id'] == $rs_key['pkid']) {
                                                echo "<option value='" . $rs_key['pkid'] . "' selected>" . $rs_key['pc_name'] . "</option>";
                                            } else {
                                                echo "<option value='" . $rs_key['pkid'] . "'>" . $rs_key['pc_name'] . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left">
                                        <?= $buttonClass->get_submit_button() ?>
                                        <? if ($postfield['submit_filter']) {
                                            echo $buttonClass->get_clear_button();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $buttonClass->get_back_button("template") ?>
                            </div>
                        </div>
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Date added</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php') ?>
<script>
    var table = $('#datatable').DataTable({
        "ajax": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        "order": [[0, "desc"]],
        responsive: true,
    });

    function ajax_add_template(pkid) {
        $.ajax({
            url: "<?=$this_folder?>/ajax",
            type: 'POST',
            data: {method: "add_template", pkid: pkid},
            dataType: 'json',
            success: function (data) {
                if (data.result == "success") {
                    table.ajax.reload(null, false);
                    swal("Bravo!", "Successfully added to your account", {
                        icon: "success",
                        timer: 1000,
                    });
                } else {
                    swal("Opps...", "Something went wrong, please try again.", {
                        icon: "error",
                        timer: 1000,
                    });
                }
            }
        });
    }
</script>
</body>

</html>
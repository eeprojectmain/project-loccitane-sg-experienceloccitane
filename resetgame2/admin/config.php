<?php
session_start();
date_default_timezone_set('Asia/Kuala_Lumpur');
error_reporting(0);

//database config
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'experien_vk');
define('DB_PASSWORD', 'Dunksme@1');
define('DB_NAME', 'experienceloccit_sg_resetgame');

$db = mysqli_connect("localhost", DB_USERNAME, DB_PASSWORD, DB_NAME) or die("Cannot connect to database.");

$tablePrefix = 'dc_';
$gamelang="sg";

$table['visitor'] = $tablePrefix . 'visitor';
$table['outlet'] = $tablePrefix . 'outlet';
$table['score'] = $tablePrefix . 'score';
$table['cafe_voucher'] = $tablePrefix . 'cafe_voucher';
$table['redeem'] = $tablePrefix . 'redeem';
$table['checkin'] = $tablePrefix . 'checkin';
$table['order'] = $tablePrefix . 'order';
$table['cart'] = $tablePrefix . 'cart';
$table['product_gwp'] = $tablePrefix . 'product_gwp';
$table['product2'] = $tablePrefix . 'product2';
$table['product'] = $tablePrefix . 'product';
$table['product_category'] = $tablePrefix . 'product_category';
$table['share'] = $tablePrefix . 'share';
$table['member'] = $tablePrefix . 'member';
$table['country'] = $tablePrefix . 'country';

$table['module'] = $tablePrefix . 'module';
$table['setting'] = $tablePrefix . 'setting';
$table['tracking'] = $tablePrefix . 'tracking';
$table['user'] = $tablePrefix . 'user';
$table['user_logs'] = $tablePrefix . 'user_logs';
$table['slot'] = $tablePrefix . 'slot';


//include extra file
require('class/member.php');
require('class/query.php');
require('class/check.php');
require('class/store-credit.php');
require('class/invoice.php');
require('class/country.php');
require('class/currency.php');
require('class/email.php');
require('class/status.php');
require('class/button.php');
require('class/user.php');
require('class/date.php');
require('class/login.php');
require('class/premium.php');
require('class/mail.php');
require('class/database.php');
require('class/merchant.php');
require('class/visitor.php');
require('include/Facebook/autoload.php');
require('include/PHPMailer-master/PHPMailerAutoload.php');
require('include/function.php');
require('include/PHPExcel/Classes/PHPExcel.php');

//site config
$site_config['site_title'] = "L'occitane";
$site_config['enquiry_email'] = "hello@byyouonline.com";
$site_config['admin_full_url'] = "https://" . $_SERVER['HTTP_HOST'] . "/resetgame/admin/";
$site_config['full_url'] = "https://" . $_SERVER['HTTP_HOST'] . "/";
$site_config['404_url'] = $site_config['full_url'] . "404/";

$site_config['fb_app_id'] = "230539367597594";
$site_config['fb_app_secret'] = "c4d07b7233723c88164fe7e7cb7977d4";

if (preg_match("/admin/", $_SERVER['PHP_SELF'])) {
    $loginClass = new login();
    $loginClass->isLoggedIn();
}

//misc config
$green = "style=\"color:#5cb85c\"";
$red = "style=\"color:#c9302c\"";
$blue = "style=\"color:#428bca\"";
$yellow = "style=\"color:#f0ad4e\"";

$active = "<td $green><b>Active</b></td>";
$inactive = "<td $red><b>Inactive</b></td>";

$time_config['now'] = date("Y-m-d H:i:s");
$time_config['today'] = date("Y-m-d");

//$user_role_array = array(1 => 'Super Admin', 2 => 'Cashier');
$user_role_array = array(1 => 'Super Admin');
$status_array = array(1 => 'Active', 2 => 'Inactive');
$month_array = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
$order_status_array = array(0 => 'Pending', 1 => 'Completed', 2 => 'Processing', 3 => 'Completed', 4 => 'Collected', 5 => 'Rejected', 6 => 'Shipped', 7 => 'Cancelled', 8 => 'Other');
$order_status_class_array = array(0 => 'info', 1 => 'success', 2 => 'warning', 3 => 'success', 4 => 'primary', 5 => 'danger', 6 => 'primary', 7 => 'danger', 8 => 'default');
$stock_status_array = array(1 => 'Collected', 2 => 'Rejected', 3 => 'Defect', 4 => 'Pending Collection');
$shipping_id_array = array(1 => 'Pos Laju', 2 => 'GDex', 3 => 'Sky Net', 4 => 'City Link');
$return_status_array = array(0 => 'Pending', 1 => 'Refunded', 2 => 'Rejected', 3 => 'Processing');
$refund_type_array = array(1 => "Cash (MYR 5.00 Payment Gateway Charge Will Be Deducted)", 2 => "Store Credits");
$station_array = array(1 => 'Provence Petal', 2 => 'Jaubert\'s Almond Milk', 3 => 'Pascale\'s Immortelle', 4 => 'Immortelle Reset', 5 => "Big Little Things Interactive Game", 6 => 'Shea Butter Creamery',7 => 'Essential Oil Bar');
$checkin_password_array = array(1 => 'simon1', 2 => 'simon2', 3 => 'simon3', 4 => 'simon4', 5 => 'simon5', 6 => 'simon6', 7 => 'simon7', 8 => 'simon8', 9 => 'simon9');

if ($_GET['utm_source'] != "") {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
if ($_GET['utm_medium'] != "") {
    $_SESSION['utm_medium'] = $_GET['utm_medium'];
}
if ($_GET['utm_campaign'] != "") {
    $_SESSION['utm_campaign'] = $_GET['utm_campaign'];
}
if ($_GET['oid'] != "") {
    $_SESSION['outlet_id'] = protect('decrypt', $_GET['oid']);
}


$array_time = array();
$array_time["10:00:00"]="10am";
$array_time["11:00:00"]="11am";
$array_time["12:00:00"]="12pm";
$array_time["13:00:00"]="1pm";
$array_time["14:00:00"]="2pm";
$array_time["15:00:00"]="3pm";
$array_time["16:00:00"]="4pm";
$array_time["17:00:00"]="5pm";
$array_time["18:00:00"]="6pm";
$array_time["19:00:00"]="7pm";
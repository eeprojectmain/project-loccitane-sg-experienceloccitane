<?php
session_start();
date_default_timezone_set('Asia/Kuala_Lumpur');
error_reporting(0);

//database config
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'experien_vk');
define('DB_PASSWORD', 'Dunksme@1');
define('DB_NAME', 'experien_event_midvalley2019');

$sql_details = array(
    'user' => DB_USERNAME,
    'pass' => DB_PASSWORD,
    'db' => DB_NAME,
    'host' => DB_HOST
);

$tablePrefix = 'dc_';

$table['score'] = $tablePrefix . 'score';
$table['cafe_voucher'] = $tablePrefix . 'cafe_voucher';
$table['redeem'] = $tablePrefix . 'redeem';
$table['checkin'] = $tablePrefix . 'checkin';
$table['order'] = $tablePrefix . 'order';
$table['cart'] = $tablePrefix . 'cart';
$table['product_gwp'] = $tablePrefix . 'product_gwp';
$table['product2'] = $tablePrefix . 'product2';
$table['product'] = $tablePrefix . 'product';
$table['product_category'] = $tablePrefix . 'product_category';
$table['share'] = $tablePrefix . 'share';
$table['member'] = $tablePrefix . 'member';
$table['country'] = $tablePrefix . 'country';

$table['module'] = $tablePrefix . 'module';
$table['setting'] = $tablePrefix . 'setting';
$table['tracking'] = $tablePrefix . 'tracking';
$table['user'] = $tablePrefix . 'user';
$table['user_logs'] = $tablePrefix . 'user_logs';

//include extra file
require('class/member.php');
require('class/query.php');
require('class/check.php');
require('class/store-credit.php');
require('class/invoice.php');
require('class/country.php');
require('class/currency.php');
require('class/email.php');
require('class/status.php');
require('class/button.php');
require('class/user.php');
require('class/date.php');
require('class/login.php');
require('class/premium.php');
require('class/mail.php');
require('class/database.php');
require('class/merchant.php');
require('include/Facebook/autoload.php');
require('include/PHPMailer-master/PHPMailerAutoload.php');
require('include/function.php');
require('include/PHPExcel/Classes/PHPExcel.php');

//site config
$site_config['site_title'] = "L'occitane";
$site_config['enquiry_email'] = "hello@byyouonline.com";
$site_config['admin_full_url'] = "https://" . $_SERVER['HTTP_HOST'] . "/admin/";
$site_config['full_url'] = "https://" . $_SERVER['HTTP_HOST'] . "/";
$site_config['404_url'] = $site_config['full_url'] . "404/";

$site_config['fb_app_id'] = "230539367597594";
$site_config['fb_app_secret'] = "c4d07b7233723c88164fe7e7cb7977d4";

if (preg_match("/admin/", $_SERVER['PHP_SELF'])) {
    $loginClass = new login();
    $loginClass->isLoggedIn();
}

//misc config
$green = "style=\"color:#5cb85c\"";
$red = "style=\"color:#c9302c\"";
$blue = "style=\"color:#428bca\"";
$yellow = "style=\"color:#f0ad4e\"";

$active = "<td $green><b>Active</b></td>";
$inactive = "<td $red><b>Inactive</b></td>";

$time_config['now'] = date("Y-m-d H:i:s");
$time_config['today'] = date("Y-m-d");

$user_role_array = array(1 => 'Super Admin', 2 => 'Cashier');
$status_array = array(1 => 'Active', 2 => 'Inactive');
$month_array = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
$order_status_array = array(0 => 'Pending', 1 => 'Completed', 2 => 'Processing', 3 => 'Completed', 4 => 'Collected', 5 => 'Rejected', 6 => 'Shipped', 7 => 'Cancelled', 8 => 'Other');
$order_status_class_array = array(0 => 'info', 1 => 'success', 2 => 'warning', 3 => 'success', 4 => 'primary', 5 => 'danger', 6 => 'primary', 7 => 'danger', 8 => 'default');
$stock_status_array = array(1 => 'Collected', 2 => 'Rejected', 3 => 'Defect', 4 => 'Pending Collection');
$shipping_id_array = array(1 => 'Pos Laju', 2 => 'GDex', 3 => 'Sky Net', 4 => 'City Link');
$return_status_array = array(0 => 'Pending', 1 => 'Refunded', 2 => 'Rejected', 3 => 'Processing');
$refund_type_array = array(1 => "Cash (MYR 5.00 Payment Gateway Charge Will Be Deducted)", 2 => "Store Credits");
$station_array = array(1 => 'Pascale\'s Immortelle', 2 => 'Provence Petal', 3 => 'Shea Butter Creamery', 4 => 'Reset Fountain', 5 => 'Jaubert\'s Almond Milk', 6 => 'Essential Oil Bar', 7 => 'Recycling', 8 => 'Precious Sample');
$checkin_password_array = array(1 => 'simon1', 2 => 'simon2', 3 => 'simon3', 4 => 'simon4', 5 => 'simon5', 6 => 'simon6', 7 => 'simon7', 8 => 'simon8', 9 => 'simon9');

if ($_GET['utm_source'] != "") {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
if ($_GET['utm_medium'] != "") {
    $_SESSION['utm_medium'] = $_GET['utm_medium'];
}
if ($_GET['utm_campaign'] != "") {
    $_SESSION['utm_campaign'] = $_GET['utm_campaign'];
}
?>
<?

class currency
{
    public function convert($amount, $from, $to)
    {

        if ($from == $to) {
            return number_format($amount,2);
        }

        if ($_SESSION['rate'][$from][$to] == "") {
            $curl = file_get_contents('http://free.currencyconverterapi.com/api/v5/convert?q='.$from.'_'.$to);
            $curl = json_decode($curl, true);
            $rate = $curl['results'][$from."_".$to]['val'];

            $_SESSION['rate'][$from][$to] = $rate;

            if (($amount * $rate) < 1) {
                $output = $amount * $rate;
            } else {
                $output = ceil($amount * $rate);
            }
        } else {
            if (($amount * $_SESSION['rate'][$from][$to]) < 1) {
                $output = $amount * $_SESSION['rate'][$from][$to];
            } else {
                $output = ceil($amount * $_SESSION['rate'][$from][$to]);
            }
        }

        return number_format($output,2);
    }
}

?>
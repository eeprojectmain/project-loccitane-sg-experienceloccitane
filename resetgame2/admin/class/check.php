<?

class check
{
    public function order($order_id)
    {
        global $table;
        $databaseClass = new database();

        $result = get_query_data($table['order'], "pkid=$order_id and member_id=" . $_SESSION['member']['id']);
        $row = $result->numRows();

        if ($row == 0) {
            header("Location: /member/order");
            exit();
        }
    }

    public function product($product_id)
    {
        global $table;
        $databaseClass = new database();

        $type = explode("-", $product_id)[0];
        $id = explode("-", $product_id)[1];

        if ($type == "C") {
            $row_1 = get_query_data_row($table['custom_result'], "pkid=$id and member_id=" . $_SESSION['member']['id']);
            $row_2 = get_query_data_row($table['order'], "find_in_set('$product_id',product_id) and member_id=" . $_SESSION['member']['id']);
        } elseif ($type == "D") {
            $row_1 = get_query_data_row($table['order'], "find_in_set('$product_id',product_id) and member_id=" . $_SESSION['member']['id']);
        }

        if ($row_1 + $row_2 == 0) {
            header("Location: /member/return");
            exit();
        }
    }
}

?>
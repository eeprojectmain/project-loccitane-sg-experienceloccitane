<?

class login
{
    function checkLogin($username, $password)
    {
        global $table;
        $databaseClass = new database();

        $password = protect('encrypt', $password);

        $row_check = get_query_data_row($table['user'], "username='$username' and password='$password'");

        if ($row_check > 0) {
            return true;
        } else {
            return false;
        }
    }

    function isLoggedIn()
    {
        global $site_config;

        if (!preg_match("/login/", $_SERVER['PHP_SELF']) && !preg_match("/logout/", $_SERVER['PHP_SELF'])) {
            if ($_SESSION['admin']['id']=="" || $_SESSION['admin']['username']=="") {
                    header("Location: " . $site_config['admin_full_url'] . "login");
                    exit();
            }
        }
    }

    function isMemberLoggedIn()
    {
        if ($_SESSION['member'] && !empty($_SESSION['member']['id'])) {
            return true;
        } else {
            return false;
        }
    }

    function memberRedirection()
    {
        global $site_config;

        if ($this->isMemberLoggedIn() === false && (preg_match("/menu/", $_SERVER['REQUEST_URI']) || preg_match("/checkin/", $_SERVER['REQUEST_URI']) || preg_match("/share/", $_SERVER['REQUEST_URI']) || preg_match("/shop/", $_SERVER['REQUEST_URI']))) {
            $_SESSION['page']['before_login'] = $_SERVER['REQUEST_URI'];

            header("Location: " . $site_config['full_url'] . "login");
            exit();
        }
    }
}

?>
<?php
class visitor
{
    public function add($member_id)
    {
        global $table;
        $databaseClass=new database();

        $today=date("Y-m-d");

        $this->check_table();

        if ($member_id=="") {
            $queryUpdate="update ".$table['visitor']." set live=live+1 where created_date like '%$today%'";
            $databaseClass->query($queryUpdate);
        } else {
            if ($this->check_member($member_id)) {
                $queryUpdate="update ".$table['visitor']." set checkin=checkin+1,live=live+1 where created_date like '%$today%'";
                $databaseClass->query($queryUpdate);
            }
        }
    }

    public function minus($member_id)
    {
        global $table;
        $databaseClass=new database();

        $today=date("Y-m-d");

        $this->check_table();

        if ($member_id=="") {
            $queryUpdate="update ".$table['visitor']." set live=live-1 where created_date like '%$today%'";
            $databaseClass->query($queryUpdate);
        } else {
            if ($this->check_member_redeem($member_id)) {
                $queryUpdate="update ".$table['visitor']." set checkout=checkout-1,live=live-1 where created_date like '%$today%'";
                $databaseClass->query($queryUpdate);
            }
        }
    }

    public function get()
    {
        global $table;
        $databaseClass=new database();

        $today=date("Y-m-d");

        $this->check_table();

        $resultVisitor=get_query_data($table['visitor'], "created_date like '%$today%'");
        $rs_visitor=$resultVisitor->fetchRow();

        return $rs_visitor['live'];
    }

    public function check_table()
    {
        global $table;
        $databaseClass=new database();

        $today=date("Y-m-d");

        $row_check=get_query_data_row($table['visitor'], "created_date like '%$today%'");

        if ($row_check==0) {
            $queryInsert=get_query_insert($table['visitor'], array('created_date'=>$today));
            $databaseClass->query($queryInsert);
        }
    }

    public function check_member($member_id)
    {
        global $table;
        $databaseClass=new database();

        $today=date("Y-m-d");

        $row_check=get_query_data_row($table['checkin'], "member_id=$member_id and created_date like '%$today%'");

        if ($row_check>1) {
            return false;
        } else {
            return true;
        }
    }

    public function check_member_redeem($member_id)
    {
        global $table;
        $databaseClass=new database();

        $today=date("Y-m-d");

        $row_check=get_query_data_row($table['redeem'], "member_id=$member_id and created_date like '%$today%'");

        if ($row_check>1) {
            return false;
        } else {
            return true;
        }
    }
}

<?

class credit
{
    public function getTotalAmount($member_id)
    {
        global $table;
        $databaseClass = new database();
        $currencyClass = new currency();

        $total_credit = 0;

        $resultCredit = get_query_data($table['store-credit'], "member_id=$member_id");
        while ($rs_credit = $resultCredit->fetchRow()) {
            $convertedAmount = $currencyClass->convert($rs_credit['amount'], $rs_credit['cureency'], $_SESSION['currency']);

            $total_credit += $convertedAmount;
        }

        return $total_credit;
    }
}

?>
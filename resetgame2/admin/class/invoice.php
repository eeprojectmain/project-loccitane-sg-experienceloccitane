<?

class invoice
{
    public function insert($order_id)
    {
        $databaseClass = new database();
        global $table, $site_config, $time_config;

        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $row_order = $resultOrder->numRows();
        $rs_order = $resultOrder->fetchRow();

        if ($row_order > 0) {
            $postfield = array(
                'order_id' => $order_id,
                'member_id' => $rs_order['member_id'],
                'shipping_id' => $rs_order['shipping_id'],
                'address' => $rs_order['address'],
                'product_id' => $rs_order['product_id'],
                'product_quantity' => $rs_order['product_quantity'],
                'shipping_amount' => $rs_order['shipping_amount'],
                'product_amount' => $rs_order['product_amount'],
                'discount_amount' => $rs_order['discount_amount'],
                'credit_amount' => $rs_order['credit_amount'],
                'gst_amount' => $rs_order['gst_amount'],
                'final_amount' => $rs_order['final_amount'],
                'currency' => $rs_order['currency'],
                'created_date' => $time_config['now']
            );

            $queryInsert = get_query_insert($table['invoice'], $postfield);
            $databaseClass->query($queryInsert);
        } else {
            return false;
        }
    }
}

?>
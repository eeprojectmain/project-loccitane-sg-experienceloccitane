<?

class member
{
    public function qrCode()
    {
        global $table;
        $databaseClass = new database();

        $resultMember = get_query_data($table['member'], "pkid=" . $_SESSION['member']['id']);
        $rs_member = $resultMember->fetchRow();

        if(strtotime("2022-02-14") >= strtotime($rs_member['created_date'])){
            $color="007f00"; //green
        }

        if(strtotime("2022-02-14") < strtotime($rs_member['created_date'])){
            $color="00163f"; //blue
        }

        $url = protect('encrypt', $_SESSION['member']['id']);

//        $qr_code="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=$url&choe=UTF-8&chld=L|1";
        $qr_code = "https://api.qrserver.com/v1/create-qr-code/?size=300x300&margin=15&data=$url&color=$color";

        return $qr_code;
    }
}

?>
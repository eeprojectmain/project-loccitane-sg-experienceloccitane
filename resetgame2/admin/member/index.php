<?php

require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/resetgame/admin/config.php";

global $table;
$database = new database();
$buttonClass = new button();
$statusClass = new status();
$countryClass = new country();

//$this_folder = basename(__DIR__);
$this_folder = "/resetgame/admin/member";

if (isset($_POST['submit_filter'])) {
    $postfield = $_POST;
    unset($_POST);

    if ($postfield['utm_source']) {
        $where_array[] = 'utm_source="' . mysqli_real_escape_string($GLOBALS["mysqli_conn"], $postfield['utm_source']) . '"';
    }

    $where_text = implode(" and ", $where_array);
}

if (mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['type']) == "get_listing_data") {
    $where = urldecode($_GET['where']);
    
    if (!empty($where)) {
        $where_query .= " and " . $where;
    }
    //$where_query .= " AND sms_status=1 ";
    //$where_query .= " AND ( (sms_status=1 AND created_date>'2023-05-05 23:59:59') OR created_date<='2023-05-05 23:59:59') ";

    $resultArray = get_query_data($table['member'], "1 " . $where_query);
    while ($rs_array = $resultArray->fetchRow()) {
        $dataTable['data'][] = array(
            $rs_array['pkid'],
            $rs_array['first_name']." ".$rs_array['last_name'],
            $rs_array['email'],
            $rs_array['mobile'],
            //$rs_array['fb_id'],
            //$rs_array['utm_source'],
            //$rs_array['source'],
            $rs_array['score'],

            implode('<br>', array(
                ($rs_array['sms_pdpa'] == 1 ? 'SMS: Yes' : 'SMS: No'),
                ($rs_array['whatsapp_pdpa'] == 1 ? 'Whatsapp: Yes' : 'Whatsapp: No'),
                ($rs_array['email_pdpa'] == 1 ? 'Email: Yes' : 'Email: No'),
                ($rs_array['call_pdpa'] == 1 ? 'Call: Yes' : 'Call: No')
            )),
            $rs_array['created_date'],
            //$buttonClass->get_edit_button($this_folder, $rs_array['pkid']) . " " . $buttonClass->get_delete_button($this_folder, $rs_array['pkid'])
            $buttonClass->get_delete_button($this_folder, $rs_array['pkid'])
        );
    }

    if (empty($dataTable)) {
        $dataTable['data'] = array();
    }

    echo json_encode($dataTable);
    exit();
}
/*
$rsx=mq("SELECT * FROM dc_member");
while ($rx=mfa($rsx)){
    print_r($rx);
}
*/

if ($_POST['method'] == "delete_listing") {
    $pkid = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['pkid']);
    $folder = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['folder']);

    $query = get_query_delete($table['member'], $pkid);
    $database->query($query);

    do_tracking($user_username, 'Delete ' . $this_folder . " - #$pkid");

    echo json_encode(array('result' => 'success'));
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../head.php'); ?>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<?php include('../pre-loader.php') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php include('../nav.php') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Customer</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Sign Up</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="member/index.php?<?= http_build_query($_GET) ?>" method="post"
                                      class="form-horizontal">
                                    <h4 class="page-title">Filter</h4>
                                    <div class="form-group">
                                        <label class="control-label col-sm-1">UTM Source</label>
                                        <div class="col-sm-3">
                                            <select class="form-control" name="utm_source">
                                                <option value="">---Please Select---</option>
                                                <?
                                                $resultMember = get_query_data($table['member'], '1 and utm_source!="" group by utm_source');
                                                while ($rs_member = $resultMember->fetchRow()) {
                                                    if($rs_member['utm_source']==$postfield['utm_source'])
                                                    echo '<option value="' . $rs_member['utm_source'] . '" selected>' . $rs_member['utm_source'] . '</option>';
                                                    else
                                                    echo '<option value="' . $rs_member['utm_source'] . '">' . $rs_member['utm_source'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" name="submit_filter" value="true" class="btn btn-success">
                                        SUBMIT
                                    </button>
                                    <? if ($postfield['submit_filter']) { ?>
                                        <button type="submit" name="submit_clear" value="true" class="btn btn-danger">
                                            CLEAR
                                        </button>
                                    <? } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <!--<th>UTM Source</th>-->
                                        <!--<th>Source</th>-->
                                        <th>Score</th>
                                        <th>Permissions</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?php include('../footer.php') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<?php include('../js-script.php');  ?>
<script>
    var table = $('#datatable').DataTable({
        "ajax": "<?=$this_folder?>/?type=get_listing_data&where=<?=urlencode($where_text)?>",
        "order": [[0, "desc"]],
        responsive: true,
        dom: "<'row'<'col-sm-12'B>><'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>
</body>

</html>
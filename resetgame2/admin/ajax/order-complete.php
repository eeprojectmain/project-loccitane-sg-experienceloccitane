<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id=mysqli_real_escape_string($GLOBALS["mysqli_conn"],$_POST['id']);

$queryUpdate=get_query_update($table['order'],$order_id,array('status'=>'1'));
$databaseClass->query($queryUpdate);

echo json_encode(array('result'=>'success'));
exit();
?>
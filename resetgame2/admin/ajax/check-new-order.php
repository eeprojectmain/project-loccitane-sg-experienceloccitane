<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$result = get_query_data($table['order'], "status=0 or print_receipt=0 order by pkid desc");
$row = $result->numRows();
$rs_array=$result->fetchRow();

if ($row > 0) {
    echo json_encode(array("print" => "true","order_id"=>$rs_array['pkid'],'print_receipt'=>$rs_array['print_receipt']));
} else {
    echo json_encode(array("print" => "false"));
}
exit();
?>
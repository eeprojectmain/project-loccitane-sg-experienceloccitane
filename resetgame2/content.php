<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body style="font-family:Gill Sans, serif;background-color:white !important;">
<div class="bg-yellow">
    <div class="container">
        <div class="row">
            <div class="col-8 col-sm-9 col-md-6 col-xs-6 user-details" style="float:left;text-align:left;padding:2vh;">
                <label class="col-form-label" style="font-size:1em;"><strong>VINCENT LEE</strong></label><label
                        class="col-form-label">+60182268875</label><label class="col-form-label">VINCENTLEE@WOWSOME.COM.MY</label>
            </div>
            <div class="col-4 col-sm-3 col-md-6"
                 style="padding:0;">
                <button class="btn btn-link float-right btn-link" type="button" data-target="#modal-qr"
                        data-toggle="modal" style="padding:2vh;"><img class="img-fluid" src="assets/img/MM_QRcode.png"
                                                                      style="width:100px;max-width:100%;"
                                                                      data-target="modal-qr" data-toggle="modal">
                </button>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="container" style="min-width:100%;background-color:black;">
        <div class="row">
            <div class="col-md-12" style="padding:0;"><a class="btn btn-warning float-left btn-yellow" role="button"
                                                         href="menu.php" style="position:absolute;margin:10px;">Back</a><label
                        class="col-form-label text-center"
                        style="color:#ffcb00;text-transform:uppercase;font-size:2em;padding:10px;">Label</label></div>
        </div>
    </div>
</div>
<label class="font-yellow" style="padding:30px;font-size:2em;">Label</label>
<div>
    <div class="container" style="min-width:100%;">
        <div class="row">
            <div class="col-md-12" style="padding:0;"><img src="assets/img/OCC_241669dece0f449d8c4c1a29559b1b1f.jpg"
                                                           style="min-width:100%;max-width:100%;/*height:auto;*/"><h4
                        style="padding:30px 0 0 30px;">Heading</h1></div>
        </div>
    </div>
</div>
<p style="padding:0 30px;"><br>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec
    quam felis, ultricies nec, pellentesque eu, pretium
    quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
    enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer
    tincidunt. Cras
    dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat
    vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla
    ut metus varius
    laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam
    eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet
    adipiscing sem neque
    sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt
    tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus
    tincidunt. Duis
    leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales,
    augue velit cursus nunc,<br><br></p>
<div class="modal fade" role="dialog" tabindex="-1" id="modal-qr">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">YOUR QR CODE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" style="text-align:center;"><img class="img-fluid" src="assets/img/MM_QRcode.png"
                                                                    style="/*width:100px;*//*max-width:100%;*//*position:absolute;*//*left:0;*//*top:0;*//*text-align:center;*/"
                                                                    data-target="modal-qr" data-toggle="modal"></div>
            <div class="modal-footer">
                <button class="btn btn-dark" type="button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="footer-basic" style="left:0;right:0;bottom:0;padding:1rem;"><img class="img-fluid logo"
                                                                             src="assets/img/loccitane-logo.png">
    <footer>
        <p class="copyright">Power by Wowsome</p>
    </footer>
</div>
<?php include('js-script.php') ?>
</body>

</html>
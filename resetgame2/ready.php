<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/resetgame/admin/config.php";
global $table;
$databaseClass = new database();

$althead=1;
?>
<?php include('head.php') ?>

<table width="100%" style="background-image:url('/resetgame/bg.jpg'); background-position:top; background-repeat:repeat-y; pointer:cursor;" onclick="window.location.href='/resetgame/ready'">
<tr><td align="center" style="padding:20px;">
    <table border=0 style="max-width:600px;color:#253783; font-size:12px;font-family:locci; pointer:cursor;"><tr>
        <tr>

            <td><br>
            <div align="center">
                <a href="/resetgame/event-index"><img src="assets/img/logo-title-blue.png" class="img-fluid" style="max-height:60px; margin:2px;"></a>
            </div>
            <div class="pgtitle" style="color:#253783;font-size:34px; text-align:center; font-family:locci-serif"><br><i>Are you ready<br>to hit the<br><b style="font-family:locci-serif-bold">"Reset"</b> button?</i></div>
            
            <div class="pgtitle" style="color:#253783;font-size:18px; text-align:center; font-weight:bold;">
                
    <br><br>
    <br>
    <a href="/resetgame/game/?language=<?=$gamelang?>">
        <img id="r1" src="ready1.png" style="">
        <img id="r2" src="ready2.png" style="display:none;">
        <img id="r3" src="ready3.png" style="display:none;">
        <img id="r4" src="ready4.png" style="display:none;">
        <img id="r5" src="ready5.png" style="display:none;">
        <img id="r6" src="ready6.png" style="display:none;">
    </a>
    <br><br>
    <br><br>&nbsp;

            </div>
            <div>

            </div>
        <tr><td style="padding-top:510px;">&nbsp;</td></tr>    
    </table>
</table>
<script>
var count=1;
function gogo(){
    document.getElementById('r'+count).style.display='none';
    count++;
    document.getElementById('r'+count).style.display='';
    if (count==6) {return;}
    setTimeout("gogo()",500);
}
setTimeout("gogo()",1000);
</script>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>

</body>
</html>
<?php
session_start();

require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/resetgame/admin/config.php";
global $table;
$databaseClass = new database();

$althead=1;

if ($_SESSION['gscore']) {} else {
    header("Location: /resetgame/game/?language=".$gamelang);
    exit();
}

?>
<?php include('head.php') ?>
<table width="100%" style="background-image:url('/resetgame/bg2.jpg?v=2'); background-position:center top; background-repeat:repeat-y;" border="0">
<tr><td align="center" style="padding:20px;">
    <table border=0 style="max-width:600px;color:#253783; font-size:12px;font-family:locci"><tr>

        <tr><td style="padding:0px;">    
            <div align="center">
                <a href="/resetgame/event-index"><img src="assets/img/logo-title-blue.png" class="img-fluid" style="max-height:40px; margin:2px;"></a>
            </div>
                        
            <div class="pgtitle" style="color:#253783;font-size:34px; text-align:center; font-family:locci-serif-bold">Congratulations</div>
            
            <div class="pgtitle" style="color:#253783;font-size:16px; text-align:center; font-family:locci-bold"><br><i>You've really hit the reset button</i></div>
            
            <br><br><br><br>
            <br><br><br><br>
            <br><br><br><br>
            <br><br><br><br>
            <br><br><br><br>
            <div class="pgtitle" style="color:#253783;font-size:18px; text-align:center; font-family:locci-bold;">
            It's time to glow!<br><br>Sign up to claim your Surprise Treats!
            <br><br>
            <i style="font-size:14px;">Please fill in your mobile number</i>
            <br>
            <?=$error?>
            <form method="post" action="smscode2?resend=1" style="margin-top:15px;" class="">
                <div class="">
                    <div class="form-group"><input
                                class="fginput form-control justify-content-center align-items-center align-content-center align-self-center"
                                type="text" name="smobile" required placeholder="+65-MOBILE NUMBER" maxlength="14"
                                value=""
                        >
                    </div>

                    <div class="form-group mt-0 text-center" align="center"><table width="100%"><tr><td align="center">
                        <button style="max-width:250px;" class="yellowb btn btn-block btn-yellow" type="submit"
                                name="submit_register" value="true">
                            Submit
                        </button>
                        </td></tr></table>
                    </div>
                    <!--
                    <div class="form-group"><input
                                class="fginput form-control justify-content-center align-items-center align-content-center align-self-center"
                                type="text" name="passcode" required="" placeholder="One-time passcode" maxlength="6"
                                value=""
                        >
                        <br>
                        Re-send one-time passcode in <span id="spntm">01:00</span>
                        <span id="spnresend" style="display:none;"><br><a style="color:blue;" href="smscode?resend=1">Resend Passcode</a></span>
                    </div>

                    <div class="form-group mt-3 text-center" align="center"><table width="100%"><tr><td align="center"><br>
                        <button style="max-width:250px;" class="yellowb btn btn-block btn-yellow" type="submit"
                                name="submit_register" value="true">
                            Submit
                        </button>
                        </td></tr></table>
                    </div>
                    <div class="form-group mt-3 text-center" align="center"><table width="100%"><tr><td align="center">
                        <button style="max-width:250px;" onclick="window.location.href='/register';" class="blueb btn btn-block" type="button"
                                 value="true">
                            Back
                        </button>
                        </td></tr></table>
                    </div>
                    -->
                </div>
            </form>
            </div>
    </table>
    <tr><td style="padding-top:580px;">&nbsp;</td></tr>
</table>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>

</body>
</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$loginClass = new login();
$memberClass = new member();

$loginClass->memberRedirection();
$qrCode = $memberClass->qrCode();

$member_id = $_SESSION['member']['id'];

if ($_GET['a'] == "game" && $_GET['s'] != "") {
    $score = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['s']);
    $row_check = get_query_data_row($table['score'], "member_id=" . $member_id . " and score=$score and created_date like '%" . $time_config['today'] . "%'");

    if ($row_check == 0) {
        $postfield = array();
        $postfield['member_id'] = $member_id;
        $postfield['type'] = 'shea';
        $postfield['score'] = $score;
        $postfield['created_date'] = $time_config['now'];

        $queryInsert = get_query_insert($table['score'], $postfield);
        $databaseClass->query($queryInsert);
    }
}

$queryScore = "select max(score) as ss from " . $table['score'] . " where member_id=$member_id and type='shea' order by ss desc";
$resultScore = $databaseClass->query($queryScore);
$rs_score = $resultScore->fetchRow();

$queryScoreTTH = "select max(score) as ss from " . $table['score'] . " where member_id=$member_id and type='blt' order by ss desc";
$resultScoreTTH = $databaseClass->query($queryScoreTTH);
$rs_scoreTTH = $resultScoreTTH->fetchRow();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body style="font-family:Gill Sans, serif !important;background-color:white !important;">
<?php include('section-qr.php') ?>
<div class="container h-100"
     style="text-align:center;background-image: url('assets/img/road-bg.jpg');background-size:cover;">
    <div class="row">
        <div class="col-5 p-0">
            <a href="event-index" class="btn btn-yellow btn-warning float-left" style="margin:20px;"><i
                        class="fa fa-angle-double-left"></i> BACK</a>
        </div>
        <div class="col-7 p-0 mt-2">
            <a href="shop">
                <img src="assets/img/icon-shop.png" class="img-fluid float-right float-icon" width="25%"
                     style="position: fixed;right:27%;">
            </a>
            <a href="share-cafe">
                <img src="assets/img/icon-snap.png" class="img-fluid float-right float-icon" width="25%"
                     style="position: fixed;right:1%;">
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 p-0">
            <img src="assets/img/icon-petals.png" class="station-1 img-fluid float-left" width="40%"
                 data-toggle="modal" data-target="#modal-petals">
        </div>
        <div class="col-12 p-0 mt-5">
            <img src="assets/img/icon-almond.png" class="station-2 img-fluid float-left" width="40%"
                 data-toggle="modal" data-target="#modal-almond">
            <img src="assets/img/icon-immortelle.png" class="station-3 img-fluid float-right" width="40%"
                 data-toggle="modal" data-target="#modal-immortelle">
        </div>
        <div class="col-12 p-0 mt-3">
            <img src="assets/img/icon-tri.png" class="station-4 img-fluid float-left mt-4" width="40%"
                 data-toggle="modal" data-target="#modal-tri">
            <img src="assets/img/icon-tth.png" class="station-5 img-fluid float-right mt-3" width="40%"
                 data-toggle="modal" data-target="#modal-tth">
        </div>
        <div class="col-12 p-0 mt-3">
            <img src="assets/img/icon-shea.png" class="station-6 img-fluid float-left" width="40%"
                 data-toggle="modal" data-target="#modal-shea">
            <img src="assets/img/icon-essential.png" class="station-7 img-fluid float-right mt-4" width="40%" data-toggle="modal" data-target="#modal-essential">
        </div>
        <div class="col-12 p-0 mt-3 mb-5">
            <img src="assets/img/icon-checkout.png" class="station-8 img-fluid float-left" width="40%"
                 style="opacity: 0.5" data-target="#modal-checkout">
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-order">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ORDER HISTORY</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" id="modal-order-body">
                <i class="fa fa-cog fa-spin fa-3x fa-fw"
                   style="top: 0;left: 0;right: 0;position: absolute;margin: auto;"></i>
            </div>
            <div class="modal-footer">
                <button class="btn btn-dark btn-black" type="button" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-essential">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h5 class="modal-title text-center w-100"><img src="assets/img/header-essential.jpg"
                                                               class="img-fluid"></h5>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                Take the guesswork out of purchasing a hair product? Our expert scalp imaging system at the beauty
                market shows you your scalp type, so that you can pick the right product for your hair and scalp
                every time.
                <br>
                <br>
                <img class="img-fluid" src="<?= $qrCode ?>">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-tth">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h5 class="modal-title text-center w-100"><img src="assets/img/header-7.png"
                                                               class="img-fluid"></h5>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                Join in the fun at the Wash & Wipe kinetic game, where you can learn the proper way of recycling
                <br>
                <br>
                <?php
                if ($rs_scoreTTH['ss'] != "") {
                    if ($rs_scoreTTH['ss'] < '150') {
                        ?>
                        <img src="assets/img/level1.png" width="50%">
                        <?php
                    } /*elseif ($rs_scoreTTH['ss'] <= '150') {
                        ?>
                        <img src="assets/img/level2.png" width="50%">
                        <?php
                    }*/ elseif ($rs_scoreTTH['ss'] >= '150') {
                        ?>
                        <img src="assets/img/level4.png" width="50%">
                        <?
                    }
                    ?>
                    <br>
                    <br>
                    <?php
                } ?>
                <img class="img-fluid" src="<?= $qrCode ?>">
                <p class="mt-3">Visit this station and score above 100 to get rewards!</p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-petals">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h5 class="modal-title text-center w-100"><img src="assets/img/header-5.png" class="img-fluid"></h5>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                Step on up and smell the most exquisite fragrances in all the land! Made from the essence of fresh
                flowers on the day of their harvest, these perfumes will transport you to the naturally beautiful
                landscapes of Provence. Only the freshest hand-picked flowers are used!
                <br>
                <br>
                <img class="img-fluid" src="<?= $qrCode ?>">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-almond">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h4 class="modal-title text-center w-100"><img src="assets/img/header-1.png" class="img-fluid"></h4>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                Make your own almond milk instantly at Jaubert’s Almond Milk! Using only fresh almonds from
                Valensole,
                this almond milk is no ordinary concoction. In fact, it looks like an oil that transforms into a
                milk…
                Dare we say it might even be magic?
                <br>
                <br>
                <img class="img-fluid" src="<?= $qrCode ?>">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-tri">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h4 class="modal-title text-center w-100"><img src="assets/img/header-4.png" class="img-fluid"></h4>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                Come one, come all to the mythical Immortelle Reset Fountain! Here, time moves backwards and water
                flows
                upwards. It is our unique way to stop the clock for your skin!
                <br>
                <br>
                <img class="img-fluid" src="<?= $qrCode ?>">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-immortelle">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h4 class="modal-title text-center w-100"><img src="assets/img/header-immotelle.jpg"
                                                               class="img-fluid"></h4>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                Grower and harvester of the legendary Immortelle flower Pascale Chérubin has brought her
                distillation
                device down to the market for all to marvel. Come see how 1,000kg of flowers are painstakingly
                distilled
                into 2 kg of golden drops containing everlasting youth!
                <br>
                <br>
                <img class="img-fluid" src="<?= $qrCode ?>">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-shea">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h4 class="modal-title text-center w-100"><img src="assets/img/header-6.png" class="img-fluid"></h4>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                Whipped cream, cream or butter — we have it all at the Shea Butter Creamery! Discover the gold of
                Burkina Faso and how it is produced. In fact, come help us produce some Shea Butter in our Shea
                Shake
                Shake game and stand a chance to win some prizes too!
                <br>
                <br>
				<!--
                <?php
                if ($rs_score['ss'] != "") {
                    if ($rs_score['ss'] < 4000) {
                        ?>
                        <img src="assets/img/level1.png" width="50%">
                        <?php
                    } elseif ($rs_score['ss'] < 9000) {
                        ?>
                        <img src="assets/img/level2.png" width="50%">
                        <?php
                    } elseif ($rs_score['ss'] < 12000) {
                        ?>
                        <img src="assets/img/level3.png" width="50%">
                        <?php
                    } elseif ($rs_score['ss'] >= 12000) {
                        ?>
                        <img src="assets/img/level4.png" width="50%">
                        <?php
                    } ?>
                    <br>
                    <br>
                    <?php
                } ?>
				-->

                <? if (strtotime('now') <= strtotime('2022-02-05')) { ?>
                    <a href="game" class="btn btn-yellow">PLAY GAME</a>
                    <br>
                    <br>
                <? } ?>
                <img class="img-fluid" src="<?= $qrCode ?>">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-walk">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h4 class="modal-title text-center w-100"><img src="assets/img/header-3.png" class="img-fluid"></h4>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                Need to freshen up after a long day at the market? Come catch a bubble or two at the Walk of Aroma!
                These magical bubbles pop to reveal the natural scent of the Aromachologie hair care range and is
                sure
                to delight you. Where do these bubbles come from? It’s a secret!
                <br>
                <br>
                <img class="img-fluid" src="<?= $qrCode ?>">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-checkout">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h4 class="modal-title text-center w-100">CHECK OUT</h4>
                <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center">
                You have now completed your tour of the Beauty Market and can proceed to redeem your treat at our
                Market
                square!
                <br>
                <br><!--
                <?php
                if ($rs_score['ss'] != "") {
                    if ($rs_score['ss'] < 4000) {
                        ?>
                        <img src="assets/img/level1.png" width="50%">
                        <?php
                    } elseif ($rs_score['ss'] < 9000) {
                        ?>
                        <img src="assets/img/level2.png" width="50%">
                        <?php
                    } elseif ($rs_score['ss'] < 12000) {
                        ?>
                        <img src="assets/img/level3.png" width="50%">
                        <?php
                    } elseif ($rs_score['ss'] >= 12000) {
                        ?>
                        <img src="assets/img/level4.png" width="50%">
                        <?php
                    } ?>
                    <br>
                    <br>
                    <?php
                } ?>
				-->
                Show this QR code to the beauty advisor to receive your gift!
                <br>
                <br>
                <img class="img-fluid" src="<?= $qrCode ?>">
            </div>
        </div>
    </div>
</div>

<?php include('js-script.php') ?>
<script>
    function open_order_modal() {
        $("#modal-order-body").load("remote-view/order-history", function (e) {
            $("#modal-order").modal('show');
        });
    }
</script>
</body>

</html>
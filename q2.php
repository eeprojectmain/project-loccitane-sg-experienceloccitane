<?php

require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/config.php";
$database = new database();

// convert currency into usd
function currencyConversion($amount, $currency){
    if($currency == 'USD')
        return $amount;

    $currenciesRateArr = 
    [
        "MYR" => 4.48,
        "JPY" => 136.83,
        "CNY" => 6.91
    ];

    if(!isset($currenciesRateArr[$currency]))
    {
        return throw new Exception("Invalid currency: $currency");
    }

    $rate = $currenciesRateArr[$currency];

    $amount = $amount / $rate;

    return 
}

function calculatePoint($amount, $currency){
    return floor(currencyConversion($amount, $currency));
}


function usePoint($memberId, $orderId, $amount, $currency){
    $amount = currencyConversion($amount, $currency);

    $selectQuery = "SELECT points FROM  members WHERE id = ".$memberId;
    $pointsData = $database->query($selectQuery);
    $availablePoint = $pointsData->fetchRow()['points'];


    $updateQuery = "UPDATE members SET points = points - ".$points." WHERE id = ".$memberId;
    $database->query($queryUpdate);

    // store point as -ve value indicate point spent
    $points = -$points;
    $insertQuery = "INSERT INTO point_history (member_id,order_id,points) VALUES(".$memberId.",".$orderId.",".$points.");"
    $database->query($insertQuery);
}

function addPoint($memberId, $orderId, $amount, $currency){
    $pointEarned = calculatePoint($amount, $currency);

    $updateQuery = "UPDATE members SET points = points + ".$pointEarned." WHERE id = ".$memberId;
    $database->query($queryUpdate);

    $expiryDate = date('Y-m-d', strtotime('+1 year', strtotime(date('Y-m-d')))); 

    $insertQuery = "INSERT INTO point_history (member_id,order_id,points,expiry_date) VALUES(".$memberId.",".$orderId.",".$points.",".$expiryDate.");"
    $database->query($insertQuery);
}
?>
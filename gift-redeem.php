<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id = $_GET['i'];
$order_id = protect('decrypt', $order_id);

if ($_POST['submit_form']) {
    $postfield = $_POST;

    unset($postfield['submit_form']);

    $queryUpdate = get_query_update($table['order'], $order_id, array('status' => '5', 'ba_code' => $postfield['ba_code'], 'updated_date' => $time_config['now']));
    $databaseClass->query($queryUpdate);

    $swal['type'] = "success";
    $swal['title'] = "Yay!";
    $swal['msg'] = "Successfully marked as redeemed";
}

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $rs_order['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg bg-light mw-400">
<div class="container-fluid">
    <div class="row mt-5 pb-5">
        <div class="col-12">
            <img src="assets/img/logo.png" class="logo"/>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <?php if ($rs_order['shipping_method'] == "delivery") { ?>
                <p class="w-100 text-center">Delivery is scheduled for
                    <?= date('d-M-Y', strtotime($rs_order['pickup_date'])) . " at " . $rs_order['pickup_time'] ?> to
                    <?= $rs_order['address'] ?> from <?=$rs_outlet['title']?>.
                </p>
                <button type="button" class="btn btn-blue btn-track"
                        data-tracking_no="<?= $rs_order['tracking_no'] ?>" data-order_id="<?= $rs_order['pkid'] ?>">
                    TRACK
                    YOUR ORDER
                </button>
            <?php } elseif ($rs_order['status'] != "6" && $rs_order['shipping_method'] == "pickup") { ?>
                <p class="w-100 text-center">Present this screen to our beauty advisor at L'OCCITANE
                    <?= $rs_outlet['title'] ?> to redeem
                    it!</p>
            <?php } else { ?>
                <p class="w-100 text-center">This voucher already redeemed on <?= $rs_order['updated_date'] ?>
                </p>
                <?php
            } ?>
            <div class="text-left m-5 mt-1">
                <p>Order ID: #<?= $rs_order['pkid'] ?>
                </p>
                <p>Placed on: <?= $rs_order['created_date'] ?>
                </p>
            </div>
            <p class="w-100 text-center">
                Please click the WhatsApp button below if you would like to contact <?= $rs_outlet['title'] ?></p>
            <a href="https://wa.me/<?= $rs_outlet['whatsapp_no'] ?>?text=Hi L'OCCITANE <?= $rs_outlet['title'] ?>, I need assist!"
               target="_blank" class="btn btn-blue">WhatsApp</a>
            <div class="d-none">
                <img src="assets/img/gift-box.png" class="img-fluid w-50"/>
                <br>
                <div class="w-100 text-center">
                    <i class="fa fa-quote-left fa-2x pl-4 w-100 text-left"></i>
                    <p class="w-100 text-center m-0 pl-5 pr-5"><?= nl2br($rs_order['receive_message']) ?>
                    </p>
                    <p class="w-100 text-right pr-5">- <?= $rs_order['name'] ?>
                    </p>
                    <i class="fa fa-quote-right fa-2x pr-4 w-100 text-right"></i>
                </div>
            </div>
        </div>

        <div class="col-12 text-center mt-5 mb-5">
            <?php
            if ($rs_order['status'] != "5" && $rs_order['shipping_method'] == "pickup") { ?>

                <button type="button" class="btn btn-yellow" data-toggle="modal" data-target="#modal_redeem">Invalidate
                    Voucher
                </button>
                <br>
                <small>Office use only</small>
            <?php }
            ?>
        </div>
    </div>
</div>

<!-- Modal Redeem -->
<div class="modal fade" id="modal_redeem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="gift-redeem?i=<?= $_GET['i'] ?>" method="post">
                    <div class="row">
                        <div class="col-12 text-center">
                            <div class="form-group">
                                <label>Please enter BA Code</label>
                                <input type="text" name="ba_code" class="form-control" required maxlenght="10"/>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="submit_form" value="true"
                                        class="btn btn-yellow">SUBMIT
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Track -->
<div class="modal fade" role="dialog" tabindex="-1" id="modal-track">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <h4 class="modal-title w-100 text-center">DELIVERY STATUS</h4>
                <button type="button" class="close p-absolute r-5 w-100 text-right" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" id="modal-track-content">

            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(".btn-track").on('click', function (e) {
        var oid = $(this).data('order_id');
        $("#modal-track-content").html('<i class="fa fa-2x fa-gear fa-spin centered"></i>');
        $("#modal-track-content").load('remote-view/delivery-status-gift?oid=' + oid, function () {
        });
        $("#modal-track").modal('show');
    });

    $('[data-toggle="popover"]').popover({
        trigger: 'hover'
    })
</script>
</body>

</html>
<?php

require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";

global $table;

$databaseClass = new database();



if ($_GET['t'] == "gift") {

    $_SESSION['type'] = "gift";

} else {

    $_SESSION['type'] = "normal";

}



if ($_POST) {

    $outlet_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['outlet_id']);



    $_SESSION['outlet_id'] = $outlet_id;



    if ($_GET['return'] != "") {

        header("Location: " . $_GET['return']);

        exit();

    }



    header("Location: shop-holiday");

    exit();

}

?>

<!DOCTYPE html>

<html>



<?php include('head.php') ?>



<body>

<div class="container-fluid">

    <? include('nav.php') ?>

    <div class="row mt-4">

        <div class="col-12">

            <div class="title">

                <h4>SELECT A STORE</h4>

                <p>Choose your preferred boutique</p>

            </div>



            <form action="shop-holiday-select?<?= http_build_query($_GET) ?>" method="post" class="store-select">

                <div class="form-group">

                    <?php

                    $resultOutlet = get_query_data($table['outlet'], "status=1");

                    while ($rs_outlet = $resultOutlet->fetchRow()) {

                        ?>

                        <div class="form-check border-btm-yellow">

                            <input class="form-check-input" type="radio" name="outlet_id" <?//=$rs_outlet['pkid']=='4'?"disabled":""?>

                                   id="outlet_<?= $rs_outlet['pkid'] ?>" value="<?= $rs_outlet['pkid'] ?>"

                                   required <?= $rs_outlet['pkid'] == "1" ? "" : "" ?>>

                            <label class="form-check-label" for="outlet_<?= $rs_outlet['pkid'] ?>">

                                <div class="row align-items-center">

                                    <div class="col-4">

                                        <img src="assets/img/store-1.png" class="img-fluid"/>

                                    </div>

                                    <div class="col-8">

                                    <?if($rs_outlet['pkid']=='4'){?>

                                        <!--<span class="badge badge-danger">Temporary Closed till Further Notice</span><br /><br />-->

                                    <?}?>

                                        <span class="store-title align-middle"><?= $rs_outlet['title'] ?></span>

                                    </div>

                                </div>

                            </label>

                        </div>

                        <?php

                    } ?>

                </div>

                <div class="form-group mt-5">

                    <button type="submit" name="submit_outlet"

                            class="btn btn-blue w-50 mx-auto d-block">NEXT

                    </button>

                </div>

            </form>

        </div>

    </div>

</div>

<?php include('footer.php') ?>

<?php include('js-script.php') ?>

<script>

    $(document).ready(function () {

        $(".btn-whatsapp").hide();

    });

</script>

</body>



</html>
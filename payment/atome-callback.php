<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass=new stock();

$callback = file_get_contents('php://input');
file_put_contents("error.txt", $callback.PHP_EOL, FILE_APPEND);
$callback = json_decode($callback, true);

$order_id = explode("LOSG", $callback['referenceId'])[0];

$row_order = get_query_data_row($table['order'], "pkid=" . $order_id);

if ($row_order > 0) {
    //check status
//    $api_url = 'https://api.apaylater.net/v2';
    $api_url = 'https://api.apaylater.com/v2';

    $ch = curl_init($api_url . '/payments/' . $callback['referenceId']);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//    curl_setopt($ch, CURLOPT_USERPWD, '28551ece56fb4274ad634444201a8289:fc57552c3712402e81ae869c5d06a54e');
    curl_setopt($ch, CURLOPT_USERPWD, '24eeebd67e0247f5add80c7bb67998dc:e78352ffaf3b45b2937fab0213c01b5b');
    curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    file_put_contents("error.txt", json_encode($result).PHP_EOL, FILE_APPEND);
    curl_close($ch);

    $result = json_decode($result, true);

    if ($result['status'] == "PAID") {
        $data_array = array(
            'status' => '1',
            'order_id' => $order_id,
            'method' => 'Atome',
            'trans_id' => $result['referenceId'],
            'amount' => $result['amount'] / 100,
            'created_date' => $time_config['now']
        );

        $queryInsert = get_query_insert($table['payment'], $data_array);
        $databaseClass->query($queryInsert);

        $resultOrder = get_query_data($table['order'], "pkid=" . $order_id);
        $rs_order = $resultOrder->fetchRow();

        $queryUpdate = get_query_update($table['order'], $order_id, array('status' => '1', 'payment_status' => '1'));
        $databaseClass->query($queryUpdate);

        if ($rs_order['discount_amount'] != "" && $rs_order['cprv_id'] == "999") {
            $resultVoucher = get_query_data($table['voucher'], "mobile='" . $rs_order['mobile'] . "'");
            $rs_voucher = $resultVoucher->fetchRow();

            $queryUpdate = get_query_update($table['voucher'], $rs_voucher['pkid'], array('status' => '0', 'updated_date' => $time_config['now']));
            $databaseClass->query($queryUpdate);
        }

        $order_id = $rs_order['pkid'];
        $stockClass->minus($order_id);
        include('hook.php');

        if ($rs_order['shipping_method'] == 'delivery') {
            include("../ajax/lalamove-order-new.php");
        }
    } else {
        $data_array = array(
            'status' => '0',
            'order_id' => $order_id,
            'method' => 'Atome',
            'trans_id' => $result['referenceId'],
            'amount' => $result['amount'] / 100,
            'created_date' => $time_config['now']
        );

        $queryInsert = get_query_insert($table['payment'], $data_array);
        $databaseClass->query($queryInsert);
    }
}

echo 'OK';
exit();
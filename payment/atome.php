<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$orderClass = new order();

$order_id = $_SESSION['member']['order_id'];

$orderClass->check();
$orderClass->update();
$orderClass->check_final();

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

if ($rs_order['cprv_id'] != "0") {
    if ($rs_order['total_amount'] < $voucher_minspend_array[$rs_order['cprv_id']]) {
        $rs_order['voucher_discount_amount'] = '0';
    }
}

$merchant_ref = $order_id . 'LOSG' . uniqid();

$postfield = array();
$postfield['method'] = 'Atome';
$postfield['order_id'] = $order_id;
$postfield['merchant_ref'] = $merchant_ref;
$postfield['created_date'] = $time_config['now'];

$queryInsert = get_query_insert($table['payment'], $postfield);
$databaseClass->query($queryInsert);

$amount = $rs_order['total_amount'] - $rs_order['promotion_discount_amount'] - $rs_order['voucher_discount_amount']-$rs_order['member_discount_amount'] + $rs_order['shipping_amount'];

$postfield = array(
    'referenceId' => $merchant_ref,
    'currency' => 'SGD',
    'amount' => $amount * 100,
    'callbackUrl' => $site_config['full_url'] . 'payment/atome-callback',
    'paymentResultUrl' => $site_config['full_url'] . 'payment-success?Ref='.$order_id,
    'paymentCancelUrl' => $site_config['full_url'] . 'payment-fail?Ref='.$order_id,
    'merchantReferenceId' => '#LDSG20' . $order_id,
    'customerInfo' => array(
        'mobileNumber' => $rs_order['mobile'],
        'fullName' => $rs_order['name'],
        'email' => $rs_order['email']
    ),
    'taxAmount' => (($amount * 100) / 100) * 7,
    'shippingAmount' => $rs_order['shipping_amount'] * 100,
    'originalAmount' => $amount * 100,
);

$postfield['shippingAddress'] = array(
    'countryCode' => 'SG',
    'lines' => array(
        '100 Woodlands Ave 5',
        'Singapore 739010'
    ),
    'postCode' => '739010'
);
$postfield['billingAddress'] = array(
    'countryCode' => 'SG',
    'lines' => array(
        '100 Woodlands Ave 5',
        'Singapore 739010'
    ),
    'postCode' => '739010'
);

foreach (explode(",", $rs_order['product_id']) as $k => $v) {
    $quantity = explode(",", $rs_order['quantity'])[$k];

    $resultProduct = get_query_data($table['product'], "pkid=$v");
    $rs_product = $resultProduct->fetchRow();

    $postfield['items'][] = array(
        'itemId' => $rs_product['item_code'],
        'name' => $rs_product['title'],
        'quantity' => $quantity,
        'price' => $amount * 100
    );
}

//$api_url = 'https://api.apaylater.net/v2';
$api_url = 'https://api.apaylater.com/v2';

$ch = curl_init($api_url . '/payments');
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postfield));
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//curl_setopt($ch, CURLOPT_USERPWD, '28551ece56fb4274ad634444201a8289:fc57552c3712402e81ae869c5d06a54e');
curl_setopt($ch, CURLOPT_USERPWD, '24eeebd67e0247f5add80c7bb67998dc:e78352ffaf3b45b2937fab0213c01b5b');
curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

$result = json_decode($result, true);

if($result['redirectUrl']) {
    header("Location: " . $result['redirectUrl']);
    exit();
}else{
    print_r($result);
}
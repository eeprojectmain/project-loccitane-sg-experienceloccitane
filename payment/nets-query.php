<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id = $_GET['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$resultPayment = get_query_data($table['payment'], "order_id=$order_id and merchant_ref!='' order by pkid desc limit 1");
$rs_payment = $resultPayment->fetchRow();

$amount = $rs_order['total_amount'] - $rs_order['discount_amount'] - $rs_order['promotion_discount_amount'] - $rs_order['voucher_discount_amount'] + $rs_order['shipping_amount'];

$api_umid = 'UMID_838457004';
$api_id = 'b4952ae3-620d-4081-99bc-a84508feca95';
$api_secret = '6b42f3a8-250e-48f1-a147-54c224a84546';

$nets_json['ss'] = '1';
$nets_json['msg'] = array(
    'netsMid' => 'UMID_838457004',
    'merchantTxnRef' => $rs_payment['merchant_ref'],
    'netsMidIndicator' => 'U',
);

$signature = 'ajpFbL/otfRmD6S9HHWnzJyURagnmSRL7H21SBzOIQQ=';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://uat-api.nets.com.sg:9065/GW2/TxnQuery");
//curl_setopt($ch, CURLOPT_URL, "https://api.nets.com.sg /GW2/TxnQuery");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($nets_json));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$headers = [
    'Content-Type: application/json',
    'keyId: ' . $api_id,
    'hmac: ' . $signature,
];
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
var_dump($server_output);
curl_close($ch);
?>
<!DOCTYPE html>
<html>
<base href="<?= $site_config['full_url'] ?>">
<body class="page-bg">
<div class="container">
    <input type="hidden" id="txnReq" name="txnReq" value='<?= json_encode($nets_json) ?>'>
    <input type="hidden" id="keyId" name="keyId" value="<?= $api_id ?>">
    <input type="hidden" id="hmac" name="hmac" value="">
    <div id="anotherSection">
        <fieldset>
            <div id="ajaxResponse"></div>
        </fieldset>
    </div>
</div>
</body>
</html>
<? include('../js-script.php') ?>
<script src="https://uat2.enets.sg/demo/assets/js/sha256.js"></script>
<script src="../assets/js/nets.js"></script>
<script>
    var gwdomain = 'https://uat2.enets.sg';
    var apigwdomain = 'https://uat-api.nets.com.sg/GW2/TxnReqListener';

    $(document).ready(function () {
        $(".btn-whatsapp").hide();

        calHMAC();
    });

    function calHMAC() {
        var txnreq = $('#txnReq').val();
        var secretKey = '<?=$api_secret?>';

        var sign = btoa(sha256(txnreq + secretKey).match(/\w{2}/g).map(function (a) {
            return String.fromCharCode(parseInt(a, 16));
        }).join(''));

        $('#hmac').val(sign);

        $.ajax({
            type: "POST",
            url: 'https://uat-api.nets.com.sg:9065/GW2/TxnQuery',
            headers: {
                "KeyId": '<?=$api_id?>',
                "hmac": sign
            },
            contentType: "application/json",
            cache: false,
            dataType: "json",
            data: '<?=json_encode($nets_json)?>',

            success: function (data, textStatus, jqXHR) {
                console.log(data);
            }
        });
    }
</script>

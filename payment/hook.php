<?php
//free shipping for trial kit
if (strtotime('now') <= strtotime('2021-07-31 23:59:59')) {
    $product_id_array = explode(",", $rs_order['product_id']);
    if (in_array('310', $product_id_array)) {
        $postfield = array(
            'status' => '1',
            'type' => 'free_shipping',
            'order_id' => $rs_order['pkid'],
            'mobile' => $rs_order['mobile'],
            'created_date' => $time_config['now']
        );
        $queryInsert = get_query_insert($table['member_voucher'], $postfield);
        $databaseClass->query($queryInsert);
    }
}

//gwp promo, 9 oct - 28 oct
if (strtotime('now') <= strtotime('2020-10-28 23:59:59') && $rs_order['campaign_id'] == '0') {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://loccitanesgwebservice.crmxs.com/?xs_app=endemande.getCustomerSummary",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\"hpno\":\"" . str_replace("+65", "", $rs_order['mobile']) . "\"}",
        CURLOPT_HTTPHEADER => array(
            "client_id: 5vus5fnhdeeghff5de8c2nrq46fhy8nh",
            "client_secret: sum388my7amm8jp7k3ru5pyb4hp8g87g",
            "Content-Type: text/plain",
        ),
    ));

    $server_response = curl_exec($curl);
    curl_close($curl);
    $voucher_array = json_decode($server_response, true);

    $unqualified_amount = 0;
    $row_check = get_query_data_row($table['order'], "pkid=$order_id and find_in_set(1040,product_id) and payment_status=1");

    if ($row_check > 0) {
        $resultOrder = get_query_data($table['order'], "pkid=$order_id");
        $rs_order = $resultOrder->fetchRow();

        $product_id = explode(",", $rs_order['product_id']);
        $product_qty = explode(",", $rs_order['quantity']);

        foreach ($product_id as $k => $v) {
            if ($v == "1040") {
                $unqualified_amount = 49 * $product_qty[$k];
            }
        }
    }

    if ($voucher_array['status'] == "error") {
        //not a member
        //add 1037
        if (($rs_order['total_amount'] - $rs_order['discount_amount'] - $rs_order['promo_discount_amount'] - $unqualified_amount) >= 250) {
            $resultOrder = get_query_data($table['order'], "pkid=$order_id");
            $rs_order = $resultOrder->fetchRow();

            $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1037", 'quantity' => $rs_order['quantity'] . ",1"));
            $databaseClass->query($queryUpdate);
        }

        if (($rs_order['total_amount'] - $rs_order['discount_amount'] - $rs_order['promo_discount_amount'] - $unqualified_amount) >= 500) {
            $resultOrder = get_query_data($table['order'], "pkid=$order_id");
            $rs_order = $resultOrder->fetchRow();

            $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1037", 'quantity' => $rs_order['quantity'] . ",1"));
            $databaseClass->query($queryUpdate);
        }
    } else {
        //member
        //add 1037
        if (($rs_order['total_amount'] - $rs_order['discount_amount'] - $rs_order['promo_discount_amount'] - $unqualified_amount) >= 200) {
            $resultOrder = get_query_data($table['order'], "pkid=$order_id");
            $rs_order = $resultOrder->fetchRow();

            $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1037", 'quantity' => $rs_order['quantity'] . ",1"));
            $databaseClass->query($queryUpdate);
        }

        if (($rs_order['total_amount'] - $rs_order['discount_amount'] - $rs_order['promo_discount_amount'] - $unqualified_amount) >= 400) {
            $resultOrder = get_query_data($table['order'], "pkid=$order_id");
            $rs_order = $resultOrder->fetchRow();

            $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1037", 'quantity' => $rs_order['quantity'] . ",1"));
            $databaseClass->query($queryUpdate);
        }
    }
}

if (strtotime('now') >= strtotime('2020-10-29') && strtotime('now') <= strtotime('2020-12-31')) {
    $resultOrder = get_query_data($table['order'], "pkid=$order_id");
    $rs_order = $resultOrder->fetchRow();

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://loccitanesgwebservice.crmxs.com/?xs_app=endemande.getCustomerSummary",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\"hpno\":\"" . str_replace("+65", "", $rs_order['mobile']) . "\"}",
        CURLOPT_HTTPHEADER => array(
            "client_id: 5vus5fnhdeeghff5de8c2nrq46fhy8nh",
            "client_secret: sum388my7amm8jp7k3ru5pyb4hp8g87g",
            "Content-Type: text/plain",
        ),
    ));

    $server_response = curl_exec($curl);
    curl_close($curl);
    $voucher_array = json_decode($server_response, true);

    $product_id = explode(",", $rs_order['product_id']);
    $product_qty = explode(",", $rs_order['quantity']);

    foreach ($product_id as $k => $v) {
        $resultProduct = get_query_data($table['product'], "pkid=$v");
        $rs_product = $resultProduct->fetchRow();

        if (!in_array($v, $product_pwp_array)) {
            $total += $rs_product['price'] * $product_qty[$k];
        }
    }

    $total = $total - $rs_order['discount_amount'] - $rs_order['promotion_discount_amount'] - $rs_order['voucher_discount_amount'];

    if ($voucher_array['status'] == "error") {
        //not a member
        if (strtotime('now') >= strtotime('2020-12-03') && strtotime('now') <= strtotime('2020-12-06')) {
            if ($total >= 370) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1293", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }
        }

        if (strtotime('now') >= strtotime('2020-12-01') && strtotime('now') <= strtotime('2021-01-03')) {
            if ($total >= 250) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1046", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }

            if ($total >= 500) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1046", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }

            if ($total >= 310) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1047", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }

            if ($total >= 620) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1047", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }
        }
    } else {
        if (strtotime('now') >= strtotime('2020-12-03') && strtotime('now') <= strtotime('2020-12-06')) {
            if ($total >= 320) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1293", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }
        }

        if (strtotime('now') >= strtotime('2020-10-29') && strtotime('now') <= strtotime('2020-11-30')) {
            if ($total >= 200) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1285", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }

            if ($total >= 400) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1285", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }

            if ($total >= 260) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1286", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }

            if ($total >= 520) {
                $resultOrder = get_query_data($table['order'], "pkid=$order_id");
                $rs_order = $resultOrder->fetchRow();

                $queryUpdate = get_query_update($table['order'], $order_id, array('product_id' => $rs_order['product_id'] . ",1286", 'quantity' => $rs_order['quantity'] . ",1"));
                $databaseClass->query($queryUpdate);
            }
        }
    }
}

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

if ($rs_order['game_voucher_id'] != '0') {
    $queryUpdate = get_query_update($table['member_voucher'], $rs_order['game_voucher_id'], array('status' => '0', 'updated_date' => $time_config['now'], 'updated_by' => 'EnDemande Payment (Online Payment)'));
    $databaseClass->query($queryUpdate);
}

if ($rs_order['voucher_id'] != '0') {
    $queryUpdate = get_query_update($table['member_voucher'], $rs_order['voucher_id'], array('status' => '0', 'updated_date' => $time_config['now'], 'updated_by' => 'EnDemande Payment (Online Payment)'));
    $databaseClass->query($queryUpdate);
}

if ($rs_order['member_voucher_id'] != '0') {
    $queryUpdate = get_query_update($table['member_voucher'], $rs_order['member_voucher_id'], array('status' => '0', 'updated_date' => $time_config['now'], 'updated_by' => 'EnDemande Payment (Online Payment)'));
    $databaseClass->query($queryUpdate);
}
<?php
header('Access-Control-Allow-Origin: *');
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$nav_step=2;

$order_id = $_SESSION['member']['order_id'];
if ($order_id == "") {
    header("Location: ../checkout-details");
    exit();
}
$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

foreach ($_SESSION['cart'] as $k => $v) {
    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    $total += $rs_product['price'] * $v['quantity'];
}
$queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('total_amount' => $total));
$databaseClass->query($queryUpdate);

if ($rs_order['outlet_id'] == "" || $rs_order['outlet_id'] == "0") {
    header("Location: ../shop-select");
    exit();
}

if ($_SESSION['shipping_method'] == "delivery") {
    $resultOrder = get_query_data($table['order'], "pkid=$order_id");
    $rs_order = $resultOrder->fetchRow();

    if ($rs_order['address'] == "" || $rs_order['lat'] == "" || $rs_order['lng'] == "" || $rs_order['shipping_amount'] == "") {
        header("Location: ../checkout-lalamove");
        exit();
    }
} else if ($_SESSION['shipping_method'] == "pickup") {
    $resultOrder = get_query_data($table['order'], "pkid=$order_id");
    $rs_order = $resultOrder->fetchRow();

    if ($rs_order['pickup_date'] == "" || $rs_order['pickup_time'] == "") {
        header("Location: ../checkout-date");
        exit();
    }
} elseif ($_SESSION['shipping_method'] == "courier") {
    $resultOrder = get_query_data($table['order'], "pkid=$order_id");
    $rs_order = $resultOrder->fetchRow();

    if ($rs_order['address'] == "") {
        header("Location: ../checkout-details");
        exit();
    }
} else {
    $resultOrder = get_query_data($table['order'], "pkid=$order_id");
    $rs_order = $resultOrder->fetchRow();

    if ($rs_order['shipping_method'] == "") {
        header("Location: ../index");
        exit();
    } else {
        $_SESSION['shipping_method'] = $rs_order['shipping_method'];

        header("Location: cc");
        exit();
    }
}

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$merchant_ref = $order_id . '_' . uniqid();

$postfield = array();
$postfield['method'] = 'eNets';
$postfield['order_id'] = $order_id;
$postfield['merchant_ref'] = $merchant_ref;
$postfield['created_date'] = $time_config['now'];

$queryInsert = get_query_insert($table['payment'], $postfield);
$databaseClass->query($queryInsert);

$amount = $rs_order['total_amount'] - $rs_order['promotion_discount_amount'] - $rs_order['voucher_discount_amount']-$rs_order['member_discount_amount'] + $rs_order['shipping_amount'];

//uat
/*$api_umid = 'UMID_838457004';
$api_id = 'b4952ae3-620d-4081-99bc-a84508feca95';
$api_secret = '6b42f3a8-250e-48f1-a147-54c224a84546';*/

//production
$api_umid = 'UMID_837848000';
$api_id = 'f9a55b6d-ba16-4aa1-b3bd-b9e91dd2c561';
$api_secret = '07183800-6ff2-461a-90a9-8248ce033447';

$microtime = floatval(substr((string)microtime(), 1, 8));
$rounded = round($microtime, 3);
$time = date("Ymd H:i:s") . substr((string)$rounded, 1, strlen($rounded));

$nets_json['ss'] = '1';
$nets_json['msg'] = array(
    'netsMid' => 'UMID_837848000',
    'tid' => '',
    'submissionMode' => 'B',
    'txnAmount' => round($amount * 100),
    'merchantTxnRef' => $merchant_ref,
    'merchantTxnDtm' => date("Ymd H:i:s.000"),
    'paymentType' => 'SALE',
    'currencyCode' => 'SGD',
    'paymentMode' => 'CC',
    'merchantTimeZone' => '+8:00',
    's2sTxnEndURL' => $site_config['full_url'] . 'payment/nets-callback',
    'b2sTxnEndURL' => $site_config['full_url'] . 'payment/nets-redirect?oid=' . $order_id,
    'clientType' => 'M',
    'supMsg' => '',
    'netsMidIndicator' => 'U',
    'ipAddress' => $_SERVER['REMOTE_ADDR'],
    'language' => 'en'
);

$txnReq = '{"ss":"1","msg":{"netsMid":"UMID_837848000","tid":"","submissionMode":"B","txnAmount":' . round($amount * 100) . ',"merchantTxnRef":"' . $merchant_ref . '","merchantTxnDtm":"' . date("Ymd H:i:s.000") . '","paymentType":"SALE","currencyCode":"SGD","paymentMode":"","merchantTimeZone":"+8:00","b2sTxnEndURL":"' . $site_config['full_url'] . 'payment/nets-redirect?oid=' . $order_id . '","s2sTxnEndURL":"' . $site_config['full_url'] . 'payment/nets-callback' . '","clientType":"W","supMsg":"","netsMidIndicator":"U","language":"en"}}';
$hmac = hash('sha256', $txnReq . $api_secret, true);
$hmac = base64_encode($hmac);
?>
<!DOCTYPE html>
<html>
<base href="<?= $site_config['full_url'] ?>"/>
<?php include('../head.php') ?>
<body class="page-bg">
<div class="container-fluid">
    <? include('../nav.php') ?>
    <? include('../nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">VISA / MASTER</h4>
                <p>Please enter your credit card details</p>
            </div>
        </div>
        <div class="col-12">
            <div id="anotherSection">
                <fieldset>
                    <div id="ajaxResponse" style="color: #6d6d6d;"></div>
                </fieldset>
            </div>
        </div>
    </div>
    <input type="hidden" id="txnReq" name="txnReq" value='<?= $txnReq ?>'/>
    <input type="hidden" id="keyId" name="keyId" value="<?= $api_id ?>"/>
    <input type="hidden" id="hmac" name="hmac" value="<?= $hmac ?>"/>
</div>

<?php include('../footer.php') ?>
<?php include('../js-script.php') ?>
</body>
</html>
<script type="text/javascript" src="https://www2.enets.sg/GW2/pluginpages/env.jsp"></script>
<script type="text/javascript" src="https://www2.enets.sg/GW2/js/apps.js"></script>
<script>
    window.onload = function () {
        var txnReq = $("#txnReq").val();
        var keyId = $("#keyId").val(); // once api key is available, assign a value
        var hmac = $("#hmac").val(); // once hmac is available, assign a value
        sendPayLoad(txnReq, hmac, keyId);
    };
</script>
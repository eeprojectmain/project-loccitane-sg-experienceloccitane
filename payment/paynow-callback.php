<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/vendor/autoload.php";
global $table;
$databaseClass = new database();
$stockClass=new stock();

$token = file_get_contents('php://input');

//$token = file_get_contents('callback.txt');

use Jose\Component\Core\AlgorithmManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Signature\Algorithm\RS256;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128GCM;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEDecrypter;
use Jose\Component\Core\JWK;
use Jose\Component\Encryption\Serializer\JWESerializerManager;
use Jose\Component\Encryption\Serializer\CompactSerializer;
use Jose\Component\Encryption\JWELoader;
use Jose\Easy\Load;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use \Firebase\JWT\JWT;

$key_private = JWKFactory::createFromKeyFile(
    __DIR__ . '/../vendor/key_production.key'
);

// The key encryption algorithm manager
$keyEncryptionAlgorithmManager = new AlgorithmManager([
    new RSAOAEP256(),
]);

// The content encryption algorithm manager
$contentEncryptionAlgorithmManager = new AlgorithmManager([
    new A128GCM(),
]);

// The compression method manager with the DEF (Deflate) method.
$compressionMethodManager = new CompressionMethodManager([
    new Deflate(),
]);

// We instantiate our JWE Decrypter.
$jweDecrypter = new JWEDecrypter(
    $keyEncryptionAlgorithmManager,
    $contentEncryptionAlgorithmManager,
    $compressionMethodManager
);

// The serializer manager. We only use the JWE Compact Serialization Mode.
$serializerManager = new JWESerializerManager([
    new CompactSerializer(),
]);

// We try to load the token.
$jwe = $serializerManager->unserialize($token);

// We decrypt the token. This method does NOT check the header.
$success = $jweDecrypter->decryptUsingKey($jwe, $key_private, 0);

if ($success == true) {
    $token = $jwe->getPayLoad();
    $token = explode(".", $token)[1];
    $result = base64_decode($token);
    $result = json_decode($result, true);

    if (preg_match("/-/", $result['txnRef'])) {
        $order_id = explode("-", $result['txnRef'])[0];
    } else {
        $order_id = $result['txnRef'];
    }
    $order_id = str_replace("LDSG20", "", $order_id);

    $resultOrder = get_query_data($table['order'], "pkid=$order_id");
    $rs_order = $resultOrder->fetchRow();

    if ($rs_order['pkid'] == "") {
        $err_text = $result['txnRef'] . "/" . $result['Ref'];
    }

    if ($result['proCode'] == '000000') {
        $postfield = array(
            'status' => '1',
            'order_id' => $order_id,
            'method' => 'PayNow',
            'trans_id' => $result['bankTxnId'],
            'amount' => ($result['amount'] / 100),
            'remark' => $result['proMsg'],
            'err' => $err_text,
            'created_date' => $time_config['now'],
        );

        $queryInsert = get_query_insert($table['payment'], $postfield);
        $databaseClass->query($queryInsert);

        $queryUpdate = get_query_update($table['order'], $order_id, array('status' => '1', 'payment_status' => '1', 'product_id' => $rs_order['product_id'], 'quantity' => $rs_order['quantity']));
        $databaseClass->query($queryUpdate);

        if ($rs_order['discount_amount'] != "" && $rs_order['cprv_id'] == "999") {
            $resultVoucher = get_query_data($table['voucher'], "mobile='" . $rs_order['mobile'] . "'");
            $rs_voucher = $resultVoucher->fetchRow();

            $queryUpdate = get_query_update($table['voucher'], $rs_voucher['pkid'], array('status' => '0', 'updated_date' => $time_config['now']));
            $databaseClass->query($queryUpdate);
        }

        $order_id = $rs_order['pkid'];
        $stockClass->minus($order_id);
        include('hook.php');
        
        if ($rs_order['shipping_method'] == 'delivery') {
            include("../ajax/lalamove-order-new.php");
        }
    } else {
        $postfield = array(
            'status' => '0',
            'order_id' => $order_id,
            'method' => 'PayNow',
            'trans_id' => $result['bankTxnId'],
            'amount' => ($result['amount'] / 100),
            'remark' => $result['proMsg'],
            'created_date' => $time_config['now'],
        );

        $queryInsert = get_query_insert($table['payment'], $postfield);
        $databaseClass->query($queryInsert);
    }
} else {
    error_log("error => " . var_dump($jwe->getPayLoad()));
}

echo json_encode(array('status' => 'SUCCESS'));
exit();

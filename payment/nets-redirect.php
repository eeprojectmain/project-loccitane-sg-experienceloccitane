<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$raw_post = file_get_contents("php://input");
parse_str(urldecode($raw_post), $raw_msg);
$raw_msg=json_decode($raw_msg['message'],true);

//file_put_contents("error.txt", json_encode($raw_msg).PHP_EOL, FILE_APPEND);

$resultOrder=get_query_data($table['order'],"pkid=".mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['oid']));
$rs_order=$resultOrder->fetchRow();

if($raw_msg['msg']['netsTxnStatus']=="0"){
    header("Location: ../payment-success?Ref=".$_GET['oid']);
    exit();
}else{
    header("Location: ../payment-fail?Ref=".$_GET['oid']);
    exit();
}
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass=new stock();

if ($_GET['vktest'] == "1") {
    $postfield['Ref'] = "EL-" . $_GET['order_id'];
    $postfield['successcode'] = "0";
} else {
    $postfield = $_POST;
}

$postfield['Ref'] = str_replace("EL-", "", $postfield['Ref']);

$row_order = get_query_data_row($table['order'], "pkid=" . $postfield['Ref']);

if ($row_order > 0) {
    if ($postfield['successcode'] == "0") {
        $data_array = array(
            'status' => '1',
            'order_id' => $postfield['Ref'],
            'method' => 'PayDollar',
            'trans_id' => $postfield['PayRef'],
            'amount' => $postfield['Amt'],
            'remark' => $postfield['remark'],
            'created_date' => $time_config['now']
        );

        $queryInsert = get_query_insert($table['payment'], $data_array);
        $databaseClass->query($queryInsert);

        $resultOrder = get_query_data($table['order'], "pkid=" . $postfield['Ref']);
        $rs_order = $resultOrder->fetchRow();

        $resultOrder = get_query_data($table['order'], "pkid=" . $postfield['Ref']);
        $rs_order = $resultOrder->fetchRow();

        $queryUpdate = get_query_update($table['order'], $postfield['Ref'], array('status' => '1', 'payment_status' => '1', 'product_id' => $rs_order['product_id'], 'quantity' => $rs_order['quantity']));
        $databaseClass->query($queryUpdate);

        if ($rs_order['discount_amount'] != "" && $rs_order['cprv_id'] == "999") {
            $resultVoucher = get_query_data($table['voucher'], "mobile='" . $rs_order['mobile'] . "'");
            $rs_voucher = $resultVoucher->fetchRow();

            $queryUpdate = get_query_update($table['voucher'], $rs_voucher['pkid'], array('status' => '0', 'updated_date' => $time_config['now']));
            $databaseClass->query($queryUpdate);
        }

        $order_id = $rs_order['pkid'];
        include('hook.php');

        if ($rs_order['shipping_method'] == 'delivery') {
            include("../ajax/lalamove-order-new.php");
        }
    } else {
        $data_array = array(
            'status' => '0',
            'order_id' => $postfield['Ref'],
            'method' => 'PayDollar',
            'trans_id' => $postfield['PayRef'],
            'amount' => $postfield['Amt'],
            'remark' => $postfield['remark'],
            'created_date' => $time_config['now']
        );

        $queryInsert = get_query_insert($table['payment'], $data_array);
        $databaseClass->query($queryInsert);
    }
}

echo 'OK';
exit();

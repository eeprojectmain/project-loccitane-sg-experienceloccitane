<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass=new stock();
$orderClass=new order();

$order_id = $_SESSION['member']['order_id'];

$orderClass->check();
$orderClass->update();
$orderClass->check_final();

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

if (($rs_order['total_amount'] - $rs_order['voucher_discount_amount']) > 0) {
    header("Location: ../checkout-payment");
    exit();
}

if($rs_order['total_amount']==""){
    header("Location: ../checkout-summary");
    exit();
}

$data_array = array(
    'status' => '1',
    'order_id' => $order_id,
    'method' => 'Free Checkout',
    'amount' => '0',
    'remark' => 'Free Checkout',
    'created_date' => $time_config['now']
);

$queryInsert = get_query_insert($table['payment'], $data_array);
$databaseClass->query($queryInsert);

$queryUpdate = get_query_update($table['order'], $order_id, array('status' => '1', 'payment_status' => '1'));
$databaseClass->query($queryUpdate);

$resultVoucher=get_query_data($table['voucher'],"mobile='".$rs_order['mobile']."'");
$rs_voucher=$resultVoucher->fetchRow();

if ($rs_order['discount_amount'] != "" && $rs_order['cprv_id']=="999") {
    $resultVoucher = get_query_data($table['voucher'], "mobile='" . $rs_order['mobile'] . "'");
    $rs_voucher = $resultVoucher->fetchRow();

    $queryUpdate = get_query_update($table['voucher'], $rs_voucher['pkid'], array('status' => '0', 'updated_date' => $time_config['now']));
    $databaseClass->query($queryUpdate);
}

//include('hook.php');
if ($rs_order['campaign_id']!="2") {
    if ($rs_order['method'] == 'delivery') {
        include("../ajax/lalamove-order-new.php");
    }
}

header("Location: ../payment-success?Ref=$order_id");
exit();
?>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass=new stock();

$raw_post = file_get_contents("php://input");
$raw_msg=json_decode($raw_post,true);
file_put_contents("error.txt", $raw_post.PHP_EOL, FILE_APPEND);

$postfield=$raw_msg['msg'];

$postfield['merchantTxnRef']=explode("_",$postfield['merchantTxnRef'])[0];

$row_order = get_query_data_row($table['order'], "pkid=" . $postfield['merchantTxnRef']);

if ($row_order > 0) {
    if ($postfield['netsTxnStatus'] == "0") {
        $data_array = array(
            'status' => '1',
            'order_id' => $postfield['merchantTxnRef'],
            'method' => 'eNets',
            'trans_id' => $postfield['netsTxnRef'],
            'amount' => number_format($postfield['netsAmountDeducted']/100,2,'.',''),
            'remark' => $postfield['netsTxnMsg'],
            'created_date' => $time_config['now']
        );

        $queryInsert = get_query_insert($table['payment'], $data_array);
        $databaseClass->query($queryInsert);

        $resultOrder = get_query_data($table['order'], "pkid=" . $postfield['merchantTxnRef']);
        $rs_order = $resultOrder->fetchRow();

        $queryUpdate = get_query_update($table['order'], $postfield['merchantTxnRef'], array('status' => '1', 'payment_status' => '1', 'product_id' => $rs_order['product_id'], 'quantity' => $rs_order['quantity']));
        $databaseClass->query($queryUpdate);

        if ($rs_order['discount_amount'] != "" && $rs_order['cprv_id'] == "999") {
            $resultVoucher = get_query_data($table['voucher'], "mobile='" . $rs_order['mobile'] . "'");
            $rs_voucher = $resultVoucher->fetchRow();

            $queryUpdate = get_query_update($table['voucher'], $rs_voucher['pkid'], array('status' => '0', 'updated_date' => $time_config['now']));
            $databaseClass->query($queryUpdate);
        }

        $order_id = $rs_order['pkid'];
        $stockClass->minus($order_id);
        include('hook.php');

        if ($rs_order['shipping_method'] == 'delivery') {
            include("../ajax/lalamove-order-new.php");
        }
    } else {
        $data_array = array(
            'status' => '0',
            'order_id' => $postfield['merchantTxnRef'],
            'method' => 'eNets',
            'trans_id' => $postfield['netsTxnRef'],
            'amount' => number_format($postfield['netsAmountDeducted']/100,2,'.',''),
            'remark' => $postfield['netsTxnMsg'],
            'created_date' => $time_config['now']
        );

        $queryInsert = get_query_insert($table['payment'], $data_array);
        $databaseClass->query($queryInsert);
    }
}

echo 'OK';
exit();
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$nav_step = '2';

if (strtotime('now') > strtotime('today 2pm')) {
    $date_array[] = date("Y-m-d", strtotime('tomorrow'));
    for ($i = 2; $i <= 7; $i++) {
        $date_array[] = date("Y-m-d", strtotime("+$i day"));
    }
} else {
    $date_array[] = date("Y-m-d");
    for ($i = 1; $i <= 6; $i++) {
        $date_array[] = date("Y-m-d", strtotime("+$i day"));
    }
}

if (strtotime("now") > strtotime('today 2pm')) {
    $date = date('Y-m-d', strtotime('tomorrow'));
} else {
    $date = date('Y-m-d');
}

if ($_POST) {
    $postfield = $_POST;

    $_SESSION['pickup_date'] = $postfield['pickup_date'];
    $_SESSION['pickup_time'] = $postfield['pickup_time'];

    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('pickup_date' => $postfield['pickup_date'], 'pickup_time' => $postfield['pickup_time'], 'shipping_amount' => '0'));
    $databaseClass->query($queryUpdate);

    header("Location: checkout-summary");
    exit();
}
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>
<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">COLLECTION DETAILS</h4>
                <p>Please select your preferred date & time</p>
            </div>
        </div>
        <div class="col-12 mt-4">
            <form action="checkout-date" method="post" class="mx-auto formValidation">
                <b class="d-block text-center">Preferred Date:</b>
                <div class="form-group mt-3">
                    <div class="date-calendar">
                        <?php
                        $i = 0;
                        foreach ($date_array as $k => $v) {
                            $i++;
                            ?>
                            <div class="date-item">
                                <span class="date-text"><?= date('D', strtotime($v)) ?></span>
                                <br>
                                <span class="date-number <?= $i == '1' ? "active" : "" ?>"
                                      data-date="<?= date('Y-m-d', strtotime($v)) ?>"><?= date('j', strtotime($v)) ?></span>
                            </div>
                        <? } ?>
                    </div>
                </div>
                <div class="form-group mt-5">
                    <b class="d-block mx-auto text-center mb-3">Preferred Time:</b>
                    <div class="div_time">
                        <i class="fa fa-2x fa-gear fa-spin"></i>
                    </div>
                </div>
                <div class="form-group mt-5 pb-5 text-center">
                    <button type="submit" name="submit_form" value="true" class="btn btn-darkblue w-80">NEXT</button>
                </div>

                <input type="hidden" name="pickup_date" value="<?= $date ?>" required/>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(document).ready(function () {
        const notEmpty = {
            validators: {
                notEmpty: {
                    message: 'The title is required'
                }
            }
        };

        $(".date-number").on('click', function (e) {
            $(".date-number.active").removeClass('active');
            $(this).addClass('active');
            $("input[name='pickup_date']").val($(this).data('date'));

            $(".div_time").load('remote-view/pickup_time?date=' + $(this).data('date'), function () {
                fv.addField('pickup_time', notEmpty);
            });
        });

        $(".div_time").load('remote-view/pickup_time?date=<?=$date?>', function () {
            fv.addField('pickup_time', notEmpty);
        });
    });

</script>
</body>
</html>
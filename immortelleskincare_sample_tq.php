<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$apiClass = new api();

// print_r($_SESSION);
// exit();
$pkid = $_SESSION['sample_immortelleskincare']['done'];

if($_GET['id']=='1111'){
    $pkid = 1;
}

if($pkid==""){
    header("Location: ".$site_config['sample_immortelleskincare']);
    exit();
}

$resultSample = get_query_data($table['sample_immortelleskincare'], "pkid=$pkid");
$rs_sample = $resultSample->fetchRow();

$postfield['first_name'] = $rs_sample['first_name'];
$postfield['last_name'] = $rs_sample['last_name'];
$postfield['mobile'] = $rs_sample['mobile'];
$postfield['email'] = $rs_sample['email'];
$postfield['sms_status'] = $rs_sample['sms_status'];
$postfield['email_status'] = $rs_sample['email_status'];
$postfield['call_status'] = $rs_sample['call_status'];
$postfield['whatsapp_status'] = $rs_sample['whatsapp_status'];
$postfield['source_id'] = '5874'; //Production ID - Always on Sampling - Immortelle Skin Care
//$postfield['source_id'] = '4737'; //UAT ID - Always on Sampling - Immortelle Skin Care

$apiClass->create_sample_profile($postfield);

unset($_SESSION['iglive']);
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4">
    <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">IMMORTELLE SKIN CARE KIT</h4>
                <p>Thank you for signing up!</p>
                <p>Please present a screenshot of this confirmation page to redeem your sample kit in stores.</p>
            </div>
        </div>
        <div class="col-12 text-center">
            <img src="assets/img/immortelleskincare.png" class="img-fluid"/>
        </div>
        <div class="col-12">
        </div>
        <div class="col-12">
            <b>First Name: </b> <?= $rs_sample['first_name'] ?><br><br>
            <b>Last Name: </b> <?= $rs_sample['last_name'] ?><br><br>
            <b>Mobile: </b> <?= $rs_sample['mobile'] ?><br><br>
            <b>Redeem sample by: </b> <?= date('d M Y', strtotime($rs_sample['created_date'] . ' +2 week')) ?>
            <!-- <b>Expires 2 weeks after registering for samples</b> -->
        </div>
        <!-- <div class="col-12 p-3">
            <div class="border border-dark p-2 text-center" style="line-height: 1.5em;">
                Exclude deal for you!<br>
                <b>5% OFF</b><br>
                <b>selected Almond range products</b><br>
                Exclusively on <a href="https://sg.loccitane.com" target="_blank">sg.loccitane.com<a> or in stores.
                <br>
                <div class="p-2 mx-auto my-2" style="background-color:#181B54; border-radius:30px; font-size:1.2em; max-width:280px; color:white;">
                    <b>5OFFALMOND</b>
                </div>
                5% OFF Expires by: <?= date('d M Y', strtotime($rs_sample['created_date'] . ' +2 month')) ?><br>
                T&Cs apply.
            </div>
        </div> -->
        <div class="col-12 mt-4">
            <div class="border border-dark p-2">
                <p><b><u>Terms & Conditions for Sampling</u></b></p>

				<ul>
                    <li>Limited to 1 sample kit per customer, while stocks last.</li>
                    <li>Valid for redemption at all L'OCCITANE boutiques except L'OCCITANE web store, Lazada, Shopee, KrisShop and Duty-Free Stores.</li>
                    <li>Non-transferrable and non-exchangeable for cash. </li>
                    <li>Collection on behalf of others is not allowed. OTP verification to registered phone number is required during collection of sample kit in stores.</li>
                    <li>Only applicable for customers who have not redeemed the same sample kit for the past 12 months.</li>
                    <li>L'OCCITANE Singapore reserves the final right to alter and/or withdraw the items, terms & conditions without prior notice.</li>
                </ul>


                <br><br>

                <!-- <p><b><u>Terms & Conditions for 5% OFF</u></b></p>
                <ol>
                    <li>Voucher is valid till indicated date, redeemable at <a href="https://sg.loccitane.com" target="_blank">sg.loccitane.com<a> or in stores except Takashimaya outlet, Lazada, Shopee, Krisshop and Duty-free stores. </li><br>
                    <li>Only applicable to all full-sized format Almond Shower Oil, Almond Milk Concentrate and Almond Supple Skin Oil, excluding sets.</li><br>
                    <li>Limited to one redemption per customer/profile.</li><br>
                    <li>Not in conjunction with other promocodes, discounts, rewards, bank vouchers, gift vouchers, gift certificates, birthday benefits and sets.</li><br>
                    <li>L'OCCITANE reserves the final right to alter and/or withdraw the items, terms and conditions without prior notice.</li>
                    
                </ol> -->
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
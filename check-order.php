<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

if ($_GET['m'] != "") {
    $row_check = get_query_data_row($table['order'], "mobile='" . protect('decrypt', $_GET['m']) . "'");
    if ($row_check > 0) {
        $_SESSION['member']['mobile'] = protect('decrypt', $_GET['m']);
    } else {
        header("Location: index");
        exit();
    }
}

if ($_POST) {
    $postfield = $_POST;

    $postfield['mobile'] = "+65" . $postfield['mobile'];

    $resultOrder = get_query_data($table['order'], "mobile='" . $postfield['mobile'] . "'");
    $row_order = $resultOrder->numRows();

    if ($row_order > 0) {
        $_SESSION['member']['mobile'] = $postfield['mobile'];
    } else {
        $swal['title'] = "Opps...";
        $swal['msg'] = "We couldn't find any order under your mobile number";
        $swal['type'] = "error";
    }
}

$member_mobile = $_SESSION['member']['mobile'];
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <?
    if ($member_mobile == "") {
        ?>
        <div class="row mt-4">
            <div class="col-12 text-center p-0">
                <div class="title">
                    <h4 class="w-100">ORDER TRACKING</h4>
                    <p>Please fill in your details</p>
                </div>
            </div>
            <div class="col-12 mt-5">
                <form action="check-order" method="post" class="mx-auto formValidation">
                    <div class="form-group">
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text">+65</span>
                            </div>
                            <input class="form-control" name="mobile" placeholder="MOBILE NUMBER" type="tel" required
                                   maxlength="15" value="<?= str_replace("+65", "", $_SESSION['member']['mobile']) ?>">
                        </div>
                    </div>
                    <div class="form-group text-center mt-4">
                        <button type="submit" class="btn btn-blue w-80" name="submit_form" value="true">CONTINUE
                        </button>
                    </div>
                </form>
            </div>
        </div>
    <? } else { ?>
        <div class="row mt-4">
            <div class="col-12 text-center p-0">
                <div class="title">
                    <h4 class="w-100">ORDER TRACKING</h4>
                    <p><?=$_SESSION['member']['mobile']?></p>
                </div>
            </div>
            <div class="col-12 mt-4">
                <div class="table-responsive">
                <table class="table table-bordered" style="min-width: 100%">
                    <thead>
                    <tr>
                        <td>Order ID</td>
                        <td>Status</td>
                        <td>Order Date</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $resultOrder = get_query_data($table['order'], "payment_status=1 and mobile='$member_mobile' and deleted=0 order by pkid desc");
                    while ($rs_order = $resultOrder->fetchRow()) {
                        ?>
                        <tr>
                            <td>#LDSG20<?= $rs_order['pkid'] ?>
                            </td>
                            <td><?= $order_status_array[$rs_order['status']] ?>
                            </td>
                            <td><?= $rs_order['created_date'] ?>
                            </td>
                            <td>
                                <button type="button" class="btn btn-sm btn-blue btn-track"
                                        data-tracking_no="<?= $rs_order['tracking_code'] ?>"
                                        data-order_id="<?= $rs_order['pkid'] ?>">TRACK
                                </button>
                            </td>
                        </tr>
                        <?php
                    } ?>
                    </tbody>
                </table>
                </div>
            </div>
            <div class="col-12 text-center mt-5">
                <a href="shop" class="btn btn-blue w-80">CONTINUE SHOPPING</a>
            </div>
        </div>
    <? } ?>
</div>

<div class="modal fade" role="dialog" tabindex="-1" id="modal-track">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content dark-bg">
            <div class="modal-header b-0">
                <button type="button" class="close p-absolute r-5 w-100 text-right" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" id="modal-track-content">

            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script src="https://www.tracking.my/track-button.js"></script>
<script>
    $(".btn-track").on('click', function (e) {
        var tracking = $(this).data('tracking_no');
        $("#modal-track-content").load('remote-view/delivery-status?oid=' + $(this).data('order_id'),
            function () {
                if (tracking) {
                    TrackButton.embed({
                        selector: "#embedTrack",
                        tracking_no: tracking,
                        theme: 'dark',
                    });
                }
            });
        $("#modal-track").modal('show');
    });
</script>
</body>
</html>
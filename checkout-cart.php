<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . '/admin/config.php';
global $table;
$databaseClass = new database();
$promotionClass = new promotion();
$stockClass = new stock();

if ($_SESSION['outlet_id'] == "0" || $_SESSION['outlet_id'] == "") {
    header("Location: shop-select?return=checkout-cart");
    exit();
}

//inventory check
$ofs_product = $stockClass->check_summary();

$ofs_product = array_filter($ofs_product);
if (count($ofs_product) > 0) {
    foreach ($ofs_product as $v) {
        $resultProduct = get_query_data($table['product'], "pkid=$v");
        $rs_product = $resultProduct->fetchRow();

        $ofs_title[] = $rs_product['title'];
    }
    $swal['icon'] = 'warning';
    $swal['title'] = 'Sorry...';
    $swal['msg'] = implode(",", $ofs_title) . ' had removed from your cart due to low stock';
}
//end inventory

$promotion_array = $promotionClass->discount();
foreach ($promotion_array as $k => $v) {
    $promotion_total_amount += $v['discount'];
    $promotion_discount_array[] = $v['discount'];
}
// $promotionClass->freegift($promotion_total_amount);
// foreach ($promotionClass->special_label() as $k => $v) {
//     $promo_product_order[] = $v['product_id'];
// }
?>
<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body>
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="d-flex justify-content-center">
        <div class="col-12 col-lg-6">
            <div class="col-12 text-center p-0">
                <h4 class="w-100 pt-4 font-title">MY BAG</h4>
            </div>
            <div class="col-12 mt-4">
                <table class="table table-cart" style="min-width: 100%">
                    <tbody>
                    <?php
                    $row_cart = count($_SESSION['cart']);

                    $total = 0;
                    
                   
                    if ($row_cart > 0) {
                        foreach ($_SESSION['cart'] as $k => $v) {
                            $badge = "";
                            $resultProduct = get_query_data($table['product'], 'pkid=' . $v['product_id']);
                            $rs_product = $resultProduct->fetchRow();

                            if ($v['free'] == "true") {
                                $rs_product['price'] = "0";
                                $badge = "<span class='badge badge-danger'>Free</span>";                            
    
                                // $rs_product['img_url'] = "error.png";
                            }

                            $total += $v['quantity'] * $rs_product['price'];
                            ?>
                            <tr class="p-3"> 
                                <td width="30%" class="text-center pl-0 pr-0"> 
                                    <div class="product-card m-0">                                     
                                    
                                    
                                        <?php if ($rs_product['img_url'] == "") { ?> 
                                        <img  src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2" onerror="this.src='assets/img/blank.png'" class="img-fluid product-img">
                                        <?php } else { ?> <img src="assets/product/<?= $rs_product['img_url'] ?>" class="img-fluid product-img"> 
                                        <?php  } ?>                                    
                                                
                                    </div>
                                </td>
                                <td class="position-relative">
                                    <?= $badge ?>
                                    <?
                                    if (in_array($rs_product['pkid'], $promo_product_order)) {
                                        foreach ($promotionClass->special_label() as $k2 => $v2) {
                                            if ($v2['product_id'] == $rs_product['pkid']) {
                                                echo '<span class="badge badge-danger">' . $v2['special_label'] . '</span><br>';
                                            }
                                        }
                                    }
                                    ?>
                                    <b><?= strtoupper($rs_product['title']); ?></b>
                                    <?if($v['free']!="true"){?>
                                    <button type="button" class="btn btn-delete float-right"
                                            onclick="delete_cart(<?= $v['product_id'] ?>)"><i class="fal fa-trash-alt"></i>
                                    </button>
                                    <?}?>
                                    <br>
                                    <p>
                                        S$ <span
                                                class="<?= $v['product_id'] ?>_price"><?= number_format($v['quantity'] * $rs_product['price']); ?></span>
                                    <?if($v['free']!="true"){?>
                                    <div class="cart-qty float-right">
                                        <button type="button" class="btn btn-sm btn-circle"
                                                onclick="minus_qty(<?= $v['product_id'] ?>)">
                                            <i class="fal fa-minus"></i>
                                        </button>
                                        <span class="<?= $v['product_id'] ?>_qty"><?= $v['quantity'] ?></span>
                                        <button type="button" class="btn btn-sm btn-circle"
                                                onclick="add_qty(<?= $v['product_id'] ?>)"><i
                                                    class="fal fa-plus"></i>
                                        </button>
                                    </div>
                                    <?}?>
                                    </p>
                                </td>
                            </tr>
                            <?php
                        }
                    } else { ?>
                        <tr>
                            <td colspan="4" class="text-center">YOUR CART IS EMPTY</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-center"><a href="shop" class="btn btn-blue w-80">BACK TO
                                    SHOP</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <div class="col-12 col-lg-6">
            <?php if ($row_cart > 0) { ?>
                <div class="row">
                    <div class="col-12">
                        <div class="float-left">SUBTOTAL</div>
                        <div class="float-right">S$ <span class="cart_total"><?= number_format($total, 2); ?></span>
                        </div>
                        <br>
                        <?
                        foreach ($promotion_array as $k => $v) {
                            $resultPromo = get_query_data($table['promotion'], "pkid=" . $v['promo_id']);
                            $rs_promo = $resultPromo->fetchRow();
                            $promotion_id_array[] = $v['promo_id'];
                            if ($v['discount'] > 0) {
                                ?>
                                <div class="float-left"><?= $v['label'] != "" ? $v['label'] : $rs_promo['title'] ?>
                                </div>
                                <div class="float-right price-subtotal">
                                    - S$ <?= number_format($v['discount'], 2) ?>
                                </div>
                                <br>
                                <?php
                            }
                        }
                        ?>
                        <div class="float-left">9% GST INCLUDED IN TOTAL</div>
                        <div class="float-right">S$ <span
                                    class="cart_gst"><?= number_format((9/109) * $total, 2); ?></span>
                        </div>
                        <br>
                        <div class="float-left mt-4"><b>TOTAL</b></div>
                        <div class="float-right mt-4"><b>S$ <span
                                        class="cart_total"><?= number_format($total - $promotion_total_amount, 2); ?>
                            </b></span>
                        </div>
                    </div>
                    <div class="col-12 mt-5 pb-5 text-center">
                        <a href="checkout-details" class="btn btn-darkblue w-80">CHECK OUT</a>
                        <a href="shop" class="btn btn-blue w-80 mt-3 mt-md-0">CONTINUE SHOPPING</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<?php include 'js-script.php'; ?>
<script>
    function delete_cart(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "It's such a great deal!",
            icon: "warning",
            showCloseButton: true,
            showCancelButton: true,
        }).then((willDelete) => {
            if (willDelete.value) {
                $.ajax({
                    method: "POST",
                    url: "ajax/delete-cart",
                    data: {
                        id: id
                    }
                })
                    .done(function (data) {
                        window.location.reload();
                    });
            }
        });
    }

    function add_qty(id) {
        $.ajax({
            method: "POST",
            url: "ajax/add-to-cart",
            dataType: "json",
            data: {id: id, method: 'add'}
        })
            .done(function (data) {
                if (data.function == 'low_stock') {
                    low_stock(data.title);
                } else if (data.result == 'error') {
                    Swal.fire({
                        title: data.title,
                        text: data.msg,
                        showCloseButton: true,
                        showCancelButton: true,
                        customClass: {
                            confirmButton: 'btn btn-black float-left',
                            cancelButton: 'btn btn-black btn-cart float-right',
                        }
                    });
                } else {
                    display_price(id, data);
                }
            });
    }

    function minus_qty(id) {
        if ($("." + id + "_qty").html() == '1') {
            delete_cart(id);
            return false;
        }

        $.ajax({
            method: "POST",
            url: "ajax/add-to-cart",
            dataType: "json",
            data: {id: id, method: 'minus'}
        })
            .done(function (data) {
                if (data.function == 'low_stock') {
                    low_stock(data.title);
                } else if (data.result == 'error') {
                    Swal.fire({
                        title: data.title,
                        text: data.msg,
                        showCloseButton: true,
                        showCancelButton: true,
                        customClass: {
                            confirmButton: 'btn btn-black float-left',
                            cancelButton: 'btn btn-black btn-cart float-right',
                        }
                    });
                } else {
                    display_price(id, data);
                }
            });
    }

    function display_price(id, data) {
        $("." + id + "_qty").html(data.qty);
        $("." + id + "_price").html(data.product_total);
        $(".cart_total").html(data.cart_total);
        $(".cart_gst").html(data.cart_gst);

        update_cart_price();
    }
</script>
</body>
</html>
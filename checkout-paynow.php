<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$orderClass=new order();

$nav_step='2';

$order_id = $_SESSION['member']['order_id'];

$orderClass->check();
//$orderClass->update();
$orderClass->check_final();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
            <h4 class="w-100">PAYNOW</h4>
            </div>
        </div>
        <div class="col-12 mt-3 mb-4">
            <img id="img_qr" class="img-fluid w-50 d-block mx-auto"/>
        </div>
        <div class="col-12 text-center mt-3">
            <div class="row">
                <div class="col-4">
                    <p><b>STEP 1</b></p>
                    <i class="fal fa-download fa-3x"></i>
                    <br>
                    <p class="mt-3"><small>Press and hold on the QR Code and select “Add to Photos” or “Save Image”</small></p>
                </div>
                <div class="col-4">
                    <p><b>STEP 2</b></p>
                    <i class="fal fa-mobile fa-3x"></i>
                    <br>
                    <p class="mt-3"><small>Open your mobile banking app and upload the QR Code from your gallery or photo album</small></p>
                </div>
                <div class="col-4">
                    <p><b>STEP 3</b></p>
                    <i class="fal fa-check-circle fa-3x"></i>
                    <br>
                    <p class="mt-3"><small>Check the transaction amount and confirm the payment</small></p>
                </div>
            </div>
        </div>
        <div class="col-12 text-center mt-5">
            <a download="Paynow-<?= uniqid() ?>" id="a_qr" class="btn btn-blue w-80">DOWNLOAD</a>
        </div>
        <div class="col-12 mt-5">
            <p class="w-100 text-center"><i class="fal fa-lock"></i> Safe and secure payment</p>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "ajax/get-qrcode",
            dataType: 'json',
        })
            .done(function (data) {
                if (data.result == 'success') {
                    $("#img_qr").attr('src', data.qrcode);
                    $("#a_qr").attr('href', data.qrcode);
                } else {
                    Swal.fire({
                        title: "Opps...",
                        text: "Some error occur, please try again later or WhatsApp us for help!",
                        icon: "error",
                    });
                }
            });

        setInterval(function () {
            $.ajax({
                method: "POST",
                url: "ajax/paynow-check",
                dataType: 'json',
            })
                .done(function (data) {
                    if (data.payment_status == "1") {
                        Swal.fire({
                            title: "Yay!",
                            text: "Payment Received!",
                            icon: "success",
                        });

                        setTimeout(function () {
                            window.location.href = "payment-success?Ref=<?=$_SESSION['member']['order_id']?>";
                        },1500);
                    }
                });
        }, 1000);
    });
</script>
</body>
</html>
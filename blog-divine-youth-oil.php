<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$shop_url = 'https://experienceloccitane.com/shop?search=DIVINE+YOUTH+OIL';
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>
<style>
    html, body {
        background-image: url("assets/img/blog-2-bg-btm.png");
        background-position: center bottom;
        background-size: contain;
        background-repeat: no-repeat;
        background-color:#FDFDFD;
    }
</style>
<body>
<div class="container-fluid container-blog">
    <div class="row p-absolute z-index-10">
        <div class="col-12 pl-2 mt-2">
            <a href="shop?cid=1" class="btn btn-black"><i class="fa fa-angle-double-left"></i> BACK</a>
        </div>
    </div>
    <div class="row p-relative">
        <div class="col-12 p-0">
            <img src="assets/img/blog-2-bg-top.png" class="img-fluid"/>
        </div>
        <div class="col-sm-12 col-md-4 blog-description-box">
            <span class="blog-title">IMMORTELLE DIVINE YOUTH OIL</span>
            <p class="mt-3">
                Achieve healthy, resilient skin with the most-loved Divine Youth Oil. Starring the breakthrough
                Immortelle Super extract, a naturally derived alternative to retinol, this lightweight dry oil will defy
                visible signs of aging to give you an everlasting youthful complexion.
            </p>
            <div class="mt-4">
                <a href="<?= $shop_url ?>" class="btn btn-blue">SHOP NOW</a>
            </div>
        </div>
    </div>
    <div class="row mt-3 text-center">
        <div class="col-md-2 offset-md-3 col-sm-12">
            <img src="assets/img/blog-2-icon1.png" class="img-fluid blog-icon">
            <p class="mt-2">Harper’s Bazaar Beauty Awards 2019</p>
        </div>
        <div class="col-md-2 col-sm-12">
            <img src="assets/img/blog-2-icon2.png" class="img-fluid blog-icon">
            <p class="mt-2">FEMALE Beauty Awards 2019</p>
        </div>
        <div class="col-md-2 col-sm-12">
            <img src="assets/img/blog-2-icon3.png" class="img-fluid blog-icon">
            <p class="mt-2">EH! Beauty Awards 2019</p>
        </div>
    </div>
    <div class="row mt-4 p-relative m-2 m-md-5 blog-testi-box">
        <div class="col-12 p-4 d-none d-md-block">
            <img src="assets/img/blog-2-testi.png" class="img-fluid"/>
        </div>
        <div class="col-12 p-4 d-sm-block d-md-none">
            <img src="assets/img/blog-2-testi1.png" class="img-fluid"/>
        </div>
        <div class="col-12 p-4 d-sm-block d-md-none">
            <img src="assets/img/blog-2-testi2.png" class="img-fluid"/>
        </div>
        <div class="col-12 p-4 d-sm-block d-md-none">
            <img src="assets/img/blog-2-testi3.png" class="img-fluid"/>
        </div>
        <div class="col-12 p-4 d-sm-block d-md-none">
            <img src="assets/img/blog-2-testi4.png" class="img-fluid"/>
        </div>
        <img src="assets/img/blog-1-flower-left.png" class="blog-flower-left d-none d-md-block"/>
        <img src="assets/img/blog-1-flower-right.png" class="blog-flower-right d-none d-md-block"/>
    </div>
    <div class="row mt-5 mb-5">
        <div class="col-12 text-center">
            <div class="embed-responsive embed-responsive-4by3">
                <video class="video-js vjs-theme-sea embed-responsive-item"
                       controls
                       playsinline
                       preload="auto"
                       data-setup="{}">
                    <source src="assets/video/2 Products ver9_Final.mp4" type="video/mp4"/>
                </video>
            </div>
        </div>
        <div class="col-12 text-center mt-3">
            <div class="embed-responsive embed-responsive-4by3">
                <video class="video-js vjs-theme-sea embed-responsive-item"
                       controls
                       playsinline
                       preload="auto"
                       data-setup="{}">
                    <source src="assets/video/Power Duo sample 2 steps ver6_Final.mp4" type="video/mp4"/>
                </video>
            </div>
        </div>
        <div class="mt-4 col-12 text-center">
            <a href="<?= $shop_url ?>" class="btn btn-blue">SHOP NOW</a>
        </div>
        <div class="col-12 mt-5">
            <h2 class="w-100 text-center">See also</h2>
            <div class="row">
                <div class="col-6">
                    <a href="blog-immortelle-reset">
                        <div class="card">
                            <img class="card-img-top"
                                 src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=27OR050I18&v=2">
                            <div class="card-body">
                                <p class="card-title text-center"><b>IMMORTELLE RESET OIL-IN-SERUM</b></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6">
                    <a href="blog-divine-creme">
                        <div class="card">
                            <img class="card-img-top"
                                 src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=27DC030I20&v=2">
                            <div class="card-body">
                                <p class="card-title text-center"><b>IMMORTELLE DIVINE CREME</b></p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>

</script>
</body>

</html>
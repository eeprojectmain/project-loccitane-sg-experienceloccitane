<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id=$_GET['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=" . $order_id);
$rs_order = $resultOrder->fetchRow();

$resultLalamove = get_query_data($table['lalamove'], "order_id=$order_id order by pkid desc");
$rs_lalamove = $resultLalamove->fetchRow();

//testing
//$api_key = 'pk_test_1b92cb5b777c05af01d321e183a4621d';
//$api_secret = 'sk_test_CM9pbc0cw/QIPnvqQeEFyfTAuRH4GTnW7L/LXazSKDlPfCLE2cUCYEZG1//s3DTR';

//production
$api_key = 'b8c5867efbc14a9596b46ed29d392c5b';
$api_secret = 'MC0CAQACBQDgE8B5AgMBAAECBAtcfL0CAwDuDwIDAPD3AgMAr7sCAgNxAgMA';


$time = time() * 1000;
$body = json_encode(array());

$raw_signature = "$time\r\nDELETE\r\n/v3/orders/" . $rs_lalamove['lalamove_id'] . "\r\n\r\n$body";
$signature = hash_hmac('sha256', $raw_signature, $api_secret);

$ch = curl_init();
//curl_setopt($ch, CURLOPT_URL, "https://rest.sandbox.lalamove.com/v3/orders/" . $rs_lalamove['lalamove_id']);
curl_setopt($ch, CURLOPT_URL, "https://rest.lalamove.com/v3/orders/".$rs_lalamove['lalamove_id']);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
//curl_setopt($ch, CURLOPT_DELETE, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Authorization: hmac ' . $api_key . ':' . $time . ':' . $signature,
    'Market: SG',
    //'X-Request-ID: ' . uniqid()
    'Content-Type: application/json'
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
$server_output = json_decode($server_output, true);
curl_close($ch);

if (!$server_output['errors']) {
    $queryUpdate = get_query_update($table['lalamove'], $rs_lalamove['pkid'], array('status' => '0'));
    $databaseClass->query($queryUpdate);
}

echo json_encode($server_output);
exit();
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$otp = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['otp']);

$order_id = $_SESSION['member']['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$mobile = $rs_order['mobile'];

$resultOtp = get_query_data($table['otp'], "mobile='$mobile' order by pkid desc limit 1");
$rs_otp = $resultOtp->fetchRow();

if ($otp == $rs_otp['otp']) {
    $_SESSION['member']['otp'] = $rs_otp['otp'];
    echo json_encode(array('result' => 'success', 'msg' => 'Verification success'));
} else {
    echo json_encode(array('result' => 'error', 'msg' => 'Incorrect OTP entered'));
}
exit();
?>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/lalamoveapi.php";
global $table;
$databaseClass = new database();
$promotionClass=new promotion();

$postfield = $_POST;

$order_id = $_SESSION['member']['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$resultOutlet = get_query_data($table['outlet'], "pkid=".$rs_order['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

$outlet_contact=$rs_outlet['whatsapp_no'];

if (strtotime('now') > strtotime('today 2pm')) {
    $pickup_date = date("Y-m-d", strtotime('tomorrow'));
} else {
    $pickup_date = date("Y-m-d");
}

//$pickup_date = date("Y-m-d");

if($postfield['pickup_time']!=""){
    $pickup_time=$postfield['pickup_time'];
}else{
    $pickup_time='3.30pm';
}


//testing
// $api_key = 'e2e1dfd397ae4c0ca9ec99866b2af7b3';
// $api_secret = 'MCwCAQACBQDXNrRrAgMBAAECBFLeVGECAwD/7wIDANdFAgIoaQIDAJMxAgJG';

//production
$api_key = 'b8c5867efbc14a9596b46ed29d392c5b';
$api_secret = 'MC0CAQACBQDgE8B5AgMBAAECBAtcfL0CAwDuDwIDAPD3AgMAr7sCAgNxAgMA';


$time = time() * 1000;

$body = array(
    "scheduleAt" => gmdate('Y-m-d\TH:i:s.000\Z', strtotime($pickup_date." ".$pickup_time.' -1 hour')), // ISOString with the format YYYY-MM-ddTHH:mm:ss.000Z at UTC time
    "serviceType" => "MOTORCYCLE",                              // string to pick the available service type
    "specialRequests" => array(),
    "requesterContact" => array(
        "name" => "LOCCITANE ".$rs_outlet['title'],
        "phone" => $outlet_contact                                 // Phone number format must follow the format of your country
    ),
    "stops" => array(
        array(
            "location" => array("lat" => $rs_outlet['lat'], "lng" => $rs_outlet['lng']),
            "addresses" => array(
                "en_SG" => array(
                    "displayString" => "LOCCITANE ".$rs_outlet['title'],
                    "market" => "SG_SIN"                                   // Country code must follow the country you are at
                )
            )
        ),
        array(
            "location" => array("lat" => $postfield['lat'], "lng" => $postfield['lng']),
            "addresses" => array(
                "en_SG" => array(
                    "displayString" => $postfield['address'],
                    "market" => "SG_SIN"                                   // Country code must follow the country you are at
                )
            )
        )
    ),
    "deliveries" => array(
        array(
            "toStop" => 1,
            "toContact" => array(
                "name" => $rs_order['name'],
                "phone" => $rs_order['mobile']                              // Phone number format must follow the format of your country
            ),
            "remarks" => "ORDER #LDSG20".$rs_order['pkid']
        )
    )
);

$body=json_encode($body);

$raw_signature = "$time\r\nPOST\r\n/v2/quotations\r\n\r\n$body";
$signature = hash_hmac('sha256', $raw_signature, $api_secret);

$ch = curl_init();
// curl_setopt($ch, CURLOPT_URL, "https://sandbox-rest.lalamove.com/v2/quotations");
curl_setopt($ch, CURLOPT_URL, "https://rest.lalamove.com/v2/quotations");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Authorization: hmac ' . $api_key.':'.$time.':'.$signature,
    'X-LLM-Country: SG_SIN',
    'X-Request-ID: ' . uniqid()
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
$server_output=json_decode($server_output, true);
curl_close($ch);

/* if ($rs_campaign['free_lalamove']=="1") {
    $server_output['totalFee']="0";
} */

$total_fee=$server_output['totalFee'];

foreach($promotionClass->discount() as $k=>$v){
    if($v['label']=="Free Shipping"){
        $total_fee='0';
    }
}

echo json_encode(array('totalFeeNum'=>$server_output['totalFee'],'totalFee'=>$total_fee));
exit();
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

use Twilio\Rest\Client;

$order_id = $_SESSION['member']['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$mobile = $rs_order['mobile'];

$resultOtp=get_query_data($table['otp'],"mobile='$mobile' order by pkid desc limit 1");
$rs_otp=$resultOtp->fetchRow();

$to_time = strtotime("now");
$from_time = strtotime($rs_otp['created_date']);

if(round(abs($to_time - $from_time) / 60,2)>=1) {
    $otp = rand('100', '999');

    $content = "L'OCCITANE: OTP code: $otp. NEVER share this code with others.";

    $ch = curl_init();
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json',
    );
    curl_setopt($ch, CURLOPT_URL, "http://www.etracker.cc/bulksms/mesapi.aspx?user=davino&pass=Wowsome%40820%23%23%23%23%21&type=0&to=$mobile&from=Loccitane&text=" . urlencode($content) . "&servid=MES01&title=EnDemande_SG_OTP");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $server_respond = curl_exec($ch);
    curl_close($ch);

    $postfield = array(
        'status' => '0',
        'order_id' => $order_id,
        'mobile' => $mobile,
        'otp' => $otp,
        'created_date' => $time_config['now'],
    );

    $queryInsert = get_query_insert($table['otp'], $postfield);
    $databaseClass->query($queryInsert);

    $_SESSION['member']['otp_request']="true";

    echo json_encode(array('result' => 'success', 'msg' => 'OTP has sent to '.$mobile));
    exit();
}else{
    echo json_encode(array('result' => 'error', 'msg' => 'You\'ve just request an OTP, please try again later.'));
    exit();
}
?>
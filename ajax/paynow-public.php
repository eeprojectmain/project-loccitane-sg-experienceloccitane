<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/vendor/autoload.php";
global $table;
$databaseClass = new database();

$http_array=array(
    'amount'=>mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['amount']),
    'order_id'=>mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['order_id']),
    'count'=>mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['count'])
);

$payload = file_get_contents($site_config['full_url'].'ajax/paynow-private?'.http_build_query($http_array));
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Algorithm\RS256;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128GCM;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEBuilder;
use Jose\Easy\Build;
use Jose\Component\Encryption\Serializer\JWESerializerManager;
use Jose\Component\Encryption\JWEDecrypter;

$api_url = 'https://cmb-api.hsbc.com.hk/glcm-mobilecoll-mcsg-ea-merchantservices-prod-proxy';
$api_method = '/v1/payment/qrCode';

$merchant_id = 'S179182S8970000';
$client_id = '2513d84f3dfa4e0cb0834c557c5d5183';
$client_secret = '69B722cf5aBd4F869b151ED3A2349F18';

$key_cert = JWKFactory::createFromCertificateFile(
    __DIR__ . '/../vendor/production.crt'
);

// The key encryption algorithm manager
$keyEncryptionAlgorithmManager = new AlgorithmManager([
    new RSAOAEP256(),
]);

// The content encryption algorithm manager
$contentEncryptionAlgorithmManager = new AlgorithmManager([
    new A128GCM(),
]);

// The compression method manager with the DEF (Deflate) method.
$compressionMethodManager = new CompressionMethodManager([
    new Deflate(),
]);

// We instantiate our JWE Builder.
$jweBuilder = new JWEBuilder(
    $keyEncryptionAlgorithmManager,
    $contentEncryptionAlgorithmManager,
    $compressionMethodManager
);

$jwe = $jweBuilder
    ->create()// We want to create a new JWE
    ->withPayload((string)$payload)// We set the payload
    ->withSharedProtectedHeader([
        'alg' => 'RSA-OAEP-256',        // Key Encryption Algorithm
        'enc' => 'A128GCM', // Content Encryption Algorithm
        'kid' => '0004',
    ])
    ->addRecipient($key_cert)// We add a recipient (a shared key or public key).
    ->build();

use Jose\Component\Encryption\Serializer\CompactSerializer;

$serializer = new CompactSerializer(); // The serializer

$token = $serializer->serialize($jwe, 0);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $api_url . $api_method);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $token);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Content-Type: application/json',
    'x-HSBC-client-id: ' . $client_id,
    'x-HSBC-client-secret: ' . $client_secret,
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
curl_close($ch);

echo $server_output;
?>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/vendor/autoload.php";
global $table;
$databaseClass = new database();

$order_id=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['order_id']);
$amount=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['amount']);
$count=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['count']);

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\Algorithm\RS256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128GCM;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEBuilder;
use Jose\Component\Signature\Serializer\CompactSerializer;

$key_private = JWKFactory::createFromKeyFile(
    __DIR__ . '/../vendor/key_production.key'
);

$merchant_id = 'S179182S8970000';

$payload = array(
    'merId' => $merchant_id,
    'txnRef' => "LDSG20".$order_id.'-'.$count,
    'txnChannel' => '02',
    'txnTime' => date('Y-m-d\TH:i:s') . 'Z',
    'qrExpiry' => date('YmdHis', strtotime('+1 hour')),
    'payMethod' => 'PAYNOW',
    'amtEditInd' => 'N',
    'qrOption' => '02',
    'country' => 'SG',
    'currency' => 'SGD',
    'amount' => (int)$amount,
//    'notifyUrl' => $site_config['full_url'].'payment/paynow-callback',
    'notifyUrl' => 'https://sg.experienceloccitane.com/payment/paynow-callback',
    'goodsDes' => 'Order #LDSG20'.$rs_order['pkid']
);

$payload = json_encode($payload);

// The algorithm manager with the HS256 algorithm.
$algorithmManager = new AlgorithmManager([
    new RS256(),
]);

// We instantiate our JWS Builder.
$jwsBuilder = new JWSBuilder($algorithmManager);

$jws = $jwsBuilder
    ->create()// We want to create a new JWS
    ->withPayload($payload)// We set the payload
    ->addSignature($key_private, ['alg' => 'RS256', 'kid' => '0001'])// We add a signature with a simple protected header
    ->build();
$serializer = new CompactSerializer(); // The serializer
$token = $serializer->serialize($jws, 0);

echo $token;
?>
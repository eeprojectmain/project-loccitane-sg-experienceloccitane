<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$promotionClass = new promotion();
$stockClass = new stock();

$product_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['id']);
$method = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['method']);

$resultProduct = get_query_data($table['product'], "pkid=" . $product_id);
$rs_productMain = $resultProduct->fetchRow();

//pwp
$pwp_promo_array = $promotionClass->pwp();

if ($pwp_promo_array != false) {
    foreach ($pwp_promo_array as $k => $v) {
        $min_spend = $v['min_spend'];
        foreach ($v['product_id'] as $k2 => $v2) {
            $product_pwp_id_array[] = $v2;
        }
        foreach ($v['product_quantity'] as $k3 => $v3) {
            $product_pwp_qty_array[] = $v3;
        }
    }
}

$total = 0;
foreach ($_SESSION['cart'] as $k => $v) {
    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    if (!in_array($v['product_id'], $product_pwp_id_array)) {
        $total += $rs_product['price'] * $v['quantity'];
    }
}

$pwp_qty = 0;
foreach ($_SESSION['cart'] as $k => $v) {
    if (in_array($v['product_id'], $product_pwp_id_array)) {
        $pwp_qty = $pwp_qty + $v['quantity'];
    }
}

if (in_array($product_id, $product_pwp_id_array) && $method!='minus') {
    if ($total < $min_spend) {
        echo json_encode(array('result' => 'error', 'title' => 'Opps...', 'msg' => 'Sorry, your cart amount did not reach S$' . $min_spend));
        exit();
    }

    $key = array_search($product_id, $product_pwp_id_array);

    if ($pwp_qty == $product_pwp_qty_array[$key]) {
        echo json_encode(array('result' => 'error', 'title' => 'Opps...', 'msg' => 'Sorry, purchase with purchase items are limited to max. ' . $product_pwp_qty_array[$key] .' qty per order.'));
        exit();
    }
}
//end pwp

if($method!='minus'){
if ($stockClass->check_cart($product_id) === false) {
    $resultProduct = get_query_data($table['product'], "pkid=" . $product_id);
    $rs_product = $resultProduct->fetchRow();

    echo json_encode(array('function' => 'low_stock', 'title' => $rs_product['title']));
    exit();
}
}

if ($method == "") {
    foreach ($_SESSION['cart'] as $k => $v) {
        if ($v['product_id'] == $product_id) {
            if ($_SESSION['cart'][$k]['quantity'] == "5") {
                echo json_encode(array('result' => 'error', 'title' => 'Opps...', 'msg' => 'Sorry, each product is limited to max. 5 qty per order.'));
                exit();
            } else {
                $_SESSION['cart'][$k]['quantity'] += 1;
                $added = true;
            }
        }
    }
} elseif ($method == "add") {
    foreach ($_SESSION['cart'] as $k => $v) {
        if ($v['product_id'] == $product_id) {
            if ($_SESSION['cart'][$k]['quantity'] == "5") {
                echo json_encode(array('result' => 'error', 'title' => 'Opps...', 'msg' => 'Sorry, each product is limited to max. 5 qty per order.'));
                exit();
            } else {
                $_SESSION['cart'][$k]['quantity'] += 1;
                $added = true;
                $new_qty = $_SESSION['cart'][$k]['quantity'];
                $product_total = $new_qty * $rs_productMain['price'];
            }
        }
    }
} elseif ($method == "minus") {
    foreach ($_SESSION['cart'] as $k => $v) {
        if ($v['product_id'] == $product_id) {
            $_SESSION['cart'][$k]['quantity'] = $_SESSION['cart'][$k]['quantity'] - 1;
            $added = true;
            $new_qty = $_SESSION['cart'][$k]['quantity'];
            $product_total = $new_qty * $rs_productMain['price'];
        }
    }
}

if (!$added) {
    $_SESSION['cart'][] = array(
        'product_id' => $product_id,
        'quantity' => 1
    );
}

// $promotionClass->freegift();

$total = 0;
foreach ($_SESSION['cart'] as $k => $v) {
    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    $total += $rs_product['price'] * $v['quantity'];
}

if ($_SESSION['member']['order_id'] != "") {
    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('total_amount' => $total));
    $databaseClass->query($queryUpdate);
}

if ($method == "") {
    echo json_encode(array('result' => 'success', 'title' => 'ITEM ADDED TO CART', 'msg' => 'TOTAL S$: ' . number_format($total, 2)));
} else {
    echo json_encode(array('result' => 'success', 'qty' => $new_qty, 'product_total' => $product_total, 'cart_total' => number_format($total, 2), 'cart_gst' => number_format(($total / 100) * 7, 2)));
}
exit();

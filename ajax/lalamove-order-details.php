<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['order_id']);

$resultOrder = get_query_data($table['order'], "pkid=" . $order_id);
$rs_order = $resultOrder->fetchRow();

$resultLala = get_query_data($table['lalamove'], "order_id=$order_id and lalamove_id!='' order by pkid desc");
$rs_lala = $resultLala->fetchRow();

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $rs_order['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();


//testing
// $api_key = 'e2e1dfd397ae4c0ca9ec99866b2af7b3';
// $api_secret = 'MCwCAQACBQDXNrRrAgMBAAECBFLeVGECAwD/7wIDANdFAgIoaQIDAJMxAgJG';

//production
$api_key = 'b8c5867efbc14a9596b46ed29d392c5b';
$api_secret = 'MC0CAQACBQDgE8B5AgMBAAECBAtcfL0CAwDuDwIDAPD3AgMAr7sCAgNxAgMA';


$time = time() * 1000;

$raw_signature = "$time\r\nGET\r\n/v2/orders/" . $rs_lala['lalamove_id'] . "\r\n\r\n";
$signature = hash_hmac('sha256', $raw_signature, $api_secret);

$ch = curl_init();
// curl_setopt($ch, CURLOPT_URL, "https://sandbox-rest.lalamove.com/v2/orders/" . $rs_lala['lalamove_id']);
curl_setopt($ch, CURLOPT_URL, "https://rest.lalamove.com/v2/orders/" . $rs_lala['lalamove_id']);
curl_setopt($ch, CURLOPT_HTTPGET, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Authorization: hmac ' . $api_key . ':' . $time . ':' . $signature,
    'X-LLM-Country: SG',
    'X-Request-ID: ' . uniqid()
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
$server_output=json_decode($server_output, true);
curl_close($ch);

$queryUpdate=get_query_update($table['lalamove'], $rs_lala['pkid'], array('driver_id'=>$server_output['driverId']));
$databaseClass->query($queryUpdate);

echo json_encode($server_output);
exit();

<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/lalamoveapi.php";
global $table;
$databaseClass = new database();
$promotionClass=new promotion();

$postfield = $_POST;

$order_id = $_SESSION['member']['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$resultOutlet = get_query_data($table['outlet'], "pkid=".$rs_order['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

$outlet_contact=$rs_outlet['whatsapp_no'];

if (strtotime('now') > strtotime('today 2pm')) {
    $pickup_date = date("Y-m-d", strtotime('tomorrow'));
} else {
    $pickup_date = date("Y-m-d");
}

if($postfield['pickup_time']!=""){
    $pickup_time=$postfield['pickup_time'];
}else{
    $pickup_time='3.30pm';
}


//testing
//$api_key = 'pk_test_1b92cb5b777c05af01d321e183a4621d';
//$api_secret = 'sk_test_CM9pbc0cw/QIPnvqQeEFyfTAuRH4GTnW7L/LXazSKDlPfCLE2cUCYEZG1//s3DTR';

//production
$api_key = 'b8c5867efbc14a9596b46ed29d392c5b';
$api_secret = 'MC0CAQACBQDgE8B5AgMBAAECBAtcfL0CAwDuDwIDAPD3AgMAr7sCAgNxAgMA';


$time = time() * 1000;

$body = array(
    "data" => array(
        "scheduleAt" => gmdate('Y-m-d\TH:i:s\Z', strtotime($pickup_date." ".$pickup_time.' -1 hour')), // ISOString with the format YYYY-MM-ddTHH:mm:ss.000Z at UTC time
        "serviceType" => "MOTORCYCLE",                              // string to pick the available service type
        "specialRequests" => array(),                               // array of strings available for the service type
        //"fleetOption"=> "FLEET_ONLY",
        "language" => "en_SG",  
        /*"requesterContact" => array(
            "name" => "LOCCITANE " . $rs_outlet['title'],
            "phone" => "+6" . $outlet_contact                                 // Phone number format must follow the format of your country
        ),*/
        "stops" => array(
            array(
                "coordinates" => array("lat" => $rs_outlet['lat'], "lng" => $rs_outlet['lng']),
                "address" => "LOCCITANE " . $rs_outlet['title']
            ),
            array(
                "coordinates" => array("lat" => $postfield['lat'], "lng" => $postfield['lng']),
                "address" => $postfield['address']
            ),
        ),
        "isRouteOptimized" => false, // optional only for quotations
        /*
        "deliveries" => array(
            array(
                "toStop" => 1,
                "toContact" => array(
                    "name" => $rs_order['first_name'] . ' ' . $rs_order['last_name'],
                    "phone" => $mobile                              // Phone number format must follow the format of your country
                ),
                "remarks" => "ORDER #" . $rs_order['pkid']
            )
        )*/
    )
);

$body = json_encode($body);

$raw_signature = "$time\r\nPOST\r\n/v3/quotations\r\n\r\n$body";
$signature = hash_hmac('sha256', $raw_signature, $api_secret);

$ch = curl_init();
//curl_setopt($ch, CURLOPT_URL, "https://rest.sandbox.lalamove.com/v3/quotations");
curl_setopt($ch, CURLOPT_URL, "https://rest.lalamove.com/v3/quotations");
curl_setopt($ch, CURLOPT_POST, 1);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $body);  //Post Fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Authorization: hmac ' . $api_key . ':' . $time . ':' . $signature,
    'Market: SG',
    //'X-Request-ID: ' . uniqid()
    'Content-Type: application/json' 
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
curl_close($ch);
$server_output = json_decode($server_output, true);

$_SESSION['member']['quotationId'] = $server_output['data']['quotationId'];
$_SESSION['member']['senderStopId'] = $server_output['data']['stops'][0]['stopId'];
$_SESSION['member']['recipientStopId'] = $server_output['data']['stops'][1]['stopId'];

$server_output['totalFeeNum'] = number_format((float)$server_output['data']['priceBreakdown']['total'], 2, '.', '');
$server_output['senderStopId'] = $server_output['data']['stops'][0]['stopId'];
$server_output['recipientStopId'] = $server_output['data']['stops'][1]['stopId'];


if ($rs_campaign['free_lalamove'] == "1" || $rs_order['total_amount']>100) {
    $server_output['totalFee'] = "0";
}else{
     $server_output['totalFee'] = number_format((float)$server_output['data']['priceBreakdown']['total'], 2, '.', '');
    // $server_output['totalFee'] = "0";
}

if($server_output['errors']){
    $server_output = json_encode($server_output['errors'][0]);
}else{
    $server_output = json_encode($server_output);
}


echo $server_output;
//echo $body;
exit();
?>
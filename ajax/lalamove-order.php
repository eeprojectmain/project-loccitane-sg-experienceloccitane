<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

if ($_GET['vktest'] == "1") {
    $order_id = $_GET['order_id'];
} elseif ($postfield['Ref'] != "") {
    $order_id = $postfield['Ref'];
} elseif ($rs_order['pkid'] != "") {
    $order_id = $rs_order['pkid'];
}

$resultOrder = get_query_data($table['order'], "pkid=" . $order_id);
$rs_order = $resultOrder->fetchRow();

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $rs_order['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

if ($rs_order['receive_mobile'] != "") {
    $rs_order['mobile'] = $rs_order['receive_mobile'];
}

if ($rs_order['receive_name'] != "") {
    $rs_order['name'] = $rs_order['receive_name'];
}

if (preg_match("/65/", $rs_order['mobile'])) {
    $mobile = $rs_order['mobile'];
} else {
    $mobile = "+65" . $rs_order['mobile'];
}
$mobile = str_replace("-", "", $mobile);

$outlet_contact = str_replace("-", "", $rs_outlet['whatsapp_no']);
$outlet_contact = str_replace(" ", "", $outlet_contact);


//testing
// $api_key = 'e2e1dfd397ae4c0ca9ec99866b2af7b3';
// $api_secret = 'MCwCAQACBQDXNrRrAgMBAAECBFLeVGECAwD/7wIDANdFAgIoaQIDAJMxAgJG';

//production
$api_key = 'b8c5867efbc14a9596b46ed29d392c5b';
$api_secret = 'MC0CAQACBQDgE8B5AgMBAAECBAtcfL0CAwDuDwIDAPD3AgMAr7sCAgNxAgMA';


$time = time() * 1000;

$body = array(
    "scheduleAt" => gmdate('Y-m-d\TH:i:s.000\Z', strtotime($rs_order['pickup_date'] . " " . $rs_order['pickup_time'].' -1 hour')), // ISOString with the format YYYY-MM-ddTHH:mm:ss.000Z at UTC time
    "serviceType" => "MOTORCYCLE",                              // string to pick the available service type
    "specialRequests" => array(),
    "requesterContact" => array(
        "name" => "LOCCITANE " . $rs_outlet['title'],
        "phone" => $outlet_contact                                 // Phone number format must follow the format of your country
    ),
    "stops" => array(
        array(
            "location" => array("lat" => $rs_outlet['lat'], "lng" => $rs_outlet['lng']),
            "addresses" => array(
                "en_SG" => array(
                    "displayString" => "LOCCITANE " . $rs_outlet['title'],
                    "market" => "SG_SIN"                                   // Country code must follow the country you are at
                )
            )
        ),
        array(
            "location" => array("lat" => $rs_order['lat'], "lng" => $rs_order['lng']),
            "addresses" => array(
                "en_SG" => array(
                    "displayString" => $rs_order['address'],
                    "market" => "SG_SIN"                                   // Country code must follow the country you are at
                )
            )
        )
    ),
    "deliveries" => array(
        array(
            "toStop" => 1,
            "toContact" => array(
                "name" => $rs_order['name'],
                "phone" => $mobile                              // Phone number format must follow the format of your country
            ),
            "remarks" => "UNIT: " . $rs_order['unit_no'] . ", ORDER #LDSG20" . $rs_order['pkid']
        )
    ),
    "quotedTotalFee" => array(
        'amount' => $rs_order['shipping_amount'],
        'currency' => 'SGD'
    ),
    "sms" => true
);

$body = json_encode($body);

$raw_signature = "$time\r\nPOST\r\n/v2/orders\r\n\r\n$body";
$signature = hash_hmac('sha256', $raw_signature, $api_secret);

$ch = curl_init();
// curl_setopt($ch, CURLOPT_URL, "https://sandbox-rest.lalamove.com/v2/orders");
curl_setopt($ch, CURLOPT_URL, "https://rest.lalamove.com/v2/orders");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Authorization: hmac ' . $api_key . ':' . $time . ':' . $signature,
    'X-LLM-Country: SG_SIN',
    'X-Request-ID: ' . uniqid()
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
$server_output = json_decode($server_output, true);
curl_close($ch);

if ($server_output['message'] != "") {
    $mail = new PHPMailer;
    $mail->IsSMTP();
    $mail->Host = "mail.experienceloccitane.com";
    $mail->SMTPAuth = true;
    $mail->Port = 587;
    $mail->Username = "no-reply@experienceloccitane.com";
    $mail->Password = "wowsome@888####!";
    $mail->SetFrom('no-reply@experienceloccitane.com', 'L\'OCCITANE');
    $mail->addAddress('vincentlee@wowsome.com.my');
    $mail->isHTML(true);
    $mail->Subject = 'ERROR OCCUR! ORDER #' . $order_id . ' - L\'OCCITANE Singapore';

    $mail->Body = "An Lalamove error has occur!<br><br>Order ID: $order_id<br>Error Msg: " . $server_output['message'] . "<br><br>$body<br><br>Request Again https://sg.experienceloccitane.com/ajax/lalamove-order.php?vktest=1&order_id=$order_id";
    $mail->send();

    echo json_encode($server_output);
    exit();
} else {
    $queryInsert = get_query_insert($table['lalamove'], array('status' => '1', 'order_id' => $order_id, 'lalamove_id' => $server_output['orderRef'], 'message' => $server_output['message'], 'created_date' => $time_config['now']));
    $databaseClass->query($queryInsert);

    echo json_encode($server_output);
    exit();
}

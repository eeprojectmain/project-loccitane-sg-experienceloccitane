<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/vendor/autoload.php";
global $table;
$databaseClass = new database();

$_SESSION['member']['count']++;

$order_id = $_SESSION['member']['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

if ($rs_order['cprv_id'] != "0") {
    if ($rs_order['total_amount'] < $voucher_minspend_array[$rs_order['cprv_id']]) {
        $rs_order['voucher_discount_amount']='0';
    }
}

$amount = $rs_order['total_amount'] - $rs_order['promotion_discount_amount'] - $rs_order['voucher_discount_amount']-$rs_order['member_discount_amount'] + $rs_order['shipping_amount'];
$amount = str_replace(".", "", ($amount * 100));

$http_array = array(
    'amount' => $amount,
    'order_id' => $order_id,
    'count' => $_SESSION['member']['count']
);

$token = file_get_contents($site_config['full_url'] . 'ajax/paynow-public?' . http_build_query($http_array));
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Signature\Algorithm\RS256;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128GCM;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEDecrypter;
use Jose\Component\Core\JWK;
use Jose\Component\Encryption\Serializer\JWESerializerManager;
use Jose\Component\Encryption\Serializer\CompactSerializer;
use Jose\Component\Encryption\JWELoader;
use Jose\Easy\Load;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use \Firebase\JWT\JWT;

$key_private = JWKFactory::createFromKeyFile(
    __DIR__ . '/../vendor/key_production.key'
);

// The key encryption algorithm manager
$keyEncryptionAlgorithmManager = new AlgorithmManager([
    new RSAOAEP256(),
]);

// The content encryption algorithm manager
$contentEncryptionAlgorithmManager = new AlgorithmManager([
    new A128GCM(),
]);

// The compression method manager with the DEF (Deflate) method.
$compressionMethodManager = new CompressionMethodManager([
    new Deflate(),
]);

// We instantiate our JWE Decrypter.
$jweDecrypter = new JWEDecrypter(
    $keyEncryptionAlgorithmManager,
    $contentEncryptionAlgorithmManager,
    $compressionMethodManager
);

// The serializer manager. We only use the JWE Compact Serialization Mode.
$serializerManager = new JWESerializerManager([
    new CompactSerializer(),
]);

// We try to load the token.
$jwe = $serializerManager->unserialize($token);

// We decrypt the token. This method does NOT check the header.
$success = $jweDecrypter->decryptUsingKey($jwe, $key_private, 0);

if ($success == true) {
    $token = $jwe->getPayLoad();
    $token = explode(".", $token)[1];
    $result = base64_decode($token);
    $result = json_decode($result, true);
//    print_r($result);
    echo json_encode(array('result' => 'success', 'qrcode' => 'data:image/png;base64,' . $result['response']['qrCode']));
} else {
    echo json_encode(array('result' => 'error', 'qrcode' => 'null'));
}
?>
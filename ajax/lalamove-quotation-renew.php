<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/include/lalamoveapi.php";
global $table;
$databaseClass = new database();

//$postfield = $_POST;

$order_id = $_POST['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $rs_order['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

$outlet_id = $rs_outlet['pkid'];

if (preg_match("/65/", $rs_order['mobile'])) {
    $mobile = $rs_order['mobile'];
} else {
    $mobile = "+65" . $rs_order['mobile'];
}
$mobile = str_replace("-", "", $mobile);

$outlet_contact = str_replace("-", "", $rs_outlet['whatsapp_no']);
$outlet_contact = str_replace(" ", "", $outlet_contact);


$pickup_date = $rs_order["pickup_date"];

$pickup_time = $rs_order["pickup_time"];


//testing
//$api_key = 'pk_test_1b92cb5b777c05af01d321e183a4621d';
//$api_secret = 'sk_test_CM9pbc0cw/QIPnvqQeEFyfTAuRH4GTnW7L/LXazSKDlPfCLE2cUCYEZG1//s3DTR';

//production
$api_key = 'b8c5867efbc14a9596b46ed29d392c5b';
$api_secret = 'MC0CAQACBQDgE8B5AgMBAAECBAtcfL0CAwDuDwIDAPD3AgMAr7sCAgNxAgMA';

$time = time() * 1000;

$body = array(
    "data" => array(
        "scheduleAt" => gmdate('Y-m-d\TH:i:s\Z', strtotime($pickup_date." ".$pickup_time.' -1 hour')), // ISOString with the format YYYY-MM-ddTHH:mm:ss.000Z at UTC time
        "serviceType" => "MOTORCYCLE",                              // string to pick the available service type
        "specialRequests" => array(),                               // array of strings available for the service type
        //"fleetOption"=> "FLEET_ONLY",
        "language" => "en_SG",  
        /*"requesterContact" => array(
            "name" => "LOCCITANE " . $rs_outlet['title'],
            "phone" => "+6" . $outlet_contact                                 // Phone number format must follow the format of your country
        ),*/
        "stops" => array(
            array(
                "coordinates" => array("lat" => $rs_outlet['lat'], "lng" => $rs_outlet['lng']),
                "address" => "LOCCITANE " . $rs_outlet['title']
            ),
            array(
                "coordinates" => array("lat" => $rs_order['lat'], "lng" => $rs_order['lng']),
                "address" => $rs_order['address']
            ),
        ),
        "isRouteOptimized" => false, // optional only for quotations
        /*
        "deliveries" => array(
            array(
                "toStop" => 1,
                "toContact" => array(
                    "name" => $rs_order['first_name'] . ' ' . $rs_order['last_name'],
                    "phone" => $mobile                              // Phone number format must follow the format of your country
                ),
                "remarks" => "ORDER #" . $rs_order['pkid']
            )
        )*/
    )
);

$body = json_encode($body);

$raw_signature = "$time\r\nPOST\r\n/v3/quotations\r\n\r\n$body";
$signature = hash_hmac('sha256', $raw_signature, $api_secret);

$ch = curl_init();
//curl_setopt($ch, CURLOPT_URL, "https://rest.sandbox.lalamove.com/v3/quotations");
curl_setopt($ch, CURLOPT_URL, "https://rest.lalamove.com/v3/quotations");
curl_setopt($ch, CURLOPT_POST, 1);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $body);  //Post Fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Authorization: hmac ' . $api_key . ':' . $time . ':' . $signature,
    'Market: SG',
    //'X-Request-ID: ' . uniqid()
    'Content-Type: application/json' 
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
curl_close($ch);
$server_output = json_decode($server_output, true);

/*
$_SESSION['member']['quotationId'] = $server_output['data']['quotationId'];
$_SESSION['member']['senderStopId'] = $server_output['data']['stops'][0]['stopId'];
$_SESSION['member']['recipientStopId'] = $server_output['data']['stops'][1]['stopId'];

$server_output['totalFeeNum'] = $server_output['data']['priceBreakdown']['total'];
$server_output['senderStopId'] = $server_output['data']['stops'][0]['stopId'];
$server_output['recipientStopId'] = $server_output['data']['stops'][1]['stopId'];
*/

//$server_output = json_encode($server_output);


//echo $server_output;
//echo $body;
//exit();


$quoteID = $server_output['data']['quotationId'];
$senderStopID = $server_output['data']['stops'][0]['stopId'];
$receiverStopID = $server_output['data']['stops'][1]['stopId'];


/* Create new lalamove order */

$time = time() * 1000;

$body = array(
    "data" => array(
        "quotationId" => $quoteID,
        "sender" => array(
            "stopId" => $senderStopID,
            "name" => "LOCCITANE " . $rs_outlet['title'],
            "phone" => $outlet_contact
        ),
        "recipients" => array(
            array(
                "stopId" => $receiverStopID,
                "name" => $rs_order['name'],
                "phone" => $mobile,
                "remarks" => "UNIT: " . $rs_order['unit_no'] . ", ORDER #LDSG20" . $rs_order['pkid']
            )
            ),
        "isPODEnabled" => true, // optional
        "isRecipientSMSEnabled" => true, // optional
        //"partner": "Lalamove Partner 1", // optional 
    )
    
    /*"data" => array(
        "quotationId" => $quoteID,
        "sender" => array(
            "stopId" => $senderStopID,
            "name" => "LOCCITANE " . $rs_outlet['title'],
            "phone" => "+6" . $outlet_contact
        ),
        "recipients" => array(
            array(
                "stopId" => $receiverStopID,
                "name" => $rs_order['first_name'] . ' ' . $rs_order['last_name'],
                "phone" => $mobile,
                "remarks" => "Unit Number: " . $rs_order['unit_no'] . ", Order #" . $rs_order['pkid'] // optional
            )
            ),
        "isPODEnabled" => true, // optional
        "isRecipientSMSEnabled" => true, // optional
        //"partner": "Lalamove Partner 1", // optional 
    )*/
);

$body = json_encode($body);

$raw_signature = "$time\r\nPOST\r\n/v3/orders\r\n\r\n$body";
$signature = hash_hmac('sha256', $raw_signature, $api_secret);

$ch = curl_init();
//curl_setopt($ch, CURLOPT_URL, "https://rest.sandbox.lalamove.com/v3/orders");
curl_setopt($ch, CURLOPT_URL, "https://rest.lalamove.com/v3/orders");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Authorization: hmac ' . $api_key . ':' . $time . ':' . $signature,
    'Market: SG',
    //'X-Request-ID: ' . uniqid()
    'Content-Type: application/json' 
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
$server_output = json_decode($server_output, true);
curl_close($ch);

if ($server_output['errors']) {
    $queryInsert = get_query_insert($table['lalamove'], array('status' => '0', 'order_id' => $order_id, 'lalamove_id' => $server_output['data']['orderId'], 'driver_id' => $server_output['data']['driverId'], 'quotation_id' => $quoteID, 'sender_stop_id' => $senderStopID, 'receiver_stop_id' => $receiverStopID, 'message' => $server_output['errors'][0]['message'], 'created_date' => $time_config['now']));
    $databaseClass->query($queryInsert);
    
    $mail = new PHPMailer;
    $mail->IsSMTP();
    $mail->Host = "mail.experienceloccitane.com";
    $mail->SMTPAuth = true;
    $mail->Port = 587;
    $mail->Username = "no-reply@experienceloccitane.com";
    $mail->Password = "wowsome@888####!";
    $mail->SetFrom('no-reply@experienceloccitane.com', 'L\'OCCITANE');
    $mail->addAddress('vincentlee@wowsome.com.my');
    $mail->isHTML(true);
    $mail->Subject = 'ERROR OCCUR! ORDER #' . $order_id . ' - L\'OCCITANE Singapore';

    $mail->Body = "An Lalamove error has occur!<br><br>Order ID: $order_id<br>Error ID: " . $server_output["errors"][0]['id'] . "<br>Error Msg: " . $server_output["errors"][0]['message'] . "<br>Error Details: " . $server_output["errors"]['detail'] . "<br><br>$body<br><br>Request Again https://sg.experienceloccitane.com/ajax/lalamove-order-new.php?vktest=1&order_id=$order_id";
    $mail->send();

    echo json_encode($server_output['errors']);
    exit();
} else {
    $queryInsert = get_query_insert($table['lalamove'], array('status' => '1', 'order_id' => $order_id, 'lalamove_id' => $server_output['data']['orderId'], 'driver_id' => $server_output['data']['driverId'], 'quotation_id' => $quoteID, 'sender_stop_id' => $senderStopID, 'receiver_stop_id' => $receiverStopID, 'message' => $server_output['errors']['message'], 'created_date' => $time_config['now']));
    $databaseClass->query($queryInsert);

    $_SESSION['member']['lalamove_order'] = json_encode($server_output);

    echo json_encode($server_output['data']);
    exit();
}
?>
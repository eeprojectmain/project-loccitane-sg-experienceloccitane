<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();

$otp = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['otp_code']);
$mobile = $_SESSION['iglive']['mobile'];

$resultOtp = get_query_data($table['otp'], "mobile='$mobile' order by pkid desc limit 1");
$rs_otp = $resultOtp->fetchRow();

if ($otp == $rs_otp['otp']) {
    $_SESSION['iglive']['otp'] = $rs_otp['otp'];
    echo json_encode(array(
        'valid' => true,
    ));
} else {
    echo json_encode(array(
        'valid' => false,
    ));
}
exit();
?>
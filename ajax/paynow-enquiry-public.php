<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/vendor/autoload.php";
global $table;
$databaseClass = new database();

$http_array=array(
    'order_id'=>mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['order_id']),
    'count'=>mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['count'])
);

$payload = file_get_contents($site_config['full_url'].'ajax/paynow-enquiry-private?'.http_build_query($http_array));

use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Algorithm\RS256;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128GCM;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEBuilder;
use Jose\Easy\Build;
use Jose\Component\Encryption\Serializer\JWESerializerManager;
use Jose\Component\Encryption\JWEDecrypter;
use Jose\Component\Encryption\Serializer\CompactSerializer;

$api_url = 'https://devclustercmb.api.p2g.netd2.hsbc.com.hk/glcm-mobilecoll-mcsg-ea-merchantservices-sct-proxy';
$api_method = '/v1/payment/enquiry';

$merchant_id = 'S237483S1090000';
$client_id = 'de32731aa356457b993fdd650bdb8666';
$client_secret = '5A06f16872bA40928045CE43da0EEE53';

$key_cert = JWKFactory::createFromCertificateFile(
    __DIR__ . '/../vendor/hsbc_4.crt'
);

// The key encryption algorithm manager
$keyEncryptionAlgorithmManager = new AlgorithmManager([
    new RSAOAEP256(),
]);

// The content encryption algorithm manager
$contentEncryptionAlgorithmManager = new AlgorithmManager([
    new A128GCM(),
]);

// The compression method manager with the DEF (Deflate) method.
$compressionMethodManager = new CompressionMethodManager([
    new Deflate(),
]);

// We instantiate our JWE Builder.
$jweBuilder = new JWEBuilder(
    $keyEncryptionAlgorithmManager,
    $contentEncryptionAlgorithmManager,
    $compressionMethodManager
);

$jwe = $jweBuilder
    ->create()// We want to create a new JWE
    ->withPayload((string)$payload)// We set the payload
    ->withSharedProtectedHeader([
        'alg' => 'RSA-OAEP-256',        // Key Encryption Algorithm
        'enc' => 'A128GCM', // Content Encryption Algorithm
        'kid' => '0004',
    ])
    ->addRecipient($key_cert)// We add a recipient (a shared key or public key).
    ->build();


$serializer = new CompactSerializer(); // The serializer

$token = $serializer->serialize($jwe, 0);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $api_url . $api_method);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $token);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Content-Type: application/json',
    'x-HSBC-client-id: ' . $client_id,
    'x-HSBC-client-secret: ' . $client_secret,
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
curl_close($ch);

echo $server_output;
?>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['id']);

$resultOrder=get_query_data($table['order'], "pkid=$order_id");
$rs_order=$resultOrder->fetchRow();

$resultSms=get_query_data($table['sms'], "order_id=$order_id");
$rs_sms=$resultSms->fetchRow();

$to_time = strtotime("now");
$from_time = strtotime($rs_sms['created_date']);

if ($rs_sms['pkid']=="" || round(abs($to_time - $from_time) / 60, 2)>=1) {

    $mobile=ltrim($rs_order['receive_mobile'],"65");
    if (preg_match("/^\+/", $mobile)) {
        $mobile=str_replace("+65", "", $mobile);
    }
    if (preg_match("/^\+/", $mobile)) {
        $mobile=str_replace("+", "", $mobile);
    }
    $mobile=str_replace(" ", "", $mobile);
    $mobile="+65".$mobile;

    if ($rs_order['type']=="gift" && $rs_order['shipping_method']=="delivery") {
        $sms_content = "Surprise! I've bought you a gift on L'OCCITANE EnDemande! The delivery is scheduled on " . date('M d', strtotime($rs_order['pickup_date'])) . " at " . $rs_order['pickup_time'] . " and will be sent to you at " . ($rs_order['unit_no'] != "" ? $rs_order['unit_no'] . ", " : "") . $rs_order['address'] . ". Click this link to find out more " . $site_config['full_url'] . "gift-for-you?i=" . protect('encrypt', $rs_order['pkid']);
    } elseif ($rs_order['type']=="gift" && $rs_order['shipping_method']=="pickup") {
        $resultOutlet=get_query_data($table['outlet'], "pkid=".$rs_order['outlet_id']);
        $rs_outlet=$resultOutlet->fetchRow();

        $sms_content = "Surprise! I've bought you a gift on L'OCCITANE EnDemande! You can pick-up your gift anytime after " . date('M d', strtotime($rs_order['pickup_date'])) . ", " . explode(" - ",$rs_order['pickup_time'])[0] . " at L'OCCITANE " . $rs_outlet['title'] . ". Click this link to find out more " . $site_config['full_url'] . "gift-for-you?i=" . protect('encrypt', $rs_order['pkid']);
    }


    $postfield['type']='gift';
    $postfield['order_id']=$order_id;
    $postfield['to_mobile']=$mobile;
    $postfield['created_date']=$time_config['now'];

    $queryInsert=get_query_insert($table['sms'], $postfield);
    $databaseClass->query($queryInsert);

    $ch = curl_init();
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json',
    );
    curl_setopt($ch, CURLOPT_URL, "http://www.etracker.cc/bulksms/mesapi.aspx?user=davino&pass=Wowsome%40820%23%23%23%23%21&type=0&to=$mobile&from=EnDemande&text=" . urlencode($sms_content) . "&servid=MES01&title=EnDemande_SG_Gift");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $server_respond = curl_exec($ch);
    curl_close($ch);

    echo json_encode(array('result'=>'success','message'=>'Successfully sent to '.$mobile));
    exit();
} else {
    echo json_encode(array('result'=>'error','message'=>'You\'ve just sent an sms to your friend, please try again later.'));
    exit();
}

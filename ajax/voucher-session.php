<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$voucher_name = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['name']);
$voucher_value = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['value']);
$voucher_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['id']);
$voucher_type = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['type']);

$_SESSION['member']['voucher_name'] = $voucher_name;
$_SESSION['member']['voucher_value'] = $voucher_value;
$_SESSION['member']['voucher_id'] = $voucher_id;
$_SESSION['member']['voucher_type'] = $voucher_type;

echo json_encode(array('result'=>'success'));
exit();
?>
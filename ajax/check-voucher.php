<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$voucher_id =mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['vid']);

$curl = curl_init();

curl_setopt_array($curl, array(
//    CURLOPT_URL => "https://loccitanesgwebservice.crmxs.com/?xs_app=loccitane_api.getCustomerSummary",
    CURLOPT_URL => "https://loccitanesgwebservice.crmxs.com/?xs_app=endemande.getCustomerSummary",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\"hpno\":\"" . str_replace("+65", "", $_SESSION['member']['mobile']) . "\"}",
    CURLOPT_HTTPHEADER => array(
//        "client_id: fy5r8dd5nqkxvuadbux3wrmmfwv3j32y",
        "client_id: 5vus5fnhdeeghff5de8c2nrq46fhy8nh",
//        "client_secret: 4b5aba936ufj3zcu2nbmutbquepyueee",
        "client_secret: sum388my7amm8jp7k3ru5pyb4hp8g87g",
        "Content-Type: text/plain",
    ),
));

$server_response = curl_exec($curl);
curl_close($curl);
$voucher_array = json_decode($server_response, true);

$error = false;
foreach ($voucher_array['vouchers'] as $k => $v) {
    if ($voucher_id == $v['id']) {
        $error = true;
        break;
    }
}

if ($error == true) {
    $return_result = array('result' => 'error');
} else {
    $return_result = array('result' => 'success');
}

echo json_encode($return_result);
exit();

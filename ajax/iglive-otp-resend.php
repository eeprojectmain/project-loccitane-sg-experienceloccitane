<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$mobile = $_SESSION['iglive']['mobile'];
$type = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['type']);

$resultOtp = get_query_data($table['otp'], "mobile='$mobile' order by pkid desc limit 1");
$rs_otp = $resultOtp->fetchRow();

$to_time = strtotime("now");
$from_time = strtotime($rs_otp['created_date']);

if (round(abs($to_time - $from_time) / 60, 2) >= 1) {
    $otp = rand('100', '999');

    $content = "L'OCCITANE: OTP code: $otp. NEVER share this code with others.";

    $ch = curl_init();
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json',
    );
    curl_setopt($ch, CURLOPT_URL, "http://www.etracker.cc/bulksms/mesapi.aspx?user=davino&pass=Wowsome%40820%23%23%23%23%21&type=0&to=$mobile&from=EnDemande&text=" . urlencode($content) . "&servid=MES01&title=EnDemande_SG_Sample" . strtoupper($type));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $server_respond = curl_exec($ch);
    curl_close($ch);

    $postfield = array(
        'status' => '0',
        'type' => 'sample_' . strtoupper($type),
        'mobile' => $mobile,
        'otp' => $otp,
        'created_date' => $time_config['now'],
    );

    $queryInsert = get_query_insert($table['otp'], $postfield);
    $databaseClass->query($queryInsert);

    echo json_encode(array('result' => 'success', 'title' => 'OTP verification', 'message' => 'Your new OTP has been sent to you.'));
    exit();
} else {
    echo json_encode(array('result' => 'error', 'title' => 'Oh no!', 'message' => 'You have just requested for an OTP. <br>Please try again later.'));
    exit();
}
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$promotionClass = new promotion();

$member_id = $_SESSION['member']['id'];
$product_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['id']);

$pwp_array = $promotionClass->pwp();

if ($pwp_array != false) {
    foreach ($pwp_array as $k => $v) {
        $min_spend = $v['min_spend'];
        foreach ($v['product_id'] as $k2 => $v2) {
            $product_pwp_array[] = $v2;
        }
        foreach ($v['product_quantity'] as $k3 => $v3) {
            $product_pwp_qty_array[] = $v3;
        }
    }
}

foreach ($_SESSION['cart'] as $k => $v) {
    if ($v['product_id'] == $product_id) {
        unset($_SESSION['cart'][$k]);
    }
}

//pwp
foreach ($_SESSION['cart'] as $k => $v) {
    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    if (!in_array($v['product_id'], $product_pwp_array)) {
        $total += $v['quantity'] * $rs_product['price'];
    }
}

if ($total <= $min_spend) {
    foreach ($_SESSION['cart'] as $k => $v) {
        if (in_array($v['product_id'], $product_pwp_array)) {
            unset($_SESSION['cart'][$k]);
        }
    }
}
//end pwp

foreach ($_SESSION['cart'] as $k => $v) {
    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    if ($v['free'] == "true") {
        $rs_product['price'] = "0";
    }

    $total += $rs_product['price'];
}

$promotionClass->freegift();

if ($_SESSION['member']['order_id'] != "") {
    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('total_amount' => $total));
    $databaseClass->query($queryUpdate);
}

echo 'success';

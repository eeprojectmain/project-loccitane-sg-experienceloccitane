<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id = $_SESSION['member']['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=" . $_SESSION['member']['order_id']);
$rs_order = $resultOrder->fetchRow();

$amount = $rs_order['total_amount'] - $rs_order['discount_amount'] + $rs_order['shipping_amount'];

$postfield = array(
    'status' => '1',
    'order_id' => $order_id,
    'method' => 'PayNow',
    'amount' => $amount,
    'created_date' => $time_config['now'],
);

$queryInsert = get_query_insert($table['payment'], $postfield);
$databaseClass->query($queryInsert);

$queryUpdate = get_query_update($table['order'], $order_id, array('status' => '1', 'payment_status' => '1'));
$databaseClass->query($queryUpdate);

echo json_encode(array('result' => 'success', 'amount' => $amount));
exit();
?>
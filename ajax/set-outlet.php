<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$outlet_id=mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['outlet_id']);

$queryUpdate=get_query_update($table['order'], $_SESSION['member']['order_id'], array('outlet_id'=>$outlet_id));
$databaseClass->query($queryUpdate);

$_SESSION['outlet_id']=$outlet_id;

echo 'success';exit();

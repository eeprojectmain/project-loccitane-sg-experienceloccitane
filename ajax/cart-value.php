<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$member_id = session_id();

$total_price = 0;

foreach ($_SESSION['cart'] as $k => $v) {
    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    $total_qty += $v['quantity'];

    $total_price += ($rs_product['price'] * $v['quantity']);
}

echo json_encode(array('result'=>'success','total_price'=>$total_price,'total_qty'=>$total_qty));
exit();

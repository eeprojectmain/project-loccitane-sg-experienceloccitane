<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$database = new database();

$mobile = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['mobile']);
$type = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_POST['type']);

$row_total = get_query_data_row($table['sample_' . $type], "mobile='+65$mobile' and date(created_date)>=date('2022-01-01')");

if ($row_total > 0) {
    echo json_encode(array(
        'valid' => false,
    ));
} else {
    echo json_encode(array(
        'valid' => true,
    ));
}
exit();
?>
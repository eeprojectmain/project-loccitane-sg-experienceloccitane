<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/vendor/autoload.php";
global $table;
$databaseClass = new database();

$order_id = $_SESSION['member']['order_id'];
$http_array=array(
    'order_id'=>$order_id,
    'count'=>$_SESSION['member']['count']
);

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

$token = file_get_contents($site_config['full_url'].'ajax/paynow-enquiry-public?'.http_build_query($http_array));

use Jose\Component\Core\AlgorithmManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Signature\Algorithm\RS256;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128GCM;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEDecrypter;
use Jose\Component\Core\JWK;
use Jose\Component\Encryption\Serializer\JWESerializerManager;
use Jose\Component\Encryption\Serializer\CompactSerializer;
use Jose\Component\Encryption\JWELoader;
use Jose\Easy\Load;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use \Firebase\JWT\JWT;

$key_private = JWKFactory::createFromKeyFile(
    __DIR__ . '/../vendor/my.key'
);

// The key encryption algorithm manager
$keyEncryptionAlgorithmManager = new AlgorithmManager([
    new RSAOAEP256(),
]);

// The content encryption algorithm manager
$contentEncryptionAlgorithmManager = new AlgorithmManager([
    new A128GCM(),
]);

// The compression method manager with the DEF (Deflate) method.
$compressionMethodManager = new CompressionMethodManager([
    new Deflate(),
]);

// We instantiate our JWE Decrypter.
$jweDecrypter = new JWEDecrypter(
    $keyEncryptionAlgorithmManager,
    $contentEncryptionAlgorithmManager,
    $compressionMethodManager
);

// The serializer manager. We only use the JWE Compact Serialization Mode.
$serializerManager = new JWESerializerManager([
    new CompactSerializer(),
]);

// We try to load the token.
$jwe = $serializerManager->unserialize($token);

// We decrypt the token. This method does NOT check the header.
$success = $jweDecrypter->decryptUsingKey($jwe, $key_private, 0);

if ($success == true) {
    $token = $jwe->getPayLoad();
    $token = explode(".", $token)[1];
    $result = base64_decode($token);
    $result = json_decode($result, true);
    print_r($result);
} else {
    echo json_encode(array('result' => 'error', 'qrcode' => 'null'));
}
?>
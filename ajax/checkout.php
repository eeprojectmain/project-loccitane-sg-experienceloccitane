<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$member_id = session_id();
$postfield=$_POST;

$total_amount = 0;

foreach($_SESSION['cart'] as $k=>$v){
    $product_id[] = $v['product_id'];
    $product_quantity[] = $v['quantity'];

    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    $total_amount += ($v['quantity'] * $rs_product['price']);
}

$product_id_text = implode(",", $product_id);
$product_quantity_text = implode(",", $product_quantity);

$postfield = array(
    'outlet_id' => $_SESSION['outlet_id'],
    'member_id' => $member_id,
    'name' => $postfield['name'],
    'mobile' => $postfield['mobile'],
    'product_id' => $product_id_text,
    'quantity' => $product_quantity_text,
    'total_amount' => $total_amount,
    'created_date' => $time_config['now']
);

$queryInsert = get_query_insert($table['order'], $postfield);
$resultInsert = $databaseClass->query($queryInsert);
$genID = $resultInsert->insertID();

unset($_SESSION['cart']);

header("Location: ../checkout?order_id=$genID");
exit();

?>
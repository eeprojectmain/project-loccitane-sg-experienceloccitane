<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$order_id = $_SESSION['member']['order_id'];

$resultOrder = get_query_data($table['order'], "pkid=$order_id");
$rs_order = $resultOrder->fetchRow();

if ($rs_order['payment_status'] == "1") {
    $return = array(
        'result' => 'success',
        'payment_status' => '1'
    );
} else {
    $return = array(
        'result' => 'error',
        'payment_status' => '0'
    );
}

echo json_encode($return);
exit();
?>
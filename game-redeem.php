<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$pkid = $_GET['i'];
$pkid = protect('decrypt', $pkid);

if ($_POST['submit_form']) {
    $postfield = $_POST;

    unset($postfield['submit_form']);

    $queryUpdate = get_query_update($table['member_voucher'], $pkid, array('status' => '0', 'ba_code' => $postfield['ba_code'], 'updated_date' => $time_config['now'], 'updated_by' => 'Store BA'));
    $databaseClass->query($queryUpdate);

    $swal['type'] = "success";
    $swal['title'] = "Yay!";
    $swal['msg'] = "Successfully marked as redeemed";
}

$resultVoucher = get_query_data($table['member_voucher'], "pkid=$pkid");
$rs_voucher = $resultVoucher->fetchRow();

if ($rs_voucher['pkid'] == "") {
    echo '<p>Invalid Link</p>';
    exit();
}

$resultGame = get_query_data($table['game_voucher'], "pkid=" . $rs_voucher['voucher_id']);
$rs_game = $resultGame->fetchRow();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<style>
    html, body {
        font-family: 'Gotham' !important;
    }

    html{
        border-top:10px solid #ffcb00;
    }

    .text-blue{
        color:#161b4d;
    }
</style>
<body class="page-bg bg-light mw-400">
<div class="container">
    <div class="row mt-2 pb-2">
        <div class="col-12">
            <img src="assets/img/logo.png" class="logo"/>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <?php if ($rs_voucher['status'] != "0") { ?>
                <p>Thank you for playing</p>
            <?php } else { ?>
                <p class="text-blue"><b>VOUCHER REDEEMED</b></p>
                <p>Continue playing</p>
                <?php
            } ?>
            <a href="https://game.experienceloccitane.com"><img src="assets/img/logo_2021Xmas_small.png" class="w-50 mx-auto d-block mb-3"/></a>
            <?php if ($rs_voucher['status'] != "0") { ?>
                <p>Please present this voucher to our Beauty Advisors in-stores to redeem your</p>
            <?php } ?>
            <p class="text-blue"><b>FREE <?= $rs_game['title'] ?></b></p>

        </div>

        <div class="text-left m-5 mt-1">
            <p><u><b>Voucher Details</b></u></p>
            <p>
                Mobile: <?= $rs_voucher['mobile'] ?>
            </p>
            <p>Voucher Confirmation No: #<?= $pkid ?>
            </p>
            <?php if ($rs_voucher['status'] != "0") { ?>
                <p>Voucher issued on: <?= $rs_voucher['created_date'] ?>
                </p>
            <?php } else { ?>
                <p>Voucher redeemed on: <?= $rs_voucher['updated_date'] ?></p>
            <? } ?>
        </div>

        <? if ($rs_voucher['status'] == "1") { ?>
            <div class="col-12 text-center mt-5 mb-5">
                <button type="button" class="btn btn-black" data-toggle="modal" data-target="#modal_redeem">
                    For staff use only
                </button>
            </div>

            <p class="p-3">
                <small>
                    <u><b>Terms & Conditions</b></u>
                    <br>• Valid until 31 December 2021 in stores and on L'OCCITANE en Demande, while stocks last.
                    <br>• Customers must flash SMS in-stores or input redeem voucher on L’OCCITANE En Demande at point of checkout to enjoy the promotion.
                    <br>• Voucher is limited to 1 redemption per customer.
                    <br>• Non-transferrable and non-exchangeable for cash.
                    <br>• L’OCCITANE Singapore reserves the final right to alter and/or withdraw the items, terms & conditions without prior notice.
                </small>
            </p>
        <? } ?>
    </div>
</div>

<!-- Modal Redeem -->
<div class="modal fade" id="modal_redeem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="game-redeem?i=<?= $_GET['i'] ?>" method="post">
                    <div class="row">
                        <div class="col-12 text-center">
                            <div class="form-group">
                                <label>Please enter BA Code</label>
                                <input type="text" name="ba_code" class="form-control" required maxlenght="10"/>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="submit_form" value="true"
                                        class="btn btn-black">SUBMIT
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(".btn-track").on('click', function (e) {
        var oid = $(this).data('order_id');
        $("#modal-track-content").html('<i class="fa fa-2x fa-gear fa-spin centered"></i>');
        $("#modal-track-content").load('remote-view/delivery-status-gift?oid=' + oid, function () {
        });
        $("#modal-track").modal('show');
    });

    $('[data-toggle="popover"]').popover({
        trigger: 'hover'
    })
</script>
</body>

</html>
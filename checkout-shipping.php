<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$nav_step = '1';

$resultOutlet = get_query_data($table['outlet'], "pkid=" . $_SESSION['outlet_id']);
$rs_outlet = $resultOutlet->fetchRow();

if ($rs_campaign['pkid'] != "") {
    $rs_outlet['lalamove_status'] = $rs_campaign['enable_lalamove'];
    $rs_outlet['selfcollect_status'] = $rs_campaign['enable_pickup'];
}

if ($_POST['submit_pickup']) {
    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('shipping_method' => 'pickup', 'pickup_date' => '', 'pickup_time' => '', 'shipping_amount' => '0'));
    $databaseClass->query($queryUpdate);

    $_SESSION['shipping_method'] = 'pickup';

    header("Location: checkout-date");
    exit();
} elseif ($_POST['submit_lalamove']) {
    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('shipping_method' => 'delivery'));
    $databaseClass->query($queryUpdate);

    $_SESSION['shipping_method'] = 'delivery';

    header("Location: checkout-lalamove");
    exit();
} elseif ($_POST['submit_courier']) {
    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('shipping_method' => 'courier', 'pickup_date' => '', 'pickup_time' => ''));
    $databaseClass->query($queryUpdate);

    $_SESSION['shipping_method'] = 'courier';

    header("Location: checkout-address");
    exit();
}

/*
print_r($_SESSION["campaign_id"]);
print_r("check");
print_r($rs_campaign['pkid']);
print_r($rs_campaign['enable_courier']);*/
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">DELIVERY METHOD</h4>
                <?php
                if ($_SESSION['type'] == "gift") {
                    ?>
                    <p>How do you want your friend to receive this gift?</p>
                    <?php
                } else { ?>
                    <!--<p>Please select your preferred mode of delivery</p>-->
                <?php }
                ?>
            </div>
        </div>
        <div class="col-12 mt-5">
            <form action="checkout-shipping" method="post" class="mx-auto w-80">
                <div class="row">
                    <? if ($rs_campaign['pkid'] != "" && $rs_campaign['enable_courier']=='1') { ?>
                        <div class="col-12 mt-3">
                            <button type="submit" name="submit_courier"
                                    value="1"
                                    class="p-0 mb-3 btn-option btn border-0 lh-1 btn-block">
                                <div class="row text-left border border-dark align-items-center">
                                    <div class="col-3 p-3">
                                        <i class="fal fa-truck fa-3x"></i>
                                    </div>
                                    <div class="col-9 p-3">
                                        <b>COURIER DELIVERY</b>
                                        <br>
                                        <small class="p-0 m-0">Please note that the items will reach you within 5-7 working days.</small>
                                    </div>
                                </div>
                                <div style="background-color:#FFC200; border-radius:15px; padding: 15px; margin: 1em auto;  line-height:1.2em;"><div style="width:100%; max-width:600px; margin:0 auto;"><strong>Free shipping available with any purchase!</strong></div></div>
                            </button>
                        </div>
                    <? } else { ?>
                        <div class="col-12">
                            <button type="submit" name="submit_pickup"
                                    value="1" <?= $rs_outlet['selfcollect_status'] == "0" ? "disabled" : "" ?>
                                    class="p-0 mb-3 btn-option btn border-0 lh-1 btn-block">
                                <div class="row text-left border border-dark align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-store fa-3x"></i>
                                    </div>
                                    <div class="col-9 p-3">
                                        <b>PICK-UP FROM STORE</b>
                                        <br>
                                        <small class="p-0 m-0">You've selected <?= $rs_outlet['title'] ?></small>
                                        <br>
                                        <a class="btn btn-sm btn-blue mt-2"
                                           href="shop-select?t=gift&return=checkout-shipping">Change
                                            store</a>
                                    </div>
                                </div>
                            </button>
                        </div>

                        <div class="col-12 mt-3">
                            <button type="submit" name="submit_lalamove"
                                    value="1" <?= $rs_outlet['lalamove_status'] == "0" ? "disabled" : "" ?>
                                    class="p-0 mb-3 btn-option btn border-0 lh-1 btn-block">
                                <div class="row text-left border border-dark align-items-center">
                                    <div class="col-3">
                                        <i class="fal fa-shipping-fast fa-3x"></i>
                                    </div>
                                    <div class="col-9 p-3">
                                        <b>EXPRESS DELIVERY</b>
                                        <br>
                                        <?if(strtotime('now')<=strtotime('2022-12-31 23:59:59')){?>
                                            <small class="p-0 m-0">Free express delivery with $100 nett spend</small>
                                        <?}else{?>
                                            <small class="p-0 m-0">Free express delivery with $100 nett spend</small>
                                        <?}?>
                                    </div>
                                </div>
                            </button>
                            <?if($rs_outlet['lalamove_status'] == "0" && strtotime('now')>=strtotime('2022-09-27 00:00:00') && strtotime('now')<=strtotime('2022-09-30 00:00:00')){?>
                                <div style="color:red;"><small>Please note that delivery services will not be available from 27 to 29 September 2022. Meanwhile, you may opt for self collection service or shop at L’OCCITANE we store - <a href="sg.loccitane.com" target="_blank" style="color:red; font-weight:bold;">sg.loccitane.com</a>.</small></div>
                            <?}?>
                        </div>
                    <? } ?>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
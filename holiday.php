<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

if($_SESSION['campaign_id']=="" && strtotime('now')<strtotime('2020-10-29')){
    header("Location: index");
    exit();
}

if($_GET['vk']==1){
}else
if(strtotime('now') >= strtotime('2021-01-01')){
    header("Location: shop");
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<div class="snowflakes" aria-hidden="true">
    <div class="snowflake">
        ❅
    </div>
    <div class="snowflake">
        ❅
    </div>
    <div class="snowflake">
        ❆
    </div>
    <div class="snowflake">
        ❄
    </div>
    <div class="snowflake">
        ❅
    </div>
    <div class="snowflake">
        ❆
    </div>
    <div class="snowflake">
        ❄
    </div>
    <div class="snowflake">
        ❅
    </div>
    <div class="snowflake">
        ❆
    </div>
    <div class="snowflake">
        ❄
    </div>
</div>
<body class="page-bg bg-light">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-0">
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <ul class="carousel-indicators">
                    </ul>
                    <div class="carousel-inner">
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 pl-0 banner-holder">
                                    <img src="assets/img/banner-1.jpg" class="img-fluid w-100" />
                                    <span class="banner-caption font-infinity fz-3 fz-md-5">New Way to Holiday</span>
                                    <a href="/shop" class="btn btn-yellow banner-button">Shop now</a>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 pl-0 banner-holder">
                                    <img src="assets/img/banner-2.jpg" class="img-fluid w-100" />
                                    <span class="banner-caption font-infinity fz-3 fz-md-5">New Way of Gifting</span>
                                    <a href="#div_banner_2" class="btn btn-yellow banner-button">Find out more</a>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 pl-0 banner-holder">
                                    <img src="assets/img/banner-3.jpg" class="img-fluid w-100" />
                                    <span class="banner-caption font-infinity fz-3 fz-md-5">New Way to Go Green</span>
                                    <a href="#div_banner_3" class="btn btn-yellow banner-button">Find out more</a>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 pl-0 banner-holder">
                                    <img src="assets/img/banner-4.jpg" class="img-fluid w-100" />
                                    <span class="banner-caption font-infinity fz-3 fz-md-5">New Way to Give Back</span>
                                    <a href="#div_banner_4" class="btn btn-yellow banner-button">Find out more</a>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 pl-0 banner-holder">
                                    <video width='100%' height='100%' autoplay muted loop playsinline class='d-none d-md-block'>
                                        <source src='assets/video/banner-calendar-desktop.mp4' type='video/mp4'>
                                    </video>
                                    <div class="embed-responsive embed-responsive-4by3 d-md-none">
                                        <video width='100%' height='100%' autoplay muted loop playsinline class="embed-responsive-item">
                                            <source src='assets/video/banner-calendar-mobile.mp4' type='video/mp4'>
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 pl-0 banner-holder">
                                    <video width='100%' height='100%' autoplay muted loop playsinline class='d-none d-md-block'>
                                        <source src='assets/video/banner-gwp-desktop.mp4' type='video/mp4'>
                                    </video>
                                    <div class="embed-responsive embed-responsive-4by3 d-md-none">
                                        <video width='100%' height='100%' autoplay muted loop playsinline class="embed-responsive-item">
                                            <source src='assets/video/banner-gwp-mobile.mp4' type='video/mp4'>
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 bg-dark p-4">
                <div class="row">
                    <div class="col-7 text-left p-0">
                        <nav class="navbar navbar-expand-lg navbar-dark pr-0 d-block mx-auto">
                            <button class="navbar-toggler p-0 text-left" type="button" data-toggle="collapse"
                                data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon icon-yellow"></span>
                                <span class="text-yellow text">BROWSE PRODUCTS</span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    <?php
                                    $resultCategory = get_query_data($table['product_category'], "status=1 and holiday_status=0 order by sort_order asc");
                                    while ($rs_category = $resultCategory->fetchRow()) {
                                    $product_category_array[$rs_category['pkid']] = $rs_category['title']; 
                                    ?>
                                    <li class="nav-item" role="presentation"><a class="nav-link"
                                            href="shop?cid=<?= $rs_category['pkid'] ?>"><?= $rs_category['title'] ?>
                                        </a>
                                    </li>
                                    <?php
                                        }
                                        
                                    if ($_SESSION['campaign_id']=="3" || (strtotime('today')>=strtotime('2020-10-29') && strtotime('today')<=strtotime('2021-01-03'))) {
                                                ?>
                                    <li class="nav-item">
                                        <a href="/holiday" class="nav-link text-yellow">Holiday Collection</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                    </div>
                    <div class="col-5 text-right mt-1 p-0">
                        <a href="#search" class="btn-link btn text-yellow p-0"><i class="fa fa-search"></i><br>Search
                        </a>
                        <button type="button" onclick="open_cart_modal()"
                            class="btn-link btn text-yellow cart p-0 pl-2"><i class="fa fa-shopping-cart"></i> <span
                                id="cart-item"></span><br>Your Cart
                        </button>
                    </div>
                    <div class="col-6 text-center mt-4">
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                PRICE
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="shop?price=30">Below $30</a>
                                <a class="dropdown-item" href="shop?price=50">Below $50</a>
                                <a class="dropdown-item" href="shop?price=100">Below $100</a>
                                <a class="dropdown-item" href="shop?price=101">$100 and above</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 text-center mt-4">
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                CATEGORY
                            </button>
                            <div class="dropdown-menu">
                                <?php
                                $resultCategory=get_query_data($table['product_category'], "holiday_status=1 order by sort_order asc");
                                while ($rs_category=$resultCategory->fetchRow()) {
                                    ?>
                                <a class="dropdown-item"
                                    href="#div_<?=$rs_category['pkid']?>"><?=$rs_category['title']?></a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-5">
                <h3 class="w-100 text-center">HOLIDAY BEST SELLERS</h3>
                <div class="row">
                    <?php
                    foreach ($product_holiday_featured_array as $v) {
                        $order_text.='pkid!='.$v.',';
                    }
                    $order_text=substr($order_text, 0, -1);
                $resultProduct=get_query_data($table['product'], "status=1 and pkid in (".implode(',', $product_holiday_featured_array).") order by $order_text");
                while ($rs_product = $resultProduct->fetchRow()) {
                    $product_name_array = explode(" ", $rs_product['title']); ?>
                    <div class="col-6 col-md-4 col-lg-3 product-card text-center mt-5">
                        <div class="card">
                            <div class="card-body text-center" id="product_<?= $rs_product['pkid'] ?>">
                                <h4 class="card-title"><img class="img-fluid product-img"
                                        <?php if ($rs_product['img_url']=="") {?>
                                        src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                                        <?php } else { ?> src="assets/product/<?=$rs_product['img_url']?>" <?php } ?>>
                                </h4>
                                <p class="card-text text-center product-title m-0">
                                    <?= strtoupper($rs_product['title']) ?>
                                </p>
                                <p class="card-text text-center product-price m-0">
                                    <?= $rs_product['size']!=""?$rs_product['size']." | ":"" ?>
                                    S$ <?= $rs_product['price'] ?>
                                    <?php if ($rs_product['pwp_status']=="1") {
                        ?>
                                    <br><small>with $30 nett spend</small>
                                    <?php
                    } ?>
                                </p>
                            </div>
                            <div class="card-footer">
                                <div class="row row-product-button">
                                    <div class="col product-button text-center">
                                        <?
                                        if (strtotime($rs_product['start_date']) > strtotime($time_config['today'])) {
                                            ?>
                                            <span class="badge">AVAILABLE ON <?= strtoupper(date('d M', strtotime($rs_product['start_date']))) ?></span>
                                            <?
                                        } else {
                                            ?>
                                            <button class="btn btn-warning btn-yellow" type="button"
                                                    onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD
                                                TO CART
                                            </button>
                                        <? } ?>
                                        <button class="btn btn-warning btn-yellow btn-details" type="button"
                                            data-product-name="<?= $rs_product['title'] ?>"
                                            onclick="open_product_modal(<?= $rs_product['pkid'] ?>)">VIEW
                                            DETAILS
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } ?>
                </div>
            </div>

            <?php
                $resultCategory=get_query_data($table['product_category'], "holiday_status=1 order by sort_order asc");
                while ($rs_category=$resultCategory->fetchRow()) {
                    ?>
            <div class="col-12 mt-5" id="div_<?=$rs_category['pkid']?>">
                <div class="row bg-category">
                    <h1 class="font-infinity fz-3 fz-md-5 w-100 text-center ml-5 mr-5 mt-4 mb-2">
                        <?=$rs_category['title']?>
                    </h1>
                    <p class="category_caption text-center fz-12 w-100 ml-5 mr-5"><?=$rs_category['description']?>
                    </p>
                </div>
                <div class="row">
                    <?php
                $resultProduct=get_query_data($table['product'], "status=1 and find_in_set(".$rs_category['pkid'].",cat_id) and (start_date='2020-10-29' or start_date='2020-11-12') order by featured_status desc,title asc limit 4");
                    while ($rs_product = $resultProduct->fetchRow()) {
                        $product_name_array = explode(" ", $rs_product['title']); ?>
                    <div class="col-6 col-md-4 col-lg-3 product-card text-center">
                        <?php
                            if (in_array($rs_product['pkid'], $campaign_product_array)) {
                                $resultCamProduct=get_query_data($table['campaign_product'], "product_id=".$rs_product['pkid']." and campaign_id=".$_SESSION['campaign_id']);
                                $rs_camProduct=$resultCamProduct->fetchRow();

                                if ($rs_camProduct['special_label']!="") {
                                    ?>
                        <small class="badge badge-danger"><?=$rs_camProduct['special_label']?></small>
                        <?php
                                }
                            } ?>
                        <div class="card">
                            <div class="card-body text-center mb-3" id="product_<?= $rs_product['pkid'] ?>">
                                <h4 class="card-title"><img class="img-fluid product-img"
                                        <?php if ($rs_product['img_url']=="") {?>
                                        src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                                        <?php } else { ?> src="assets/product/<?=$rs_product['img_url']?>" <?php } ?>>
                                </h4>
                                <p class="card-text text-center product-title m-0">
                                    <?= strtoupper($rs_product['title']) ?>
                                </p>
                                <p class="card-text text-center product-price m-0">
                                    <?= $rs_product['size']!=""?$rs_product['size']." | ":"" ?>
                                    S$ <?= $rs_product['price'] ?>
                                </p>
                            </div>
                            <div class="card-footer">
                                <div class="row row-product-button">
                                    <div class="col product-button text-center">
                                        <?
                                        if (strtotime($rs_product['start_date']) > strtotime($time_config['today'])) {
                                            ?>
                                            <span class="badge">AVAILABLE ON <?= strtoupper(date('d M', strtotime($rs_product['start_date']))) ?></span>
                                            <?
                                        } else {
                                            ?>
                                            <button class="btn btn-warning btn-yellow" type="button"
                                                    onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD
                                                TO CART
                                            </button>
                                        <? } ?>
                                        <button class="btn btn-warning btn-yellow btn-details" type="button"
                                            data-product-name="<?= $rs_product['title'] ?>"
                                            onclick="open_product_modal(<?= $rs_product['pkid'] ?>)">VIEW
                                            DETAILS
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    } ?>
                    <div class="col-12 text-center mt-5">
                        <a href="shop?cid=<?=$rs_category['pkid']?>" class="btn btn-black">DISCOVER MORE</a>
                    </div>
                    <?php
                }
                ?>
                </div>
            </div>
        </div>
        <div class="row row-eq-height mt-5" id='div_banner_2'>
            <div class="col-md-4 col-12 p-0">
                <div class="content_holder">
                    <img src="assets/img/banner-2.jpg" class="img-fluid" />
                    <div class="mx-auto category-text p-3">
                        <h2>New Way of Gifting</h2>
                        <p>We want to give you a safe and easy way to give thanks to those you love this year. Send a
                            gift now from the comfort of your own home!</p>
                        <a href="index" class="btn btn-sm btn-yellow">Send gift</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-12 p-0" id='div_banner_3'>
                <div class="content_holder">
                    <img src="assets/img/banner-3.jpg" class="img-fluid" />
                    <div class="mx-auto category-text p-3">
                        <h2>New Way to Go Green</h2>
                        <p>Skip the paper bag at our retail stores in return for 1 stamp on the Big Little Things
                            Recycling Program.
                            <br>
                            *Limited to one redemption per transaction
                        </p>
                        <br>
                        <a href="https://sg.loccitane.com/pages?fdid=recycling-big-little-things" target="_blank"
                            class="btn btn-sm btn-yellow">For more details</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-12 p-0" id='div_banner_4'>
                <div class="content_holder">
                    <img src="assets/img/banner_tth.png" class="img-fluid" />
                    <div class="mx-auto category-text p-3">
                        <h2>New Way to Give Back</h2>
                        <p>Embrace the spirit of gifting with your friends through our NEW Tap Tap harvest game! Unlock
                            in-game achievements to redeem L'OCCITANE products or do good by donating a pair of
                            spectacles to the less fortunate.</p>
                        <a href="http://game.experienceloccitane.com/index.html" class="btn btn-sm btn-yellow">Play Now!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="search">
        <button type="button" class="close" style="color:white !important;">×</button>
        <form action="shop" method="get">
            <input type="text" name="search" placeholder="SEARCH" />
            <button type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;">SUBMIT</button>
        </form>
    </div>

    <div class="modal fade" role="dialog" tabindex="-1" id="modal-cart">
        <form action="ajax/checkout" method="post">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content dark-bg">
                    <div class="modal-header b-0">
                        <h4 class="modal-title text-center w-100">SHOPPING CART</h4>
                        <button type="button" class="close p-absolute r-5" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    </div>
                    <div id="modal-cart-body">
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"
                            style="top: 0;left: 0;right: 0;position: absolute;margin: auto;"></i>
                    </div>
                </div>
            </div>
            <input type="hidden" name="token" value="<?= protect('encrypt', session_id()) ?>">
        </form>
    </div>

    <div class="modal fade" role="dialog" tabindex="-1" id="modal-product">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content dark-bg">
                <div class="modal-header b-0">
                    <h4 class="modal-title w-100 text-center" data-type="product-name">
                        DESCRIPTION</h4>
                    <button type="button" class="close p-absolute r-5 w-100 text-right" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" id="modal-product-body">
                    <i class="fa fa-cog fa-spin fa-3x fa-fw"
                        style="top: 0;left: 0;right: 0;position: absolute;margin: auto;"></i>
                </div>
            </div>
        </div>
    </div>

    <?php include('footer.php') ?>
    <?php include('js-script.php') ?>
    <script>
    $(document).ready(function() {
        $(".carousel-item").each(function(index, element) {
            $(this).addClass('active');
            return false;
        });

        $(".carousel-item").each(function(index, element) {
            if (index == 0) {
                $(".carousel-indicators").append('<li data-target="#demo" data-slide-to="' +
                    index +
                    '" class="active"></li>');
            } else {
                $(".carousel-indicators").append('<li data-target="#demo" data-slide-to="' +
                    index +
                    '"></li>');
            }
        });

        $('a[href="#search"]').on('click', function(event) {
            event.preventDefault();
            $('#search').addClass('open');
            $('#search > form > input[type="text"]').focus();
        });

        $('.close').on('click', function(event) {
            $("#search").removeClass('open');
        });
    });

    $(window).on('load', function(e) {
        $('.carousel').carousel({
            interval: 5000
        });
        update_cart_price();
    });

    function add_to_cart(id) {
        fbq('track', 'AddToCart');

        $.ajax({
                method: "POST",
                url: "ajax/add-to-cart",
                dataType: 'json',
                data: {
                    id: id,
                    token: "<?=protect("encrypt", session_id())?>"
                }
            })
            .done(function(data) {
                Swal.fire({
                    title: data.title,
                    text: data.msg,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'CONTINUE SHOPPING',
                    cancelButtonText: '<span onclick="open_cart_modal();">VIEW CART</span>',
                    customClass: {
                        confirmButton: 'btn btn-black float-left',
                        cancelButton: 'btn btn-black btn-cart float-right',
                    }
                });
                update_cart_price();
                $(".checkout").show(500);
            });
    }

    function open_cart_modal() {
        $("#modal-cart-body").load("remote-view/cart2", function(e) {
            $("#modal-cart").modal('show');
        });
    }

    function delete_cart(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "It's such a great deal!",
            icon: "warning",
            showCloseButton: true,
            showCancelButton: true,
        }).then((willDelete) => {
            if (willDelete.value) {
                $.ajax({
                        method: "POST",
                        url: "ajax/delete-cart",
                        data: {
                            id: id
                        }
                    })
                    .done(function(data) {
                        open_cart_modal();
                        Swal.fire("Successfully removed from cart", {
                            icon: "success",
                            timer: 1000,
                        });
                    });
            }
        });
    }

    function update_cart_price() {
        $.ajax({
                method: "POST",
                url: "ajax/cart-item",
            })
            .done(function(data) {
                $("#cart-item").text(data);
            });
    }

    function open_product_modal(id) {
        $("#modal-product-body").load("remote-view/product-details?product_id=" + id, function(e) {
            $("#modal-product").modal('show');
        });
    }
    </script>
</body>

</html>
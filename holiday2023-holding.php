<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass = new stock();

/*unset($_SESSION['type']);

if($_GET['campaign'] == ""){
    header("Location: GMHoliday23?".$_SERVER['QUERY_STRING']."&campaign=GMHoliday23");
}*/

if($_GET['oid']!=""){
    $_SESSION['outlet_id']=protect('decrypt',$_GET['oid']);
}

if ($_SESSION['method'] == "") {
    $_SESSION['method'] = "delivery";
}

if ($_SESSION['outlet_id'] == "") {
    header("Location: holiday2023-select");
    exit();
}

$resultOutlet=get_query_data($table['outlet'],"pkid=".$_SESSION['outlet_id']);
$rs_outlet=$resultOutlet->fetchRow();

$i = 0;
$resultCategory = get_query_data($table['product_category'], "status=1 or holiday_status=1 order by sort_order asc");
while ($rs_category = $resultCategory->fetchRow()) {
    $i++;
    $category_array[$rs_category['pkid']] = $i;
}

//$where = " and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)";

$cat_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['cid']);
$keyword = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['search']);
$price = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['price']);

if ($keyword != "") {
    $keyword_array = explode(" ", $keyword);

    if(count($keyword_array)>0) {
        $where .= " and (";

        foreach ($keyword_array as $k => $v) {
            $where .= " lower(title) like '%" . mysqli_real_escape_string($GLOBALS["mysqli_conn"], strtolower($v)) . "%' or";
        }
        $where = substr($where, 0, -2);
        $where .= ')';
    }else{
        $where.=" and (lower(title) like '%".mysqli_real_escape_string($GLOBALS["mysqli_conn"], strtolower($keyword))."%')";
    }
}

if ($rs_campaign['pkid'] != "") {
    $resultCampaignProduct = get_query_data($table['campaign_product'], "campaign_id=" . $rs_campaign['pkid'] . " order by sort_order asc");
    $row_campaignProduct = $resultCampaignProduct->numRows();

    if ($row_campaignProduct > 0) {
        while ($rs_campaignProduct = $resultCampaignProduct->fetchRow()) {
            $campaign_product_array[] = $rs_campaignProduct['product_id'];
            $order_by .= "pkid!=" . $rs_campaignProduct['product_id'] . ",";
        }

        $where .= " or pkid in (" . implode(",", $campaign_product_array) . ")";
    }
}

if ($price != "") {
    if ($price == "50") {
        $where .= " and price <=50";
    } elseif ($price == '100') {
        $where .= " and price >50 and price <=100";
    } elseif ($price == '101') {
        $where .= " and price >100";
    }
    $pagi_where[] = "price=$price";
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<style>
    .bg-nav {
        background-color: #fffefa !important;
        color: black !important;
    }

    .div-banner-text {
        position: absolute;
        bottom: 0;
    }
</style>
<body>
<div class="container-fluid">
    <? include('nav-holiday2023-holding.php') ?>
    <div class="row row-banner pt-5">
        <div class="col-12 pt-5 text-center">
            <h3 class="holiday-title w-100 text-center pt-5 mt-5">Gold Member Holiday Exclusive Launch </h3>
            <span class="w-100 text-center black mx-3">Only on 19 October, stay tuned!</span>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    var timeout, page, prev_page, owl_nav, c_index;

    $(window).on("load", function (e) {
        setTimeout(() => {
            $(".div_notice").hide(500);
        }, 10000);

        <?if($cat_id == "" && $keyword == "" && $page == "1" && $price == ""){?>
        $("#popup_modal").modal('show');
        <?}?>

        $('.owl-carousel-banner').owlCarousel({
            loop: true,
            margin: 10,
            // nav: true,
            items: 1,
            center: true,
            autoplay: true,
            autoplayTimeout: 10000,
            autoplayHoverPause: true,
            dots: false,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar
        });
    });

    $(document).ready(function () {
        loadProduct();

        $(".option").on('click', function (e) {
            e.preventDefault();
            var type = $(this).data('type');
            var value = $(this).data('value');

            $(".fa-check-circle.option-" + type).remove();
            $(".option[data-type='" + type + "']").removeClass('border-green');
            $(this).parent().append('<i class="fas fa-check-circle option-tick option-' + type + '"></i>');
            $(this).addClass('border-green');

            if (type == 'category') {
                $("input[type='hidden'][name='cid']").val(value);
            } else {
                $("input[type='hidden'][name='price']").val(value);
            }
        });

        owl_nav = $('.owl-carousel-nav').owlCarousel({
            loop: false,
            margin: 10,
            items: 3,
            center: true,
            autoplay: false,
            dots: false,
            responsive: {
                768: {
                    items: 5,
                }
            }
        });

        $(".owl-carousel-nav .owl-item").on('click', function (e) {
            $(owl_nav).trigger('to.owl.carousel', $(this).children('.item').data('index'));
        });

        <?if($cat_id != ""){?>
        $(owl_nav).trigger('to.owl.carousel', <?=$category_array[$cat_id]?>);
        c_index = <?=$category_array[$cat_id]?>;
        loadProduct(c_index);
        <?}?>

        owl_nav.on('changed.owl.carousel', function (event) {
            c_index = event.item.index;
            loadProduct(c_index);
        })

        $(".btn-details").on('click', function (e) {
            $("*[data-type='product-name'").html($(this).data('product-name'));
        });

        <?if($_GET['shortcut']=='gift-finder'){?>
        $("#modalFinder").modal('show');
        <?}?>
    });

    function loadProduct(c_index, page) {
        $(".div-product-load").html("<div class='loader-wrapper'><img src='assets/img/loader.svg' class='loader w-25' /></div>");

        if (c_index == "" || typeof c_index == 'undefined') {
            $("body").css('background-image', 'url("assets/holiday2023/bg-holiday.png")');
            $("body").css('background-size', 'cover');
            $("body").css('background-position', 'bottom');
        } else {
            $(owl_nav).trigger('to.owl.carousel', c_index);
        }

        clearTimeout(timeout);
        timeout = setTimeout(() => {
            $(".div-product-load").load("remote-view/product-holiday2023?where=<?=protect('encrypt',$where)?>&page=" + page + "&c_index=" + c_index + "&order_by=<?=$order_by?>&price=<?=$price?>", function (e) {
                $(".page-link").on('click', function (e) {
                    e.preventDefault();
                    page = $(this).attr('href');

                    if ($.isNumeric(page)) {
                        loadProduct(c_index, page);
                    }
                });
            });
        }, 1000);
    }

    function goCategory(id) {
        window.location.href = 'holiday2023?cid=' + id;
    }

    function startProgressBar() {
        // apply keyframe animation
        $(".slide-progress").css({
            width: "100%",
            transition: "width 10s linear"
        });
    }

    function resetProgressBar() {
        $(".slide-progress").css({
            width: 0,
            transition: "width 0s"
        });
    }

</script>
</body>

</html>
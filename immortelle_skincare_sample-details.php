<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

if($_GET["vk"]=="1"){

}else
if ($_SESSION['iglive']['mobile'] == "" || $_SESSION['iglive']['otp_request'] == "") {
    header("Location: " . $site_config['sample_immortelle_skincare_sample']);
    exit();
}

/*$row_check = get_query_data_row($table['sample_immortelle_skincare_sample'], "date(created_date) >= date('2021-08-19')");
if ($row_check >= 300) {
    header("Location: ".$site_config['sample_immortelle_skincare_sample']);
    exit();
}*/

if ($_POST) {
    $postfield = $_POST;
    unset($postfield['submit_form']);
    unset($postfield['year']);
    unset($postfield['month']);
    unset($postfield['day']);
    unset($postfield['tnc1']);
    unset($postfield['tnc2']);
    unset($postfield['tnc3']);

    $postfield['mobile'] = $_SESSION['iglive']['mobile'];

    $postfield['dob'] = $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'];
    $postfield['created_date'] = $time_config['now'];

    $queryInsert = get_query_insert($table['sample_immortelle_skincare_sample'], $postfield);
    $resultInsert = $databaseClass->query($queryInsert);
    $genID = $resultInsert->insertID();

    $_SESSION['sample_immortelle_skincare_sample']['done']=$genID;

    header("Location: " . $site_config['sample_immortelle_skincare_sample'].'-tq');
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4">
        <!--<div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">ALMOND BODY CARE SAMPLE</h4>
                <p>Explore hydrating and smooth skin with our bestselling Almond body care range!
                    <br>
                    Try our Almond body care sample featuring Almond Shower Oil and Almond Milk Concentrate! Sign up to redeem your 3 pc samples today. 

                    
                </p>
            </div>
        </div>-->
        <div class="col-12 mt-4">
            <label class="w-100 text-center">Please fill in your details.</label>
            <form action="<?= $site_config['sample_immortelle_skincare_sample'] ?>-details" method="post" class="mx-auto formValidation">
                <div class="form-group d-none">
                    <select class="form-control" name="title" required>
                        <option value="">Salutation</option>
                        <option value="Miss">Miss</option>
                        <option value="Ms">Ms</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Madam">Madam</option>
                        <option value="Mr">Mr</option>
                        <option value="Dr">Dr</option>
                        <option value="Prof">Prof</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="first_name" class="form-control" maxlength="100" placeholder="FIRST NAME"
                           required/>
                </div>
                <div class="form-group">
                    <input type="text" name="last_name" class="form-control" maxlength="100" placeholder="LAST NAME"
                           required/>
                </div>
                <div class="form-group d-none">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">Gender</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="gender_1" required
                               value="Male">
                        <label class="form-check-label" for="gender_1">Male</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="gender_2" required
                               value="Female">
                        <label class="form-check-label" for="gender_2">Female</label>
                    </div>
                </div>
                <div class="form-group d-none">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">DOB</label>
                    </div>
                    <div class="form-check form-check-inline m-0">
                        <select class="form-control" name="year" required>
                            <option value="">Year</option>
                            <? for ($i = date("Y"); $i >= (date("Y") - 90); $i--) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            } ?>
                        </select>
                    </div>
                    <div class="form-check form-check-inline m-0">
                        <select class="form-control" name="month" required>
                            <option value="">Month</option>
                            <? for ($i = 1; $i <= 12; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            } ?>
                        </select>
                    </div>
                    <div class="form-check form-check-inline m-0">
                        <select class="form-control" name="day" required>
                            <option value="">Day</option>
                            <? for ($i = 1; $i <= 31; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control" placeholder="MOBILE NUMBER" name="mobile"
                           value="<?= $_SESSION['iglive']['mobile'] ?>" disabled
                           maxlength="12">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" maxlength="100" placeholder="EMAIL ADDRESS"
                           required/>
                </div>
                <div class="form-group">
                    <p class="w-100">Preferred Boutique</p>
                    <select class="form-control" name="outlet_name" required>
                        <option value="">Please select store</option>
                        <option value="ION Orchard">ION Orchard #B2-39</option>
                        <option value="Northpoint City">NorthPoint City (South Wing) #01-140</option>
                        <option value="Paragon">Paragon #B1-24</option>
                        <option value="Plaza Singapura">Plaza Singapura #01-65</option>
                        <option value="Raffles City">Raffles City #01-43A</option>
                        <option value="Tampines Mall">Tampines Mall #01-31A</option>
                        <option value="Vivocity">Vivocity #01-188D</option>
                        <option value="Westgate">Westgate #01-15</option>
                        <option value="Takashimaya">Takashimaya Department Store B1</option>
                    </select>
                </div>
                <div class="form-group">
                    <p class="w-100">Tell us how you heard about this?</p>
                    <select class="form-control" name="how_to">
                        <option value="">Please indicate source</option>
                        <option value="Friends and family">Friends and family</option>
                        <option value="Influencers">Influencers</option>
                        <option value="Instagram">Instagram</option>
                        <option value="Facebook">Facebook</option>
                        <option value="Email">Email</option>
                        <option value="SMS">SMS</option>
                        <option value="In-store beauty advisors">In-store beauty advisors</option>
                    </select>
                </div>
                <div class="form-group how-to-other">
                    <input type="text" name="how_to_other" class="form-control" maxlength="100"
                           placeholder="Name of influencer"/>
                </div>
                <div class="form-group">
                    <p class="w-100">Opt-in to receive follow-ups, exclusive promotions, and updates from us.</p>
                    
                    <div class="form-group">
                        <div class="form-check form-check-inline col-4">
                            <label class="form-check-label">WhatsApp</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="whatsapp_status" id="whatsapp_1"
                                value="1" required>
                            <label class="form-check-label" for="whatsapp_1">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="whatsapp_status" id="whatsapp_0"
                                value="0" required>
                            <label class="form-check-label" for="whatsapp_0">No</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline col-4">
                        <label class="form-check-label">SMS</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sms_status" id="sms_1"
                               value="1" required>
                        <label class="form-check-label" for="sms_1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sms_status" id="sms_2"
                               value="0" required>
                        <label class="form-check-label" for="sms_2">No</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check form-check-inline col-4">
                        <label class="form-check-label">Email</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="email_status" id="email_1"
                               value="1" required>
                        <label class="form-check-label" for="email_1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="email_status" id="email_2"
                               value="0" required>
                        <label class="form-check-label" for="email_2">No</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check form-check-inline col-4">
                        <label class="form-check-label">Call</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="call_status" id="call_1"
                               value="1" required>
                        <label class="form-check-label" for="call_1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="call_status" id="call_0"
                               value="0" required>
                        <label class="form-check-label" for="call_0">No</label>
                    </div>
                </div>
                <div class="form-group border border-dark" style="max-height: 300px;overflow: scroll">
                    <div class="p-2">
                        <b>Data Protection and Privacy Policy</b>
                        <ul>
                            <li>By submitting your particulars and/or by signing this form, you consent to L&rsquo;OCCITANE
                                Singapore:
                            </li>
                            <li>collecting, using and disclosing your personal data obtained by us as a result of your
                                membership, for purposes in accordance with the Personal Data Protection Act 2010 and
                                our
                                <a href="https://sg.loccitane.com/" target="_blank">privacy policy</a>.
                            </li>
                            <li>collecting and using your date of birth and nationality for the purpose of data
                                analytics
                            </li>
                            <li>if you have ticked the &lsquo;Yes&rsquo; box for SMS, Email, Call and/or WhatsApp,
                                collecting
                                and using, your phone number and/or email address for the purpose of sending you special
                                promotions, services, offers, surveys, product
                            </li>
                            <li>disclosing the personal data in it to our service providers where necessary for the
                                above
                                purposes and, in the case of some service providers for their own use and disclosing the
                                personal data in it to our regional headquarters in Hong Kong and our global
                            </li>
                            <li>headquarters in Geneva for the purpose of our internal account administration.</li>

                            <li>If you have any questions, issues or requests about the personal data you have with us,
                                or
                                require any changes or updates to your personal particulars, you can write to
                                info@loccitane.com.sg or call our customer service hotline at 6732 0663.
                            </li>

                            <li>Please note that we may require 7 business days to process your request for any update
                                of
                                particulars or withdrawal of consent to take effect.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="tnc1" id="tnc_1" required value="1">
                        <label class="form-check-label" for="tnc_1">*I agree to L’OCCITANE Singapore using and
                            disclosing my personal information to contact me for purposes related to goods, services,
                            and direct marketing including contact by phone, email, SMS and/or other electronic
                            means.</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="tnc2" id="tnc_2" required value="1">
                        <label class="form-check-label" for="tnc_2">*I hereby consent to the Processing of my Personal
                            Data for the above Purpose and agree to the terms in the <a
                                    href="https://sg.loccitane.com/pages?fdid=terms-conditions" target="_blank">Data
                                Protection</a> and <a href="https://sg.loccitane.com/private-policy" target="_blank">Privacy
                                Policy Notice</a>.</label>
                    </div>
                </div>
                <div class="form-group d-none">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="tnc3" id="tnc_3" required value="1">
                        <label class="form-check-label" for="tnc_3">*I hereby agree to the Terms and Conditions of SG
                            L’OCCITANE <a href="https://sg.loccitane.com/become-a-member" target="_blank">membership
                                program</a>.
                        </label>
                    </div>
                </div>
                <div class="form-group mt-5 pb-5 text-center">
                    <button type="submit" name="submit_form" value="online" class="btn btn-darkblue w-50">SUBMIT
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(document).ready(function () {
        $(".how-to-other").hide();

        $("select[name='how_to']").on('change', function (e) {
            if ($(this).val() == 'Influencers') {
                $(".how-to-other").show(500);
            } else {
                $(".how-to-other").hide(500);
            }
        });
    });
</script>
</body>
</html>
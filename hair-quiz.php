<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<link rel="stylesheet" href="assets/css/smart_wizard_all.min.css"/>

<body class="hair-bg">
<div class="container-fluid">
    <div class="row">
        <div class="col-12 mt-3">
            <a href="shop?cid=5" class="btn btn-black">BACK TO SHOP</a>
        </div>
    </div>
    <div class="row mt-5 mb-5">
        <div id="smartwizard" class="w-100">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="#step-1">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#step-2">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#step-3">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#step-4">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#step-5">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#step-6">
                    </a>
                </li>
            </ul>

            <form action="hair-result" method="post">
                <div class="tab-content mt-5">
                    <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                        <h4 class="w-100 text-center">How do you describe your hair volume?</h4>
                        <div class="content-option text-center">
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="1op_1" name="q1"
                                           value="Volume & Strength"/>
                                    <label class="custom-control-label" for="1op_1">Fine / thin hair</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="1op_2" name="q1" value="All"/>
                                    <label class="custom-control-label" for="1op_2">Average</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="1op_3" name="q1" value="All"/>
                                    <label class="custom-control-label" for="1op_3">Thick</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                        <h4 class="w-100 text-center">What is your hair length?</h4>
                        <div class="content-option text-center">
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="2op_1" name="q2"
                                           value="All"/>
                                    <label class="custom-control-label" for="2op_1">Long</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="2op_2" name="q2" value="All"/>
                                    <label class="custom-control-label" for="2op_2">Medium</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="2op_3" name="q2" value="All"/>
                                    <label class="custom-control-label" for="2op_3">Short</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                        <h4 class="w-100 text-center">What is your current hair condition?</h4>
                        <div class="content-option text-center">
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="3op_1" name="q3"
                                           value="Intense Repair"/>
                                    <label class="custom-control-label" for="3op_1">Colored / permed / straightened /
                                        bleached</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="3op_2" name="q3"
                                           value="Gentle & Balance"/>
                                    <label class="custom-control-label" for="3op_2">Normal</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="3op_3" name="q3"
                                           value="Volume & Strength"/>
                                    <label class="custom-control-label" for="3op_3">Flat / lack of volume</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="3op_4" name="q3"
                                           value="Purifying Freshness"/>
                                    <label class="custom-control-label" for="3op_4">Oily</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="3op_5" name="q3"
                                           value="Gentle & Balance"/>
                                    <label class="custom-control-label" for="3op_5">Naturally dry </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-4">
                        <h4 class="w-100 text-center">How frequently do you wash you hair?</h4>
                        <div class="content-option text-center">
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="4op_1" name="q4"
                                           value="All"/>
                                    <label class="custom-control-label" for="4op_1">Every day</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="4op_2" name="q4" value="All"/>
                                    <label class="custom-control-label" for="4op_2">Once in two days</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="4op_3" name="q4" value="All"/>
                                    <label class="custom-control-label" for="4op_3">Twice a week</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-5" class="tab-pane" role="tabpanel" aria-labelledby="step-5">
                        <h4 class="w-100 text-center">What are your main scalp & hair concerns?</h4>
                        <div class="content-option text-center">
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="5op_1" name="q5"
                                           value="Intensive Repair"/>
                                    <label class="custom-control-label" for="5op_1">Hair breakage, spilt ends,
                                        frizziness</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="5op_2" name="q5"
                                           value="Volume & Strength"/>
                                    <label class="custom-control-label" for="5op_2">Fine hair, lack of volume, flat
                                        hair</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="5op_3" name="q5"
                                           value="Purifying Freshness"/>
                                    <label class="custom-control-label" for="5op_3">Oiliness, dandruff</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="5op_4" name="q5"
                                           value="Gentle & Balance"/>
                                    <label class="custom-control-label" for="5op_4">I don't have any major hair
                                        concerns</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-6" class="tab-pane" role="tabpanel" aria-labelledby="step-6">
                        <h4 class="w-100 text-center">What is your ideal hair & scalp condition?</h4>
                        <div class="content-option text-center">
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="6op_1" name="q6"
                                           value="Intensive Repair"/>
                                    <label class="custom-control-label" for="6op_1">To have less breakage & stronger
                                        hair</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="6op_2" name="q6"
                                           value="Volume & Strength"/>
                                    <label class="custom-control-label" for="6op_2">To have more volume &
                                        density</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="6op_3" name="q6"
                                           value="Purifying Freshness"/>
                                    <label class="custom-control-label" for="6op_3">To be less oily & itchy</label>
                                </div>
                            </div>
                            <div class="form-check">
                                <div class="custom-radio custom-control">
                                    <input type="radio" class="custom-control-input" id="6op_4" name="q6"
                                           value="Gentle & Balance"/>
                                    <label class="custom-control-label" for="6op_4">To be less irritated &
                                        sensitive</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $("input[type='radio']").on('click', function (e) {
        $(".form-check").removeClass('active');
        $(this).closest('.form-check').addClass('active');
        $(".sw-btn-next").attr('disabled', false);
        $(".sw-btn-next").removeClass('disabled');
    });

    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
        $(".sw-btn-next").attr("disabled","disabled");
        if (stepNumber + 1 == 6) {
            $(".sw-btn-next").html("Submit");
            $(".sw-btn-next").on("click", function (e) {
                $("form")[0].submit();
            });
        } else {
            $(".sw-btn-next").html("Next");
            $(".sw-btn-next").on('click', function (e) {
            });
        }
    });

    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'dots', // default, arrows, dots, progress
        transition: {
            animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
        },
        toolbarSettings: {
            toolbarPosition: 'bottom', // both bottom
            toolbarButtonPosition: 'center',
            showPreviousButton: false,
        },
        keyboardSettings: {
            keyNavigation: false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
        },
    });
</script>
</body>
</html>
<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta property="og:image" content="https://experienceloccitane.com/assets/img/loccitane-logo-square.png"/>

    <meta property="og:description" content="More great ways to shop with L’OCCITANE en Demande!">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="apple-mobile-web-app-status-bar-style" content="#ffcb00">

    <meta name="apple-mobile-web-app-title" content="EnDemande">

    <link rel="apple-touch-icon" sizes="57x57" href="assets/favicon/apple-icon-57x57.png">

    <link rel="apple-touch-icon" sizes="60x60" href="assets/favicon/apple-icon-60x60.png">

    <link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-icon-72x72.png">

    <link rel="apple-touch-icon" sizes="76x76" href="assets/favicon/apple-icon-76x76.png">

    <link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-icon-114x114.png">

    <link rel="apple-touch-icon" sizes="120x120" href="assets/favicon/apple-icon-120x120.png">

    <link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-icon-144x144.png">

    <link rel="apple-touch-icon" sizes="152x152" href="assets/favicon/apple-icon-152x152.png">

    <link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-icon-180x180.png">

    <link rel="icon" type="image/png" sizes="192x192" href="assets/favicon/android-icon-192x192.png">

    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">

    <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon/favicon-96x96.png">

    <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">

    <link rel="manifest" href="assets/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffcb00">

    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">

    <meta name="theme-color" content="#ffcb00">

    <title><?= $site_config['site_title'] ?></title>



    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="assets/css/mCustomScrollbar.min.css"/>

    <link rel="stylesheet" href="assets/formvalidation/css/formValidation.min.css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"/>

    <link rel="stylesheet"

          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>

    <link rel="stylesheet"

          href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"/>

    <link ref="stylesheet/css" href="https://cdn.rawgit.com/mfd/f3d96ec7f0e8f034cc22ea73b3797b59/raw/856f1dbb8d807aabceb80b6d4f94b464df461b3e/gotham.css">

    <link rel="stylesheet/less" href="assets/fa5/css/all.min.css"/>

    <link rel="stylesheet/less" href="assets/less/style.less"/>

    <link rel="stylesheet/less" href="assets/less/holiday.less?v=2022"/>

    <link rel="stylesheet" href="assets/css/blog.css">

    <link rel="stylesheet" href="assets/css/flash.css">

</head>



<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151309496-2"></script>

<script>

    window.dataLayer = window.dataLayer || [];



    function gtag() {

        dataLayer.push(arguments);

    }



    gtag('js', new Date());



    gtag('config', 'UA-151309496-2');

</script>

<?

if (!preg_match("/gift-redeem/", $_SERVER['PHP_SELF'])) {

    if ($_SESSION['outlet_id'] != "") {

        $resultOutltet = get_query_data($table['outlet'], "pkid=" . $_SESSION['outlet_id']);

        $rs_outlet = $resultOutltet->fetchRow();

        $rs_outlet['whatsapp_no'] = str_replace("+", "", $rs_outlet['whatsapp_no']);

    } else {

        $rs_outlet['whatsapp_no'] = '6596566381';

    }

}

?>

<!-- <? if($rs_outlet['whatsapp_no'] != ""){?>
<a href="https://api.whatsapp.com/send?phone=<?= $rs_outlet['whatsapp_no'] ?>&text=Hello L’OCCITANE, I have a question."

   target="_blank" class="btn-whatsapp">

    <i class="fab fa-whatsapp-square fa-3x"></i>

</a>
<?}?> -->



<?php

if ($_SESSION['type'] != "gift" && (preg_match("/shop.php/", $_SERVER['PHP_SELF']) || preg_match('/holiday/', $_SERVER['PHP_SELF']))) {

    ?>

    <a data-toggle="modal" data-target="#modal_gift"

       class="d-none btn-gift animate__animated animate__pulse animate__infinite">

        <i class="fa fa-gift fa-3x"></i>

        <small>Send Gift<br>To Friend</small>

    </a>

    <?php

}

?>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass = new stock();

if (strtotime('now') > strtotime('2023-10-26 00:00:00') && strtotime('now') < strtotime('2024-01-03 23:59:00')) {
    header("Location: shop-holiday?".http_build_query($_GET));
    exit();
}

if($_GET['oid']!=""){
    $_SESSION['outlet_id'] = protect('decrypt',$_GET['oid']);
}

if($_GET['shortcut']=="gift-finder"){
    header("Location: GMHoliday22?".http_build_query($_GET));
    exit();
}

if ($_SESSION['method'] == "") {
    $_SESSION['method'] = "delivery";
}

if ($_SESSION['outlet_id'] == "") {
    header("Location: shop-select");
    exit();
}

if($_SESSION['campaign_id']=='9'){
    //header("Location: GMHoliday22?".http_build_query($_GET));
    //exit();
}

$i = 0;
$resultCategory = get_query_data($table['product_category'], "status=1 and holiday_status=0 order by sort_order asc");
while ($rs_category = $resultCategory->fetchRow()) {
    $i++;
    $category_array[$rs_category['pkid']] = $i;
}

$where = " and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)";

$cat_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['cid']);
$keyword = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['search']);
$price = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['price']);

if ($keyword != "") {
    $keyword_array = explode(" ", $keyword);

    $where .= " and (";

    foreach ($keyword_array as $k => $v) {
        $where .= " lower(title) like '%" . strtolower($v) . "%' or";
    }
    $where = substr($where, 0, -2);
    $where .= ')';
}

if ($rs_campaign['pkid'] != "") {
    $resultCampaignProduct = get_query_data($table['campaign_product'], "campaign_id=" . $rs_campaign['pkid']." order by sort_order asc");
    $row_campaignProduct = $resultCampaignProduct->numRows();

    if ($row_campaignProduct > 0) {
        while ($rs_campaignProduct = $resultCampaignProduct->fetchRow()) {
            $campaign_product_array[] = $rs_campaignProduct['product_id'];
            $order_by.="pkid!=".$rs_campaignProduct['product_id'].",";
        }

        $where .= " or pkid in (" . implode(",", $campaign_product_array) . ")";
    }
}

if ($price != "") {
    if ($price == "30") {
        $where .= " and price <=30";
    } elseif ($price == '50') {
        $where .= " and price >30 and price <=49";
    } elseif ($price == '100') {
        $where .= " and price >50 and price <=99";
    } elseif ($price == '101') {
        $where .= " and price >100";
    }
    $pagi_where[] = "price=$price";
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<style>
    .bg-nav {
        background-color: #fffefa !important;
        color: black !important;
        margin-right: 30px;
        margin-left: 30px;
        border-radius: 0 0 10px 10px;
    }

    .div-banner-text {
        position: absolute;
        bottom: 0;
    }
</style>
<body>
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row row-banner">
        <div class="col-12 p-0">
            <? if ($rs_campaign['pkid'] != '') { ?>
                <div class="col-12 bg-yellow text-dark p-2 w-100 text-center">
                    Free shipping with any purchase till 17 September
<!--                    Members Preview: Explore the New & Improved Immortelle Reset Serum with Exclusive Skincare Sets-->
                </div>
            <? } else { ?>
                <div class="col-12 bg-yellow text-dark p-2 w-100 text-center">
                    Free shipping with $100 nett spend
                </div>
            <? } ?>
            <div class="owl-carousel owl-carousel-banner owl-theme">
                <?php
                if ($_SESSION['campaign_id'] != "") {
                    $resultBanner = get_query_data($table['banner'], "status=1 and campaign_id=" . $_SESSION['campaign_id'] . " and date('" . $time_config['today'] . "') between date(start_date) and date(end_date) order by sort_order asc");
                } else {
                    $resultBanner = get_query_data($table['banner'], "status=1 and campaign_id=0 and date('" . $time_config['today'] . "') between date(start_date) and date(end_date) order by sort_order asc");
                }
                while ($rs_banner = $resultBanner->fetchRow()) {
                    ?>
                    <div class="item">
                        <? if ($rs_banner['link'] != ""){ ?>
                        <a href="<?= $rs_banner['link'] ?>">
                            <? } ?>
                            <?php if ($rs_banner['img_url'] != "") {
                                if (!preg_match("/mp4/", $rs_banner['img_url'])) {
                                    ?>
                                    <img src="assets/banner/<?= $rs_banner['img_url'] ?>"
                                         class="img-fluid w-100 d-none d-md-block img-banner"/>
                                    <?php
                                } else {
                                    ?>
                                    <video width='100%' height='100%' autoplay muted loop playsinline
                                           class='d-none d-md-block'>
                                        <source src='assets/banner/<?= $rs_banner['img_url'] ?>' type='video/mp4'>
                                    </video>
                                    <?php
                                }
                            }

                            if ($rs_banner['mobile_img_url'] != "") {
                                if (!preg_match("/mp4/", $rs_banner['mobile_img_url'])) {
                                    ?>
                                    <img src="assets/banner/<?= $rs_banner['mobile_img_url'] ?>"
                                         class="img-fluid w-100 d-md-none img-banner"/>
                                    <?php
                                } else {
                                    ?>
                                    <video width='100%' height='100%' autoplay muted loop playsinline
                                           class="d-md-none">
                                        <source src='assets/banner/<?= $rs_banner['mobile_img_url'] ?>'
                                                type='video/mp4'>
                                    </video>
                                    <?php
                                }
                            }

                            if ($rs_banner['text'] != "") {
                                ?>
                                <div class="col-12 bg-darkblue text-light p-2 w-100 div-banner-text">
                                    <?= $rs_banner['text'] ?>
                                </div>
                            <? }
                            ?>

                            <? if ($rs_banner['link'] != ""){ ?>
                        </a>
                    <? } ?>
                    </div>
                <?php } ?>
            </div>
            <div class="slide-progress"></div>
        </div>
    </div>
    <? if ($keyword != "" || $price != "") { ?>
        <h4 class="text-center mt-4 font-title">
            <?php
            if ($keyword != "") {
                echo 'RESULT OF "' . strtoupper($keyword) . '"';
            } elseif ($price != "") {
                echo $product_price_filter_array[$price];
            }
            ?>
        </h4>
    <? } else { ?>
        <div class="row mt-4 mb-4">
            <div class="col-12">
                <div class="owl-carousel owl-carousel-nav owl-theme">
                    <div class="item text-center font-title" data-cid="" data-index="0">
                        ALL PRODUCTS
                    </div>
                    <?
                    $i = 0;
                    $resultCategory = get_query_data($table['product_category'], "status=1 and holiday_status=0 order by sort_order asc");
                    while ($rs_category = $resultCategory->fetchRow()) {
                        $i++;
                        ?>
                        <div class="item text-center font-title" data-cid="<?= $rs_category['pkid'] ?>"
                             data-index="<?= $i ?>">
                            <?= strtoupper($rs_category['title']) ?>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
    <? } ?>
    <? if ($cat_id == "1") { ?>
        <div class="row w-100 p-0 m-0 mt-4">
            <div class="col-6 p-0">
                <a href="blog-immortelle-reset">
                    <img src="assets/img/blog-banner-1.jpg" class="img-fluid"/>
                </a>
            </div>
            <div class="col-6 p-0">
                <a href="blog-divine-youth-oil">
                    <img src="assets/img/blog-banner-2.jpg" class="img-fluid"/>
                </a>
            </div>
        </div>
    <? } elseif ($cat_id == "2") { ?>
        <div class="row w-100 p-0 m-0 mt-4">
            <div class="col-12 p-0 text-center">
                <a href="hair-quiz">
                    <img src="assets/img/banner-hair.jpg" class="img-fluid"/>
                </a>
            </div>
        </div>
    <? } ?>
    <div class="row justify-content-center div-product-load">
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    var timeout, page, prev_page;

    $(window).on("load", function (e) {
        setTimeout(() => {
            $(".div_notice").hide(500);
        }, 10000);

        <?if($cat_id == "" && $keyword == "" && $page == "1" && $price == ""){?>
        $("#popup_modal").modal('show');
        <?}?>

        $('.owl-carousel-banner').owlCarousel({
            loop: true,
            margin: 10,
            // nav: true,
            items: 1,
            center: true,
            autoplay: true,
            autoplayTimeout: 10000,
            autoplayHoverPause: true,
            dots: false,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar
        });
    });

    $(document).ready(function () {
        loadProduct();

        var owl_nav = $('.owl-carousel-nav').owlCarousel({
            loop: false,
            margin: 10,
            items: 3,
            center: true,
            autoplay: false,
            dots: false,
            responsive: {
                768: {
                    items: 5,
                }
            }
        });

        $(".owl-carousel-nav .owl-item").on('click', function (e) {
            $(owl_nav).trigger('to.owl.carousel', $(this).children('.item').data('index'));
        });

        <?if($cat_id != ""){?>
        $(owl_nav).trigger('to.owl.carousel', <?=$category_array[$cat_id]?>);
        <?}?>

        owl_nav.on('changed.owl.carousel', function (event) {
            var c_index = event.item.index;
            loadProduct(c_index);
        })

        $(".btn-details").on('click', function (e) {
            $("*[data-type='product-name'").html($(this).data('product-name'));
        });
    });

    function loadProduct(c_index, page) {
        $(".div-product-load").html("<div class='loader-wrapper'><img src='assets/img/loader.svg' class='loader w-25' /></div>");

        clearTimeout(timeout);
        timeout = setTimeout(() => {
            $(".div-product-load").load("remote-view/shop-product?where=<?=urlencode($where)?>&page=" + page + "&c_index=" + c_index+"&order_by=<?=$order_by?>", function (e) {
                $(".page-link").on('click', function (e) {
                    e.preventDefault();
                    page = $(this).attr('href');

                    if ($.isNumeric(page)) {
                        loadProduct(c_index, page);
                    }
                });
            });
        }, 1000);
    }

    function startProgressBar() {
        // apply keyframe animation
        $(".slide-progress").css({
            width: "100%",
            transition: "width 10s linear"
        });
    }

    function resetProgressBar() {
        $(".slide-progress").css({
            width: 0,
            transition: "width 0s"
        });
    }

</script>
</body>

</html>
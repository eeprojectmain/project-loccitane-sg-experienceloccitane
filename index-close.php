<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<body>
<div class="container">
    <div class="row m-5" style="border:8px solid #ffcb00;border-radius: 25px;">
        <div class="col-12 col-md-6 text-center align-items-center d-flex">
            <img src="assets/img/index-temp.png" class="img-fluid" />
        </div>
        <div class="col-12 col-md-6 text-center">
            <img src="assets/img/logo.png" class="img-fluid w-75" />
            <p>Thank you for visiting L'OCCITANE En Demande!</p>
                <p>Our site is closed for upgrades to serve you better.
            </p>
            <p>We look forward to seeing you in our boutiques and our official e-store sg.loccitane.com</p>
            <p><a href="https://sg.loccitane.com" class="btn btn-yellow">SHOP NOW</a></p>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(document).ready(function() {
    });
</script>
</body>

</html>
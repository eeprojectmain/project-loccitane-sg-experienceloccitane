<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass = new stock();

unset($_SESSION['type']);

if(strtotime('now')>=strtotime('2022-10-19 23:59:59')){
    header("Location: index");
    exit();
}

if($_GET['campaign'] == ""){
    header("Location: GMHoliday22?".$_SERVER['QUERY_STRING']."&campaign=GMHoliday22");
}

if($_GET['oid']!=""){
    $_SESSION['outlet_id']=protect('decrypt',$_GET['oid']);
}else{
    $_SESSION['outlet_id'] = '999';
}

if ($_SESSION['method'] == "") {
    $_SESSION['method'] = "delivery";
}

if ($_SESSION['outlet_id'] == "") {
    header("Location: shop-select");
    exit();
    
}

$resultOutlet=get_query_data($table['outlet'],"pkid=".$_SESSION['outlet_id']);
$rs_outlet=$resultOutlet->fetchRow();

$i = 0;
$resultCategory = get_query_data($table['product_category'], "status=1 or holiday_status=1 order by sort_order asc");
while ($rs_category = $resultCategory->fetchRow()) {
    $i++;
    $category_array[$rs_category['pkid']] = $i;
}

//$where = " and date('" . $time_config['today'] . "') between date(start_date) and date(end_date)";

$cat_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['cid']);
$keyword = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['search']);
$price = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['price']);

if ($keyword != "") {
    $keyword_array = explode(" ", $keyword);

    $where .= " and (";

    foreach ($keyword_array as $k => $v) {
        $where .= " lower(title) like '%" . strtolower($v) . "%' or";
    }
    $where = substr($where, 0, -2);
    $where .= ')';
}


if ($rs_campaign['pkid'] != "") {
    $resultCampaignProduct = get_query_data($table['campaign_product'], "campaign_id=" . $rs_campaign['pkid'] . " order by sort_order asc");
    $row_campaignProduct = $resultCampaignProduct->numRows();

    if ($row_campaignProduct > 0) {
        while ($rs_campaignProduct = $resultCampaignProduct->fetchRow()) {
            $campaign_product_array[] = $rs_campaignProduct['product_id'];
            $order_by .= "pkid!=" . $rs_campaignProduct['product_id'] . ",";
        }

        $where .= " or pkid in (" . implode(",", $campaign_product_array) . ")";
    }
}

if ($price != "") {
    if ($price == "50") {
        $where .= " and price <=50";
    } elseif ($price == '100') {
        $where .= " and price >50 and price <=100";
    } elseif ($price == '101') {
        $where .= " and price >100";
    }
    $pagi_where[] = "price=$price";
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<style>
    .bg-nav {
        background-color: #fffefa !important;
        color: black !important;
    }

    .div-banner-text {
        position: absolute;
        bottom: 0;
    }
</style>
<body>
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row row-banner">
        <div class="col-12 p-0">
            <!--<? if ($rs_campaign['pkid'] != '') { ?>
                <div class="col-12 bg-yellow text-dark p-2 w-100 text-center">
                    Gold Member Exclusive!<br>Free Shipping with Any Purchase on 21 Oct
                </div>
            <? } else { ?>
                <div class="col-12 bg-yellow text-dark p-2 w-100 text-center">
                    Free shipping with $100 nett spend
                </div>
            <? } ?>-->

            <div class="col-12 bg-yellow text-dark p-2 w-100 text-center">
                    Gold Member Exclusive!<br>Free Shipping with Any Purchase on 19 Oct
            </div>

            <div class="owl-carousel owl-carousel-banner owl-theme">
                <div class="item">
                    <img src="assets/holiday22/Holiday_Wonder_Inside.jpg"
                            class="img-fluid w-100 img-banner"/>
                </div>
                <div class="item">
                    <img src="assets/holiday22/Gold Member_Mobile.jpg"
                            class="img-fluid w-100 img-banner"/>
                </div>
                <div class="item">
                    <img src="assets/holiday22/GWP_ Tier1_2_Mobile.jpg"
                            class="img-fluid w-100 img-banner"/>
                </div>
                
                <?
                /*if ($cat_id != "") {
                    if (file_exists('assets/holiday/banner-' . $cat_id . '.png')) {
                        ?>
                        <div class="item">
                            <img src="assets/holiday/banner-<?=$cat_id?>.png"
                                 class="img-fluid w-100 img-banner"/>
                        </div>
                        <?
                    }
                }*/
                ?>
                <?php/*
                if ($_SESSION['campaign_id'] != "") {
                    $resultBanner = get_query_data($table['banner'], "status=1 and campaign_id=" . $_SESSION['campaign_id'] . " and date('" . $time_config['today'] . "') between date(start_date) and date(end_date) order by sort_order asc");
                } else {
                    $resultBanner = get_query_data($table['banner'], "status=1 and campaign_id=0 and date('" . $time_config['today'] . "') between date(start_date) and date(end_date) order by sort_order asc");
                }
                while ($rs_banner = $resultBanner->fetchRow()) {
                    ?>
                    <div class="item">
                        <? if ($rs_banner['link'] != ""){ ?>
                        <a href="<?= $rs_banner['link'] ?>">
                            <? } ?>
                            <?php if ($rs_banner['img_url'] != "") {
                                if (!preg_match("/mp4/", $rs_banner['img_url'])) {
                                    ?>
                                    <img src="assets/banner/<?= $rs_banner['img_url'] ?>"
                                         class="img-fluid w-100 d-none d-md-block img-banner"/>
                                    <?php
                                } else {
                                    ?>
                                    <video width='100%' height='100%' autoplay muted loop playsinline
                                           class='d-none d-md-block'>
                                        <source src='assets/banner/<?= $rs_banner['img_url'] ?>' type='video/mp4'>
                                    </video>
                                    <?php
                                }
                            }

                            if ($rs_banner['mobile_img_url'] != "") {
                                if (!preg_match("/mp4/", $rs_banner['mobile_img_url'])) {
                                    ?>
                                    <img src="assets/banner/<?= $rs_banner['mobile_img_url'] ?>"
                                         class="img-fluid w-100 d-md-none img-banner"/>
                                    <?php
                                } else {
                                    ?>
                                    <video width='100%' height='100%' autoplay muted loop playsinline
                                           class="d-md-none">
                                        <source src='assets/banner/<?= $rs_banner['mobile_img_url'] ?>'
                                                type='video/mp4'>
                                    </video>
                                    <?php
                                }
                            }

                            if ($rs_banner['text'] != "") {
                                ?>
                                <div class="col-12 bg-darkblue text-light p-2 w-100 div-banner-text">
                                    <?= $rs_banner['text'] ?>
                                </div>
                            <? }
                            ?>

                            <? if ($rs_banner['link'] != ""){ ?>
                        </a>
                    <? } ?>
                    </div>
                <?php } */?>
            </div>
            <div class="slide-progress"></div>
        </div>
    </div>
    <? if ($keyword != "" || $price != "") { ?>
        <h4 class="text-center mt-4 font-title">
            <?php
            if ($keyword != "") {
                echo 'RESULT OF "' . strtoupper($keyword) . '"';
            }
            if ($_GET['filter'] == 'gift-finder') {
                $resultCategory = get_query_data($table['product_category'], "pkid=$cat_id");
                $rs_category = $resultCategory->fetchRow();

                $find_category = ($rs_category['title']=="")?"":$rs_category['title']. ", ";
                echo 'Gift Finder';
                echo '<br><small>' . $find_category . ' ' . $product_price_filter_array[$price] . '</small>';
            }
            ?>
        </h4>
    <? } else { ?>
        <div class="row mt-4 mb-4">
            <div class="col-12">
                <div class="owl-carousel owl-carousel-nav owl-theme">
                    <div class="item text-center font-title" data-cid="" data-index="0">
                        HOLIDAY
                    </div>
                    <?
                    $i = 0;
                    $resultCategory = get_query_data($table['product_category'], "status=1 or holiday_status=1 order by sort_order asc");
                    while ($rs_category = $resultCategory->fetchRow()) {
                        $i++;
                        ?>
                        <div class="item text-center font-title" data-cid="<?= $rs_category['pkid'] ?>"
                             data-index="<?= $i ?>">
                            <?= strtoupper($rs_category['title']) ?>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
    <? } ?>
    <div class="row justify-content-center div-product-load" id="div-product-load">
    </div>
</div>

<!-- Modal Gift finder -->
<div class="modal fade" id="modalFinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-finder">
            <div class="modal-header">
                <h4 class="w-100 text-center green holiday-title modal-title">Find your perfect gift!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <form action="GMHoliday22" method="get">
                    <h5 class="w-100 text-center green holiday-title">Select by Category</h5>
                    
                    <div class="row text-center">
                        <div class="col-4 mt-3">
                            <img src="assets/holiday22/icon-face-care-min.png" class="img-fluid option" data-type="category"
                                 data-value="1">
                        </div>
                        <div class="col-4 mt-3">
                            <img src="assets/holiday22/icon-bath-body-min.png" class="img-fluid option" data-type="category"
                                 data-value="3">
                        </div>
                        <div class="col-4 mt-3">
                            <img src="assets/holiday22/icon-hair-care-min.png" class="img-fluid option" data-type="category"
                                 data-value="2">
                        </div>
                        <div class="col-4 mt-3">
                            <img src="assets/holiday22/icon-hand-care-min.png" class="img-fluid option" data-type="category"
                                 data-value="4">
                        </div>
                        <div class="col-4 mt-3">
                            <img src="assets/holiday22/icon-home-min.png" class="img-fluid option" data-type="category"
                                 data-value="16">
                        </div>
                        <div class="col-4 mt-3">
                            <img src="assets/holiday22/icon-men-min.png" class="img-fluid option" data-type="category"
                                 data-value="24">
                        </div>
                    </div>

                    <h5 class="w-100 text-center green holiday-title mt-4">Select by Value</h5>
                    
                    <div class="row text-center">
                        <div class="col-4 mt-3">
                            <a href="#" class="btn btn-price option font-locci-sans-bl" style="border-radius: 0;" data-type="price" data-value="50"><b>Below<br>SGD 50</b></a>
                        </div>
                        <div class="col-4 mt-3">
                            <a href="#" class="btn btn-price option font-locci-sans-bl" style="border-radius: 0;" data-type="price" data-value="100"><b>Between<br>SGD 50-100</b></a>
                        </div>
                        <div class="col-4 mt-3">
                            <a href="#" class="btn btn-price option font-locci-sans-bl" style="border-radius: 0;" data-type="price" data-value="101"><b>Above<br>SGD 100</b></a>
                        </div>
                    </div>
                    <button type="submit" name="submit_finder" value="true" class="btn btn-red mx-auto mt-4 mb-4 font-locci-sans-bl">FIND
                        GIFT
                    </button>
                    <input type="hidden" name="cid">
                    <input type="hidden" name="price">
                    <input type="hidden" name="filter" value="gift-finder">
                </form>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    var timeout, page, prev_page, owl_nav, c_index;

    $(window).on("load", function (e) {
        setTimeout(() => {
            $(".div_notice").hide(500);
        }, 10000);

        <?if($cat_id == "" && $keyword == "" && $page == "1" && $price == ""){?>
        $("#popup_modal").modal('show');
        <?}?>

        $('.owl-carousel-banner').owlCarousel({
            loop: true,
            margin: 10,
            // nav: true,
            items: 1,
            center: true,
            autoplay: true,
            //autoplayTimeout: 10000,
            autoplayHoverPause: true,
            dots: false,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar
        });
    });

    $(document).ready(function () {
        loadProduct();

        $(".option").on('click', function (e) {
            e.preventDefault();
            var type = $(this).data('type');
            var value = $(this).data('value');

            $(".fa-check-circle.option-" + type).remove();
            $(".option[data-type='" + type + "']").removeClass('border-green');
            $(this).parent().append('<i class="fas fa-check-circle option-tick option-' + type + '"></i>');
            $(this).addClass('border-green');

            if (type == 'category') {
                $("input[type='hidden'][name='cid']").val(value);
            } else {
                $("input[type='hidden'][name='price']").val(value);
            }
        });

        owl_nav = $('.owl-carousel-nav').owlCarousel({
            loop: false,
            margin: 10,
            items: 3,
            center: true,
            autoplay: false,
            dots: false,
            responsive: {
                768: {
                    items: 5,
                }
            }
        });

        $(".owl-carousel-nav .owl-item").on('click', function (e) {
            $(owl_nav).trigger('to.owl.carousel', $(this).children('.item').data('index'));
        });

        <?if($cat_id != ""){?>
        $(owl_nav).trigger('to.owl.carousel', <?=$category_array[$cat_id]?>);
        c_index = <?=$category_array[$cat_id]?>;
        loadProduct(c_index);
        <?}?>

        owl_nav.on('changed.owl.carousel', function (event) {
            c_index = event.item.index;
            loadProduct(c_index);
        })

        $(".btn-details").on('click', function (e) {
            $("*[data-type='product-name'").html($(this).data('product-name'));
        });

        <?if($_GET['shortcut']=='gift-finder'){?>
        $("#modalFinder").modal('show');
        <?}?>
    });

    function loadProduct(c_index, page) {
        $(".div-product-load").html("<div class='loader-wrapper'><img src='assets/img/loader.svg' class='loader w-25' /></div>");

        if (c_index == "" || typeof c_index == 'undefined') {
            $("body").css('background-image', 'url("assets/holiday22/new-holiday-bg-min.png")');
            $("body").css('background-size', 'contain');
            $("body").css('background-position', 'bottom');
        } else {
            $(owl_nav).trigger('to.owl.carousel', c_index);
        }

        clearTimeout(timeout);
        timeout = setTimeout(() => {
            $(".div-product-load").load("remote-view/shop-product-holiday22?where=<?=urlencode($where)?>&page=" + page + "&c_index=" + c_index + "&order_by=<?=$order_by?>&price=<?=$price?>", function (e) {
                $(".page-link").on('click', function (e) {
                    e.preventDefault();
                    page = $(this).attr('href');

                    if ($.isNumeric(page)) {
                        loadProduct(c_index, page);
                    }
                });
            });
        }, 1000);
    }

    function goCategory(id) {
        window.location.href = 'GMHoliday22?cid=' + id;
    }

    function startProgressBar() {
        // apply keyframe animation
        $(".slide-progress").css({
            width: "100%",
            transition: "width 10s linear"
        });
    }

    function resetProgressBar() {
        $(".slide-progress").css({
            width: 0,
            transition: "width 0s"
        });
    }

</script>
</body>

</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$nav_step='2';
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
            <h4 class="w-100">PAYMENT UNSUCCESSFUL</h4>
            </div>
        </div>
        <div class="col-12 mt-5 text-center">
            <p class="w-100">There was an error regarding your payment.</p>
            <a href="checkout-payment" class="btn btn-darkblue w-80">Try paying again</a>
            <br>
            <br>
            <a href="index" class="btn btn-blue w-80">Cancel order</a>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>
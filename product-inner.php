<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$stockClass = new stock();

$product_id = mysqli_real_escape_string($GLOBALS["mysqli_conn"], $_GET['id']);

$resultProduct = get_query_data($table['product'], "pkid=$product_id");
$rs_product = $resultProduct->fetchRow();

$resultSize = get_query_data($table['product'], "title='" . $rs_product['title'] . "'");
while ($rs_size = $resultSize->fetchRow()) {
    if ($rs_size['pkid'] != $rs_product['pkid']) {
        $other_size[] = $rs_size['pkid'];
    }
}

$rs_product['other_size'] = implode(",", $other_size);
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body>
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-3 text-center pl-3 pr-3">
        <div class="col-12">
            <img class="img-fluid"
                <?php if ($rs_product['img_url'] == "") { ?>
                    src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                <?php } else { ?> src="assets/product/<?= $rs_product['img_url'] ?>" <?php } ?>>
        </div>
        <div class="col-12 text-left">
            <h5 class="m-0 w-100"><?= $rs_product['title'] ?></h5>
            S$ <?= $rs_product['price'] ?>
            <br>
            <button class="btn btn-option active mr-3 mt-3"><?= $rs_product['size'] ?></button>
            <?
            $resultSize = get_query_data($table['product'], "find_in_set(pkid,'" . $rs_product['other_size'] . "')");
            while ($rs_size = $resultSize->fetchRow()) {
                ?>
                <a href="product-inner?id=<?= $rs_size['pkid'] ?>"
                   class="btn btn-option mr-3 mt-3"><?= $rs_size['size'] ?></a>
            <? } ?>
        </div>
        <div class="col-12 mt-4 text-left">
            <b>Description</b>
            <p><?= nl2br($rs_product['details']) ?></p>
        </div>
        <?
        $included_pwp_products = array(560, 764, 765, 766, 770);

        if (strtotime($rs_product['start_date']) > strtotime($time_config['today']) && $rs_product['holiday_status'] = '1') {
            if (in_array($rs_product['pkid'], $included_pwp_products)) {
                ?>
                <span class="badge text-center badge-danger mx-auto">Purchase with min $45 spend. Only available upon check out.</span>
                <?
            } else {
                ?>
                <span class="badge w-100 text-center">AVAILABLE ON <?= strtoupper(date('d M', strtotime($rs_product['start_date']))) ?></span>
                <!--<button class="btn btn-sm btn-blue" type="button"
                            onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                </button>-->
                <br>
                <?
            }
        } else {
            if ($stockClass->check($rs_product['pkid']) === true) {
                if ($rs_product['cat_id'] == '40' && $_SESSION['outlet_id'] != "1" && $_SESSION['outlet_id'] != "2" && $_SESSION['outlet_id'] != "4") {
                    ?>
                        <span class="badge">Please note that Rose Vine Peach range is only available at ION Orchard, Raffles City and Paragon outlets.</span>
                        <br>
                    <?
                } elseif (!in_array($rs_product['pkid'], $included_pwp_products)) {
                    ?>
                    <button class="btn btn-sm btn-blue" type="button"
                            onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                    </button>
                <? }
            } else {
                if ($rs_product['cat_id'] == '40' && $_SESSION['outlet_id'] != "1" && $_SESSION['outlet_id'] != "2" && $_SESSION['outlet_id'] != "4") {
                    ?>
                        <span class="badge">Please note that Rose Vine Peach range is only available at ION Orchard, Raffles City and Paragon outlets.</span>
                        <br>
                    <?
                } elseif (!in_array($rs_product['pkid'], $included_pwp_products)) {
                ?>
                <!--<button class="btn btn-sm btn-blue" type="button"
                        onclick="low_stock('<?= mysqli_real_escape_string($GLOBALS["mysqli_conn"], $rs_product['title']); ?>');">
                    <i class="fa fa-exclamation-circle"></i> LOW STOCK
                </button> cegid-hezril-bypass -->
                <button class="btn btn-sm btn-blue" type="button"
                            onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                    </button>
                <?
                }
            }
        } ?>
    </div>
    <hr class="w-90 mx-auto mt-5">
    <div class="col-12">
        <h5>You might also like</h5>
        <div class="row">
            <?
            $resultOther = get_query_data($table['product'], " not find_in_set(cat_id,'" . $rs_product['cat_id'] . "') and status=1 and pkid!=$product_id and date('" . $time_config['today'] . "') between date(start_date) and date(end_date) order by rand() limit 2");
            while ($rs_product = $resultOther->fetchRow()) {
                ?>
                <div class="col-5 col-sm-6 col-md-5 mx-auto col-lg-4 col-xl-4 product-card text-center same-height">
                    <div class="card">
                        <a href="product-inner?id=<?= $rs_product['pkid'] ?>">
                            <div class="card-body text-center mb-3" id="product_<?= $rs_product['pkid'] ?>">
                                <h4 class="card-title"><img class="img-fluid product-img"
                                        <?php if ($rs_product['img_url'] == "") { ?>
                                            src="https://img.loccitane.com/P.aspx?l=en-MY&s=500&e=png&id=<?= $rs_product['item_code'] ?>&v=2"
                                        <?php } else { ?> src="assets/product/<?= $rs_product['img_url'] ?>" <?php } ?>>
                                </h4>
                                <p class="card-text text-center product-title m-0">
                                    <?= strtoupper($rs_product['title']) ?>
                                </p>
                                <p class="card-text text-center product-price m-0">
                                    <?= $rs_product['size'] != "" ? $rs_product['size'] . " | " : "" ?>
                                    S$ <?= $rs_product['price'] ?>
                                </p>
                            </div>
                        </a>
                        <div class="card-footer">
                            <div class="row row-product-button">
                                <div class="col product-button text-center">
                                    <?
                                    if (strtotime($rs_product['start_date']) > strtotime($time_config['today']) && $rs_product['holiday_status'] = '1') {
                                        ?>
                                        <span class="badge">AVAILABLE ON <?= strtoupper(date('d M', strtotime($rs_product['start_date']))) ?></span>
                                        <br>
                                        <?
                                    } else {
                                        if ($stockClass->check($rs_product['pkid']) === true) {
                                            ?>
                                            <button class="btn btn-sm btn-blue" type="button"
                                                    onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                                            </button>
                                        <? } else {
                                            ?>
                                            <!--<button class="btn btn-sm btn-blue" type="button"
                                                    onclick="low_stock('<?= mysqli_real_escape_string($GLOBALS["mysqli_conn"], $rs_product['title']); ?>');">
                                                <i class="fa fa-exclamation-circle"></i> LOW STOCK
                                            </button> cegid-hezril-bypass -->
                                            <button class="btn btn-sm btn-blue" type="button"
                                                    onclick="add_to_cart(<?= $rs_product['pkid'] ?>);">ADD TO BAG
                                            </button>
                                            <?
                                        }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
</body>
</html>

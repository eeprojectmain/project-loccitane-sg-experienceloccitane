<nav class="sidebar light">
    <ul class="list-unstyled mt-2">
        <a class="scroll-link dismiss btn-close" href="#"><i
                    class="fal fa-times"></i></a>
        <li class="pt-5">
            <a href="#search" class="scroll-link text-dark dismiss"><i class="fal fa-search fa-2x"></i><br>SEARCH</a>
        </li>
        <li>
            <a href="shop" class="scroll-link"><i class="fal fa-boxes fa-2x"></i><br>ALL PRODUCTS</a>
        </li>
        <li class="d-none">
            <a href="#" class="scroll-link"><i class="fal fa-heart fa-2x"></i><br>WISHLIST</a>
        </li>
        <li>
            <a href="#" onclick="open_cart_modal()" class="scroll-link"><i class="fal fa-shopping-bag fa-2x"></i><br>SHOPPING
                BAG</a>
        </li>
        <li>
            <a href="how-to-gift" class="scroll-link"><i class="fal fa-gift fa-2x"></i><br>SEND A GIFT</a>
        </li>
        <li>
            <a href="check-order" class="scroll-link"><i class="fal fa-truck-loading fa-2x"></i><br>TRACK YOUR ORDER</a>
        </li>
        <li>
            <a href="shop-select" class="scroll-link"><i class="fal fa-sign-out-alt fa-2x"></i><br>CHANGE OUTLET</a>
        </li>
    </ul>
    <img src="assets/img/logo.png" class="img-fluid d-block mx-auto"/>
</nav>
<div class="overlay"></div>

<div class="row bg-nav">
    <div class="col-12">
        <a class="open-menu text-darkblue btn-menu" href="#" role="button">
            <i class="fa fa-bars"></i>
        </a>
        <a onclick="open_cart_modal()" class="btn-cart"><i class="fal fa-shopping-bag"></i> <span
                    id="cart-qty" class="badge badge-blue"></span></a>
        <a href="#search" class="btn-search"><i class="fal fa-search"></i>
        </a>
        <img src="assets/img/logo.png" class="logo w-50" onclick="window.location.href='shop-holiday';" />
        <br>
        <small class="w-100 text-center mx-auto d-block"><?=$rs_outlet['title']?></small>
        <br>
    </div>
</div>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$promotionClass = new promotion();

$nav_step = '0';

if ($_SESSION['outlet_id'] == "0" || $_SESSION['outlet_id'] == "") {
    header("Location: shop-select?return=checkout-details");
    exit();
}

if ($_POST) {
    $postfield = $_POST;
    unset($postfield['submit_form']);
    unset($postfield['prefix']);

    foreach ($_SESSION['cart'] as $k => $v) {
        $product_array[] = $v['product_id'];
        $quantity_array[] = $v['quantity'];

        $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
        $rs_product = $resultProduct->fetchRow();

        $postfield['total_amount'] += $rs_product['price'] * $v['quantity'];
    }

    if ($_SESSION['campaign_id'] != "") {
        $postfield['campaign_id'] = $_SESSION['campaign_id'];
        if ($rs_campaign['free_courier'] == "1") {
            $postfield['shipping_amount'] = "0";
        } else {
            $postfield['shipping_amount'] = "8";
        }

        if ($rs_campaign['enable_warehouse'] == "1") {
            $postfield['outlet_id'] = "999";
            $_SESSION['outlet_id']='999';
        }
    }

    // $postfield['method']=$_SESSION['method'];
    $postfield['mobile'] = "+65" . $postfield['mobile'];
    $postfield['type'] = $_SESSION['type'];
    $postfield['outlet_id'] = $_SESSION['outlet_id'];
    $postfield['pickup_time'] = $_SESSION['pickup_time'];
    $postfield['pickup_date'] = $_SESSION['pickup_date'];
    $postfield['mobile'] = $_POST['prefix'] . $postfield['mobile'];
    $postfield['product_id'] = implode(",", $product_array);
    $postfield['quantity'] = implode(",", $quantity_array);
    $postfield['created_date'] = $time_config['now'];

    foreach ($postfield as $k => $v) {
        $_SESSION['member'][$k] = $v;
    }

    $queryInsert = get_query_insert($table['order'], $postfield);
    $resultInsert = $databaseClass->query($queryInsert);
    $order_id = $resultInsert->insertID();

    $_SESSION['member']['order_id'] = $order_id;

    $pwp_array = $promotionClass->pwp();

    if ($pwp_array != false) {
        header("Location: checkout-pwp");
    } else {
        header("Location: checkout-shipping");
    }
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body>
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4>PERSONAL DETAILS</h4>
                <p>Please fill in your details</p>
            </div>
        </div>
        <div class="col-12 mt-4">
            <form action="checkout-details" method="post" class="mx-auto formValidation">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" maxlength="100" placeholder="NAME" required
                           value="<?= $_SESSION['member']['name'] ?>"/>
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" maxlength="100"
                           placeholder="EMAIL ADDRESS" value="<?= $_SESSION['member']['email'] ?>" required/>
                </div>
                <div class="form-group">
                    <div class="input-group input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text">+65</span>
                        </div>
                        <input class="form-control" name="mobile" placeholder="MOBILE NUMBER" type="tel" required
                               minlength="8" maxlength="9"
                               value="<?= str_replace("+65", "", $_SESSION['member']['mobile']) ?>">
                    </div>
                </div>
                <p>Please tick the following if you consent to us contact you for purposes pertaining to your
                    order (kindly note that order confirmation will be sent via WhatsApp or email):</p>
                <div class="form-group">
                    <div class="form-check form-check-inline col-4">
                        <label class="form-check-label">Call</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="call_status" id="call_1" value="1"
                               required>
                        <label class="form-check-label" for="call_1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="call_status" id="call_2" value="0"
                               required>
                        <label class="form-check-label" for="call_2">No</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check form-check-inline col-4">
                        <label class="form-check-label">WhatsApp</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="whatsapp_status" id="whatsapp_1"
                               value="1" required>
                        <label class="form-check-label" for="whatsapp_1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="whatsapp_status" id="whatsapp_2"
                               value="0" required>
                        <label class="form-check-label" for="whatsapp_2">No</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check form-check-inline col-4">
                        <label class="form-check-label">Email</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="email_status" id="email_1" value="1"
                               required>
                        <label class="form-check-label" for="email_1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="email_status" id="email_2" value="0"
                               required>
                        <label class="form-check-label" for="email_2">No</label>
                    </div>
                </div>
                <p class="w-100">Please tick the following to be the first to know about our special offers and
                    exclusive loyalty
                    benefits!*:</p>
                <div class="form-group">
                    <div class="form-check form-check-inline col-4">
                        <label class="form-check-label">SMS</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sms_marketing_status"
                               id="sms_marketing_1" value="1" required>
                        <label class="form-check-label" for="sms_marketing_1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sms_marketing_status"
                               id="sms_marketing_2" value="0" required>
                        <label class="form-check-label" for="sms_marketing_2">No</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check form-check-inline col-4">
                        <label class="form-check-label">Email</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="email_marketing_status"
                               id="email_marketing_1" value="1" required>
                        <label class="form-check-label" for="email_marketing_1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="email_marketing_status"
                               id="email_marketing_2" value="0" required>
                        <label class="form-check-label" for="email_marketing_2">No</label>
                    </div>
                </div>
                <div class="content-tnc mt-4">
                    <p><strong>Data Protection and Privacy Policy</strong><br/><br/>
                        By submitting your particulars and/or by signing this form, you consent to L&rsquo;OCCITANE
                        Singapore:<br/><br/>
                        collecting, using and disclosing your personal data obtained by us as a result of your
                        membership, for purposes in accordance with the Personal Data Protection Act 2010 and our
                        privacy policy (available at our website hyperlink website to https://sg.loccitane.com/
                        ).<br/><br/>
                        collecting and using your date of birth and nationality for the purpose of data
                        analytics<br/><br/>
                        if you have ticked the &lsquo;Yes&rsquo; box for SMS, Email, Call and/or WhatsApp, collecting
                        and using, your phone number and/or email address for the purpose of sending you special
                        promotions, services, offers, surveys, product<br/><br/>
                        disclosing the personal data in it to our service providers where necessary for the above
                        purposes and, in the case of some service providers for their own use and disclosing the
                        personal data in it to our regional headquarters in Hong Kong and our global<br/><br/>
                        headquarters in Geneva for the purpose of our internal account administration.</p>

                    <p>If you have any questions, issues or requests about the personal data you have with us, or
                        require any changes or updates to your personal particulars, you can write to
                        info@loccitane.com.sg or call our customer service hotline at 6732 0663.</p>

                    <p>Please note that we may require 7 business days to process your request for any update of
                        particulars or withdrawal of consent to take effect.
                        &nbsp;</p>
                </div>
                <div class="form-group mt-5 pb-5 text-center">
                    <button type="submit" name="submit_form" value="true" class="btn btn-blue w-50">CONTINUE</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    <?php
    foreach ($_SESSION['member'] as $k => $v) {
    if (preg_match("/_status/", $k)) {
    ?>
    $("input[name='<?=$k?>'][value='<?=$v?>']")
        .attr('checked', 'checked');
    <?php
    }
    } ?>

    /*$('form').on('submit', function (e) {
        e.preventDefault();

        var email = $("input[name='email_status']:checked").val();
        var whatsapp = $("input[name='whatsapp_status']:checked").val();

        if (typeof email != "undefined" && typeof whatsapp != "undefined") {
            if (email === '0' && whatsapp === '0') {
                Swal.fire({
                    text: "We are unable to proceed with this order as order confirmation can only be sent via WhatsApp and/or email.",
                    icon: "error",
                });
            } else {
                if ($('.has-error').length == "0") {
                    $('form')[0].submit();
                }
            }
        }
    });*/

</script>
</body>

</html>
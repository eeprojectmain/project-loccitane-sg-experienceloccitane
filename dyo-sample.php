<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

//$row_check = get_query_data_row($table['sample_dyo'], "date(created_date) >= date('2022-01-14')");

if (strtotime('now') > strtotime('2023-7-10 00:00:00')) {
    $row_check = 999999;
}

if ($_GET['vk'] == '1') {
    $row_check = 0;
}

if ($_SESSION['sample_dyo']['done'] != "") {
    header("Location: " . $site_config['sample_dyo']);
    exit();
}

if ($_POST) {
    $postfield = $_POST;

    $mobile = str_replace("-", "", $postfield['mobile']);
    $mobile = str_replace(" ", "", $mobile);

    $mobile = "+65" . $mobile;
    $_SESSION['iglive']['mobile'] = $mobile;

    $resultOtp = get_query_data($table['otp'], "mobile='$mobile' and type='sample_dyo' order by pkid desc limit 1");
    $rs_otp = $resultOtp->fetchRow();

    $to_time = strtotime("now");
    $from_time = strtotime($rs_otp['created_date']);

    if (round(abs($to_time - $from_time) / 60, 2) >= 1) {
        $otp = rand('100', '999');

        $content = "L'OCCITANE: OTP code: $otp. NEVER share this code with others.";

        $ch = curl_init();
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
        );
        curl_setopt($ch, CURLOPT_URL, "http://www.etracker.cc/bulksms/mesapi.aspx?user=davino&pass=Wowsome%40820%23%23%23%23%21&type=0&to=$mobile&from=EnDemande&text=" . urlencode($content) . "&servid=MES01&title=EnDemande_SG_SampleDYO");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $server_respond = curl_exec($ch);
        curl_close($ch);
        
        // var_dump($server_respond);
        $postfield = array(
            'status' => '0',
            'type' => 'sample_dyo',
            'mobile' => $mobile,
            'otp' => $otp,
            'created_date' => $time_config['now'],
        );

        $queryInsert = get_query_insert($table['otp'], $postfield);
        $databaseClass->query($queryInsert);

        $_SESSION['iglive']['otp_request'] = "true";

        header("Location: " . $site_config['sample_dyo'] . "-otp");
        exit();
    } else {
        $swal['title'] = 'Opps...';
        $swal['msg'] = 'You\'ve just request an OTP, please try again later.';
        $swal['icon'] = 'error';
    }
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">
<div class="container-fluid">
    <? include('nav.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4 class="w-100">DIVINE YOUTH OIL SAMPLE KIT</h4>
                <div class="col-12 mb-3">
                    <img src="assets/img/Divine Youth Oil_1280x1280.jpg" class="img-fluid"/>
                </div>
                <div class="col-12" style="line-height:1.2em;">
                    <p>Discover L’OCCITANE Immortelle Divine Youth Oil. Enriched With Immortelle Essential Oil that melts into skin, leaving it plumped, radiant and firm.
                        Sign up to redeem your free sample kit today.
                    <br>
                        <!--<small>*Only applicable to customers who have yet redeem Divine Youth Oil Sample Kit.</small>-->
                        <!--10 Jul till 6 Oct (sign up valid for 2 weeks and last redemption will end on 20 Oct).-->
                    </p>
                </div>
            </div>
        </div>
    </div>
 
    <?php
    $start_date = '2023-07-10';
    $end_date = '2023-08-07';

    $current_date = date('Y-m-d');

    $start_timestamp = strtotime($start_date);
    $end_timestamp = strtotime($end_date);
    $current_timestamp = time();

        if ($current_timestamp >= $start_timestamp && $current_timestamp <= $end_timestamp) {
            ?>
            <div class="col-12 mt-4">
                <label class="w-100 text-center">Please fill in your mobile number.</label>
                <form action="<?= $site_config['sample_dyo'] ?>" method="post" class="w-80 mx-auto formIglive">
                    <div class="form-group">
                        <input type="tel" class="form-control" placeholder="MOBILE NUMBER" name="mobile"
                            required
                            minlength="8" maxlength="9">
                    </div>
                    <div class="form-group mt-5 pb-5 text-center">
                        <button type="submit" name="submit_mobile" value="true" class="btn btn-darkblue w-50">NEXT
                        </button>
                    </div>
                </form>
            </div>
        <?php
        } else {
            ?>
            <div class="col-12 mt-4">
                <label class="w-100 text-center font-weight-bold">Kindly be informed that the sign-up form is closed. We invite you to explore our wide range of products at our L’OCCITANE boutique stores, where you can discover more about what we offer.</label>
            </div>
        <?php
        }
    ?>

</div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>
    $(".formIglive").each(function (index, element) {
        form = $(".formIglive")[index];
        fv = FormValidation.formValidation(
            form, {
                fields: {
                    mobile: {
                        message: 'This field is required',
                        validators: {
                            remote: {
                                message: 'Mobile number has already been registered',
                                url: '/ajax/sample-mobile-check',
                                method: 'POST',
                                data: {
                                    type: 'dyo',
                                },
                            }
                        }
                    }
                },
                plugins: {
                    declarative: new FormValidation.plugins.Declarative({
                        html5Input: true,
                    }),
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    excluded: new FormValidation.plugins.Excluded(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    icon: new FormValidation.plugins.Icon({
                        valid: 'fal fa-check',
                        invalid: 'fal fa-times',
                        validating: 'fal fa-refresh'
                    }),
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                },
            }
        ).on('core.form.valid', function () {
            $("button[type='submit']").attr('disabled', 'disabled');
        });
    });
</script>
</body>
</html>
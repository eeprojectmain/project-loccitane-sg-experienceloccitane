<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();
$orderClass = new order();

$nav_step = '2';

$orderClass->check();

foreach ($_SESSION['cart'] as $k => $v) {
    $resultProduct = get_query_data($table['product'], "pkid=" . $v['product_id']);
    $rs_product = $resultProduct->fetchRow();

    $total += $rs_product['price'] * $v['quantity'];
}
$queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], array('total_amount' => $total));
$databaseClass->query($queryUpdate);

if ($_SESSION['outlet_id'] == "1") {
    $resultOutlet = get_query_data($table['outlet'], "status=1 and pkid!=2 order by pkid asc");
} elseif ($_SESSION['outlet_id'] == "2") {
    $resultOutlet = get_query_data($table['outlet'], "status=1 and pkid!=1 order by pkid asc");
} else {
    $resultOutlet = get_query_data($table['outlet'], "status=1 and pkid!=3 order by pkid asc");
}
$outlet_text = "{";
while ($rs_outlet = $resultOutlet->fetchRow()) {
    $rs_outlet['title']=explode("#",$rs_outlet['title'])[0];

    $outlet_text .= $rs_outlet['pkid'] . ":'" . $rs_outlet['title'] . "',";
    $gmap_outlet_text .= "'" . $rs_outlet['title'] . "',";
}
$outlet_text = substr($outlet_text, 0, -1);
$gmap_outlet_text = substr($gmap_outlet_text, 0, -1);
$outlet_text .= "}";

if (strtotime('now') > strtotime('today 2pm')) {
    $pickup_date = date("Y-m-d", strtotime('tomorrow'));
    $pickup_date_text = "Next day delivery";
    $time_array = array('3.30pm', '4.30pm', '5.30pm');
} else {
    $pickup_date = date("Y-m-d");
    $pickup_date_text = "Same day delivery";
    $time_array = array('3.30pm', '4.30pm', '5.30pm');
}

if ($_POST) {
    $postfield = $_POST;

    unset($postfield['submit_form']);

    if ($postfield['send'] != "1") {
        unset($postfield['receive_name']);
        unset($postfield['receive_mobile']);
    }
    unset($postfield['send']);

    $postfield['pickup_date'] = $pickup_date;

    $queryUpdate = get_query_update($table['order'], $_SESSION['member']['order_id'], $postfield);
    $databaseClass->query($queryUpdate);

    header("Location: checkout-summary");
    exit();
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body>
<div class="container-fluid">
    <? include('nav.php') ?>
    <? include('nav-step.php') ?>
    <div class="row mt-4">
        <div class="col-12 text-center p-0">
            <div class="title">
                <h4>EXPRESS DELIVERY</h4>
                <?php
                if ($_SESSION['type'] == "gift") {
                    ?>
                    <p class="text-center w-100">Please enter your recipient address</p>
                    <?php
                } else {
                    ?>
                    <p class="text-center w-100">Please enter your delivery address</p>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="col-12 mt-5">
            <form action="checkout-lalamove" method="post" class="mx-auto formValidation">
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input id="autocomplete" class="form-control" name="address" placeholder="POSTAL CODE" required
                               type="text"/>
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-blue btn_clear"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" name="unit_no" class="form-control" placeholder="UNIT NO.">
                </div>
                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <label>Send to other recipient?</label>
                    </div>
                    <br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="send" id="send_0" value="0" checked
                               required>
                        <label class="form-check-label" for="send_0">No</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="send" id="send_1" value="1" required>
                        <label class="form-check-label" for="send_1">Yes</label>
                    </div>
                </div>
                <div class="div_receive">
                    <div class="form-group">
                        <input type="text" name="receive_name" class="form-control" placeholder="RECIPIENT NAME"
                               required>
                    </div>
                    <div class="form-group">
                        <input type="tel" name="receive_mobile" class="form-control"
                               placeholder="RECIPIENT MOBILE NO." required>
                    </div>
                </div>
                <div class="form-group">
                    <div id="map" style="height: 250px"></div>
                </div>
                <div class="form-group mt-3 text-center">
                    <p><span class="span_distance"></p>
                    <div class="border-yellow p-3 p_price">
                        <p class="p_price">DELIVERY FEE:</p>
                        <h4 class="span_price font-weight-bold m-0"></h4>
                    </div>
                    <p class="p_warning text-danger"></p>
                </div>
                <div class="form-group mt-4 div_time">
                    <label><b>Lalamove pick-up time</b><br><?= $pickup_date_text ?> ON <?= $pickup_date ?></label>
                    <div class="text-center">
                        <?php foreach ($time_array as $k => $v) { ?>
                            <div class="form-check form-check-inline btn-white p-2 btn btn-sm btn-blue">
                                <input class="form-check-input" type="radio" name="pickup_time"
                                       id="time_<?= $k ?>"
                                       value="<?= $v ?>" required>
                                <label class="form-check-label"
                                       for="time_<?= $k ?>">
                                    <?= $v ?>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group text-center mt-5 div_btn">
                    <button type="submit" name="submit_form" class="btn btn-darkblue w-50">NEXT</button>
                </div>
                <div class="form-group text-center">
                    <a href="checkout-shipping" class="btn btn-blue w-50">BACK</a>
                </div>

                <input type="hidden" name="shipping_amount">
                <input type="hidden" name="lat">
                <input type="hidden" name="lng">
            </form>
        </div>
        <div class="w-100 mx-auto m-0 content-tnc mt-5" style="width: 90% !important;">
            <b>Terms & Conditions</b>
            <br>
            <ul>
                <li>The way we display the delivery charges is based on our appointed transportation vendor,
                    lalamove. A standard base fare of $10 applies for every delivery order and subsequent charges
                    may vary based on the distance of the delivery location.
                </li>
                <li>Same day delivery is available, provided the order is made before 2pm on a business day. Any
                    delivery orders made after 2pm on a business day will be processed as orders of the next
                    business day.
                </li>
                <li>We do not accept returns or exchanges unless the item you purchased is defective. Products with
                    manufacturing or formulation defects may be exchanged within 6 months of purchase with original
                    receipt, in accordance with Singapore’s lemon law. For exchanges and refunds, please email
                    webinfo@loccitane.com.sg or call +65 6732 0663 within 7 days of receiving your order and submit
                    your original invoice for further assistance.
                </li>
                <li>For customers who are eligible for the special offers featured on sg.experienceloccitane.com,
                    any gift with purchase will be given based on nett spend while stocks last. Orders made on
                    sg.experienceloccitane.com are not eligible for any other ongoing discounts or promotions not
                    featured on this website. Any gift with purchase and samples entitlement will be reflected in
                    the order confirmation invoice. If the final order does not meet the promotion criteria,
                    customers will not be eligible to redeem the promotion.
                </li>
                <li>We reserve the right to refuse or cancel an order for any reason, including limitations on
                    quantities available for purchase, inaccuracies, or errors in product or pricing information, or
                    problems identified by our credit and fraud prevention department. If your order is cancelled
                    after your credit card (or other payment account) has been charged, we will issue a credit to
                    your credit card (or other applicable payment account) in the amount of the charge. We will
                    contact you if all or any portion of your order is cancelled or if additional information is
                    required to accept your order.
                </li>
                <li>L’OCCITANE Singapore reserves the final right to alter and/or withdraw the items, terms and
                    conditions without prior notice.
                </li>
            </ul>
        </div>
    </div>
</div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<!--<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQEDktCHtvfMoFOFEQx_7Z1vIrgFVewFM&libraries=places&callback=initMap"
        async defer></script>-->

<!-- google account from wowsomemalaysia -->
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-meP8O4H3yLtVBG1XuxLq-2MH3tc-gG0&libraries=places&callback=initMap"
        async defer></script>

<script>
    var destinationAutocomplete, map, icons, place, AddLat, AddLng, markers = [];
    var outlet_array = <?=$outlet_text?>;
    var ori_outlet_id = <?=$_SESSION['outlet_id']?>;
    var gmap_outlet_array = [<?=$gmap_outlet_text?>];
    var distance_array = new Array();

    $(document).ready(function () {
        $(".p_price").hide();
        $(".p_warning").hide();
        $(".div_btn").hide();
        $(".div_time").hide();
        $(".div_receive").hide();

        $(window).keydown(function(event){
            if(event.keyCode == 13) {
            event.preventDefault();
            return false;
            }
        });

        $("input[name='pickup_time']").on('click', function (e) {
            getQuotation();
        });

        $(".btn_clear").on('click', function (e) {
            $(".div_btn").hide(500);
            $(".div_time").hide(500);
            $(".p_price").hide(500);
            $("#autocomplete").val("").focus();
        });

        $("input[name='send']").on('change', function (e) {
            if ($(this).val() == "1") {
                $(".div_receive").show(500);
            } else {
                $(".div_receive").hide(500);
            }
            $(fv).data('formValidation').resetForm();
        });

        $("form").on('submit', function (e) {
            e.preventDefault();
            var lat = $("input[name='lat']").val();
            var lng = $("input[name='lng']").val();
            var time = $("input[name='pickup_time']:checked").val();

            if (lat == "" && lng == "") {
                Swal.fire({
                    title: "Opps...",
                    text: "Please select your address from the list",
                    icon: "error",
                });
            } else if (time == "" || typeof time === "undefined") {
                Swal.fire({
                    title: "Opps...",
                    text: "Please select delivery time",
                    icon: "error",
                });
            } else {
                // console.log(time);
                // console.log('submit');
                $("form")[0].submit();
            }
        });
    });

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {
                lat: 1.3519852,
                lng: 103.7495453
            },
            zoom: 11,
            disableDefaultUI: true
        });

        icons = {
            start: new google.maps.MarkerImage(
                'assets/img/store-icon.png',
                // (width,height)
                new google.maps.Size(44, 44),
                // The origin point (x,y)
                new google.maps.Point(0, 0),
                // The anchor point (x,y)
                new google.maps.Point(22, 32)
            ),
            end: new google.maps.MarkerImage(
                'assets/img/house-icon.png',
                // (width,height)
                new google.maps.Size(44, 44),
                // The origin point (x,y)
                new google.maps.Point(0, 0),
                // The anchor point (x,y)
                new google.maps.Point(22, 32)
            )
        };

        new AutocompleteDirectionsHandler(map);
    }

    function AutocompleteDirectionsHandler(map) {
        this.map = map;
        this.destinationPlaceId = null;
        this.travelMode = 'DRIVING';
        this.directionsService = new google.maps.DirectionsService;
        this.directionsRenderer = new google.maps.DirectionsRenderer({
            suppressMarkers: true
        });
        this.directionsRenderer.setMap(map);

        var destinationInput = document.getElementById('autocomplete');

        destinationAutocomplete =
            new google.maps.places.Autocomplete(destinationInput);
        destinationAutocomplete.setComponentRestrictions({
            'country': ['sg']
        });

        this.setupPlaceChangedListener(destinationAutocomplete);

        destinationAutocomplete.addListener('place_changed', getQuotation);
    }

    AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function (
        autocomplete) {
        var me = this;
        autocomplete.bindTo('bounds', this.map);

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();

            AddLat = place.geometry.location.lat();
            AddLng = place.geometry.location.lng();

            if (!place.place_id) {
                window.alert('Please select an option from the dropdown list.');
                return;
            }
            me.destinationPlaceId = place.place_id;
            me.route();
        });
    };

    AutocompleteDirectionsHandler.prototype.route = function () {
        if (!this.destinationPlaceId) {
            return;
        }
        var me = this;

        this.directionsService.route({
                origin: {
                    'lat': <?=$rs_outlet['lat']?>,
                    'lng': <?=$rs_outlet['lng']?>
                },
                destination: {
                    'placeId': this.destinationPlaceId
                },
                travelMode: this.travelMode
            },
            function (response, status) {
                if (status === 'OK') {
                    removeMarker();
                    me.directionsRenderer.setDirections(response);
                    var leg = response.routes[0].legs[0];
                    makeMarker(leg.start_location, icons.start);
                    makeMarker(leg.end_location, icons.end);

                    $(".span_distance").html(outlet_array[ori_outlet_id] +
                        "<br />" + response.routes[0].legs[0].distance
                            .text + ", " + response.routes[0].legs[0]
                            .duration
                            .text);

                    //search nearest store
                    var origin = {
                        'lat': AddLat,
                        'lng': AddLng
                    };
                    var geocoder = new google.maps.Geocoder;
                    var service = new google.maps.DistanceMatrixService;
                    service.getDistanceMatrix({
                        origins: [origin],
                        destinations: gmap_outlet_array,
                        travelMode: 'DRIVING',
                        unitSystem: google.maps.UnitSystem.METRIC,
                        avoidHighways: false,
                        avoidTolls: false
                    }, function (response, status) {
                        if (status !== 'OK') {
                            alert('Error was: ' + status);
                        } else {
                            var originList = response.originAddresses;
                            var destinationList = response.destinationAddresses;

                            var showGeocodedAddressOnMap = function (asDestination) {
                                return function (results, status) {
                                };
                            };

                            var results = response.rows[0].elements;
                            console.log(results);
                            for (var j = 0; j < results.length; j++) {
                                geocoder.geocode({
                                        'address': destinationList[j]
                                    },
                                    showGeocodedAddressOnMap(true));

                                if(results[j].status!='NOT_FOUND') {
                                    distance_array.push(results[j].distance.value);
                                }
                            }

                            var distance = Math.min.apply(Math, distance_array);
                            outlet_id = getOutlet(distance);

                            if (outlet_id != ori_outlet_id) {
                                Swal.fire({
                                    text: 'We found that ' + outlet_array[outlet_id] +
                                        ' is the nearest store to your delivery address (' + (
                                            distance / 1000).toFixed(2) +
                                        ' km), change to this store instead?',
                                    showCancelButton: true,
                                    confirmButtonText: 'Yes, please',
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        ori_outlet_id = outlet_id;
                                        $.ajax({
                                            method: "POST",
                                            url: "ajax/set-outlet",
                                            data: {
                                                outlet_id: outlet_id
                                            },
                                            dataType: "json"
                                        })
                                            .done(function (msg) {
                                            });
                                        Swal.fire('Yay!', 'Changed to ' + outlet_array[
                                            outlet_id], 'success');

                                        var request = {
                                            radius: 500,
                                            location: map.getCenter(),
                                            query: outlet_array[outlet_id]
                                        };

                                        var service = new google.maps.places
                                            .PlacesService(
                                                map);
                                        service.textSearch(request, function (results,
                                                                              status) {
                                            getQuotation();
                                            me.destinationPlaceId = results[0]
                                                .place_id;
                                            me.directionsService.route({
                                                    origin: {
                                                        'lat': AddLat,
                                                        'lng': AddLng
                                                    },
                                                    destination: {
                                                        'placeId': me
                                                            .destinationPlaceId
                                                    },
                                                    travelMode: me
                                                        .travelMode
                                                },
                                                function (response, status) {
                                                    if (status === 'OK') {
                                                        removeMarker();
                                                        me.directionsRenderer
                                                            .setDirections(
                                                                response);
                                                        var leg = response
                                                            .routes[0].legs[
                                                            0];
                                                        makeMarker(leg
                                                                .start_location,
                                                            icons.end);
                                                        makeMarker(leg
                                                                .end_location,
                                                            icons.start);

                                                        $(".span_distance")
                                                            .html(
                                                                outlet_array[
                                                                    outlet_id
                                                                    ] +
                                                                "<br />" +
                                                                response
                                                                    .routes[
                                                                    0].legs[
                                                                    0]
                                                                    .distance
                                                                    .text +
                                                                ", " +
                                                                response
                                                                    .routes[
                                                                    0].legs[
                                                                    0]
                                                                    .duration
                                                                    .text);
                                                    } else {
                                                        window.alert(
                                                            'Directions request failed due to ' +
                                                            status);
                                                    }
                                                });
                                        });


                                    }
                                })
                            }

                            distance_array = new Array();
                        }
                    });

                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
    };

    function removeMarker() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
    }

    function makeMarker(position, icon) {
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            animation: google.maps.Animation.DROP,
        });
        markers.push(marker);
    }

    function getOutlet(v) {
        var i;
        $(distance_array).each(function (index, element) {
            if (element == v) {
                i = index;
            }
        });

        var c = 0;
        var oid;
        $.each(outlet_array, function (key, value) {
            if (c == i) {
                oid = key;
            }
            c++;
        });

        return oid;
    }

    function getQuotation() {
        $(".p_price").hide(500);
        $(".p_warning").hide(500);
        $(".span_price").hide(500);

        var place = destinationAutocomplete.getPlace();
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();

        $("input[name='lat']").val(lat);
        $("input[name='lng']").val(lng);

        $.ajax({
            method: "POST",
            url: "ajax/lalamove-quotation-new",
            dataType: 'json',
            data: {
                address: $("#autocomplete").val(),
                lat: lat,
                lng: lng,
                pickup_time: $("input[name='pickup_time']:checked").val()
            }
        })
            .done(function (data) {
                console.log(data);
                if (data.message) {
                    $(".p_warning").show(500);
                    $(".div_btn").hide(500);
                    $(".p_price").hide(500);
                    $(".div_time").hide(500);
                    $(".p_warning").html(data.message);
                } else {
                    $(".div_btn").show(500);
                    $(".div_time").show(500);
                    $(".span_price").show(500);
                    $(".p_price").show(500);
                    if (data.totalFee == "0") {
                        $(".span_price").html("<strike>S$" + data.totalFeeNum + "</strike><br>FREE");
                    } else {
                        $(".span_price").html("S$ " + data.totalFee);
                    }
                    $("input[name='shipping_amount']").val(data.totalFeeNum);
                }
            });
    }
</script>
</body>

</html>
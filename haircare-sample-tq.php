<?php

require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;

$databaseClass = new database();
$apiClass = new api();

$pkid = $_SESSION['sample_haircare']['done'];

if ($pkid == "") {
    header("Location: ".$site_config['sample_haircare']);
    exit();
}

$resultSample = get_query_data($table['sample_haircare'], "pkid=$pkid");
$rs_sample = $resultSample->fetchRow();

$postfield['first_name'] = $rs_sample['first_name'];
$postfield['last_name'] = $rs_sample['last_name'];
$postfield['mobile'] = $rs_sample['mobile'];
$postfield['email'] = $rs_sample['email'];
$postfield['sms_status'] = $rs_sample['sms_status'];
$postfield['email_status'] = $rs_sample['email_status'];
$postfield['call_status'] = $rs_sample['call_status'];
$postfield['whatsapp_status'] = $rs_sample['whatsapp_status'];
$postfield['source_id'] = '4950'; // Production ID - Campaign Name: Always on Sampling - Essential Oils Hair Care
// $postfield['source_id'] = '4734'; // UAT ID - Campaign Name: Always on Sampling - Essential Oils Hair Care
// $postfield['source_id'] = '4516';

$apiClass->create_sample_profile($postfield);

unset($_SESSION['iglive']);

?>

<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body class="page-bg">

    <div class="container-fluid">

        <? include('nav.php') ?>

        <div class="row mt-4">
            <div class="col-12 text-center p-0">
                <div class="title">
                    <h4 class="w-100">FIVE ESSENTIAL OILS HAIR CARE SAMPLE KIT</h4>
                    <p>Thank you for signing up!</p>
                    <p>Please present a screenshot of this confirmation page to redeem your sample kit in stores.</p>
                </div>
            </div>

            <div class="col-12">
                <img src="assets/img/Shampoo 4pc.png" class="img-fluid"/>
            </div>

            <div class="col-12">
            </div>

            <div class="col-12">
                <b>First Name: </b> <?= $rs_sample['first_name'] ?><br><br>
                <b>Last Name: </b> <?= $rs_sample['last_name'] ?><br><br>
                <b>Mobile: </b> <?= $rs_sample['mobile'] ?><br><br>
                <b>Redeem sample by: </b> <?= date('d M Y', strtotime($rs_sample['created_date'] . ' +2 week')) ?>
            </div>

            <?php if (false) { ?>
            <div class="col-12 p-3">
                <div class="border border-dark p-2 text-center" style="line-height: 1.5em;">
                    
                    Exclude deal for you!<br>
                    <b>5% OFF</b><br>
                    <b>selected Five Essential Oils hair care range</b><br>
                    Exclusively on <a href="https://sg.loccitane.com" target="_blank">sg.loccitane.com<a> or in stores.
                    <br>
                    <div class="p-2 mx-auto my-2" style="background-color:#181B54; border-radius:30px; font-size:1.2em; max-width:280px; color:white;">
                        <b>5OFFHAIRCARE</b>
                    </div>
                    5% OFF Expires by: <?= date('d M Y', strtotime($rs_sample['created_date'] . ' +2 month')) ?><br>
                    T&Cs apply.
                </div>
            </div>
            <?php } ?>

            <div class="col-12 mt-4">
                <div class="border border-dark p-2">
                    <p><b><u>Terms & Conditions for Sampling</u></b></p>

    				<ol>
                        <li>Limited to one sample kit per customer, while stocks last.</li><br>

                        <li>Valid for redemption at all L'OCCITANE boutiques except L'OCCITANE web store, Lazada, Shopee, Krisshop and Duty-Free Stores.</li><br>

                        <li>Non-transferrable and no collection is allowed on behalf of others. OTP verification to registered phone number is required during collection of sample kit in stores.</li><br>

                        <li>Not exchangeable for cash.</li><br>

                        <li>Only applicable for customers who have not redeemed this sample kit.</li><br>

                        <li>L'OCCITANE Singapore reserves the final right to alter and/or withdraw the items, terms & conditions without prior notice.</li>
                    </ol>


                    <br><br>

                    <?php if (false) { ?>
                    <p><b><u>Terms & Conditions for 5% OFF</u></b></p>
                    <ol>
                        <li>Voucher is valid till indicated date, redeemable at <a href="https://sg.loccitane.com" target="_blank">sg.loccitane.com<a> or stores except Takashimaya outlet, Lazada, Shopee, Krisshop and Duty-Free Stores.</li><br>

                        <li>Only applicable to Five Essential Oils Shampoo 300ml and 500ml, limited to one redemption per customer/profile.</li><br>

                        <li>Not valid in conjunction with other promocodes, discounts, rewards, bank vouchers, gift vouchers, gift certifications, birthday benefits and sets.</li><br>

                        <li>L'OCCITANE reserves the final right to alter and/or withdraw the items, terms & conditions without prior notice.</li>
                    </ol>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

    <?php include('footer.php') ?>

    <?php include('js-script.php') ?>

</body>
</html>
<?php
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/admin/config.php";
global $table;
$databaseClass = new database();

$product_subtitle = array(61 => 'Gives 3 scalp benefits', 50 => 'Bye-bye brittle hair!', 33 => 'Hello strong & smooth hair!', 42 => 'Flip your lightweight hair!');
$product_highlight = array(
    61 => '<ul><li>Calms irritated scalp<br><small>with micellar technology</small></li><li>Relax scalp<br><small>with PDO lavender oil</small></li><li>Rebalances scalp microbiome<br><small>with natural sugar complex (probiotic)</small></li></ul>',
    50 => '<ul><li>3x less hair breakage<br><small>with natural vegetal keratin</small></li><img src="assets/img/hair-icon-50.png" class="img-fluid" /></ul>',
    33 => '<ul><li>3x more repaired hair<br><small>with Angelica and Black Oat Amino acids and complex of 5 essential oil</small></li><img src="assets/img/hair-icon-33.png" class="img-fluid" /></ul>',
    42 => '<ul><li>3x less oily hair<br><small>with Provencal peppermint</small></li><img src="assets/img/hair-icon-42.png" class="img-fluid" /></ul>'
);
$product_code=array(61=>'17AS250E19',50=>'17SH300FV21',33=>'17SH300G18',42=>'17SH300P19');

$postfield = $_POST;

if(count($postfield)=="0"){
    header("Location: hair-quiz");
    exit();
}

foreach ($postfield as $k => $v) {
    if($v=="All"){
        continue;
    }
    $result_data[$v]++;
}

arsort($result_data);
$product = key($result_data);

if ($product == "Gentle & Balance" || $product == "All") {
    $suggest_product[] = 61;
    $also_product = 435;
    $result_title = 'NORMAL';
    $result_text = 'Beautiful, strong hair starts with a healthy & balanced scalp. Make the Gentle & Balance hair care range your must-have to have a balanced & healthy scalp microbiome to grow strong, luscious hair!';
} elseif ($product == "Purifying Freshness") {
    $suggest_product[] = 61;
    $suggest_product[] = 42;
    $also_product = 435;
    $result_title = 'OILY';
    $result_text = 'Give the Purifying Freshness hair care range a go to get rid of clogged pores while leaving a cooling sensation on your scalp! Our best routine for you is to start with the Gentle & Balance Shampoo today to achieve a balanced scalp, then alternate with the Purifying Freshness Shampoo tomorrow to avoid sebum build-up & grow stronger hair!
<br><br>
By alternating your shampoos daily, you can grow strong, beautiful hair when you start with a healthy & balanced scalp!';
} elseif ($product == "Volume & Strength") {
    $suggest_product[] = 61;
    $suggest_product[] = 50;
    $also_product = 436;
    $result_title = 'FINE & BRITTLE';
    $result_text = 'Try the Volume & Strength hair care range to achieve fuller, thicker & volumised hair! For best results, we highly recommend you alternate between the Gentle & Balance Shampoo for a healthy scalp, then use the Volume & Strength Shampoo the next day to grow stronger, fuller hair.
<br><br>
By alternating your shampoos daily, you can grow strong, beautiful hair when you start with a healthy & balanced scalp! ';
} elseif ($product == "Intensive Repair") {
    $suggest_product[] = 61;
    $suggest_product[] = 33;
    $also_product = 436;
    $result_title = 'DRY & DAMAGED';
    $result_text = 'Try the Intensive Repair hair care range to soften & smoothen damaged hair! Our recommended routine is to start with the Gentle & Balance Shampoo today to achieve a balanced scalp, then alternate with the Intensive Repair Shampoo tomorrow for stronger, smoother locks.
<br><br>
By alternating your shampoos daily, you can grow strong, beautiful hair when you start with a healthy & balanced scalp! ';
}
?>
<!DOCTYPE html>
<html>

<?php include('head.php') ?>
<link rel="stylesheet" href="assets/css/smart_wizard_all.min.css"/>
<style>
    body {
        background-color: #FCF9F5 !important;
        background-image: url("assets/img/hair-result-bg.jpg");
        background-position: center bottom;
        background-size: contain;
        background-repeat: no-repeat;
    }

    .product-border {
        border: 2px solid #AEC07C;
        border-radius: 25px;
        margin-top: 2em;
    }

    ul {
        text-align: left;
        padding-left: 0.5em;
        margin-top: 2em;
    }

    ul > img{
        max-width: 60% !important;
    }
</style>
<body class="">
<div class="container-fluid">
    <div class="row">
        <div class="col-12 mt-3">
            <a href="shop?cid=2" class="btn btn-black">BACK TO SHOP</a>
        </div>
    </div>
    <div class="row mt-5 mb-5">
        <div class="col-12 text-center">
            <h5 class="text-dark w-100">YOUR HAIR TYPE:</h5>
            <h5 class="text-yellow w-100"><?= $result_title ?></h5>
            <p class="pl-3 pr-4 mt-4"><?= $result_text ?></p>
            <a class="btn btn-black mx-auto" href="/hair-quiz">RETAKE</a>
        </div>
        <? if (count($suggest_product) == "1") { ?>
            <div class="product-border m-2 mt-4">
                <div class="col-12">
                    <div class="row">
                        <div class="col-4 p-4">
                            <img src="assets/img/<?= $suggest_product[0] ?>.png"
                                 class="img-fluid"/>
                        </div>
                        <div class="col-8 text-center">
                            <h5 class="mt-3"><b><?= $product == "All" ? "Gentle & Balance" : $product ?></b>
                            </h5>
                            <small><?= $product_subtitle[$suggest_product[0]] ?></small>
                            <div class="row mt-4">
                                <div class="col-4 p-1"><img src="assets/img/61-1.png" class="img-fluid"/></div>
                                <div class="col-4 p-1"><img src="assets/img/61-2.png" class="img-fluid"/></div>
                                <div class="col-4 p-1"><img src="assets/img/61-3.png" class="img-fluid"/></div>
                            </div>
                            <a class="btn btn-yellow mt-4 mb-4"
                               href="shop?search=<?= urlencode($product == "All" ? "Gentle & Balance" : $product) ?>">SHOP
                                NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        <? } else {
            ?>
            <div class="product-border m-2 mt-4">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-8 text-center">
                                    <h5 class="mt-3"><b>Gentle & Balance</b>
                                    </h5>
                                    <small><?= $product_subtitle[$suggest_product[0]] ?></small>
                                    <?= $product_highlight[$suggest_product[0]] ?>
                                    <a class="btn btn-yellow" href="shop?search=<?= urlencode("Gentle & Balance") ?>">SHOP NOW</a>
                                </div>
                                <div class="col-4 p-2">
                                    <img src="assets/img/<?= $suggest_product[0] ?>.png"
                                         class="img-fluid mt-3"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-4 mb-4">
                            <div class="row">
                                <div class="col-4 p-2">
                                    <img src="assets/img/<?= $suggest_product[1] ?>.png"
                                         class="img-fluid mt-3"/>
                                </div>
                                <div class="col-8 text-center">
                                    <h5 class="mt-3"><b><?= $product == "All" ? "Gentle & Balance" : $product ?></b>
                                    </h5>
                                    <small><?= $product_subtitle[$suggest_product[1]] ?></small>
                                    <?= $product_highlight[$suggest_product[1]] ?>
                                    <a class="btn btn-yellow" href="shop?search=<?=urlencode($product)?>">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? } ?>
    </div>
</div>
<?php include('footer.php') ?>
<?php include('js-script.php') ?>
<script>

</script>
</body>
</html>